//===- RISCVInsertVSETVLI.cpp - Insert VSETVLI instructions ---------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file implements a function pass that inserts VSETVLI instructions where
// needed.
//
// This pass consists of 3 phases:
//
// Phase 1 collects how each basic block affects VL/VTYPE.
//
// Phase 2 uses the information from phase 1 to do a data flow analysis to
// propagate the VL/VTYPE changes through the function. This gives us the
// VL/VTYPE at the start of each basic block.
//
// Phase 3 inserts VSETVLI instructions in each basic block. Information from
// phase 2 is used to prevent inserting a VSETVLI before the first vector
// instruction in the block if possible.
//
//===----------------------------------------------------------------------===//

#include "RISCV.h"
#include "RISCVSubtarget.h"
#include "llvm/ADT/MapVector.h"
#include "llvm/CodeGen/LiveIntervals.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include <queue>
using namespace llvm;

#define DEBUG_TYPE "riscv-insert-vsetvli"
#define RISCV_INSERT_VSETVLI_NAME "RISCV Insert VSETVLI pass"

static cl::opt<bool> DisableInsertVSETVLPHIOpt(
    "riscv-disable-insert-vsetvl-phi-opt", cl::init(false), cl::Hidden,
    cl::desc("Disable looking through phis when inserting vsetvlis."));

namespace {

using MBBNumber = int;

class VSETVLIInfo {
  Register AVLReg;

  enum : uint8_t {
    Uninitialized,
    AVLIsReg,
    Unknown,
  } State = Uninitialized;


  // Fields from VTYPE.
  uint8_t VLMul = 1;
  // SEW Values: 8, 16, 32, 64
  uint8_t SEW = 0;
  uint8_t MaskRegOp : 1;
  uint8_t SEWLMULRatioOnly : 1;

  enum TempStatus {
    NoMem, // This is not a memory operation so it doesn't care
    Temporal, // Positively temporal
    NonTemporal, // Positively non-temporal
    TemporalIsRegister, // A register is selecting the temporality
    TemporalUnknown, // Positively unknown
  } Temporality = NoMem; // We assume instructions don't care by default.

  enum AltFormatStatus {
    AltFmtIrrelevant,  // The status of the bit is not relevant
    AltFmtNeededSet,   // Positively the bit has to be set
    AltFmtNeededUnset, // Positively the bit has to be unset
    AltFmtUnknown,     // No information on the status the bit has to be
  } AlternativeFormat = AltFmtIrrelevant,
    AltFmtCvtDest = AltFmtIrrelevant;

  Register TemporalRegister = RISCV::NoRegister;

public:
  VSETVLIInfo() : MaskRegOp(false), SEWLMULRatioOnly(false) {}

  static VSETVLIInfo getUnknown() {
    VSETVLIInfo Info;
    Info.setUnknown();
    return Info;
  }

  bool isValid() const { return State != Uninitialized; }
  void setUnknown() { State = Unknown; }
  bool isUnknown() const { return State == Unknown; }

  void setAVLReg(Register Reg) {
    AVLReg = Reg;
    State = AVLIsReg;
  }

  bool hasAVLReg() const { return State == AVLIsReg; }
  Register getAVLReg() const {
    assert(hasAVLReg());
    return AVLReg;
  }

  bool hasSameAVL(const VSETVLIInfo &Other) const {
    assert(isValid() && Other.isValid() &&
           "Can't compare invalid VSETVLIInfos");
    assert(!isUnknown() && !Other.isUnknown() &&
           "Can't compare AVL in unknown state");
    return getAVLReg() == Other.getAVLReg();
  }

  void setVTYPE(unsigned VType) {
    assert(isValid() && !isUnknown() &&
           "Can't set VTYPE for uninitialized or unknown");
    unsigned SEWBits = (VType >> 2) & 0x7;
    unsigned VMulBits = VType & 0x3;

    SEW = (1 << SEWBits) * 8;
    VLMul = 1 << VMulBits;
    AlternativeFormat = VType & (1 << 10) ? AltFmtNeededSet : AltFmtNeededUnset;
    AltFmtCvtDest = VType & (1 << 8) ? AltFmtNeededSet : AltFmtNeededUnset;
  }

  void setNonTemporal() { Temporality = NonTemporal; }
  void setTemporal() { Temporality = Temporal; }
  void setTemporalByRegister(Register R) {
    Temporality = TemporalIsRegister;
    TemporalRegister = R;
  }

  void setAlternativeFormat(bool isNeeded) {
    AlternativeFormat = isNeeded ? AltFmtNeededSet : AltFmtNeededUnset;
  }

  void setAltFmtCvtDest(bool isNeeded) {
    AltFmtCvtDest = isNeeded ? AltFmtNeededSet : AltFmtNeededUnset;
  }

  bool isTemporalRegister() const { return Temporality == TemporalIsRegister; }
  Register getTemporalRegister() const {
    assert(isTemporalRegister());
    return TemporalRegister;
  }

  void setVTYPE(unsigned L, unsigned S, bool MRO) {
    assert(isValid() && !isUnknown() &&
           "Can't set VTYPE for uninitialized or unknown");
    VLMul = L;
    SEW = S;
    MaskRegOp = MRO;
  }

  unsigned encodeVTYPE() const {
    assert(isValid() && !isUnknown() && !SEWLMULRatioOnly &&
           "Can't encode VTYPE for uninitialized or unknown");
    uint64_t SEWBits = (Log2_64(SEW / 8)) << 2;
    uint64_t VMulBits = Log2_64(VLMul);
    uint64_t NT = (Temporality == NonTemporal) ? (1u << 9) : 0;
    uint64_t AlternativeFormatBit =
        (AlternativeFormat == AltFmtNeededSet) ? (1u << 10) : 0;
    uint64_t AltFormatForConversionDestBit =
        (AltFmtCvtDest == AltFmtNeededSet) ? (1u << 8) : 0;
    uint64_t VTypeI = SEWBits | VMulBits | NT | AlternativeFormatBit |
                      AltFormatForConversionDestBit;
    return VTypeI;
  }

  bool hasSEWLMULRatioOnly() const { return SEWLMULRatioOnly; }

  bool hasSameVTYPE(const VSETVLIInfo &Other) const {
    assert(isValid() && Other.isValid() &&
           "Can't compare invalid VSETVLIInfos");
    assert(!isUnknown() && !Other.isUnknown() &&
           "Can't compare VTYPE in unknown state");
    assert(!SEWLMULRatioOnly && !Other.SEWLMULRatioOnly &&
           "Can't compare when only LMUL/SEW ratio is valid.");
    return std::tie(VLMul, SEW) == std::tie(Other.VLMul, Other.SEW);
  }

  bool hasSameTemporality(const VSETVLIInfo &Other) const {
    // If the temporality is via register in both, check if it is the same.
    if (Temporality == Other.Temporality && Temporality == TemporalIsRegister) {
      return TemporalRegister == Other.TemporalRegister;
    }
    // If either has unknown temporality we conservatively assume they cannot be
    // the same.
    return Temporality == Other.Temporality && Temporality != TemporalUnknown &&
           Other.Temporality != TemporalUnknown;
  }

  bool hasSameAlternativeFormat(const VSETVLIInfo &Other) const {
    return AlternativeFormat == Other.AlternativeFormat;
  }

  bool hasSameAltFmtCvtDest(const VSETVLIInfo &Other) const {
    return AltFmtCvtDest == Other.AltFmtCvtDest;
  }

  // Convert VLMUL to a fixed point value with 3 bits of fraction.
  unsigned getSEWLMULRatio() const {
    assert(isValid() && !isUnknown() &&
           "Can't use VTYPE for uninitialized or unknown");
    return SEW / VLMul;
  }

  // Check if the VTYPE for these two VSETVLIInfos produce the same VLMAX.
  bool hasSameVLMAX(const VSETVLIInfo &Other) const {
    assert(isValid() && Other.isValid() &&
           "Can't compare invalid VSETVLIInfos");
    assert(!isUnknown() && !Other.isUnknown() &&
           "Can't compare VTYPE in unknown state");
    return getSEWLMULRatio() == Other.getSEWLMULRatio();
  }

  // This only checks temporality (not SEW/LMUL).
  bool isCompatibleTemporality(const VSETVLIInfo &InstrInfo) const {
    assert(InstrInfo.Temporality != TemporalUnknown &&
           "This is only possible from intersection");
    // If we do not know our temporality and the instruction does need a
    // specific temporality, we are not compatible.
    if (Temporality == TemporalUnknown && InstrInfo.Temporality != NoMem)
      return false;

    // We are explicitly setting a temporality and the instruction
    // does requires one, it must be the same.
    if (InstrInfo.Temporality != NoMem && !hasSameTemporality(InstrInfo) &&
        // Bias nontemporality towards temporal (i.e. regular) memory accesses.
        !(Temporality == NoMem && InstrInfo.Temporality == Temporal))
      return false;

    return true;
  }

  bool isCompatibleAlternativeFormat(const VSETVLIInfo &InstrInfo) const {
    assert(InstrInfo.AlternativeFormat != AltFmtUnknown &&
           "This is only possible from intersection");
    // If we do not know our alternative format bit and the instruction does
    // need a specific bit set (i.e. not irrelevant), we are not compatible.
    if (AlternativeFormat == AltFmtUnknown &&
        InstrInfo.AlternativeFormat != AltFmtIrrelevant)
      return false;

    // If we are explicitly setting a value to the bit and the instruction
    // does requires one, it must be the same.
    if (InstrInfo.AlternativeFormat != AltFmtIrrelevant &&
        !hasSameAlternativeFormat(InstrInfo)
        // Bias irrelevance towards not setting the alternate format bit.
        && !(AlternativeFormat == AltFmtIrrelevant &&
             InstrInfo.AlternativeFormat == AltFmtNeededUnset))
      return false;

    return true;
  }

  bool isCompatibleAltFmtCvtDest(const VSETVLIInfo &InstrInfo) const {
    assert(InstrInfo.AltFmtCvtDest != AltFmtUnknown &&
           "This is only possible from intersection");
    // If we do not know our alternative format bit for conversions and the
    // instruction does need a specific bit set (i.e. not irrelevant), we are
    // not compatible.
    if (AltFmtCvtDest == AltFmtUnknown &&
        InstrInfo.AltFmtCvtDest != AltFmtIrrelevant)
      return false;

    // If we are explicitly setting a value to the bit and the instruction
    // does requires one, it must be the same.
    if (InstrInfo.AltFmtCvtDest != AltFmtIrrelevant &&
        !hasSameAltFmtCvtDest(InstrInfo)
        // Bias irrelevance towards not setting the alternate format bit for
        // the conversion.
        && !(AltFmtCvtDest == AltFmtIrrelevant &&
             InstrInfo.AltFmtCvtDest == AltFmtNeededUnset))
      return false;

    return true;
  }

  // Determine whether the vector instructions requirements represented by
  // InstrInfo are compatible with the previous vsetvli instruction represented
  // by this.
  bool isCompatible(const VSETVLIInfo &InstrInfo) const {
    assert(isValid() && InstrInfo.isValid() &&
           "Can't compare invalid VSETVLIInfos");
    assert(!InstrInfo.SEWLMULRatioOnly &&
           "Expected a valid VTYPE for instruction!");
    // Nothing is compatible with Unknown.
    if (isUnknown() || InstrInfo.isUnknown())
      return false;

    // If only our VLMAX ratio is valid, then this isn't compatible.
    if (SEWLMULRatioOnly)
      return false;

    // Temporality.
    if (!isCompatibleTemporality(InstrInfo))
      return false;

    // Alternative format bits
    if (!isCompatibleAlternativeFormat(InstrInfo))
      return false;

    if (!isCompatibleAltFmtCvtDest(InstrInfo))
      return false;

    // If the instruction doesn't need an AVLReg and the SEW matches, consider
    // it compatible.
    if (InstrInfo.hasAVLReg() && InstrInfo.AVLReg == RISCV::NoRegister) {
      if (SEW == InstrInfo.SEW)
        return true;
    }

    // VTypes must match unless the instruction is a mask reg operation, then it
    // only care about VLMAX.
    // FIXME: Mask reg operations are probably ok if "this" VLMAX is larger
    // than "InstrInfo".
    if (!hasSameVTYPE(InstrInfo) &&
        !(InstrInfo.MaskRegOp && hasSameVLMAX(InstrInfo)))
      return false;

    return hasSameAVL(InstrInfo);
  }

  bool operator==(const VSETVLIInfo &Other) const {
    // Uninitialized is only equal to another Uninitialized.
    if (!isValid())
      return !Other.isValid();
    if (!Other.isValid())
      return !isValid();

    // Unknown is only equal to another Unknown.
    if (isUnknown())
      return Other.isUnknown();
    if (Other.isUnknown())
      return isUnknown();

    if (!hasSameAVL(Other))
      return false;

    // Temporality must match.
    if (Temporality != Other.Temporality
        || TemporalRegister != Other.TemporalRegister)
      return false;

    // Check the alternative format bits
    if (AlternativeFormat != Other.AlternativeFormat)
      return false;

    if (AltFmtCvtDest != Other.AltFmtCvtDest)
      return false;

    // If only the VLMAX is valid, check that it is the same.
    if (SEWLMULRatioOnly && Other.SEWLMULRatioOnly)
      return hasSameVLMAX(Other);

    // If the full VTYPE is valid, check that it is the same.
    if (!SEWLMULRatioOnly && !Other.SEWLMULRatioOnly)
      return hasSameVTYPE(Other);

    // If the SEWLMULRatioOnly bits are different, then they aren't equal.
    return false;
  }

  static std::tuple<TempStatus, Register>
  intersectTemporality(const VSETVLIInfo &A, const VSETVLIInfo &B) {
    if (A.Temporality == B.Temporality && A.Temporality != TemporalIsRegister)
      return {A.Temporality, A.TemporalRegister};

    if (A.Temporality == B.Temporality && A.Temporality == TemporalIsRegister &&
        A.TemporalRegister == B.TemporalRegister)
      return {A.Temporality, A.TemporalRegister};

    if (A.Temporality == NoMem) {
      return {B.Temporality, B.TemporalRegister};
    } else if (B.Temporality == NoMem) {
      return {A.Temporality, A.TemporalRegister};
    } else {
      return {TemporalUnknown, RISCV::NoRegister};
    }
  }

  static AltFormatStatus intersectAltFormatStatus(const AltFormatStatus &A,
                                                  const AltFormatStatus &B) {
    if (A == B)
      return A;
    else if (A == AltFmtIrrelevant)
      return B;
    else if (B == AltFmtIrrelevant)
      return A;
    else
      return AltFmtUnknown;
  }

  static std::tuple<TempStatus, Register, AltFormatStatus, AltFormatStatus>
  intersectIntersectable(const VSETVLIInfo &A, const VSETVLIInfo &B) {
    TempStatus Temporality;
    Register TemporalRegister;
    AltFormatStatus AlternativeFormat;
    AltFormatStatus AltFmtCvtDest;

    std::tie(Temporality, TemporalRegister) = intersectTemporality(A, B);

    AlternativeFormat =
        intersectAltFormatStatus(A.AlternativeFormat, B.AlternativeFormat);

    AltFmtCvtDest = intersectAltFormatStatus(A.AltFmtCvtDest, B.AltFmtCvtDest);

    return {Temporality, TemporalRegister, AlternativeFormat, AltFmtCvtDest};
  }

  // Calculate the VSETVLIInfo visible to a block assuming this and Other are
  // both predecessors.
  VSETVLIInfo intersect(const VSETVLIInfo &Other) {
    // If the new value isn't valid, ignore it.
    if (!Other.isValid())
      return *this;

    // If this value isn't valid, Other must be the first predecessor, use it.
    if (!isValid())
      return Other;

    // If either is unknown, the result is unknown.
    if (isUnknown() || Other.isUnknown())
      return VSETVLIInfo::getUnknown();

    // If we have an exact match, return this.
    if (*this == Other)
      return *this;

    bool SameIgnoringIntersectable = [&]() {
      VSETVLIInfo CurrentWithoutTemp = *this;
      CurrentWithoutTemp.Temporality = NoMem;
      CurrentWithoutTemp.AlternativeFormat = AltFmtIrrelevant;
      CurrentWithoutTemp.AltFmtCvtDest = AltFmtIrrelevant;
      VSETVLIInfo OtherWithoutTemp = Other;
      OtherWithoutTemp.Temporality = NoMem;
      OtherWithoutTemp.AlternativeFormat = AltFmtIrrelevant;
      OtherWithoutTemp.AltFmtCvtDest = AltFmtIrrelevant;
      return CurrentWithoutTemp == OtherWithoutTemp;
    }();

    // If only temporality differs, intersect it.
    if (SameIgnoringIntersectable) {
      VSETVLIInfo MergeInfo = *this;
      std::tie(MergeInfo.Temporality, MergeInfo.TemporalRegister,
               MergeInfo.AlternativeFormat, MergeInfo.AltFmtCvtDest) =
          intersectIntersectable(*this, Other);
      return MergeInfo;
    }

    // Not an exact match, but maybe the AVL and VLMAX are the same. If so,
    // return an SEW/LMUL ratio only value.
    if (hasSameAVL(Other) && hasSameVLMAX(Other)) {
      VSETVLIInfo MergeInfo = *this;
      MergeInfo.SEWLMULRatioOnly = true;
      std::tie(MergeInfo.Temporality, MergeInfo.TemporalRegister,
               MergeInfo.AlternativeFormat, MergeInfo.AltFmtCvtDest) =
          intersectIntersectable(*this, Other);
      return MergeInfo;
    }

    // Otherwise the result is unknown.
    return VSETVLIInfo::getUnknown();
  }

  // Calculate the VSETVLIInfo visible at the end of the block assuming this
  // is the predecessor value, and Other is change for this block.
  VSETVLIInfo merge(const VSETVLIInfo &Other) const {
    assert(isValid() && "Can only merge with a valid VSETVLInfo");

    // Nothing changed from the predecessor, keep it.
    if (!Other.isValid())
      return *this;

    // If the change is compatible with the input, we won't create a VSETVLI
    // and should keep the predecessor.
    if (isCompatible(Other))
      return *this;

    // Otherwise just use whatever is in this block.
    return Other;
  }
};

struct BlockData {
  // The VSETVLIInfo that represents the net changes to the VL/VTYPE registers
  // made by this block. Calculated in Phase 1.
  VSETVLIInfo Change;

  // The VSETVLIInfo that represents the VL/VTYPE settings on exit from this
  // block. Calculated in Phase 2.
  VSETVLIInfo Exit;

  // The VSETVLIInfo that represents the VL/VTYPE settings from all predecessor
  // blocks. Calculated in Phase 2, and used by Phase 3.
  VSETVLIInfo Pred;

  // Keeps track of whether the block is already in the queue.
  bool InQueue = false;

  BlockData() {}
};

class RISCVInsertVSETVLI : public MachineFunctionPass {
  const RISCVInstrInfo *TII;
  MachineRegisterInfo *MRI;

  std::vector<BlockData> BlockInfo;
  std::queue<const MachineBasicBlock *> WorkList;

public:
  static char ID;

  RISCVInsertVSETVLI() : MachineFunctionPass(ID) {
    initializeRISCVInsertVSETVLIPass(*PassRegistry::getPassRegistry());
  }
  bool runOnMachineFunction(MachineFunction &MF) override;

  void getAnalysisUsage(AnalysisUsage &AU) const override {
    AU.setPreservesCFG();
    MachineFunctionPass::getAnalysisUsage(AU);
  }

  StringRef getPassName() const override { return RISCV_INSERT_VSETVLI_NAME; }

private:
  bool needVSETVLI(const VSETVLIInfo &Require, const VSETVLIInfo &CurInfo);
  bool needVSETVLIPHI(const VSETVLIInfo &Require, const MachineBasicBlock &MBB);
  void insertVSETVLI(MachineBasicBlock &MBB, MachineInstr &MI,
                     const VSETVLIInfo &Info);
  void forwardPropagateAVL(MachineBasicBlock &MBB);

  bool computeVLVTYPEChanges(const MachineBasicBlock &MBB);
  void computeIncomingVLVTYPE(const MachineBasicBlock &MBB);
  void emitVSETVLIs(MachineBasicBlock &MBB);
};

} // end anonymous namespace

char RISCVInsertVSETVLI::ID = 0;

INITIALIZE_PASS(RISCVInsertVSETVLI, DEBUG_TYPE, RISCV_INSERT_VSETVLI_NAME,
                false, false)


static bool isLoadOrStore(unsigned Opcode) {
  return Opcode == RISCV::VLE_V || Opcode == RISCV::VLSE_V ||
         Opcode == RISCV::VLXE_V || Opcode == RISCV::VSE_V ||
         Opcode == RISCV::VSSE_V || Opcode == RISCV::VSXE_V;
}

// Has float input and the format of the float is relevant.
static bool hasFloatInput(unsigned Opcode) {
  switch (Opcode) {
  default:
    return false;
  // Vector Extension v0.7.1 spec, section 14.2
  case RISCV::VFADD_VV:
  case RISCV::VFADD_VF:
  case RISCV::VFSUB_VV:
  case RISCV::VFSUB_VF:
  case RISCV::VFRSUB_VF:
  // Vector Extension v0.7.1 spec, section 14.3
  case RISCV::VFWADD_VV:
  case RISCV::VFWADD_VF:
  case RISCV::VFWSUB_VV:
  case RISCV::VFWSUB_VF:
  case RISCV::VFWADD_WV:
  case RISCV::VFWADD_WF:
  case RISCV::VFWSUB_WV:
  case RISCV::VFWSUB_WF:
  // Vector Extension v0.7.1 spec, section 14.4
  case RISCV::VFMUL_VV:
  case RISCV::VFMUL_VF:
  case RISCV::VFDIV_VV:
  case RISCV::VFDIV_VF:
  case RISCV::VFRDIV_VF:
  // Vector Extension v0.7.1 spec, section 14.5
  case RISCV::VFWMUL_VV:
  case RISCV::VFWMUL_VF:
  // Vector Extension v0.7.1 spec, section 14.6
  case RISCV::VFMACC_VV:
  case RISCV::VFMACC_VF:
  case RISCV::VFNMACC_VV:
  case RISCV::VFNMACC_VF:
  case RISCV::VFMSAC_VV:
  case RISCV::VFMSAC_VF:
  case RISCV::VFNMSAC_VV:
  case RISCV::VFNMSAC_VF:
  case RISCV::VFMADD_VV:
  case RISCV::VFMADD_VF:
  case RISCV::VFNMADD_VV:
  case RISCV::VFNMADD_VF:
  case RISCV::VFMSUB_VV:
  case RISCV::VFMSUB_VF:
  case RISCV::VFNMSUB_VV:
  case RISCV::VFNMSUB_VF:
  // Vector Extension v0.7.1 spec, section 14.7
  case RISCV::VFWMACC_VV:
  case RISCV::VFWMACC_VF:
  case RISCV::VFWNMACC_VV:
  case RISCV::VFWNMACC_VF:
  case RISCV::VFWMSAC_VV:
  case RISCV::VFWMSAC_VF:
  case RISCV::VFWNMSAC_VV:
  case RISCV::VFWNMSAC_VF:
  // Vector Extension v0.7.1 spec, section 14.8
  case RISCV::VFSQRT_V:
  // Vector Extension v0.7.1 spec, section 14.9
  case RISCV::VFMIN_VV:
  case RISCV::VFMIN_VF:
  case RISCV::VFMAX_VV:
  case RISCV::VFMAX_VF:
  // Vector Extension v0.7.1 spec, section 14.10
  case RISCV::VFSGNJ_VV:
  case RISCV::VFSGNJ_VF:
  case RISCV::VFSGNJN_VV:
  case RISCV::VFSGNJN_VF:
  case RISCV::VFSGNJX_VV:
  case RISCV::VFSGNJX_VF:
  // Vector Extension v0.7.1 spec, section 14.11
  case RISCV::VMFEQ_VV:
  case RISCV::VMFEQ_VF:
  case RISCV::VMFNE_VV:
  case RISCV::VMFNE_VF:
  case RISCV::VMFLT_VV:
  case RISCV::VMFLT_VF:
  case RISCV::VMFLE_VV:
  case RISCV::VMFLE_VF:
  case RISCV::VMFGT_VF:
  case RISCV::VMFGE_VF:
  case RISCV::VMFORD_VV:
  case RISCV::VMFORD_VF:
  // Vector Extension v0.7.1 spec, section 14.12
  case RISCV::VFCLASS_V:
  // Vector Extension v0.7.1 spec, section 14.14
  case RISCV::VFCVT_XU_F_V:
  case RISCV::VFCVT_X_F_V:
  // Vector Extension v0.7.1 spec, section 14.15
  case RISCV::VFWCVT_XU_F_V:
  case RISCV::VFWCVT_X_F_V:
  case RISCV::VFWCVT_F_F_V:
  // Vector Extension v0.7.1 spec, section 14.16
  case RISCV::VFNCVT_XU_F_W:
  case RISCV::VFNCVT_X_F_W:
  case RISCV::VFNCVT_F_F_W:
    return true;
  }
}

static bool isConversionDestFloat(unsigned Opcode) {
  switch (Opcode) {
  default:
    return false;
  case RISCV::VFCVT_F_XU_V:
  case RISCV::VFCVT_F_X_V:
  case RISCV::VFWCVT_F_F_V:
  case RISCV::VFWCVT_F_XU_V:
  case RISCV::VFWCVT_F_X_V:
  case RISCV::VFNCVT_F_F_W:
  case RISCV::VFNCVT_F_XU_W:
  case RISCV::VFNCVT_F_X_W:
    return true;
  }
}

static VSETVLIInfo computeInfoForEPIInstr(const MachineInstr &MI, int VLIndex,
                                          unsigned SEWIndex, unsigned VLMul,
                                          int MaskOpIdx,
                                          int BaseInstr,
                                          int DynamicFlags,
                                          MachineRegisterInfo *MRI) {
  VSETVLIInfo InstrInfo;

  unsigned SEW = MI.getOperand(SEWIndex).getImm() & ~(0x1 << 9);
  bool AlternativeFormat = SEW & (0x1 << 10);
  bool AltFormatForConversionDest = SEW & (0x1 << 8);
  SEW = SEW & ~(0x1 << 10) & ~(0x1 << 8);
  assert(isPowerOf2_32(SEW) && 8 <= SEW && SEW <= 64 && "Unexpected SEW");

  // We used to do this in the custom inserter but as long as it happens before
  // regalloc we should be fine.
  // Masked instructions under LMUL > 1 are a bit problematic as we don't want
  // the destination to overlap the mask. So if they are VR register classes,
  // make sure we use one that does not include V0.
  // VLMul values: 1, 2, 4, 8
  bool LMULOver1 = VLMul > 1;
  if (LMULOver1 && MaskOpIdx >= 0 && MI.getOperand(MaskOpIdx).isReg() &&
      MI.getOperand(MaskOpIdx).getReg() != RISCV::NoRegister &&
      MI.getNumExplicitDefs() != 0) {
    assert(MI.getNumExplicitDefs() == 1 && "Too many explicit definitions!");
    assert(MI.getOperand(0).isDef() && "Expecting a def here");
    if (MI.getOperand(0).isReg()) {
      Register Def = MI.getOperand(0).getReg();
      assert(Register::isVirtualRegister(Def) && "Def should be virtual here");
      const TargetRegisterClass *RC = MRI->getRegClass(Def);
      // FIXME: what about tuples?
      if (RC->hasSuperClassEq(&RISCV::VRRegClass)) {
        MRI->setRegClass(Def, &RISCV::VRNoV0RegClass);
      } else if (RC->hasSuperClassEq(&RISCV::VRM2RegClass)) {
        MRI->setRegClass(Def, &RISCV::VRM2NoV0RegClass);
      } else if (RC->hasSuperClassEq(&RISCV::VRM4RegClass)) {
        MRI->setRegClass(Def, &RISCV::VRM4NoV0RegClass);
      } else if (RC->hasSuperClassEq(&RISCV::VRM8RegClass)) {
        MRI->setRegClass(Def, &RISCV::VRM8NoV0RegClass);
      }
    }
  }

  if (VLIndex >= 0) {
    const MachineOperand &VLOp = MI.getOperand(VLIndex);
    InstrInfo.setAVLReg(VLOp.getReg());
  } else
    InstrInfo.setAVLReg(RISCV::NoRegister);

  // This does not set temporality, which is by default NoMem.
  InstrInfo.setVTYPE(VLMul, SEW, /* MaskRegOp */ false);

  // Set or unset the the alternative format bits according to those
  // encoded in the SEW
  if (hasFloatInput(BaseInstr)) {
    InstrInfo.setAlternativeFormat(AlternativeFormat);
  }

  if (isConversionDestFloat(BaseInstr)) {
    InstrInfo.setAltFmtCvtDest(AltFormatForConversionDest);
  }

  if (isLoadOrStore(BaseInstr)) {
    // Explicitly mark the temporality of loads and stores.
    bool IsNonTemporal = MI.getOperand(SEWIndex).getImm() & (1u << 9);
    if (IsNonTemporal) {
      InstrInfo.setNonTemporal();
    } else if (DynamicFlags >= 0) {
      Register R = MI.getOperand(DynamicFlags).getReg();
      InstrInfo.setTemporalByRegister(R);
    } else {
      InstrInfo.setTemporal();
    }
  }

  return InstrInfo;
}

void RISCVInsertVSETVLI::insertVSETVLI(MachineBasicBlock &MBB, MachineInstr &MI,
                                       const VSETVLIInfo &Info) {
  DebugLoc DL = MI.getDebugLoc();
  unsigned InfoVTYPE = Info.encodeVTYPE();
  Register AVLReg = Info.getAVLReg();

  if (Info.isTemporalRegister()) {
    assert(AVLReg != RISCV::NoRegister);

    Register TmpReg = MRI->createVirtualRegister(&RISCV::GPRRegClass);
    BuildMI(MBB, MI, DL, TII->get(RISCV::ORI))
        .addReg(TmpReg, RegState::Define)
        .addReg(Info.getTemporalRegister())
        .addImm(InfoVTYPE);
    BuildMI(MBB, MI, DL, TII->get(RISCV::PseudoVSETVL))
        .addReg(RISCV::X0, RegState::Define | RegState::Dead)
        .addReg(AVLReg)
        .addReg(TmpReg, RegState::Kill);
    return;
  }

  if (AVLReg == RISCV::NoRegister) {
    BuildMI(MBB, MI, DL, TII->get(RISCV::PseudoVSETVLI))
        .addReg(RISCV::X0, RegState::Define | RegState::Dead)
        .addReg(RISCV::X0, RegState::Kill)
        .addImm(InfoVTYPE)
        .addReg(RISCV::VL, RegState::Implicit);
    return;
  }

  BuildMI(MBB, MI, DL, TII->get(RISCV::PseudoVSETVLI))
      .addReg(RISCV::X0, RegState::Define | RegState::Dead)
      .addReg(AVLReg)
      .addImm(InfoVTYPE);
}

// Return a VSETVLIInfo representing the changes made by this VSETVLI or
// VSETIVLI instruction.
static VSETVLIInfo getInfoForVSETVLI(const MachineInstr &MI) {
  VSETVLIInfo NewInfo;

  if (MI.getOpcode() == RISCV::PseudoVSETVLI) {
    Register AVLReg = MI.getOperand(1).getReg();
    assert((AVLReg != RISCV::X0 || MI.getOperand(0).getReg() != RISCV::X0) &&
           "Can't handle X0, X0 vsetvli yet");
    NewInfo.setAVLReg(AVLReg);
  } else {
    llvm_unreachable("Unexpected instruction");
  }

  NewInfo.setVTYPE(MI.getOperand(2).getImm());

  return NewInfo;
}

bool RISCVInsertVSETVLI::needVSETVLI(const VSETVLIInfo &Require,
                                     const VSETVLIInfo &CurInfo) {
  if (CurInfo.isCompatible(Require))
    return false;

  // We didn't find a compatible value. If our AVL is a virtual register,
  // it might be defined by a VSETVLI. If it has the same VTYPE
  // we need and the last VL/VTYPE we observed is the same, we don't need
  // a VSETVLI here.
  if (!CurInfo.isUnknown() && Require.hasAVLReg() &&
      Require.getAVLReg().isVirtual() && !CurInfo.hasSEWLMULRatioOnly() &&
      Require.hasSameVTYPE(CurInfo)) {
    if (MachineInstr *DefMI = MRI->getVRegDef(Require.getAVLReg())) {
      if (DefMI->getOpcode() == RISCV::PseudoVSETVLI) {
        VSETVLIInfo DefInfo = getInfoForVSETVLI(*DefMI);
        if (DefInfo.hasSameAVL(CurInfo) && DefInfo.hasSameVTYPE(CurInfo)
            && CurInfo.isCompatibleTemporality(Require))
          return false;
      }
    }
  }

  return true;
}

void RISCVInsertVSETVLI::forwardPropagateAVL(MachineBasicBlock &MBB) {
  for (const MachineInstr &MI : MBB) {
    if (MI.getOpcode() != RISCV::PseudoVSETVLI) {
      continue;
    }

    VSETVLIInfo VI = getInfoForVSETVLI(MI);
    const MachineOperand &GVLOp = MI.getOperand(0);
    assert(GVLOp.isReg());
    Register GVLReg = GVLOp.getReg();
    // Cycle through all uses of this GVL
    for (MachineRegisterInfo::use_nodbg_iterator
             UI = MRI->use_nodbg_begin(GVLReg),
             UIEnd = MRI->use_nodbg_end();
         UI != UIEnd;) {
      MachineOperand &Use(*UI++);
      assert(Use.getParent() != nullptr);
      const MachineInstr &UseMI = *Use.getParent();
      const int UseIndex = UseMI.getOperandNo(&Use);

      // EPI instructions
      if (const RISCVEPIPseudosTable::EPIPseudoInfo *EPI =
              RISCVEPIPseudosTable::getEPIPseudoInfo(UseMI.getOpcode())) {
        if (UseIndex == EPI->getVLIndex()) {
          VSETVLIInfo UseInfo = computeInfoForEPIInstr(
              UseMI, EPI->getVLIndex(), EPI->getSEWIndex(), EPI->VLMul,
              EPI->getMaskOpIndex(), EPI->BaseInstr,
              EPI->getDynamicFlagsIndex(), MRI);
          if (UseInfo.hasSameVLMAX(VI)) {
            Use.setReg(VI.getAVLReg());
          }
        }
      }
    }

    // Assure that the AVLReg is not being killed
    MRI->clearKillFlags(VI.getAVLReg());

    // Update liveness.
    if (MRI->use_nodbg_empty(GVLReg)) {
      assert(Register::isVirtualRegister(GVLReg));
      MachineRegisterInfo::def_iterator GVLOpIt = MRI->def_begin(GVLReg);
      assert(GVLOpIt != MRI->def_end());
      MachineOperand &GVLOp = *GVLOpIt;
      GVLOp.setIsDead();
    }
  }
}

bool RISCVInsertVSETVLI::computeVLVTYPEChanges(const MachineBasicBlock &MBB) {
  bool HadVectorOp = false;

  BlockData &BBInfo = BlockInfo[MBB.getNumber()];
  for (const MachineInstr &MI : MBB) {

    // If this is an explicit VSETVLI, update our state.
    if (MI.getOpcode() == RISCV::PseudoVSETVLI) {
      HadVectorOp = true;
      BBInfo.Change = getInfoForVSETVLI(MI);
      continue;
    }

    if (const RISCVEPIPseudosTable::EPIPseudoInfo *EPI =
            RISCVEPIPseudosTable::getEPIPseudoInfo(MI.getOpcode())) {
      int VLIndex = EPI->getVLIndex();
      int SEWIndex = EPI->getSEWIndex();
      int MaskOpIndex = EPI->getMaskOpIndex();

      HadVectorOp = true;

      assert(SEWIndex >= 0 && "SEWIndex must be >= 0");
      VSETVLIInfo NewInfo = computeInfoForEPIInstr(
          MI, VLIndex, SEWIndex, EPI->VLMul, MaskOpIndex, EPI->BaseInstr,
          EPI->getDynamicFlagsIndex(), MRI);

      if (!BBInfo.Change.isValid()) {
        BBInfo.Change = NewInfo;
      } else {
        // If this instruction isn't compatible with the previous VL/VTYPE
        // we need to insert a VSETVLI.
        if (needVSETVLI(NewInfo, BBInfo.Change)) {
          BBInfo.Change = NewInfo;
        }
      }
    }

    // If this is something that updates VL/VTYPE that we don't know about, set
    // the state to unknown.
    if (MI.isCall() || MI.isInlineAsm() || MI.modifiesRegister(RISCV::VL) ||
        MI.modifiesRegister(RISCV::VTYPE)) {
      BBInfo.Change = VSETVLIInfo::getUnknown();
    }
  }

  // Initial exit state is whatever change we found in the block.
  BBInfo.Exit = BBInfo.Change;

  return HadVectorOp;
}

void RISCVInsertVSETVLI::computeIncomingVLVTYPE(const MachineBasicBlock &MBB) {
  BlockData &BBInfo = BlockInfo[MBB.getNumber()];
  BBInfo.InQueue = false;

  VSETVLIInfo InInfo = BBInfo.Pred;
  if (MBB.pred_empty()) {
    // There are no predecessors, so use the default starting status.
    InInfo.setUnknown();
  } else {
    for (MachineBasicBlock *P : MBB.predecessors()) {
      BlockData &PredBBInfo = BlockInfo[P->getNumber()];
      InInfo = InInfo.intersect(PredBBInfo.Exit);
    }
  }

  // If we don't have any valid predecessor value, wait until we do.
  if (!InInfo.isValid())
    return;

  BBInfo.Pred = InInfo;

  VSETVLIInfo TmpStatus = BBInfo.Pred.merge(BBInfo.Change);
  bool UpdatedVSETVLIInfo = false;
  if (!(BBInfo.Exit == TmpStatus)) {
    BBInfo.Exit = TmpStatus;
    UpdatedVSETVLIInfo = true;
  }

  // If the new exit values match the old exit values,
  // we don't need to revisit any blocks.
  if (UpdatedVSETVLIInfo) {
    // Add the successors to the work list so we can propagate the
    // changed exit status.
    for (MachineBasicBlock *S : MBB.successors())
      if (!BlockInfo[S->getNumber()].InQueue)
        WorkList.push(S);
  }
}

// If we weren't able to prove a vsetvli was directly unneeded, it might still
// be unneeded if the AVL is a phi node where all incoming values are VL
// outputs from the last VSETVLI in their respective basic blocks.
bool RISCVInsertVSETVLI::needVSETVLIPHI(const VSETVLIInfo &Require,
                                        const MachineBasicBlock &MBB) {
  if (DisableInsertVSETVLPHIOpt)
    return true;

  if (!Require.hasAVLReg())
    return true;

  Register AVLReg = Require.getAVLReg();
  if (!AVLReg.isVirtual())
    return true;

  // We need the AVL to be produce by a PHI node in this basic block.
  MachineInstr *PHI = MRI->getVRegDef(AVLReg);
  if (!PHI || PHI->getOpcode() != RISCV::PHI || PHI->getParent() != &MBB)
    return true;

  for (unsigned PHIOp = 1, NumOps = PHI->getNumOperands(); PHIOp != NumOps;
       PHIOp += 2) {
    Register InReg = PHI->getOperand(PHIOp).getReg();
    MachineBasicBlock *PBB = PHI->getOperand(PHIOp + 1).getMBB();
    const BlockData &PBBInfo = BlockInfo[PBB->getNumber()];
    // If the exit from the predecessor has the VTYPE we are looking for
    // we might be able to avoid a VSETVLI.
    if (PBBInfo.Exit.isUnknown() || !PBBInfo.Exit.hasSameVTYPE(Require) ||
        !PBBInfo.Exit.isCompatibleTemporality(Require) ||
        !PBBInfo.Exit.isCompatibleAlternativeFormat(Require) ||
        !PBBInfo.Exit.isCompatibleAltFmtCvtDest(Require))
      return true;

    // We need the PHI input to the be the output of a VSETVL(I/EXT).
    MachineInstr *DefMI = MRI->getVRegDef(InReg);
    if (!DefMI || (DefMI->getOpcode() != RISCV::PseudoVSETVLI))
      return true;

    // We found a VSETVL(I/EXT) make sure it matches the output of the
    // predecessor block.
    VSETVLIInfo DefInfo = getInfoForVSETVLI(*DefMI);
    if (!DefInfo.hasSameAVL(PBBInfo.Exit) ||
        !DefInfo.hasSameVTYPE(PBBInfo.Exit) ||
        !DefInfo.hasSameTemporality(PBBInfo.Exit) ||
        !DefInfo.hasSameAlternativeFormat(PBBInfo.Exit) ||
        !DefInfo.hasSameAltFmtCvtDest(PBBInfo.Exit))
      return true;
  }

  // If all the incoming values to the PHI checked out, we don't need
  // to insert a VSETVLI.
  return false;
}

void RISCVInsertVSETVLI::emitVSETVLIs(MachineBasicBlock &MBB) {
  BlockData &BBInfo = BlockInfo[MBB.getNumber()];
  VSETVLIInfo CurInfo;

  for (MachineInstr &MI : MBB) {
    // If this is an explicit VSETVLI, update our state.
    if (MI.getOpcode() == RISCV::PseudoVSETVLI) {
      // Conservatively, mark the VL and VTYPE as live.
      unsigned NumOperands = MI.getNumOperands();
      assert(MI.getOperand(NumOperands - 2).getReg() == RISCV::VL &&
             MI.getOperand(NumOperands - 1).getReg() == RISCV::VTYPE &&
             "Unexpected operands where VL and VTYPE should be");
      MI.getOperand(NumOperands - 2).setIsDead(false);
      MI.getOperand(NumOperands - 1).setIsDead(false);
      CurInfo = getInfoForVSETVLI(MI);
      continue;
    }

    // Handle EPI pseudos here.
    if (const RISCVEPIPseudosTable::EPIPseudoInfo *EPI =
            RISCVEPIPseudosTable::getEPIPseudoInfo(MI.getOpcode())) {
      int VLIndex = EPI->getVLIndex();
      int SEWIndex = EPI->getSEWIndex();
      int MaskOpIndex = EPI->getMaskOpIndex();

      assert(SEWIndex >= 0 && "SEWIndex must be >= 0");
      VSETVLIInfo NewInfo = computeInfoForEPIInstr(
          MI, VLIndex, SEWIndex, EPI->VLMul, MaskOpIndex, EPI->BaseInstr,
          EPI->getDynamicFlagsIndex(), MRI);

      if (VLIndex >= 0) {
        MachineOperand &VLOp = MI.getOperand(VLIndex);
        // We don't lower to VL immediates in EPI yet.
        assert(VLOp.isReg());
        // Erase the AVL operand from the instruction.
        VLOp.setReg(RISCV::NoRegister);
        VLOp.setIsKill(false);
      }

      if (EPI->getDynamicFlagsIndex() >= 0) {
        MachineOperand &DynamicFlags =
            MI.getOperand(EPI->getDynamicFlagsIndex());
        // Erase the DynamicFlags operand from the instruction.
        DynamicFlags.setReg(RISCV::NoRegister);
        DynamicFlags.setIsKill(false);
      }

      if (!CurInfo.isValid()) {
        // We haven't found any vector instructions or VL/VTYPE changes yet,
        // use the predecessor information.
        assert(BBInfo.Pred.isValid() && "Expected a valid predecessor state.");
        if (needVSETVLI(NewInfo, BBInfo.Pred) && needVSETVLIPHI(NewInfo, MBB)) {
          insertVSETVLI(MBB, MI, NewInfo);
          CurInfo = NewInfo;
        }
      } else {
        // If this instruction isn't compatible with the previous VL/VTYPE
        // we need to insert a VSETVLI.
        if (needVSETVLI(NewInfo, CurInfo)) {
          insertVSETVLI(MBB, MI, NewInfo);
          CurInfo = NewInfo;
        }
      }
    }

    // If this is something updates VL/VTYPE that we don't know about, set
    // the state to unknown.
    if (MI.isCall() || MI.isInlineAsm() || MI.modifiesRegister(RISCV::VL) ||
        MI.modifiesRegister(RISCV::VTYPE)) {
      CurInfo = VSETVLIInfo::getUnknown();
    }
  }
}

bool RISCVInsertVSETVLI::runOnMachineFunction(MachineFunction &MF) {
  // Skip if the vector extension is not enabled.
  const RISCVSubtarget &ST = MF.getSubtarget<RISCVSubtarget>();
  if (!ST.hasStdExtV())
    return false;

  TII = ST.getInstrInfo();
  MRI = &MF.getRegInfo();

  assert(BlockInfo.empty() && "Expect empty block infos");
  BlockInfo.resize(MF.getNumBlockIDs());

  // Phase 0 - propagate AVL when VLMAX is the same
  for (MachineBasicBlock &MBB : MF)
    forwardPropagateAVL(MBB);

  bool HaveVectorOp = false;

  // Phase 1 - determine how VL/VTYPE are affected by the each block.
  for (const MachineBasicBlock &MBB : MF)
    HaveVectorOp |= computeVLVTYPEChanges(MBB);

  // If we didn't find any instructions that need VSETVLI, we're done.
  if (HaveVectorOp) {
    // Phase 2 - determine the exit VL/VTYPE from each block. We add all
    // blocks to the list here, but will also add any that need to be revisited
    // during Phase 2 processing.
    for (const MachineBasicBlock &MBB : MF) {
      WorkList.push(&MBB);
      BlockInfo[MBB.getNumber()].InQueue = true;
    }
    while (!WorkList.empty()) {
      const MachineBasicBlock &MBB = *WorkList.front();
      WorkList.pop();
      computeIncomingVLVTYPE(MBB);
    }

    // Phase 3 - add any vsetvli instructions needed in the block. Use the
    // Phase 2 information to avoid adding vsetvlis before the first vector
    // instruction in the block if the VL/VTYPE is satisfied by its
    // predecessors.
    for (MachineBasicBlock &MBB : MF)
      emitVSETVLIs(MBB);
  }

  BlockInfo.clear();

  return HaveVectorOp;
}

/// Returns an instance of the Insert VSETVLI pass.
FunctionPass *llvm::createRISCVInsertVSETVLIPass() {
  return new RISCVInsertVSETVLI();
}
