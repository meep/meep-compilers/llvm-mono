//===-- RISCVISelLowering.cpp - RISCV DAG Lowering Implementation  --------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file defines the interfaces that RISCV uses to lower LLVM code into a
// selection DAG.
//
//===----------------------------------------------------------------------===//

#include "RISCVISelLowering.h"
#include "RISCV.h"
#include "RISCVMachineFunctionInfo.h"
#include "RISCVRegisterInfo.h"
#include "RISCVSubtarget.h"
#include "RISCVTargetMachine.h"
#include "Utils/RISCVMatInt.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/CodeGen/CallingConvLower.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/IR/DiagnosticInfo.h"
#include "llvm/IR/DiagnosticPrinter.h"
#include "llvm/IR/IntrinsicsRISCV.h"
#include "llvm/IR/IntrinsicsEPI.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

#define DEBUG_TYPE "riscv-lower"

static cl::opt<bool> EnableFoldingFMA(
    "riscv-enable-folding-fma", cl::init(true), cl::Hidden,
    cl::desc("Enables folding negation into FMAs"));

STATISTIC(NumTailCalls, "Number of tail calls");

RISCVTargetLowering::RISCVTargetLowering(const TargetMachine &TM,
                                         const RISCVSubtarget &STI)
    : TargetLowering(TM), Subtarget(STI) {

  if (Subtarget.isRV32E())
    report_fatal_error("Codegen not yet implemented for RV32E");

  RISCVABI::ABI ABI = Subtarget.getTargetABI();
  assert(ABI != RISCVABI::ABI_Unknown && "Improperly initialised target ABI");

  if ((ABI == RISCVABI::ABI_ILP32F || ABI == RISCVABI::ABI_LP64F) &&
      !Subtarget.hasStdExtF()) {
    errs() << "Hard-float 'f' ABI can't be used for a target that "
                "doesn't support the F instruction set extension (ignoring "
                          "target-abi)\n";
    ABI = Subtarget.is64Bit() ? RISCVABI::ABI_LP64 : RISCVABI::ABI_ILP32;
  } else if ((ABI == RISCVABI::ABI_ILP32D || ABI == RISCVABI::ABI_LP64D) &&
             !Subtarget.hasStdExtD()) {
    errs() << "Hard-float 'd' ABI can't be used for a target that "
              "doesn't support the D instruction set extension (ignoring "
              "target-abi)\n";
    ABI = Subtarget.is64Bit() ? RISCVABI::ABI_LP64 : RISCVABI::ABI_ILP32;
  }

  switch (ABI) {
  default:
    report_fatal_error("Don't know how to lower this ABI");
  case RISCVABI::ABI_ILP32:
  case RISCVABI::ABI_ILP32F:
  case RISCVABI::ABI_ILP32D:
  case RISCVABI::ABI_LP64:
  case RISCVABI::ABI_LP64F:
  case RISCVABI::ABI_LP64D:
    break;
  }

  MVT XLenVT = Subtarget.getXLenVT();

  // Set up the register classes.
  addRegisterClass(XLenVT, &RISCV::GPRRegClass);

  if (Subtarget.hasStdExtF())
    addRegisterClass(MVT::f32, &RISCV::FPR32RegClass);
  if (Subtarget.hasStdExtD())
    addRegisterClass(MVT::f64, &RISCV::FPR64RegClass);

  if (Subtarget.hasStdExtV()) {
    addRegisterClass(MVT::nxv1i1, &RISCV::VRRegClass);
    addRegisterClass(MVT::nxv2i1, &RISCV::VRRegClass);
    addRegisterClass(MVT::nxv4i1, &RISCV::VRRegClass);
    addRegisterClass(MVT::nxv8i1, &RISCV::VRRegClass);

    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv16i1, &RISCV::VRRegClass);
      addRegisterClass(MVT::nxv32i1, &RISCV::VRRegClass);
      addRegisterClass(MVT::nxv64i1, &RISCV::VRRegClass);

      addRegisterClass(MVT::nxv32i2, &RISCV::VRRegClass);
      addRegisterClass(MVT::nxv16i4, &RISCV::VRRegClass);
    }

    // addRegisterClass(MVT::nxv1i8, &RISCV::VRRegClass); // FIXME illegal type
    // addRegisterClass(MVT::nxv2i8, &RISCV::VRRegClass); // FIXME illegal type
    // addRegisterClass(MVT::nxv4i8, &RISCV::VRRegClass); // FIXME illegal type
    addRegisterClass(MVT::nxv8i8, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv16i8, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv32i8, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv64i8, &RISCV::VRM8RegClass);
    }

    // addRegisterClass(MVT::nxv1i16, &RISCV::VRRegClass); // FIXME illegal type
    // addRegisterClass(MVT::nxv2i16, &RISCV::VRRegClass); // FIXME illegal type
    addRegisterClass(MVT::nxv4i16, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv8i16, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv16i16, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv32i16, &RISCV::VRM8RegClass);
    }

    // addRegisterClass(MVT::nxv1i32, &RISCV::VRRegClass); // FIXME illegal type
    addRegisterClass(MVT::nxv2i32, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv4i32, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv8i32, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv16i32, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv1i64, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv2i64, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv4i64, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv8i64, &RISCV::VRM8RegClass);
    }

    // addRegisterClass(MVT::nxv1f32, &RISCV::VRRegClass); // FIXME illegal type
    addRegisterClass(MVT::nxv2f32, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv4f32, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv8f32, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv16f32, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv1f64, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv2f64, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv4f64, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv8f64, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv8f8_143, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv16f8_143, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv32f8_143, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv64f8_143, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv8f8_152, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv16f8_152, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv32f8_152, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv64f8_152, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv4f16, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv8f16, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv16f16, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv32f16, &RISCV::VRM8RegClass);
    }

    addRegisterClass(MVT::nxv4bf16, &RISCV::VRRegClass);
    if (!Subtarget.onlyLMUL1()) {
      addRegisterClass(MVT::nxv8bf16, &RISCV::VRM2RegClass);
      addRegisterClass(MVT::nxv16bf16, &RISCV::VRM4RegClass);
      addRegisterClass(MVT::nxv32bf16, &RISCV::VRM8RegClass);
    }

    setBooleanVectorContents(ZeroOrOneBooleanContent);

    for (auto VT : MVT::integer_scalable_vector_valuetypes()) {
      setOperationAction(ISD::SPLAT_VECTOR, VT, Legal);
      setOperationAction(ISD::VECTOR_SHUFFLE, VT, Custom);
    }
    for (auto VT : MVT::fp_scalable_vector_valuetypes()) {
      setOperationAction(ISD::SPLAT_VECTOR, VT, Legal);
      setOperationAction(ISD::VECTOR_SHUFFLE, VT, Custom);
    }
  }

  // Compute derived properties from the register classes.
  computeRegisterProperties(STI.getRegisterInfo());

  setStackPointerRegisterToSaveRestore(RISCV::X2);

  for (auto N : {ISD::EXTLOAD, ISD::SEXTLOAD, ISD::ZEXTLOAD})
    setLoadExtAction(N, XLenVT, MVT::i1, Promote);

  // TODO: add all necessary setOperationAction calls.
  setOperationAction(ISD::DYNAMIC_STACKALLOC, XLenVT, Expand);

  setOperationAction(ISD::BR_JT, MVT::Other, Expand);
  setOperationAction(ISD::BR_CC, XLenVT, Expand);
  setOperationAction(ISD::SELECT, XLenVT, Custom);
  setOperationAction(ISD::SELECT_CC, XLenVT, Expand);

  setOperationAction(ISD::STACKSAVE, MVT::Other, Expand);
  setOperationAction(ISD::STACKRESTORE, MVT::Other, Expand);

  setOperationAction(ISD::VASTART, MVT::Other, Custom);
  setOperationAction(ISD::VAARG, MVT::Other, Expand);
  setOperationAction(ISD::VACOPY, MVT::Other, Expand);
  setOperationAction(ISD::VAEND, MVT::Other, Expand);

  setOperationAction(ISD::SIGN_EXTEND_INREG, MVT::i1, Expand);
  if (!Subtarget.hasStdExtZbb()) {
    setOperationAction(ISD::SIGN_EXTEND_INREG, MVT::i8, Expand);
    setOperationAction(ISD::SIGN_EXTEND_INREG, MVT::i16, Expand);
  }

  if (Subtarget.is64Bit()) {
    setOperationAction(ISD::ADD, MVT::i32, Custom);
    setOperationAction(ISD::SUB, MVT::i32, Custom);
    setOperationAction(ISD::SHL, MVT::i32, Custom);
    setOperationAction(ISD::SRA, MVT::i32, Custom);
    setOperationAction(ISD::SRL, MVT::i32, Custom);
  }

  if (!Subtarget.hasStdExtM()) {
    setOperationAction(ISD::MUL, XLenVT, Expand);
    setOperationAction(ISD::MULHS, XLenVT, Expand);
    setOperationAction(ISD::MULHU, XLenVT, Expand);
    setOperationAction(ISD::SDIV, XLenVT, Expand);
    setOperationAction(ISD::UDIV, XLenVT, Expand);
    setOperationAction(ISD::SREM, XLenVT, Expand);
    setOperationAction(ISD::UREM, XLenVT, Expand);
  }

  if (Subtarget.is64Bit() && Subtarget.hasStdExtM()) {
    setOperationAction(ISD::MUL, MVT::i32, Custom);
    setOperationAction(ISD::SDIV, MVT::i32, Custom);
    setOperationAction(ISD::UDIV, MVT::i32, Custom);
    setOperationAction(ISD::UREM, MVT::i32, Custom);
  }

  setOperationAction(ISD::SDIVREM, XLenVT, Expand);
  setOperationAction(ISD::UDIVREM, XLenVT, Expand);
  setOperationAction(ISD::SMUL_LOHI, XLenVT, Expand);
  setOperationAction(ISD::UMUL_LOHI, XLenVT, Expand);

  setOperationAction(ISD::SHL_PARTS, XLenVT, Custom);
  setOperationAction(ISD::SRL_PARTS, XLenVT, Custom);
  setOperationAction(ISD::SRA_PARTS, XLenVT, Custom);

  if (Subtarget.hasStdExtZbb() || Subtarget.hasStdExtZbp()) {
    if (Subtarget.is64Bit()) {
      setOperationAction(ISD::ROTL, MVT::i32, Custom);
      setOperationAction(ISD::ROTR, MVT::i32, Custom);
    }
  } else {
    setOperationAction(ISD::ROTL, XLenVT, Expand);
    setOperationAction(ISD::ROTR, XLenVT, Expand);
  }

  if (Subtarget.hasStdExtZbp()) {
    setOperationAction(ISD::BITREVERSE, XLenVT, Legal);

    if (Subtarget.is64Bit()) {
      setOperationAction(ISD::BITREVERSE, MVT::i32, Custom);
      setOperationAction(ISD::BSWAP, MVT::i32, Custom);
    }
  } else {
    setOperationAction(ISD::BSWAP, XLenVT, Expand);
  }

  if (Subtarget.hasStdExtZbb()) {
    setOperationAction(ISD::SMIN, XLenVT, Legal);
    setOperationAction(ISD::SMAX, XLenVT, Legal);
    setOperationAction(ISD::UMIN, XLenVT, Legal);
    setOperationAction(ISD::UMAX, XLenVT, Legal);
  } else {
    setOperationAction(ISD::CTTZ, XLenVT, Expand);
    setOperationAction(ISD::CTLZ, XLenVT, Expand);
    setOperationAction(ISD::CTPOP, XLenVT, Expand);
  }

  if (Subtarget.hasStdExtZbt()) {
    setOperationAction(ISD::FSHL, XLenVT, Legal);
    setOperationAction(ISD::FSHR, XLenVT, Legal);

    if (Subtarget.is64Bit()) {
      setOperationAction(ISD::FSHL, MVT::i32, Custom);
      setOperationAction(ISD::FSHR, MVT::i32, Custom);
    }
  }

  ISD::CondCode FPCCToExtend[] = {
      ISD::SETOGT, ISD::SETOGE, ISD::SETONE, ISD::SETUEQ, ISD::SETUGT,
      ISD::SETUGE, ISD::SETULT, ISD::SETULE, ISD::SETUNE, ISD::SETGT,
      ISD::SETGE,  ISD::SETNE};

  ISD::NodeType FPOpToExtend[] = {
      ISD::FSIN, ISD::FCOS, ISD::FSINCOS, ISD::FPOW, ISD::FREM, ISD::FP16_TO_FP,
      ISD::FP_TO_FP16, ISD::FP_TO_BF16, ISD::FP_TO_F8_152, ISD::FP_TO_F8_143, ISD::F8_152_TO_FP, ISD::F8_143_TO_FP};

  if (Subtarget.hasStdExtF()) {
    setOperationAction(ISD::FMINNUM, MVT::f32, Legal);
    setOperationAction(ISD::FMAXNUM, MVT::f32, Legal);
    for (auto CC : FPCCToExtend)
      setCondCodeAction(CC, MVT::f32, Expand);
    setOperationAction(ISD::SELECT_CC, MVT::f32, Expand);
    setOperationAction(ISD::SELECT, MVT::f32, Custom);
    setOperationAction(ISD::BR_CC, MVT::f32, Expand);
    for (auto Op : FPOpToExtend)
      setOperationAction(Op, MVT::f32, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f32, MVT::f16, Expand);
    setTruncStoreAction(MVT::f32, MVT::f16, Expand);

    setTargetDAGCombine(ISD::FP16_TO_FP);

    setOperationAction(ISD::BF16_TO_FP, MVT::f32, Custom);

    setOperationAction(ISD::F8_152_TO_FP, MVT::i64, Expand);
    setOperationAction(ISD::F8_143_TO_FP, MVT::i64, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f32, MVT::f8_143, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f32, MVT::f8_152, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f32, MVT::bf16, Expand);
    setTruncStoreAction(MVT::f32, MVT::f8_143, Expand);
    setTruncStoreAction(MVT::f32, MVT::f8_152, Expand);
    setTruncStoreAction(MVT::f32, MVT::bf16, Expand);
  }

  if (Subtarget.hasStdExtF() && Subtarget.is64Bit())
    setOperationAction(ISD::BITCAST, MVT::i32, Custom);

  if (Subtarget.hasStdExtD()) {
    setOperationAction(ISD::FMINNUM, MVT::f64, Legal);
    setOperationAction(ISD::FMAXNUM, MVT::f64, Legal);
    for (auto CC : FPCCToExtend)
      setCondCodeAction(CC, MVT::f64, Expand);
    setOperationAction(ISD::SELECT_CC, MVT::f64, Expand);
    setOperationAction(ISD::SELECT, MVT::f64, Custom);
    setOperationAction(ISD::BR_CC, MVT::f64, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f64, MVT::f32, Expand);
    setTruncStoreAction(MVT::f64, MVT::f32, Expand);
    for (auto Op : FPOpToExtend)
      setOperationAction(Op, MVT::f64, Expand);
    setLoadExtAction(ISD::EXTLOAD, MVT::f64, MVT::f16, Expand);
    setTruncStoreAction(MVT::f64, MVT::f16, Expand);
  }

  if (Subtarget.is64Bit()) {
    setOperationAction(ISD::FP_TO_UINT, MVT::i32, Custom);
    setOperationAction(ISD::FP_TO_SINT, MVT::i32, Custom);
    setOperationAction(ISD::STRICT_FP_TO_UINT, MVT::i32, Custom);
    setOperationAction(ISD::STRICT_FP_TO_SINT, MVT::i32, Custom);
  }

  setOperationAction(ISD::GlobalAddress, XLenVT, Custom);
  setOperationAction(ISD::BlockAddress, XLenVT, Custom);
  setOperationAction(ISD::ConstantPool, XLenVT, Custom);

  setOperationAction(ISD::GlobalTLSAddress, XLenVT, Custom);

  // TODO: On M-mode only targets, the cycle[h] CSR may not be present.
  // Unfortunately this can't be determined just from the ISA naming string.
  setOperationAction(ISD::READCYCLECOUNTER, MVT::i64,
                     Subtarget.is64Bit() ? Legal : Custom);

  setOperationAction(ISD::TRAP, MVT::Other, Legal);
  setOperationAction(ISD::DEBUGTRAP, MVT::Other, Legal);
  setOperationAction(ISD::INTRINSIC_WO_CHAIN, MVT::Other, Custom);

  if (Subtarget.hasStdExtA()) {
    setMaxAtomicSizeInBitsSupported(Subtarget.getXLen());
    setMinCmpXchgSizeInBits(32);
  } else {
    setMaxAtomicSizeInBitsSupported(0);
  }

  setBooleanContents(ZeroOrOneBooleanContent);

  // Function alignments.
  const Align FunctionAlignment(Subtarget.hasStdExtC() ? 2 : 4);
  setMinFunctionAlignment(FunctionAlignment);
  setPrefFunctionAlignment(FunctionAlignment);

  // Effectively disable jump table generation.
  setMinimumJumpTableEntries(INT_MAX);

  // Jumps are expensive, compared to logic
  setJumpIsExpensive();

  // We can use any register for comparisons
  setHasMultipleConditionRegisters();

  if (Subtarget.hasStdExtZbp()) {
    setTargetDAGCombine(ISD::OR);
  }

  if (Subtarget.hasStdExtV()) {
    // EPI & VPred intrinsics may have illegal operands/results
    for (auto VT :
         {MVT::i1, MVT::i8, MVT::i16, MVT::i32, MVT::f8_143, MVT::f8_152,
          MVT::f16, MVT::bf16, MVT::nxv4i8, MVT::nxv2i16, MVT::nxv2i8,
          MVT::nxv1i32, MVT::nxv1i16, MVT::nxv1i8, MVT::nxv1f32}) {
      setOperationAction(ISD::INTRINSIC_WO_CHAIN, VT, Custom);
      setOperationAction(ISD::INTRINSIC_W_CHAIN, VT, Custom);
      setOperationAction(ISD::INTRINSIC_VOID, VT, Custom);
    }

    // Extract subvector illegal cases
    for (auto VT : {MVT::nxv4i8, MVT::nxv2i16, MVT::nxv2i8,
                    MVT::nxv1i32, MVT::nxv1i16, MVT::nxv1i8,
                    MVT::nxv1f32}) {
      setOperationAction(ISD::EXTRACT_SUBVECTOR, VT, Custom);
    }

    // Custom-legalize this node for illegal result types or mask operands.
    for (auto VT : {MVT::i8, MVT::i16, MVT::i32, MVT::nxv1i1, MVT::nxv2i1,
                    MVT::nxv4i1, MVT::nxv8i1, MVT::nxv16i1, MVT::nxv32i1,
                    MVT::nxv64i1}) {
      setOperationAction(ISD::EXTRACT_VECTOR_ELT, VT, Custom);
    }

    // Custom legalize the whole operation for masks only.
    // We already have patterns for other cases.
    if (Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv1i1, MVT::nxv2i1, MVT::nxv4i1, MVT::nxv8i1}) {
        setOperationAction(ISD::INSERT_VECTOR_ELT, VT, Custom);
        setOperationAction(ISD::VSELECT, VT, Expand);
      }
    } else {
      for (auto VT : {MVT::nxv1i1, MVT::nxv2i1, MVT::nxv4i1, MVT::nxv8i1,
                      MVT::nxv16i1, MVT::nxv32i1, MVT::nxv64i1}) {
        setOperationAction(ISD::INSERT_VECTOR_ELT, VT, Custom);
        setOperationAction(ISD::VSELECT, VT, Expand);
      }
    }

    // Concatenation of masks and small vectors. For now only in LMUL1
    if (Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv1i1, MVT::nxv2i1, MVT::nxv4i1, MVT::nxv8i1}) {
        setOperationAction(ISD::CONCAT_VECTORS, VT, Legal);
      }
      for (auto VT : {MVT::nxv1i32, MVT::nxv2i16, MVT::nxv4i8, MVT::nxv1f32}) {
        setOperationAction(ISD::CONCAT_VECTORS, VT, Custom);
      }
    }

    // Custom-legalize this node for scalable vectors.
    for (auto VT : {MVT::nxv1i64, MVT::nxv2i32, MVT::nxv4i16}) {
      setOperationAction(ISD::SIGN_EXTEND_VECTOR_INREG, VT, Custom);
      setOperationAction(ISD::ZERO_EXTEND_VECTOR_INREG, VT, Custom);
    }

    // Custom-legalize these nodes for scalable vectors.
    if (!Subtarget.onlyLMUL1()) {
      for (auto VT :
           {MVT::nxv8i8,  MVT::nxv16i8, MVT::nxv32i8,  MVT::nxv64i8,
            MVT::nxv4i16, MVT::nxv8i16, MVT::nxv16i16, MVT::nxv32i16,
            MVT::nxv2i32, MVT::nxv4i32, MVT::nxv8i32,  MVT::nxv16i32,
            MVT::nxv1i64, MVT::nxv2i64, MVT::nxv4i64,  MVT::nxv8i64,
            MVT::nxv2f32, MVT::nxv4f32, MVT::nxv8f32,  MVT::nxv16f32,
            MVT::nxv1f64, MVT::nxv2f64, MVT::nxv4f64,  MVT::nxv8f64}) {
        setOperationAction(ISD::MGATHER, VT, Custom);
        setOperationAction(ISD::MSCATTER, VT, Custom);
        setOperationAction(ISD::SELECT, VT, Custom);
        setOperationAction(ISD::VSELECT, VT, Custom);
        setOperationAction(ISD::SIGN_EXTEND, VT, Custom);
        setOperationAction(ISD::ZERO_EXTEND, VT, Custom);
        setOperationAction(ISD::TRUNCATE, VT, Custom);
      }
    }
    else
    {
      for (auto VT : {MVT::nxv8i8, MVT::nxv4i16, MVT::nxv2i32, MVT::nxv1i64,
                      MVT::nxv2f32, MVT::nxv1f64}) {
        setOperationAction(ISD::MGATHER, VT, Custom);
        setOperationAction(ISD::MSCATTER, VT, Custom);
        setOperationAction(ISD::SELECT, VT, Custom);
        setOperationAction(ISD::VSELECT, VT, Custom);
        setOperationAction(ISD::SIGN_EXTEND, VT, Custom);
        setOperationAction(ISD::ZERO_EXTEND, VT, Custom);
        setOperationAction(ISD::TRUNCATE, VT, Custom);
      }
    }

    // Register libcalls for fp vector functions.
    setLibcallName(RTLIB::EXP_NXV1F64, "__epi_exp_nxv1f64");
    setLibcallName(RTLIB::EXP2_NXV1F64, "__epi_exp2_nxv1f64");
    setLibcallName(RTLIB::SIN_NXV1F64, "__epi_sin_nxv1f64");
    setLibcallName(RTLIB::COS_NXV1F64, "__epi_cos_nxv1f64");
    setLibcallName(RTLIB::FLOG_NXV1F64, "__epi_flog_nxv1f64");
    setLibcallName(RTLIB::FLOG2_NXV1F64, "__epi_flog2_nxv1f64");
    setLibcallName(RTLIB::FLOG10_NXV1F64, "__epi_flog10_nxv1f64");
    if (!Subtarget.onlyLMUL1()) {
      setLibcallName(RTLIB::EXP_NXV2F64, "__epi_exp_nxv2f64");
      setLibcallName(RTLIB::EXP_NXV4F64, "__epi_exp_nxv4f64");
      setLibcallName(RTLIB::EXP_NXV8F64, "__epi_exp_nxv8f64");

      setLibcallName(RTLIB::EXP2_NXV2F64, "__epi_exp2_nxv2f64");
      setLibcallName(RTLIB::EXP2_NXV4F64, "__epi_exp2_nxv4f64");
      setLibcallName(RTLIB::EXP2_NXV8F64, "__epi_exp2_nxv8f64");

      setLibcallName(RTLIB::SIN_NXV2F64, "__epi_sin_nxv2f64");
      setLibcallName(RTLIB::SIN_NXV4F64, "__epi_sin_nxv4f64");
      setLibcallName(RTLIB::SIN_NXV8F64, "__epi_sin_nxv8f64");

      setLibcallName(RTLIB::COS_NXV2F64, "__epi_cos_nxv2f64");
      setLibcallName(RTLIB::COS_NXV4F64, "__epi_cos_nxv4f64");
      setLibcallName(RTLIB::COS_NXV8F64, "__epi_cos_nxv8f64");

      setLibcallName(RTLIB::FLOG_NXV2F64, "__epi_flog_nxv2f64");
      setLibcallName(RTLIB::FLOG_NXV4F64, "__epi_flog_nxv4f64");
      setLibcallName(RTLIB::FLOG_NXV8F64, "__epi_flog_nxv8f64");

      setLibcallName(RTLIB::FLOG2_NXV2F64, "__epi_flog2_nxv2f64");
      setLibcallName(RTLIB::FLOG2_NXV4F64, "__epi_flog2_nxv4f64");
      setLibcallName(RTLIB::FLOG2_NXV8F64, "__epi_flog2_nxv8f64");

      setLibcallName(RTLIB::FLOG10_NXV2F64, "__epi_flog10_nxv2f64");
      setLibcallName(RTLIB::FLOG10_NXV4F64, "__epi_flog10_nxv4f64");
      setLibcallName(RTLIB::FLOG10_NXV8F64, "__epi_flog10_nxv8f64");
    }
    setLibcallName(RTLIB::EXP_NXV2F32, "__epi_exp_nxv2f32");
    setLibcallName(RTLIB::EXP2_NXV2F32, "__epi_exp2_nxv2f32");
    setLibcallName(RTLIB::SIN_NXV2F32, "__epi_sin_nxv2f32");
    setLibcallName(RTLIB::COS_NXV2F32, "__epi_cos_nxv2f32");
    setLibcallName(RTLIB::FLOG_NXV2F32, "__epi_flog_nxv2f32");
    setLibcallName(RTLIB::FLOG2_NXV2F32, "__epi_flog2_nxv2f32");
    setLibcallName(RTLIB::FLOG10_NXV2F32, "__epi_flog10_nxv2f32");
    if (!Subtarget.onlyLMUL1()) {
      setLibcallName(RTLIB::EXP_NXV4F32, "__epi_exp_nxv4f32");
      setLibcallName(RTLIB::EXP_NXV8F32, "__epi_exp_nxv8f32");
      setLibcallName(RTLIB::EXP_NXV16F32, "__epi_exp_nxv16f32");

      setLibcallName(RTLIB::EXP2_NXV4F32, "__epi_exp2_nxv4f32");
      setLibcallName(RTLIB::EXP2_NXV8F32, "__epi_exp2_nxv8f32");
      setLibcallName(RTLIB::EXP2_NXV16F32, "__epi_exp2_nxv16f32");

      setLibcallName(RTLIB::SIN_NXV4F32, "__epi_sin_nxv4f32");
      setLibcallName(RTLIB::SIN_NXV8F32, "__epi_sin_nxv8f32");
      setLibcallName(RTLIB::SIN_NXV16F32, "__epi_sin_nxv16f32");

      setLibcallName(RTLIB::COS_NXV4F32, "__epi_cos_nxv4f32");
      setLibcallName(RTLIB::COS_NXV8F32, "__epi_cos_nxv8f32");
      setLibcallName(RTLIB::COS_NXV16F32, "__epi_cos_nxv16f32");

      setLibcallName(RTLIB::FLOG_NXV4F32, "__epi_flog_nxv4f32");
      setLibcallName(RTLIB::FLOG_NXV8F32, "__epi_flog_nxv8f32");
      setLibcallName(RTLIB::FLOG_NXV16F32, "__epi_flog_nxv16f32");

      setLibcallName(RTLIB::FLOG2_NXV4F32, "__epi_flog2_nxv4f32");
      setLibcallName(RTLIB::FLOG2_NXV8F32, "__epi_flog2_nxv8f32");
      setLibcallName(RTLIB::FLOG2_NXV16F32, "__epi_flog2_nxv16f32");

      setLibcallName(RTLIB::FLOG10_NXV4F32, "__epi_flog10_nxv4f32");
      setLibcallName(RTLIB::FLOG10_NXV8F32, "__epi_flog10_nxv8f32");
      setLibcallName(RTLIB::FLOG10_NXV16F32, "__epi_flog10_nxv16f32");
    }

    setLibcallName(RTLIB::POW_NXV1F64, "__epi_pow_nxv1f64");
    setLibcallName(RTLIB::POW_NXV2F64, "__epi_pow_nxv2f64");
    setLibcallName(RTLIB::POW_NXV4F64, "__epi_pow_nxv4f64");
    setLibcallName(RTLIB::POW_NXV8F64, "__epi_pow_nxv8f64");
    setLibcallName(RTLIB::POW_NXV1F32, "__epi_pow_nxv1f32");
    setLibcallName(RTLIB::POW_NXV2F32, "__epi_pow_nxv2f32");
    setLibcallName(RTLIB::POW_NXV4F32, "__epi_pow_nxv4f32");
    setLibcallName(RTLIB::POW_NXV8F32, "__epi_pow_nxv8f32");
    setLibcallName(RTLIB::POW_NXV16F32, "__epi_pow_nxv16f32");

    setLibcallName(RTLIB::REM_NXV1F64, "__epi_fmod_nxv1f64");
    setLibcallName(RTLIB::REM_NXV2F64, "__epi_fmod_nxv2f64");
    setLibcallName(RTLIB::REM_NXV4F64, "__epi_fmod_nxv4f64");
    setLibcallName(RTLIB::REM_NXV8F64, "__epi_fmod_nxv8f64");
    setLibcallName(RTLIB::REM_NXV2F32, "__epi_fmod_nxv2f32");
    setLibcallName(RTLIB::REM_NXV4F32, "__epi_fmod_nxv4f32");
    setLibcallName(RTLIB::REM_NXV8F32, "__epi_fmod_nxv8f32");
    setLibcallName(RTLIB::REM_NXV16F32, "__epi_fmod_nxv16f32");

    // Register libcalls for VP Intrinsics.
    setLibcallName(RTLIB::VP_FREM_NXV1F64, "__epi_vp_fmod_nxv1f64");
    setLibcallName(RTLIB::VP_FREM_NXV2F64, "__epi_vp_fmod_nxv2f64");
    setLibcallName(RTLIB::VP_FREM_NXV4F64, "__epi_vp_fmod_nxv4f64");
    setLibcallName(RTLIB::VP_FREM_NXV8F64, "__epi_vp_fmod_nxv8f64");
    setLibcallName(RTLIB::VP_FREM_NXV2F32, "__epi_vp_fmod_nxv2f32");
    setLibcallName(RTLIB::VP_FREM_NXV4F32, "__epi_vp_fmod_nxv4f32");
    setLibcallName(RTLIB::VP_FREM_NXV8F32, "__epi_vp_fmod_nxv8f32");
    setLibcallName(RTLIB::VP_FREM_NXV16F32, "__epi_vp_fmod_nxv16f32");

    // Custom-legalize these nodes for fp scalable vectors.
    if (!Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv2f32, MVT::nxv4f32, MVT::nxv8f32, MVT::nxv16f32,
                      MVT::nxv1f64, MVT::nxv2f64, MVT::nxv4f64, MVT::nxv8f64}) {
        setOperationAction(ISD::FEXP, VT, Custom);
        setOperationAction(ISD::FEXP2, VT, Custom);
        setOperationAction(ISD::FSIN, VT, Custom);
        setOperationAction(ISD::FCOS, VT, Custom);
        setOperationAction(ISD::FLOG, VT, Custom);
        setOperationAction(ISD::FLOG2, VT, Custom);
        setOperationAction(ISD::FLOG10, VT, Custom);
        setOperationAction(ISD::FPOW, VT, Custom);
        setOperationAction(ISD::FREM, VT, Custom);
        setOperationAction(ISD::SETCC, VT, Custom);
      }
    } else {
      for (auto VT : {MVT::nxv2f32, MVT::nxv1f64}) {
        setOperationAction(ISD::FEXP, VT, Custom);
        setOperationAction(ISD::FEXP2, VT, Custom);
        setOperationAction(ISD::FSIN, VT, Custom);
        setOperationAction(ISD::FCOS, VT, Custom);
        setOperationAction(ISD::FLOG, VT, Custom);
        setOperationAction(ISD::FLOG2, VT, Custom);
        setOperationAction(ISD::FLOG10, VT, Custom);
        setOperationAction(ISD::FPOW, VT, Custom);
        setOperationAction(ISD::FREM, VT, Custom);
        setOperationAction(ISD::SETCC, VT, Custom);
      }
    }

    // Vector integer reductions.
    if (!Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv8i8, MVT::nxv16i8, MVT::nxv32i8, MVT::nxv64i8,
                      MVT::nxv4i16, MVT::nxv8i16, MVT::nxv16i16, MVT::nxv32i16,
                      MVT::nxv2i32, MVT::nxv4i32, MVT::nxv8i32, MVT::nxv16i32,
                      MVT::nxv1i64, MVT::nxv2i64, MVT::nxv4i64, MVT::nxv8i64}) {
        setOperationAction(ISD::VECREDUCE_ADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_AND, VT, Legal);
        setOperationAction(ISD::VECREDUCE_OR, VT, Legal);
        setOperationAction(ISD::VECREDUCE_XOR, VT, Legal);
        setOperationAction(ISD::VECREDUCE_SMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_SMIN, VT, Legal);
        setOperationAction(ISD::VECREDUCE_UMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_UMIN, VT, Legal);
      }
      for (auto VT : {MVT::nxv1i1, MVT::nxv2i1, MVT::nxv4i1, MVT::nxv8i1,
                      MVT::nxv16i1, MVT::nxv32i1, MVT::nxv64i1}) {
        setOperationAction(ISD::VECREDUCE_AND, VT, Custom);
        setOperationAction(ISD::VECREDUCE_OR, VT, Custom);
        setOperationAction(ISD::VECREDUCE_XOR, VT, Custom);
      }
    } else {
      for (auto VT : {MVT::nxv8i8, MVT::nxv4i16, MVT::nxv2i32, MVT::nxv1i64}) {
        setOperationAction(ISD::VECREDUCE_ADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_AND, VT, Legal);
        setOperationAction(ISD::VECREDUCE_OR, VT, Legal);
        setOperationAction(ISD::VECREDUCE_XOR, VT, Legal);
        setOperationAction(ISD::VECREDUCE_SMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_SMIN, VT, Legal);
        setOperationAction(ISD::VECREDUCE_UMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_UMIN, VT, Legal);
      }
      for (auto VT : {MVT::nxv1i1, MVT::nxv2i1, MVT::nxv4i1, MVT::nxv8i1}) {
        setOperationAction(ISD::VECREDUCE_AND, VT, Custom);
        setOperationAction(ISD::VECREDUCE_OR, VT, Custom);
        setOperationAction(ISD::VECREDUCE_XOR, VT, Custom);
      }
    }

    // Vector fp reductions.
    if (!Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv2f32, MVT::nxv4f32, MVT::nxv8f32, MVT::nxv16f32,
                      MVT::nxv1f64, MVT::nxv2f64, MVT::nxv4f64, MVT::nxv8f64}) {
        setOperationAction(ISD::VECREDUCE_SEQ_FADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMUL, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMIN, VT, Legal);
      }
    } else {
      for (auto VT : {MVT::nxv2f32, MVT::nxv1f64}) {
        setOperationAction(ISD::VECREDUCE_SEQ_FADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FADD, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMUL, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMAX, VT, Legal);
        setOperationAction(ISD::VECREDUCE_FMIN, VT, Legal);
      }
    }

    // Vector fp min/max operations.
    if (!Subtarget.onlyLMUL1()) {
      for (auto VT : {MVT::nxv2f32, MVT::nxv4f32, MVT::nxv8f32, MVT::nxv16f32,
                      MVT::nxv1f64, MVT::nxv2f64, MVT::nxv4f64, MVT::nxv8f64}) {
        setOperationAction(ISD::FMINNUM, VT, Legal);
        setOperationAction(ISD::FMAXNUM, VT, Legal);

        setOperationAction(ISD::FTRUNC, VT, Custom);
        setOperationAction(ISD::FCEIL, VT, Custom);
        setOperationAction(ISD::FFLOOR, VT, Custom);

        setOperationAction(ISD::FCOPYSIGN, VT, Custom);
      }
    } else {
      for (auto VT : {MVT::nxv2f32, MVT::nxv1f64}) {
        setOperationAction(ISD::FMINNUM, VT, Legal);
        setOperationAction(ISD::FMAXNUM, VT, Legal);

        setOperationAction(ISD::FTRUNC, VT, Custom);
        setOperationAction(ISD::FCEIL, VT, Custom);
        setOperationAction(ISD::FFLOOR, VT, Custom);

        setOperationAction(ISD::FCOPYSIGN, VT, Custom);
      }
    }
  }
}

EVT RISCVTargetLowering::getSetCCResultType(const DataLayout &DL, LLVMContext &,
                                            EVT VT) const {
  if (!VT.isVector())
    return getPointerTy(DL);
  if (Subtarget.hasStdExtV())
    return MVT::getVectorVT(MVT::i1, VT.getVectorElementCount());
  else
    return VT.changeVectorElementTypeToInteger();
}

bool RISCVTargetLowering::getTgtMemIntrinsic(IntrinsicInfo &Info,
                                             const CallInst &I,
                                             MachineFunction &MF,
                                             unsigned Intrinsic) const {
  auto &DL = I.getModule()->getDataLayout();
  switch (Intrinsic) {
  default:
    return false;
  case Intrinsic::riscv_masked_atomicrmw_xchg_i32:
  case Intrinsic::riscv_masked_atomicrmw_add_i32:
  case Intrinsic::riscv_masked_atomicrmw_sub_i32:
  case Intrinsic::riscv_masked_atomicrmw_nand_i32:
  case Intrinsic::riscv_masked_atomicrmw_max_i32:
  case Intrinsic::riscv_masked_atomicrmw_min_i32:
  case Intrinsic::riscv_masked_atomicrmw_umax_i32:
  case Intrinsic::riscv_masked_atomicrmw_umin_i32:
  case Intrinsic::riscv_masked_cmpxchg_i32: {
    PointerType *PtrTy = cast<PointerType>(I.getArgOperand(0)->getType());
    Info.opc = ISD::INTRINSIC_W_CHAIN;
    Info.memVT = MVT::getVT(PtrTy->getElementType());
    Info.ptrVal = I.getArgOperand(0);
    Info.offset = 0;
    Info.align = Align(4);
    Info.flags = MachineMemOperand::MOLoad | MachineMemOperand::MOStore |
                 MachineMemOperand::MOVolatile;
    return true;
  }
  case Intrinsic::epi_vload:
  case Intrinsic::epi_vload_strided:
  case Intrinsic::epi_vload_indexed: {
    PointerType *PtrTy = cast<PointerType>(I.getArgOperand(0)->getType());
    Info.opc = ISD::INTRINSIC_W_CHAIN;
    Info.memVT = MVT::getVT(PtrTy->getElementType());
    Info.ptrVal = I.getArgOperand(0);
    Info.offset = 0;
    Info.align = MaybeAlign(DL.getABITypeAlignment(PtrTy->getElementType()));
    Info.flags = MachineMemOperand::MOLoad;
    return true;
  }
  case Intrinsic::epi_vload_mask:
  case Intrinsic::epi_vload_strided_mask:
  case Intrinsic::epi_vload_indexed_mask: {
    PointerType *PtrTy = cast<PointerType>(I.getArgOperand(1)->getType());
    Info.opc = ISD::INTRINSIC_W_CHAIN;
    Info.memVT = MVT::getVT(PtrTy->getElementType());
    Info.ptrVal = I.getArgOperand(1);
    Info.offset = 0;
    Info.align = MaybeAlign(DL.getABITypeAlignment(PtrTy->getElementType()));
    Info.flags = MachineMemOperand::MOLoad;
    return true;
  }
  case Intrinsic::epi_vstore:
  case Intrinsic::epi_vstore_strided:
  case Intrinsic::epi_vstore_indexed:
  case Intrinsic::epi_vstore_mask:
  case Intrinsic::epi_vstore_strided_mask:
  case Intrinsic::epi_vstore_indexed_mask: {
    PointerType *PtrTy = cast<PointerType>(I.getArgOperand(1)->getType());
    Info.opc = ISD::INTRINSIC_VOID;
    Info.memVT = MVT::getVT(PtrTy->getElementType());
    Info.ptrVal = I.getArgOperand(1);
    Info.offset = 0;
    Info.align = MaybeAlign(DL.getABITypeAlignment(PtrTy->getElementType()));
    Info.flags = MachineMemOperand::MOStore;
    return true;
  }
  }
}

bool RISCVTargetLowering::isLegalAddressingMode(const DataLayout &DL,
                                                const AddrMode &AM, Type *Ty,
                                                unsigned AS,
                                                Instruction *I) const {
  // No global is ever allowed as a base.
  if (AM.BaseGV)
    return false;

  // Require a 12-bit signed offset.
  if (!isInt<12>(AM.BaseOffs))
    return false;

  switch (AM.Scale) {
  case 0: // "r+i" or just "i", depending on HasBaseReg.
    break;
  case 1:
    if (!AM.HasBaseReg) // allow "r+i".
      break;
    return false; // disallow "r+r" or "r+r+i".
  default:
    return false;
  }

  return true;
}

bool RISCVTargetLowering::isLegalICmpImmediate(int64_t Imm) const {
  return isInt<12>(Imm);
}

bool RISCVTargetLowering::isLegalAddImmediate(int64_t Imm) const {
  return isInt<12>(Imm);
}

// On RV32, 64-bit integers are split into their high and low parts and held
// in two different registers, so the trunc is free since the low register can
// just be used.
bool RISCVTargetLowering::isTruncateFree(Type *SrcTy, Type *DstTy) const {
  if (Subtarget.is64Bit() || !SrcTy->isIntegerTy() || !DstTy->isIntegerTy())
    return false;
  unsigned SrcBits = SrcTy->getPrimitiveSizeInBits();
  unsigned DestBits = DstTy->getPrimitiveSizeInBits();
  return (SrcBits == 64 && DestBits == 32);
}

bool RISCVTargetLowering::isTruncateFree(EVT SrcVT, EVT DstVT) const {
  if (Subtarget.is64Bit() || SrcVT.isVector() || DstVT.isVector() ||
      !SrcVT.isInteger() || !DstVT.isInteger())
    return false;
  unsigned SrcBits = SrcVT.getSizeInBits();
  unsigned DestBits = DstVT.getSizeInBits();
  return (SrcBits == 64 && DestBits == 32);
}

bool RISCVTargetLowering::isZExtFree(SDValue Val, EVT VT2) const {
  // Zexts are free if they can be combined with a load.
  if (auto *LD = dyn_cast<LoadSDNode>(Val)) {
    EVT MemVT = LD->getMemoryVT();
    if ((MemVT == MVT::i8 || MemVT == MVT::i16 ||
         (Subtarget.is64Bit() && MemVT == MVT::i32)) &&
        (LD->getExtensionType() == ISD::NON_EXTLOAD ||
         LD->getExtensionType() == ISD::ZEXTLOAD))
      return true;
  }

  return TargetLowering::isZExtFree(Val, VT2);
}

bool RISCVTargetLowering::isSExtCheaperThanZExt(EVT SrcVT, EVT DstVT) const {
  return Subtarget.is64Bit() && SrcVT == MVT::i32 && DstVT == MVT::i64;
}

bool RISCVTargetLowering::isCheapToSpeculateCttz() const {
  return Subtarget.hasStdExtZbb();
}

bool RISCVTargetLowering::isCheapToSpeculateCtlz() const {
  return Subtarget.hasStdExtZbb();
}

bool RISCVTargetLowering::isFPImmLegal(const APFloat &Imm, EVT VT,
                                       bool ForCodeSize) const {
  if (VT == MVT::f32 && !Subtarget.hasStdExtF())
    return false;
  if (VT == MVT::f64 && !Subtarget.hasStdExtD())
    return false;
  if (Imm.isNegZero())
    return false;
  return Imm.isZero();
}

bool RISCVTargetLowering::hasBitPreservingFPLogic(EVT VT) const {
  return (VT == MVT::f32 && Subtarget.hasStdExtF()) ||
         (VT == MVT::f64 && Subtarget.hasStdExtD());
}

// Changes the condition code and swaps operands if necessary, so the SetCC
// operation matches one of the comparisons supported directly in the RISC-V
// ISA.
static void normaliseSetCC(SDValue &LHS, SDValue &RHS, ISD::CondCode &CC) {
  switch (CC) {
  default:
    break;
  case ISD::SETGT:
  case ISD::SETLE:
  case ISD::SETUGT:
  case ISD::SETULE:
    CC = ISD::getSetCCSwappedOperands(CC);
    std::swap(LHS, RHS);
    break;
  }
}

// Return the RISC-V branch opcode that matches the given DAG integer
// condition code. The CondCode must be one of those supported by the RISC-V
// ISA (see normaliseSetCC).
static unsigned getBranchOpcodeForIntCondCode(ISD::CondCode CC) {
  switch (CC) {
  default:
    llvm_unreachable("Unsupported CondCode");
  case ISD::SETEQ:
    return RISCV::BEQ;
  case ISD::SETNE:
    return RISCV::BNE;
  case ISD::SETLT:
    return RISCV::BLT;
  case ISD::SETGE:
    return RISCV::BGE;
  case ISD::SETULT:
    return RISCV::BLTU;
  case ISD::SETUGE:
    return RISCV::BGEU;
  }
}

SDValue RISCVTargetLowering::lowerVECTOR_SHUFFLE(SDValue Op,
                                                 SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT VT = Op.getValueType();

  ShuffleVectorSDNode *SVN = cast<ShuffleVectorSDNode>(Op.getNode());

  if (!SVN->isSplat())
    return SDValue();

  // FIXME - Use splat index!
  if (SVN->getSplatIndex() != 0)
    return SDValue();

  // Recover the scalar value.
  SDValue SplatVector = Op.getOperand(0);
  // FIXME - Look other targets what they do with suffles, it should be
  // possible to use vmv/vfmv or vrgather.
  if (SplatVector.getOpcode() != ISD::INSERT_VECTOR_ELT)
    return SDValue();
  if (SplatVector.getOperand(0).getOpcode() != ISD::UNDEF)
    return SDValue();
  SDValue ScalarValue = SplatVector.getOperand(1);
  auto *ConstantValue = dyn_cast<ConstantSDNode>(SplatVector.getOperand(2));
  if (!ConstantValue || ConstantValue->getZExtValue() != 0)
    return SDValue();

  return DAG.getNode(ISD::SPLAT_VECTOR, DL, VT, ScalarValue);
}

SDValue RISCVTargetLowering::lowerExtend(SDValue Op, SelectionDAG &DAG,
                                         int Opcode) const {
  SDLoc DL(Op);
  EVT VT = Op.getValueType();

  SDValue Src = Op.getOperand(0);
  EVT SrcVT = Src.getValueType();

  // Skip masks.
  if (SrcVT.getVectorElementType() == MVT::i1)
    return Op;

  EVT ResultVT = SrcVT;
  SDValue Result = Src;
  do {
    ResultVT = ResultVT.widenIntegerVectorElementType(*DAG.getContext());
    Result = DAG.getNode(Opcode, DL, ResultVT, Result);
  } while (ResultVT != VT);

  return Result;
}

SDValue RISCVTargetLowering::lowerSIGN_EXTEND(SDValue Op,
                                              SelectionDAG &DAG) const {
  return lowerExtend(Op, DAG, RISCVISD::SIGN_EXTEND_VECTOR);
}

SDValue RISCVTargetLowering::lowerZERO_EXTEND(SDValue Op,
                                              SelectionDAG &DAG) const {
  return lowerExtend(Op, DAG, RISCVISD::ZERO_EXTEND_VECTOR);
}

SDValue RISCVTargetLowering::lowerTRUNCATE(SDValue Op,
                                           SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT VT = Op.getValueType();

  SDValue Src = Op.getOperand(0);
  EVT SrcVT = Src.getValueType();

  // Skip masks.
  if (VT.getVectorElementType() == MVT::i1)
    return Op;

  auto HalfIntegerVectorElementType = [&DAG](EVT PrevVT) {
    EVT EltVT = PrevVT.getVectorElementType();
    assert(EltVT.getSizeInBits() % 2 == 0 && "Invalid bit size");
    EltVT = EVT::getIntegerVT(*DAG.getContext(), EltVT.getSizeInBits() / 2);
    return EVT::getVectorVT(*DAG.getContext(), EltVT,
                            PrevVT.getVectorElementCount());
  };

  EVT ResultVT = SrcVT;
  SDValue Result = Src;
  do {
    ResultVT = HalfIntegerVectorElementType(ResultVT);
    Result = DAG.getNode(RISCVISD::TRUNCATE_VECTOR, DL, ResultVT, Result);
  } while (ResultVT != VT);

  return Result;
}

SDValue RISCVTargetLowering::lowerExtendVectorInReg(SDValue Op,
                                                    SelectionDAG &DAG,
                                                    int Opcode) const {
  SDLoc DL(Op);
  EVT VT = Op.getValueType();

  EVT SrcVT = Op.getOperand(0).getValueType();

  uint64_t ResTyBits = VT.getScalarSizeInBits();
  uint64_t OpTyBits = SrcVT.getScalarSizeInBits();

  assert(isPowerOf2_64(ResTyBits) && isPowerOf2_64(OpTyBits) &&
         (ResTyBits > OpTyBits));

  // For this to work we need to shuffle the elements first.
  SDValue Shuffled =
      DAG.getNode(RISCVISD::SHUFFLE_EXTEND, DL, SrcVT, Op.getOperand(0),
                  DAG.getConstant(Log2_64(ResTyBits / OpTyBits), DL, MVT::i64));

  // Compute the number of bits to sign-extend.
  SDValue ExtendBits = DAG.getConstant(ResTyBits - OpTyBits, DL, MVT::i64);
  SDValue SextInreg = DAG.getNode(Opcode, DL, VT, Shuffled, ExtendBits);

  return SextInreg;
}

SDValue
RISCVTargetLowering::lowerSIGN_EXTEND_VECTOR_INREG(SDValue Op,
                                                   SelectionDAG &DAG) const {
  return lowerExtendVectorInReg(Op, DAG, RISCVISD::SIGN_EXTEND_BITS_INREG);
}

SDValue
RISCVTargetLowering::lowerZERO_EXTEND_VECTOR_INREG(SDValue Op,
                                                   SelectionDAG &DAG) const {
  return lowerExtendVectorInReg(Op, DAG, RISCVISD::ZERO_EXTEND_BITS_INREG);
}

SDValue RISCVTargetLowering::lowerMGATHER(SDValue Op, SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT VT = Op.getValueType();
  assert(VT.isScalableVector() && "Unexpected type");

  SDValue Indices = Op.getOperand(4);
  EVT IndexVT = Indices.getValueType();
  assert(IndexVT == VT.changeVectorElementTypeToInteger() &&
         "Unexpected type for indices");

  SDValue Offsets;
  if (Op.getConstantOperandVal(5) == 1) {
    Offsets = Indices;
  } else {
    SDValue VSLLOperands[] = {
        DAG.getTargetConstant(Intrinsic::epi_vsll, DL, MVT::i64), Indices,
        DAG.getConstant(Log2_64(Op.getConstantOperandVal(5)), DL,
                        MVT::i64),           // log2(Scale).
        DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX.
    };
    // FIXME: This may overflow. RVV-0.9 provides mechanisms to decouple the
    // index width from the data element width, solving this problem.
    Offsets = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, IndexVT, VSLLOperands);
  }

  SDValue VLXEOperands[] = {
      Op.getOperand(0), // Chain.
      DAG.getTargetConstant(Intrinsic::epi_vload_indexed_mask, DL, MVT::i64),
      Op.getOperand(1), // Merge.
      Op.getOperand(3), // Base address.
      Offsets,
      Op.getOperand(2),                    // Mask.
      DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX.
  };

  return DAG.getNode(ISD::INTRINSIC_W_CHAIN, DL, Op->getVTList(), VLXEOperands);
}

SDValue RISCVTargetLowering::lowerMSCATTER(SDValue Op,
                                           SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT VT = Op.getOperand(1).getValueType();
  assert(VT.isScalableVector() && "Unexpected type");

  SDValue Indices = Op.getOperand(4);
  EVT IndexVT = Indices.getValueType();
  assert(IndexVT == VT.changeVectorElementTypeToInteger() &&
         "Unexpected type for indices");

  SDValue Offsets;
  if (Op.getConstantOperandVal(5) == 1) {
    Offsets = Indices;
  } else {
    SDValue VSLLOperands[] = {
        DAG.getTargetConstant(Intrinsic::epi_vsll, DL, MVT::i64), Indices,
        DAG.getConstant(Log2_64(Op.getConstantOperandVal(5)), DL,
                        MVT::i64),           // log2(Scale).
        DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX.
    };
    // FIXME: This may overflow. RVV-0.9 provides mechanisms to decouple the
    // index width from the data element width, solving this problem.
    Offsets = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, IndexVT, VSLLOperands);
  }

  // Val, OutChain = GATHER (InChain, PassThru, Mask, BasePtr, Index, Scale)
  //      OutChain = SCATTER(InChain, Value,    Mask, BasePtr, Index, Scale)
  SDValue VSXEOperands[] = {
      Op.getOperand(0), // Chain.
      DAG.getTargetConstant(Intrinsic::epi_vstore_indexed_mask, DL, MVT::i64),
      Op.getOperand(1), // Data
      Op.getOperand(3), // Base address.
      Offsets,
      Op.getOperand(2),                    // Mask.
      DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX.
  };

  return DAG.getNode(ISD::INTRINSIC_VOID, DL, Op->getVTList(), VSXEOperands);
}

SDValue RISCVTargetLowering::lowerEXTRACT_VECTOR_ELT(SDValue Op,
                                                     SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT VectorVT = Op.getOperand(0).getValueType();

  if (VectorVT.getVectorElementType() != MVT::i1)
    return SDValue();

  // Extend logical vector type before extracting an element:
  //
  // Before:
  //   (i64 (extract_vector_elt (nxv1i1), i64))
  // After:
  //   (i64 (extract_vector_elt (nxv1i64 (any_extend (nxv1i1))), i64))
  //
  // Before:
  //   (i64 (extract_vector_elt (nxv2i1), i64))
  // After:
  //   (i64 (extract_vector_elt (nxv2i32 (any_extend (nxv2i1))), i64))
  //
  // For LMUL>1 logical types we use LMUL>1 integer types (as opposed to LMUL=1
  // sub-byte types. Eg. nxv16i8 instead of nxv8i8 with two i4 elements in each
  // i8).
  // Before:
  //   (i64 (extract_vector_elt (nxv16i1), i64))
  // After:
  //   (i64 (extract_vector_elt (nxv16i8 (any_extend (nxv16i1))), i64))

  MVT ExtVectorEltVT =
      VectorVT.getVectorMinNumElements() <= 8
          ? MVT::getIntegerVT(64 / VectorVT.getVectorMinNumElements())
          : MVT::i8; // FIXME: ELEN=64 hardcoded.
  EVT ExtVectorVT = VectorVT.changeVectorElementType(ExtVectorEltVT);

  unsigned Opcode = ExtVectorEltVT == MVT::i64 ? ISD::EXTRACT_VECTOR_ELT
                                               : RISCVISD::EXTRACT_VECTOR_ELT;

  SDValue ExtOp0 =
      DAG.getNode(ISD::ANY_EXTEND, DL, ExtVectorVT, Op.getOperand(0));

  MVT ScalarResVT = Op.getValueType().getSimpleVT(); // Type already legalized.
  SDValue ExtractElt =
      DAG.getNode(Opcode, DL, ScalarResVT, ExtOp0, Op.getOperand(1));

  return DAG.getNode(ISD::AssertZext, DL, ScalarResVT, ExtractElt,
                     DAG.getValueType(MVT::i1));
}

SDValue RISCVTargetLowering::lowerINSERT_VECTOR_ELT(SDValue Op,
                                                    SelectionDAG &DAG) const {
  SDLoc DL(Op);
  MVT VecVT = Op.getSimpleValueType();
  SDValue Vec = Op.getOperand(0);
  SDValue Val = Op.getOperand(1);
  SDValue Idx = Op.getOperand(2);

  assert(VecVT.getVectorElementType() == MVT::i1 && "Unexpected type");
  MVT WideVT;
  if (Subtarget.onlyLMUL1()) {
    assert(VecVT.getVectorElementCount().getKnownMinValue() <= 8 &&
           "Mask type was unexpected");
    // 0.7.1 has a complicated story with mask types so let's use the easiest
    // matching integer vector type for the extension.
    MVT IntegerType =
        MVT::getIntegerVT(64 / VecVT.getVectorElementCount().getKnownMinValue());
    WideVT = MVT::getVectorVT(IntegerType, VecVT.getVectorElementCount());
  } else {
    // FIXME: For now we just promote to an i8 vector and insert into that,
    // but this is probably not optimal.
    WideVT = MVT::getVectorVT(MVT::i8, VecVT.getVectorElementCount());
  }
  Vec = DAG.getNode(ISD::ZERO_EXTEND, DL, WideVT, Vec);
  Vec = DAG.getNode(ISD::INSERT_VECTOR_ELT, DL, WideVT, Vec, Val, Idx);
  return DAG.getNode(ISD::TRUNCATE, DL, VecVT, Vec);
}

SDValue
RISCVTargetLowering::lowerVECLIBCALL(EVT VT, SDLoc DL, ArrayRef<SDValue> Operands,
                                     SelectionDAG &DAG,
                                     ArrayRef<VTToLibCall> VTToLC) const {
  auto LCIt = std::find_if(
      VTToLC.begin(), VTToLC.end(),
      [VT](const std::pair<EVT, RTLIB::Libcall> &P) { return P.first == VT; });
  assert(LCIt != VTToLC.end() && "Unexpected VT");

  RTLIB::Libcall LC = LCIt->second;

  MakeLibCallOptions CallOptions;
  SDValue Chain;
  SDValue Result;
  std::tie(Result, Chain) =
      makeLibCall(DAG, LC, VT, Operands, CallOptions, DL);
  return Result;
}

SDValue
RISCVTargetLowering::lowerVECLIBCALL(SDValue Op, SelectionDAG &DAG,
                                     ArrayRef<VTToLibCall> VTToLC) const {
  SDLoc DL(Op);
  std::vector<SDValue> Operands(Op->op_begin(), Op->op_end());
  return lowerVECLIBCALL(Op.getValueType(), DL, Operands, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFEXP(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::EXP_NXV1F64}, {MVT::nxv2f64, RTLIB::EXP_NXV2F64},
      {MVT::nxv4f64, RTLIB::EXP_NXV4F64}, {MVT::nxv8f64, RTLIB::EXP_NXV8F64},
      {MVT::nxv2f32, RTLIB::EXP_NXV2F32}, {MVT::nxv4f32, RTLIB::EXP_NXV4F32},
      {MVT::nxv8f32, RTLIB::EXP_NXV8F32}, {MVT::nxv16f32, RTLIB::EXP_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFEXP2(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::EXP2_NXV1F64}, {MVT::nxv2f64, RTLIB::EXP2_NXV2F64},
      {MVT::nxv4f64, RTLIB::EXP2_NXV4F64}, {MVT::nxv8f64, RTLIB::EXP2_NXV8F64},
      {MVT::nxv2f32, RTLIB::EXP2_NXV2F32}, {MVT::nxv4f32, RTLIB::EXP2_NXV4F32},
      {MVT::nxv8f32, RTLIB::EXP2_NXV8F32}, {MVT::nxv16f32, RTLIB::EXP2_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFSIN(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::SIN_NXV1F64}, {MVT::nxv2f64, RTLIB::SIN_NXV2F64},
      {MVT::nxv4f64, RTLIB::SIN_NXV4F64}, {MVT::nxv8f64, RTLIB::SIN_NXV8F64},
      {MVT::nxv2f32, RTLIB::SIN_NXV2F32}, {MVT::nxv4f32, RTLIB::SIN_NXV4F32},
      {MVT::nxv8f32, RTLIB::SIN_NXV8F32}, {MVT::nxv16f32, RTLIB::SIN_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFCOS(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::COS_NXV1F64}, {MVT::nxv2f64, RTLIB::COS_NXV2F64},
      {MVT::nxv4f64, RTLIB::COS_NXV4F64}, {MVT::nxv8f64, RTLIB::COS_NXV8F64},
      {MVT::nxv2f32, RTLIB::COS_NXV2F32}, {MVT::nxv4f32, RTLIB::COS_NXV4F32},
      {MVT::nxv8f32, RTLIB::COS_NXV8F32}, {MVT::nxv16f32, RTLIB::COS_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFLOG(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::FLOG_NXV1F64}, {MVT::nxv2f64, RTLIB::FLOG_NXV2F64},
      {MVT::nxv4f64, RTLIB::FLOG_NXV4F64}, {MVT::nxv8f64, RTLIB::FLOG_NXV8F64},
      {MVT::nxv2f32, RTLIB::FLOG_NXV2F32}, {MVT::nxv4f32, RTLIB::FLOG_NXV4F32},
      {MVT::nxv8f32, RTLIB::FLOG_NXV8F32}, {MVT::nxv16f32, RTLIB::FLOG_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFLOG2(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::FLOG2_NXV1F64}, {MVT::nxv2f64, RTLIB::FLOG2_NXV2F64},
      {MVT::nxv4f64, RTLIB::FLOG2_NXV4F64}, {MVT::nxv8f64, RTLIB::FLOG2_NXV8F64},
      {MVT::nxv2f32, RTLIB::FLOG2_NXV2F32}, {MVT::nxv4f32, RTLIB::FLOG2_NXV4F32},
      {MVT::nxv8f32, RTLIB::FLOG2_NXV8F32}, {MVT::nxv16f32, RTLIB::FLOG2_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFLOG10(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::FLOG10_NXV1F64}, {MVT::nxv2f64, RTLIB::FLOG10_NXV2F64},
      {MVT::nxv4f64, RTLIB::FLOG10_NXV4F64}, {MVT::nxv8f64, RTLIB::FLOG10_NXV8F64},
      {MVT::nxv2f32, RTLIB::FLOG10_NXV2F32}, {MVT::nxv4f32, RTLIB::FLOG10_NXV4F32},
      {MVT::nxv8f32, RTLIB::FLOG10_NXV8F32}, {MVT::nxv16f32, RTLIB::FLOG10_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFPOW(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::POW_NXV1F64},
      {MVT::nxv2f64, RTLIB::POW_NXV2F64},
      {MVT::nxv4f64, RTLIB::POW_NXV4F64},
      {MVT::nxv8f64, RTLIB::POW_NXV8F64},
      {MVT::nxv1f32, RTLIB::POW_NXV1F32},
      {MVT::nxv2f32, RTLIB::POW_NXV2F32},
      {MVT::nxv4f32, RTLIB::POW_NXV4F32},
      {MVT::nxv8f32, RTLIB::POW_NXV8F32},
      {MVT::nxv16f32, RTLIB::POW_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue RISCVTargetLowering::lowerFREM(SDValue Op, SelectionDAG &DAG) const {
  VTToLibCall VTToLC[] = {
      {MVT::nxv1f64, RTLIB::REM_NXV1F64}, {MVT::nxv2f64, RTLIB::REM_NXV2F64},
      {MVT::nxv4f64, RTLIB::REM_NXV4F64}, {MVT::nxv8f64, RTLIB::REM_NXV8F64},
      {MVT::nxv2f32, RTLIB::REM_NXV2F32}, {MVT::nxv4f32, RTLIB::REM_NXV4F32},
      {MVT::nxv8f32, RTLIB::REM_NXV8F32}, {MVT::nxv16f32, RTLIB::REM_NXV16F32},
  };
  return lowerVECLIBCALL(Op, DAG, VTToLC);
}

SDValue
RISCVTargetLowering::lowerVectorMaskVecReduction(SDValue Op,
                                                 SelectionDAG &DAG) const {
  SDLoc DL(Op);
  unsigned OpCode;
  MVT XLenVT = Subtarget.getXLenVT();

  switch (Op.getOpcode()) {
  default:
    llvm_unreachable("Unhandled reduction");
  case ISD::VECREDUCE_AND:
    OpCode = RISCVISD::REDUCE_AND_MASK;
    break;
  case ISD::VECREDUCE_OR:
    OpCode = RISCVISD::REDUCE_OR_MASK;
    break;
  case ISD::VECREDUCE_XOR:
    OpCode = RISCVISD::REDUCE_XOR_MASK;
    break;
  }

  return DAG.getNode(OpCode, DL, XLenVT, Op.getOperand(0));
}

static SDValue lowerSETCC(SDValue Op, SelectionDAG &DAG) {

  SDLoc DL(Op);
  MVT VT = Op.getSimpleValueType();
  assert(VT.isScalableVector() && VT.getVectorElementType() == MVT::i1);
  SDValue V1 = Op.getOperand(0);
  SDValue V2 = Op.getOperand(1);
  MVT InputVT = V1.getSimpleValueType();
  assert(InputVT.isScalableVector() && InputVT.isFloatingPoint());
  assert(InputVT == V2.getSimpleValueType());

  SDValue VLMAX = DAG.getRegister(RISCV::X0, MVT::i64);
  ISD::CondCode CC = cast<CondCodeSDNode>(Op.getOperand(2))->get();
  switch (CC) {
  case ISD::CondCode::SETOEQ:
  case ISD::CondCode::SETOGT:
  case ISD::CondCode::SETOGE:
  case ISD::CondCode::SETOLT:
  case ISD::CondCode::SETOLE:
  case ISD::CondCode::SETO:
  case ISD::CondCode::SETUNE:
    // These condition codes are already handled with patterns
    return Op;
  case ISD::CondCode::SETFALSE:
  case ISD::CondCode::SETFALSE2:
    return DAG.getNode(ISD::SPLAT_VECTOR, DL, VT,
                       DAG.getTargetConstant(0, DL, MVT::i64));
  case ISD::CondCode::SETTRUE:
  case ISD::CondCode::SETTRUE2:
    return DAG.getNode(ISD::SPLAT_VECTOR, DL, VT,
                       DAG.getTargetConstant(1, DL, MVT::i64));
  case ISD::CondCode::SETONE: {
    SDValue SetOrd =
      DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                  {DAG.getTargetConstant(Intrinsic::epi_vmford, DL, MVT::i64),
                   V1, V2, VLMAX});
    return DAG.getNode(
        ISD::INTRINSIC_WO_CHAIN, DL, VT,
        {DAG.getTargetConstant(Intrinsic::epi_vmfne_mask, DL, MVT::i64),
         DAG.getNode(ISD::UNDEF, DL, VT), // Merge
         V1, V2, SetOrd, VLMAX});
  }
  default:
    break;
  }

  // Common code
  SDValue SetOrd =
      DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                  {DAG.getTargetConstant(Intrinsic::epi_vmford, DL, MVT::i64),
                   V1, V2, VLMAX});
  SDValue NotSetOrd =
      DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                  {DAG.getTargetConstant(Intrinsic::epi_vmnand, DL, MVT::i64),
                   SetOrd, SetOrd, VLMAX});

  switch (CC) {
  default:
    llvm_unreachable("Invalid CondCode for comparison between floats");
  case ISD::CondCode::SETUEQ: {
    SDValue Cmp = DAG.getNode(
        ISD::INTRINSIC_WO_CHAIN, DL, VT,
        {DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64), V1, V2, VLMAX});
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                       {DAG.getTargetConstant(Intrinsic::epi_vmor, DL, MVT::i64),
                        NotSetOrd, Cmp, VLMAX});
  }
  case ISD::CondCode::SETUGT: {
    unsigned EPIIntNo = Intrinsic::not_intrinsic;
    if (V2.getOpcode() == ISD::SPLAT_VECTOR) {
      EPIIntNo = Intrinsic::epi_vmfgt;
    } else {
      EPIIntNo = Intrinsic::epi_vmflt;
      std::swap(V1, V2);
    }
    SDValue Cmp =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                    {DAG.getTargetConstant(EPIIntNo, DL, MVT::i64), V1, V2, VLMAX});
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                       {DAG.getTargetConstant(Intrinsic::epi_vmor, DL, MVT::i64),
                        NotSetOrd, Cmp, VLMAX});
  }
  case ISD::CondCode::SETUGE: {
    unsigned EPIIntNo = Intrinsic::not_intrinsic;
    if (V2.getOpcode() == ISD::SPLAT_VECTOR) {
      EPIIntNo = Intrinsic::epi_vmfge;
    } else {
      EPIIntNo = Intrinsic::epi_vmfle;
      std::swap(V1, V2);
    }
    SDValue Cmp =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                    {DAG.getTargetConstant(EPIIntNo, DL, MVT::i64), V1, V2, VLMAX});
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                       {DAG.getTargetConstant(Intrinsic::epi_vmor, DL, MVT::i64),
                        NotSetOrd, Cmp, VLMAX});
  }
  case ISD::CondCode::SETULT: {
    unsigned EPIIntNo = Intrinsic::not_intrinsic;
    if (V1.getOpcode() == ISD::SPLAT_VECTOR) {
      EPIIntNo = Intrinsic::epi_vmfgt;
      std::swap(V1, V2);
    } else {
      EPIIntNo = Intrinsic::epi_vmflt;
    }
    SDValue Cmp =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                    {DAG.getTargetConstant(EPIIntNo, DL, MVT::i64), V1, V2, VLMAX});
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                       {DAG.getTargetConstant(Intrinsic::epi_vmor, DL, MVT::i64),
                        NotSetOrd, Cmp, VLMAX});
  }
  case ISD::CondCode::SETULE: {
    unsigned EPIIntNo = Intrinsic::not_intrinsic;
    if (V1.getOpcode() == ISD::SPLAT_VECTOR) {
      EPIIntNo = Intrinsic::epi_vmfge;
      std::swap(V1, V2);
    } else {
      EPIIntNo = Intrinsic::epi_vmfle;
    }
    SDValue Cmp =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                    {DAG.getTargetConstant(EPIIntNo, DL, MVT::i64), V1, V2, VLMAX});
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT,
                       {DAG.getTargetConstant(Intrinsic::epi_vmor, DL, MVT::i64),
                        NotSetOrd, Cmp, VLMAX});
  }
  case ISD::CondCode::SETUO:
    return NotSetOrd;
  }
}

static SDValue lowerFCOPYSIGN(SDValue Op, SelectionDAG &DAG) {
  assert(Op.getSimpleValueType().isScalableVector());

  SDLoc DL(Op);
  MVT VT = Op.getSimpleValueType();
  SDValue Mag = Op.getOperand(0);
  SDValue Sign = Op.getOperand(1);
  assert(Mag.getValueType() == Sign.getValueType() &&
         "Can only handle COPYSIGN with matching types.");

  SDValue VFSGNJOps[] = {
      DAG.getTargetConstant(Intrinsic::epi_vfsgnj, DL, MVT::i64), Mag, Sign,
      DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX
  };

  return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, VFSGNJOps);
}

static SDValue lowerVSELECT(SDValue Op, SelectionDAG &DAG) {
  assert(Op.getSimpleValueType().isScalableVector());

  SDLoc DL(Op);
  MVT VT = Op.getSimpleValueType();
  unsigned IntNo =
      VT.isFloatingPoint() ? Intrinsic::epi_vfmerge : Intrinsic::epi_vmerge;

  SDValue TrueOp = Op.getOperand(1);
  if (TrueOp.getOpcode() == ISD::SPLAT_VECTOR) {
    TrueOp = TrueOp.getOperand(0);
  }

  SDValue VMERGEOps[] = {
    DAG.getTargetConstant(IntNo, DL, MVT::i64),
    Op.getOperand(2),                    // False
    TrueOp,
    Op.getOperand(0),                    // Mask
    DAG.getRegister(RISCV::X0, MVT::i64) // VLMAX
  };

  return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, VMERGEOps);
}

// Expand vector FTRUNC, FCEIL, and FFLOOR by converting to the integer domain
// and back. Taking care to avoid converting values that are nan or already
// correct.
// TODO: Floor and ceil could be shorter by changing rounding mode, but we don't
// have FRM dependencies modeled yet.
static SDValue lowerFTRUNC_FCEIL_FFLOOR(SDValue Op, SelectionDAG &DAG) {
  MVT VT = Op.getSimpleValueType();
  assert(VT.isVector() && "Unexpected type");

  SDLoc DL(Op);

  // Freeze the source since we are increasing the number of uses.
  SDValue Src = DAG.getNode(ISD::FREEZE, DL, VT, Op.getOperand(0));

  // Truncate to integer and convert back to FP.
  MVT IntVT = VT.changeVectorElementTypeToInteger();
  SDValue Truncated = DAG.getNode(ISD::FP_TO_SINT, DL, IntVT, Src);
  Truncated = DAG.getNode(ISD::SINT_TO_FP, DL, VT, Truncated);

  MVT SetccVT = MVT::getVectorVT(MVT::i1, VT.getVectorElementCount());

  if (Op.getOpcode() == ISD::FCEIL) {
    // If the truncated value is the greater than or equal to the original
    // value, we've computed the ceil. Otherwise, we went the wrong way and
    // need to increase by 1.
    // FIXME: This should use a masked operation. Handle here or in isel?
    SDValue Adjust = DAG.getNode(ISD::FADD, DL, VT, Truncated,
                                 DAG.getConstantFP(1.0, DL, VT));
    SDValue NeedAdjust = DAG.getSetCC(DL, SetccVT, Truncated, Src, ISD::SETOLT);
    Truncated = DAG.getSelect(DL, VT, NeedAdjust, Adjust, Truncated);
  } else if (Op.getOpcode() == ISD::FFLOOR) {
    // If the truncated value is the less than or equal to the original value,
    // we've computed the floor. Otherwise, we went the wrong way and need to
    // decrease by 1.
		// FIXME: This should use a masked operation. Handle here or in isel?
    SDValue Adjust = DAG.getNode(ISD::FSUB, DL, VT, Truncated,
                                 DAG.getConstantFP(1.0, DL, VT));
    SDValue NeedAdjust = DAG.getSetCC(DL, SetccVT, Truncated, Src, ISD::SETOGT);
    Truncated = DAG.getSelect(DL, VT, NeedAdjust, Adjust, Truncated);
  }

  // Restore the original sign so that -0.0 is preserved.
  Truncated = DAG.getNode(ISD::FCOPYSIGN, DL, VT, Truncated, Src);

  // Determine the largest integer that can be represented exactly. This and
  // values larger than it don't have any fractional bits so don't need to
  // be converted.
  const fltSemantics &FltSem = DAG.EVTToAPFloatSemantics(VT);
  unsigned Precision = APFloat::semanticsPrecision(FltSem);
  APFloat MaxVal = APFloat(FltSem);
  MaxVal.convertFromAPInt(APInt::getOneBitSet(Precision, Precision - 1),
                          /*IsSigned*/ false, APFloat::rmNearestTiesToEven);
  SDValue MaxValNode = DAG.getConstantFP(MaxVal, DL, VT);

  // If abs(Src) was larger than MaxVal or nan, keep it.
  SDValue Abs = DAG.getNode(ISD::FABS, DL, VT, Src);
  SDValue Setcc = DAG.getSetCC(DL, SetccVT, Abs, MaxValNode, ISD::SETOLT);
	return DAG.getSelect(DL, VT, Setcc, Truncated, Src);
}

SDValue RISCVTargetLowering::lowerCONCAT_VECTORS(SDValue Op,
                                                 SelectionDAG &DAG) const {
  assert(Op->getNumOperands() == 2 && "Unexpected number of operands");

  SDValue Head = Op->getOperand(0);
  SDValue Tail = Op->getOperand(1);
  unsigned Idx;

  SDLoc DL(Op);
  EVT VT = Op.getValueType();
  assert(Tail.getOpcode() == ISD::EXTRACT_SUBVECTOR && "Unexpected node");
  Idx = cast<ConstantSDNode>(Tail->getOperand(1))->getZExtValue();
  assert(Idx == 0 && "Unexpected index");
  Tail = Tail->getOperand(0);

  assert(Head.getOpcode() == ISD::EXTRACT_SUBVECTOR && "Unexpected node");
  Idx = cast<ConstantSDNode>(Head->getOperand(1))->getZExtValue();
  assert(Idx == 0 && "Unexpected index");
  Head = Head->getOperand(0);

  SDValue Offset = DAG.getVScale(
      DL, MVT::i64,
      APInt(64, Op->getOperand(0).getValueType().getVectorMinNumElements()));

  SDValue VLMax = DAG.getVScale(
      DL, MVT::i64,
      APInt(64, Head.getValueType().getVectorMinNumElements()));

  SDValue SEW = DAG.getTargetConstant(
      Head.getValueType().getVectorElementType().getScalarSizeInBits(), DL,
      MVT::i64);

  // FIXME: We use a machine node here because we want to exploit the merging
  // feature which is not accesible via intrinsic nodes at this level.
  SDValue Result = SDValue(
      DAG.getMachineNode(RISCV::PseudoVSLIDEUP_VX_M1, DL, VT,
                         {Head, Tail, Offset,
                          DAG.getRegister(RISCV::NoRegister,
                                          VT.changeVectorElementType(MVT::i1)),
                          VLMax, SEW}),
      0);

  return Result;
}

SDValue RISCVTargetLowering::lowerLOAD(SDValue Op, SelectionDAG &DAG) const {
  return Op;
}

SDValue RISCVTargetLowering::lowerSTORE(SDValue Op, SelectionDAG &DAG) const {
  SDLoc DL(Op);
  auto *StoreNode = cast<StoreSDNode>(Op.getNode());

  SDValue OpFp32 = DAG.getNode(ISD::BF16_TO_FP, DL, MVT::f32, StoreNode->getValue());
  SDValue OpBitcasted = DAG.getNode(ISD::BITCAST, DL, MVT::i32, OpFp32);
  SDValue OpTruncated = DAG.getNode(ISD::TRUNCATE, DL, MVT::i16, OpBitcasted);
  
  return DAG.getStore(Op.getOperand(0), DL, OpTruncated, StoreNode->getBasePtr(), StoreNode->getMemOperand());
}

SDValue RISCVTargetLowering::lowerBF16_TO_FP(SDValue Op, SelectionDAG &DAG) const {
  SDValue Operand = Op.getOperand(0);
  assert(Operand->getValueType(0) == MVT::i64 && "lowerBF16_TO_FP received something different than i64");
  SDLoc DL(Op);

  SDValue Sixteen = DAG.getConstant(16, DL, MVT::i64);

  SDValue Shifted = DAG.getNode(ISD::SHL, DL, MVT::i64, Operand, Sixteen);
  SDValue Result = DAG.getNode(RISCVISD::FMV_W_X_RV64, DL, MVT::f32, Shifted);

  return Result;
}

SDValue RISCVTargetLowering::LowerOperation(SDValue Op,
                                            SelectionDAG &DAG) const {
  switch (Op.getOpcode()) {
  default:
    report_fatal_error("unimplemented operand");
  case ISD::GlobalAddress:
    return lowerGlobalAddress(Op, DAG);
  case ISD::BlockAddress:
    return lowerBlockAddress(Op, DAG);
  case ISD::ConstantPool:
    return lowerConstantPool(Op, DAG);
  case ISD::GlobalTLSAddress:
    return lowerGlobalTLSAddress(Op, DAG);
  case ISD::SETCC:
    return lowerSETCC(Op, DAG);
  case ISD::SELECT:
    return lowerSELECT(Op, DAG);
  case ISD::VSELECT:
    return lowerVSELECT(Op, DAG);
  case ISD::VASTART:
    return lowerVASTART(Op, DAG);
  case ISD::FRAMEADDR:
    return lowerFRAMEADDR(Op, DAG);
  case ISD::RETURNADDR:
    return lowerRETURNADDR(Op, DAG);
  case ISD::SHL_PARTS:
    return lowerShiftLeftParts(Op, DAG);
  case ISD::SRA_PARTS:
    return lowerShiftRightParts(Op, DAG, true);
  case ISD::SRL_PARTS:
    return lowerShiftRightParts(Op, DAG, false);
  case ISD::BITCAST: {
        //assert(Subtarget.is64Bit() && Subtarget.hasStdExtF() &&
        //   "Unexpected custom legalisation");
    SDLoc DL(Op);
    SDValue Op0 = Op.getOperand(0);
    if (Op.getValueType() == MVT::f32 && Op0.getValueType() == MVT::i32){
      SDValue NewOp0 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, Op0);
      SDValue FPConv = DAG.getNode(RISCVISD::FMV_W_X_RV64, DL, MVT::f32, NewOp0);
      return FPConv;
    } else {
      return SDValue();
    }
  }
  case ISD::INTRINSIC_WO_CHAIN:
    return LowerINTRINSIC_WO_CHAIN(Op, DAG);
  case ISD::INTRINSIC_W_CHAIN:
    return LowerINTRINSIC_W_CHAIN(Op, DAG);
  case ISD::INTRINSIC_VOID:
    return LowerINTRINSIC_VOID(Op, DAG);
  case ISD::VECTOR_SHUFFLE:
    return lowerVECTOR_SHUFFLE(Op, DAG);
  case ISD::SIGN_EXTEND:
    return lowerSIGN_EXTEND(Op, DAG);
  case ISD::ZERO_EXTEND:
    return lowerZERO_EXTEND(Op, DAG);
  case ISD::TRUNCATE:
    return lowerTRUNCATE(Op, DAG);
  case ISD::SIGN_EXTEND_VECTOR_INREG:
    return lowerSIGN_EXTEND_VECTOR_INREG(Op, DAG);
  case ISD::ZERO_EXTEND_VECTOR_INREG:
    return lowerZERO_EXTEND_VECTOR_INREG(Op, DAG);
  case ISD::MGATHER:
    return lowerMGATHER(Op, DAG);
  case ISD::MSCATTER:
    return lowerMSCATTER(Op, DAG);
  case ISD::EXTRACT_VECTOR_ELT:
    return lowerEXTRACT_VECTOR_ELT(Op, DAG);
  case ISD::INSERT_VECTOR_ELT:
    return lowerINSERT_VECTOR_ELT(Op, DAG);
  case ISD::FEXP:
    return lowerFEXP(Op, DAG);
  case ISD::FEXP2:
    return lowerFEXP2(Op, DAG);
  case ISD::FSIN:
    return lowerFSIN(Op, DAG);
  case ISD::FCOS:
    return lowerFCOS(Op, DAG);
  case ISD::FLOG:
    return lowerFLOG(Op, DAG);
  case ISD::FLOG2:
    return lowerFLOG2(Op, DAG);
  case ISD::FLOG10:
    return lowerFLOG10(Op, DAG);
  case ISD::FREM:
    return lowerFREM(Op, DAG);
  case ISD::FPOW:
    return lowerFPOW(Op, DAG);
  case ISD::VECREDUCE_AND:
  case ISD::VECREDUCE_OR:
  case ISD::VECREDUCE_XOR:
    assert(Op.getOperand(0).getValueType().getVectorElementType() == MVT::i1 &&
           "Unexpected reduction");
    return lowerVectorMaskVecReduction(Op, DAG);
  case ISD::FTRUNC:
  case ISD::FCEIL:
  case ISD::FFLOOR:
    return lowerFTRUNC_FCEIL_FFLOOR(Op, DAG);
  case ISD::FCOPYSIGN:
    return lowerFCOPYSIGN(Op, DAG);
  case ISD::CONCAT_VECTORS:
    return lowerCONCAT_VECTORS(Op, DAG);
  case ISD::LOAD:
    return lowerLOAD(Op, DAG);
  case ISD::STORE:
    return lowerSTORE(Op, DAG);
  case ISD::BF16_TO_FP:
    return lowerBF16_TO_FP(Op, DAG);
  }
}

static SDValue getTargetNode(GlobalAddressSDNode *N, SDLoc DL, EVT Ty,
                             SelectionDAG &DAG, unsigned Flags) {
  return DAG.getTargetGlobalAddress(N->getGlobal(), DL, Ty, 0, Flags);
}

static SDValue getTargetNode(BlockAddressSDNode *N, SDLoc DL, EVT Ty,
                             SelectionDAG &DAG, unsigned Flags) {
  return DAG.getTargetBlockAddress(N->getBlockAddress(), Ty, N->getOffset(),
                                   Flags);
}

static SDValue getTargetNode(ConstantPoolSDNode *N, SDLoc DL, EVT Ty,
                             SelectionDAG &DAG, unsigned Flags) {
  return DAG.getTargetConstantPool(N->getConstVal(), Ty, N->getAlign(),
                                   N->getOffset(), Flags);
}

template <class NodeTy>
SDValue RISCVTargetLowering::getAddr(NodeTy *N, SelectionDAG &DAG,
                                     bool IsLocal) const {
  SDLoc DL(N);
  EVT Ty = getPointerTy(DAG.getDataLayout());

  if (isPositionIndependent()) {
    SDValue Addr = getTargetNode(N, DL, Ty, DAG, 0);
    if (IsLocal)
      // Use PC-relative addressing to access the symbol. This generates the
      // pattern (PseudoLLA sym), which expands to (addi (auipc %pcrel_hi(sym))
      // %pcrel_lo(auipc)).
      return SDValue(DAG.getMachineNode(RISCV::PseudoLLA, DL, Ty, Addr), 0);

    // Use PC-relative addressing to access the GOT for this symbol, then load
    // the address from the GOT. This generates the pattern (PseudoLA sym),
    // which expands to (ld (addi (auipc %got_pcrel_hi(sym)) %pcrel_lo(auipc))).
    return SDValue(DAG.getMachineNode(RISCV::PseudoLA, DL, Ty, Addr), 0);
  }

  switch (getTargetMachine().getCodeModel()) {
  default:
    report_fatal_error("Unsupported code model for lowering");
  case CodeModel::Small: {
    // Generate a sequence for accessing addresses within the first 2 GiB of
    // address space. This generates the pattern (addi (lui %hi(sym)) %lo(sym)).
    SDValue AddrHi = getTargetNode(N, DL, Ty, DAG, RISCVII::MO_HI);
    SDValue AddrLo = getTargetNode(N, DL, Ty, DAG, RISCVII::MO_LO);
    SDValue MNHi = SDValue(DAG.getMachineNode(RISCV::LUI, DL, Ty, AddrHi), 0);
    return SDValue(DAG.getMachineNode(RISCV::ADDI, DL, Ty, MNHi, AddrLo), 0);
  }
  case CodeModel::Medium: {
    // Generate a sequence for accessing addresses within any 2GiB range within
    // the address space. This generates the pattern (PseudoLLA sym), which
    // expands to (addi (auipc %pcrel_hi(sym)) %pcrel_lo(auipc)).
    SDValue Addr = getTargetNode(N, DL, Ty, DAG, 0);
    return SDValue(DAG.getMachineNode(RISCV::PseudoLLA, DL, Ty, Addr), 0);
  }
  }
}

SDValue RISCVTargetLowering::lowerGlobalAddress(SDValue Op,
                                                SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT Ty = Op.getValueType();
  GlobalAddressSDNode *N = cast<GlobalAddressSDNode>(Op);
  int64_t Offset = N->getOffset();
  MVT XLenVT = Subtarget.getXLenVT();

  const GlobalValue *GV = N->getGlobal();
  bool IsLocal = getTargetMachine().shouldAssumeDSOLocal(*GV->getParent(), GV);
  SDValue Addr = getAddr(N, DAG, IsLocal);

  // In order to maximise the opportunity for common subexpression elimination,
  // emit a separate ADD node for the global address offset instead of folding
  // it in the global address node. Later peephole optimisations may choose to
  // fold it back in when profitable.
  if (Offset != 0)
    return DAG.getNode(ISD::ADD, DL, Ty, Addr,
                       DAG.getConstant(Offset, DL, XLenVT));
  return Addr;
}

SDValue RISCVTargetLowering::lowerBlockAddress(SDValue Op,
                                               SelectionDAG &DAG) const {
  BlockAddressSDNode *N = cast<BlockAddressSDNode>(Op);

  return getAddr(N, DAG);
}

SDValue RISCVTargetLowering::lowerConstantPool(SDValue Op,
                                               SelectionDAG &DAG) const {
  ConstantPoolSDNode *N = cast<ConstantPoolSDNode>(Op);

  return getAddr(N, DAG);
}

SDValue RISCVTargetLowering::getStaticTLSAddr(GlobalAddressSDNode *N,
                                              SelectionDAG &DAG,
                                              bool UseGOT) const {
  SDLoc DL(N);
  EVT Ty = getPointerTy(DAG.getDataLayout());
  const GlobalValue *GV = N->getGlobal();
  MVT XLenVT = Subtarget.getXLenVT();

  if (UseGOT) {
    // Use PC-relative addressing to access the GOT for this TLS symbol, then
    // load the address from the GOT and add the thread pointer. This generates
    // the pattern (PseudoLA_TLS_IE sym), which expands to
    // (ld (auipc %tls_ie_pcrel_hi(sym)) %pcrel_lo(auipc)).
    SDValue Addr = DAG.getTargetGlobalAddress(GV, DL, Ty, 0, 0);
    SDValue Load =
        SDValue(DAG.getMachineNode(RISCV::PseudoLA_TLS_IE, DL, Ty, Addr), 0);

    // Add the thread pointer.
    SDValue TPReg = DAG.getRegister(RISCV::X4, XLenVT);
    return DAG.getNode(ISD::ADD, DL, Ty, Load, TPReg);
  }

  // Generate a sequence for accessing the address relative to the thread
  // pointer, with the appropriate adjustment for the thread pointer offset.
  // This generates the pattern
  // (add (add_tprel (lui %tprel_hi(sym)) tp %tprel_add(sym)) %tprel_lo(sym))
  SDValue AddrHi =
      DAG.getTargetGlobalAddress(GV, DL, Ty, 0, RISCVII::MO_TPREL_HI);
  SDValue AddrAdd =
      DAG.getTargetGlobalAddress(GV, DL, Ty, 0, RISCVII::MO_TPREL_ADD);
  SDValue AddrLo =
      DAG.getTargetGlobalAddress(GV, DL, Ty, 0, RISCVII::MO_TPREL_LO);

  SDValue MNHi = SDValue(DAG.getMachineNode(RISCV::LUI, DL, Ty, AddrHi), 0);
  SDValue TPReg = DAG.getRegister(RISCV::X4, XLenVT);
  SDValue MNAdd = SDValue(
      DAG.getMachineNode(RISCV::PseudoAddTPRel, DL, Ty, MNHi, TPReg, AddrAdd),
      0);
  return SDValue(DAG.getMachineNode(RISCV::ADDI, DL, Ty, MNAdd, AddrLo), 0);
}

SDValue RISCVTargetLowering::getDynamicTLSAddr(GlobalAddressSDNode *N,
                                               SelectionDAG &DAG) const {
  SDLoc DL(N);
  EVT Ty = getPointerTy(DAG.getDataLayout());
  IntegerType *CallTy = Type::getIntNTy(*DAG.getContext(), Ty.getSizeInBits());
  const GlobalValue *GV = N->getGlobal();

  // Use a PC-relative addressing mode to access the global dynamic GOT address.
  // This generates the pattern (PseudoLA_TLS_GD sym), which expands to
  // (addi (auipc %tls_gd_pcrel_hi(sym)) %pcrel_lo(auipc)).
  SDValue Addr = DAG.getTargetGlobalAddress(GV, DL, Ty, 0, 0);
  SDValue Load =
      SDValue(DAG.getMachineNode(RISCV::PseudoLA_TLS_GD, DL, Ty, Addr), 0);

  // Prepare argument list to generate call.
  ArgListTy Args;
  ArgListEntry Entry;
  Entry.Node = Load;
  Entry.Ty = CallTy;
  Args.push_back(Entry);

  // Setup call to __tls_get_addr.
  TargetLowering::CallLoweringInfo CLI(DAG);
  CLI.setDebugLoc(DL)
      .setChain(DAG.getEntryNode())
      .setLibCallee(CallingConv::C, CallTy,
                    DAG.getExternalSymbol("__tls_get_addr", Ty),
                    std::move(Args));

  return LowerCallTo(CLI).first;
}

SDValue RISCVTargetLowering::lowerGlobalTLSAddress(SDValue Op,
                                                   SelectionDAG &DAG) const {
  SDLoc DL(Op);
  EVT Ty = Op.getValueType();
  GlobalAddressSDNode *N = cast<GlobalAddressSDNode>(Op);
  int64_t Offset = N->getOffset();
  MVT XLenVT = Subtarget.getXLenVT();

  TLSModel::Model Model = getTargetMachine().getTLSModel(N->getGlobal());

  if (DAG.getMachineFunction().getFunction().getCallingConv() ==
      CallingConv::GHC)
    report_fatal_error("In GHC calling convention TLS is not supported");

  SDValue Addr;
  switch (Model) {
  case TLSModel::LocalExec:
    Addr = getStaticTLSAddr(N, DAG, /*UseGOT=*/false);
    break;
  case TLSModel::InitialExec:
    Addr = getStaticTLSAddr(N, DAG, /*UseGOT=*/true);
    break;
  case TLSModel::LocalDynamic:
  case TLSModel::GeneralDynamic:
    Addr = getDynamicTLSAddr(N, DAG);
    break;
  }

  // In order to maximise the opportunity for common subexpression elimination,
  // emit a separate ADD node for the global address offset instead of folding
  // it in the global address node. Later peephole optimisations may choose to
  // fold it back in when profitable.
  if (Offset != 0)
    return DAG.getNode(ISD::ADD, DL, Ty, Addr,
                       DAG.getConstant(Offset, DL, XLenVT));
  return Addr;
}

SDValue RISCVTargetLowering::lowerSELECT(SDValue Op, SelectionDAG &DAG) const {
  SDValue CondV = Op.getOperand(0);
  SDValue TrueV = Op.getOperand(1);
  SDValue FalseV = Op.getOperand(2);
  SDLoc DL(Op);
  EVT VT = Op.getValueType();
  MVT XLenVT = Subtarget.getXLenVT();

  if (VT.isScalableVector()) {
    // FIXME ELEN=64 hardcoded.
    unsigned MLEN = 64 / VT.getVectorMinNumElements();

    if (MLEN < 8) {
      // E.g. (nxv64i1:copy (nxv8i8:splat (i8:sext (i1:trunc (i64:CondV)))))
      SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, MVT::i1, CondV);
      SDValue Sext = DAG.getNode(ISD::SIGN_EXTEND, DL, MVT::i8, Trunc);
      SDValue VecCondV = DAG.getNode(ISD::SPLAT_VECTOR, DL, MVT::nxv8i8, Sext);

      SDValue RC =
          DAG.getTargetConstant(RISCV::VRRegClass.getID(), DL, MVT::i64);
      SDValue CastVecCondV = SDValue(
          DAG.getMachineNode(TargetOpcode::COPY_TO_REGCLASS, DL,
                             VT.changeVectorElementType(MVT::i1), VecCondV, RC),
          0);
      return DAG.getNode(ISD::VSELECT, DL, VT, {CastVecCondV, TrueV, FalseV});
    }

    // MLEN >= 8. Note this is only valid in RVV-0.8. In RVV-0.9 MLEN=1.
    // E.g. (nxv2i1:trunc (nxv2i32:splat (i32:trunc (i64:CondV))))
    MVT EltVT = MVT::getIntegerVT(MLEN);
    SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, EltVT, CondV);
    SDValue VecCondV = DAG.getNode(ISD::SPLAT_VECTOR, DL,
                                   VT.changeVectorElementType(EltVT), Trunc);
    SDValue TruncVecCondV = DAG.getNode(
        ISD::TRUNCATE, DL, VT.changeVectorElementType(MVT::i1), VecCondV);
    return DAG.getNode(ISD::VSELECT, DL, VT, {TruncVecCondV, TrueV, FalseV});
  }

  // If the result type is XLenVT and CondV is the output of a SETCC node
  // which also operated on XLenVT inputs, then merge the SETCC node into the
  // lowered RISCVISD::SELECT_CC to take advantage of the integer
  // compare+branch instructions. i.e.:
  // (select (setcc lhs, rhs, cc), truev, falsev)
  // -> (riscvisd::select_cc lhs, rhs, cc, truev, falsev)
  if (VT.getSimpleVT() == XLenVT && CondV.getOpcode() == ISD::SETCC &&
      CondV.getOperand(0).getSimpleValueType() == XLenVT) {
    SDValue LHS = CondV.getOperand(0);
    SDValue RHS = CondV.getOperand(1);
    auto CC = cast<CondCodeSDNode>(CondV.getOperand(2));
    ISD::CondCode CCVal = CC->get();

    normaliseSetCC(LHS, RHS, CCVal);

    SDValue TargetCC = DAG.getConstant(CCVal, DL, XLenVT);
    SDValue Ops[] = {LHS, RHS, TargetCC, TrueV, FalseV};
    return DAG.getNode(RISCVISD::SELECT_CC, DL, Op.getValueType(), Ops);
  }

  // Otherwise:
  // (select condv, truev, falsev)
  // -> (riscvisd::select_cc condv, zero, setne, truev, falsev)
  SDValue Zero = DAG.getConstant(0, DL, XLenVT);
  SDValue SetNE = DAG.getConstant(ISD::SETNE, DL, XLenVT);

  SDValue Ops[] = {CondV, Zero, SetNE, TrueV, FalseV};

  return DAG.getNode(RISCVISD::SELECT_CC, DL, Op.getValueType(), Ops);
}

SDValue RISCVTargetLowering::lowerVASTART(SDValue Op, SelectionDAG &DAG) const {
  MachineFunction &MF = DAG.getMachineFunction();
  RISCVMachineFunctionInfo *FuncInfo = MF.getInfo<RISCVMachineFunctionInfo>();

  SDLoc DL(Op);
  SDValue FI = DAG.getFrameIndex(FuncInfo->getVarArgsFrameIndex(),
                                 getPointerTy(MF.getDataLayout()));

  // vastart just stores the address of the VarArgsFrameIndex slot into the
  // memory location argument.
  const Value *SV = cast<SrcValueSDNode>(Op.getOperand(2))->getValue();
  return DAG.getStore(Op.getOperand(0), DL, FI, Op.getOperand(1),
                      MachinePointerInfo(SV));
}

SDValue RISCVTargetLowering::lowerFRAMEADDR(SDValue Op,
                                            SelectionDAG &DAG) const {
  const RISCVRegisterInfo &RI = *Subtarget.getRegisterInfo();
  MachineFunction &MF = DAG.getMachineFunction();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  MFI.setFrameAddressIsTaken(true);
  Register FrameReg = RI.getFrameRegister(MF);
  int XLenInBytes = Subtarget.getXLen() / 8;

  EVT VT = Op.getValueType();
  SDLoc DL(Op);
  SDValue FrameAddr = DAG.getCopyFromReg(DAG.getEntryNode(), DL, FrameReg, VT);
  unsigned Depth = cast<ConstantSDNode>(Op.getOperand(0))->getZExtValue();
  while (Depth--) {
    int Offset = -(XLenInBytes * 2);
    SDValue Ptr = DAG.getNode(ISD::ADD, DL, VT, FrameAddr,
                              DAG.getIntPtrConstant(Offset, DL));
    FrameAddr =
        DAG.getLoad(VT, DL, DAG.getEntryNode(), Ptr, MachinePointerInfo());
  }
  return FrameAddr;
}

SDValue RISCVTargetLowering::lowerRETURNADDR(SDValue Op,
                                             SelectionDAG &DAG) const {
  const RISCVRegisterInfo &RI = *Subtarget.getRegisterInfo();
  MachineFunction &MF = DAG.getMachineFunction();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  MFI.setReturnAddressIsTaken(true);
  MVT XLenVT = Subtarget.getXLenVT();
  int XLenInBytes = Subtarget.getXLen() / 8;

  if (verifyReturnAddressArgumentIsConstant(Op, DAG))
    return SDValue();

  EVT VT = Op.getValueType();
  SDLoc DL(Op);
  unsigned Depth = cast<ConstantSDNode>(Op.getOperand(0))->getZExtValue();
  if (Depth) {
    int Off = -XLenInBytes;
    SDValue FrameAddr = lowerFRAMEADDR(Op, DAG);
    SDValue Offset = DAG.getConstant(Off, DL, VT);
    return DAG.getLoad(VT, DL, DAG.getEntryNode(),
                       DAG.getNode(ISD::ADD, DL, VT, FrameAddr, Offset),
                       MachinePointerInfo());
  }

  // Return the value of the return address register, marking it an implicit
  // live-in.
  Register Reg = MF.addLiveIn(RI.getRARegister(), getRegClassFor(XLenVT));
  return DAG.getCopyFromReg(DAG.getEntryNode(), DL, Reg, XLenVT);
}

SDValue RISCVTargetLowering::lowerShiftLeftParts(SDValue Op,
                                                 SelectionDAG &DAG) const {
  SDLoc DL(Op);
  SDValue Lo = Op.getOperand(0);
  SDValue Hi = Op.getOperand(1);
  SDValue Shamt = Op.getOperand(2);
  EVT VT = Lo.getValueType();

  // if Shamt-XLEN < 0: // Shamt < XLEN
  //   Lo = Lo << Shamt
  //   Hi = (Hi << Shamt) | ((Lo >>u 1) >>u (XLEN-1 - Shamt))
  // else:
  //   Lo = 0
  //   Hi = Lo << (Shamt-XLEN)

  SDValue Zero = DAG.getConstant(0, DL, VT);
  SDValue One = DAG.getConstant(1, DL, VT);
  SDValue MinusXLen = DAG.getConstant(-(int)Subtarget.getXLen(), DL, VT);
  SDValue XLenMinus1 = DAG.getConstant(Subtarget.getXLen() - 1, DL, VT);
  SDValue ShamtMinusXLen = DAG.getNode(ISD::ADD, DL, VT, Shamt, MinusXLen);
  SDValue XLenMinus1Shamt = DAG.getNode(ISD::SUB, DL, VT, XLenMinus1, Shamt);

  SDValue LoTrue = DAG.getNode(ISD::SHL, DL, VT, Lo, Shamt);
  SDValue ShiftRight1Lo = DAG.getNode(ISD::SRL, DL, VT, Lo, One);
  SDValue ShiftRightLo =
      DAG.getNode(ISD::SRL, DL, VT, ShiftRight1Lo, XLenMinus1Shamt);
  SDValue ShiftLeftHi = DAG.getNode(ISD::SHL, DL, VT, Hi, Shamt);
  SDValue HiTrue = DAG.getNode(ISD::OR, DL, VT, ShiftLeftHi, ShiftRightLo);
  SDValue HiFalse = DAG.getNode(ISD::SHL, DL, VT, Lo, ShamtMinusXLen);

  SDValue CC = DAG.getSetCC(DL, VT, ShamtMinusXLen, Zero, ISD::SETLT);

  Lo = DAG.getNode(ISD::SELECT, DL, VT, CC, LoTrue, Zero);
  Hi = DAG.getNode(ISD::SELECT, DL, VT, CC, HiTrue, HiFalse);

  SDValue Parts[2] = {Lo, Hi};
  return DAG.getMergeValues(Parts, DL);
}

SDValue RISCVTargetLowering::lowerShiftRightParts(SDValue Op, SelectionDAG &DAG,
                                                  bool IsSRA) const {
  SDLoc DL(Op);
  SDValue Lo = Op.getOperand(0);
  SDValue Hi = Op.getOperand(1);
  SDValue Shamt = Op.getOperand(2);
  EVT VT = Lo.getValueType();

  // SRA expansion:
  //   if Shamt-XLEN < 0: // Shamt < XLEN
  //     Lo = (Lo >>u Shamt) | ((Hi << 1) << (XLEN-1 - Shamt))
  //     Hi = Hi >>s Shamt
  //   else:
  //     Lo = Hi >>s (Shamt-XLEN);
  //     Hi = Hi >>s (XLEN-1)
  //
  // SRL expansion:
  //   if Shamt-XLEN < 0: // Shamt < XLEN
  //     Lo = (Lo >>u Shamt) | ((Hi << 1) << (XLEN-1 - Shamt))
  //     Hi = Hi >>u Shamt
  //   else:
  //     Lo = Hi >>u (Shamt-XLEN);
  //     Hi = 0;

  unsigned ShiftRightOp = IsSRA ? ISD::SRA : ISD::SRL;

  SDValue Zero = DAG.getConstant(0, DL, VT);
  SDValue One = DAG.getConstant(1, DL, VT);
  SDValue MinusXLen = DAG.getConstant(-(int)Subtarget.getXLen(), DL, VT);
  SDValue XLenMinus1 = DAG.getConstant(Subtarget.getXLen() - 1, DL, VT);
  SDValue ShamtMinusXLen = DAG.getNode(ISD::ADD, DL, VT, Shamt, MinusXLen);
  SDValue XLenMinus1Shamt = DAG.getNode(ISD::SUB, DL, VT, XLenMinus1, Shamt);

  SDValue ShiftRightLo = DAG.getNode(ISD::SRL, DL, VT, Lo, Shamt);
  SDValue ShiftLeftHi1 = DAG.getNode(ISD::SHL, DL, VT, Hi, One);
  SDValue ShiftLeftHi =
      DAG.getNode(ISD::SHL, DL, VT, ShiftLeftHi1, XLenMinus1Shamt);
  SDValue LoTrue = DAG.getNode(ISD::OR, DL, VT, ShiftRightLo, ShiftLeftHi);
  SDValue HiTrue = DAG.getNode(ShiftRightOp, DL, VT, Hi, Shamt);
  SDValue LoFalse = DAG.getNode(ShiftRightOp, DL, VT, Hi, ShamtMinusXLen);
  SDValue HiFalse =
      IsSRA ? DAG.getNode(ISD::SRA, DL, VT, Hi, XLenMinus1) : Zero;

  SDValue CC = DAG.getSetCC(DL, VT, ShamtMinusXLen, Zero, ISD::SETLT);

  Lo = DAG.getNode(ISD::SELECT, DL, VT, CC, LoTrue, LoFalse);
  Hi = DAG.getNode(ISD::SELECT, DL, VT, CC, HiTrue, HiFalse);

  SDValue Parts[2] = {Lo, Hi};
  return DAG.getMergeValues(Parts, DL);
}

static SDValue LowerVPUnorderedFCmp(unsigned EPIIntNo, const SDValue &Op1,
                                    const SDValue &Op2, const SDValue &EVL,
                                    EVT VT, SelectionDAG &DAG,
                                    const SDLoc &DL) {
  // Note: Implementing masked intrinsic as unmasked (this is correct given
  // that masked-off elements are undef and the operation is only applied
  // to ordered elements).
  //
  // %0 = vmfeq.vv %a, %a
  // %1 = vmfeq.vv %b, %b
  // %2 = vmand.mm %0, %1
  // v0 = %2
  // %3 = vm<fcmp>.vv %a, %b, v0.t
  // %result = vmornot.mm %3, %2

  assert(EVL.getValueType() == MVT::i32 && "Unexpected operand");
  SDValue AnyExtEVL = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, EVL);

  SDValue FeqOp1Operands[] = {
      DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64), Op1, Op1,
      AnyExtEVL
  };
  SDValue FeqOp2Operands[] = {
      DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64), Op2, Op2,
      AnyExtEVL
  };
  SDValue FeqOp1 = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, FeqOp1Operands);
  SDValue FeqOp2 = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, FeqOp2Operands);

  SDValue AndOperands[] = {
      DAG.getTargetConstant(Intrinsic::epi_vmand, DL, MVT::i64), FeqOp1, FeqOp2,
      AnyExtEVL
  };
  SDValue And = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, AndOperands);

  SDValue FCmpOperands[] = {
      DAG.getTargetConstant(EPIIntNo, DL, MVT::i64),
      DAG.getNode(ISD::UNDEF, DL, VT), // Merge.
      Op1, Op2, And, AnyExtEVL
  };
  SDValue FCmp = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, FCmpOperands);

  SDValue OrNotOperands[] = {
      DAG.getTargetConstant(Intrinsic::epi_vmornot, DL, MVT::i64), FCmp, And,
      AnyExtEVL
  };
  return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, VT, OrNotOperands);
}

static bool IsSplatOfOne(const SDValue &MaskOp) {
  ConstantSDNode *C;
  // Ideally we'd have folded those.
  if (MaskOp.getOpcode() == ISD::EXTRACT_SUBVECTOR)
    return IsSplatOfOne(MaskOp.getOperand(0));
  if (MaskOp.getOpcode() == ISD::INSERT_SUBVECTOR &&
      MaskOp.getOperand(0).isUndef())
    return IsSplatOfOne(MaskOp.getOperand(1));

  return MaskOp.getOpcode() == ISD::SPLAT_VECTOR &&
         (C = dyn_cast<ConstantSDNode>(MaskOp.getOperand(0))) &&
         C->getZExtValue() == 1;
}

static EVT ToMaskVT(EVT VectorVT, EVT MaskVT, SelectionDAG &DAG) {
  return EVT::getVectorVT(*DAG.getContext(), MaskVT.getVectorElementType(),
                          VectorVT.getVectorElementCount());
}

static SDValue GetExtractOrInsertSubvector(SDValue Op, EVT DestVT,
                                           SelectionDAG &DAG,
                                           const RISCVSubtarget &Subtarget) {
  SDLoc DL(Op);
  EVT OpVT = Op.getValueType();
  if (OpVT.getVectorMinNumElements() < DestVT.getVectorMinNumElements())
    return DAG.getNode(ISD::INSERT_SUBVECTOR, DL, DestVT,
                       DAG.getNode(ISD::UNDEF, DL, DestVT), Op,
                       DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
  else if (OpVT.getVectorMinNumElements() > DestVT.getVectorMinNumElements())
    return DAG.getNode(ISD::EXTRACT_SUBVECTOR, DL, DestVT, Op,
                       DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
  else
    return Op;
}

static SDValue AdjustMaskElements(EVT WidenVT, SDValue MaskOp,
                                  const RISCVSubtarget &Subtarget,
                                  SelectionDAG &DAG) {
  SDLoc DL(MaskOp);
  EVT MaskVT = MaskOp.getValueType();
  EVT WidenMaskVT = ToMaskVT(WidenVT, MaskVT, DAG);

  return GetExtractOrInsertSubvector(MaskOp, WidenMaskVT, DAG, Subtarget);
}

static SDValue LowerVPIntrinsicConversion(SDValue Op,
                                          const RISCVTargetLowering &Lowering,
                                          const RISCVSubtarget &Subtarget,
                                          SelectionDAG &DAG) {
  SDLoc DL(Op);
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(0))->getZExtValue();
  unsigned MaskOpNo = 2;
  bool IsMasked = !IsSplatOfOne(Op.getOperand(MaskOpNo));
  unsigned EVLOpNo = 3;
  assert(Op.getOperand(EVLOpNo).getValueType() == MVT::i32 &&
         "Unexpected operand");

  EVT DstType = Op.getValueType();
  uint64_t DstTypeSize = DstType.getScalarSizeInBits();
  SDValue SrcOp = Op.getOperand(1);
  EVT SrcType = SrcOp.getValueType();
  uint64_t SrcTypeSize = SrcType.getScalarSizeInBits();
  assert(isPowerOf2_64(DstTypeSize) && isPowerOf2_64(SrcTypeSize) &&
         "Types must be powers of two");
  int Ratio =
      std::max(DstTypeSize, SrcTypeSize) / std::min(DstTypeSize, SrcTypeSize);

  if (IntNo == Intrinsic::vp_ptrtoint || IntNo == Intrinsic::vp_inttoptr) {
    IntNo =
        DstTypeSize > SrcTypeSize ? Intrinsic::vp_zext : Intrinsic::vp_trunc;
  }

  // Before we continue, make sure the SrcOp has been widened, otherwise some
  // of the logic later around masks will break.
  EVT OrigSrcType = SrcType;
  if (Lowering.getPreferredVectorAction(SrcType.getSimpleVT()) ==
      RISCVTargetLowering::TypeWidenVector) {
    assert(SrcType.getVectorElementType() != MVT::i1 &&
           "Masks should not be widened");
    SrcType = Lowering.getTypeToTransformTo(*DAG.getContext(), SrcType);
    SrcOp = DAG.getNode(ISD::INSERT_SUBVECTOR, DL, SrcType,
                        DAG.getNode(ISD::UNDEF, DL, SrcType), SrcOp,
                        DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
  }

  // Handle VP conversions on masks
  if (Ratio > 1) {
    if (SrcType.getVectorElementType() == MVT::i1) {
      EVT ToType = DstType;
      uint64_t ExtValue = 0;
      unsigned CvtIntNo = Intrinsic::not_intrinsic;
      // Use same lowering as the llvm IR opcode
      switch (IntNo) {
      default:
        llvm_unreachable("SrcType == i1 not valid for this intrinsic");
      case Intrinsic::vp_sitofp:
        ToType = DstType.changeVectorElementTypeToInteger();
        CvtIntNo = Intrinsic::epi_vfcvt_f_x;
        LLVM_FALLTHROUGH;
      case Intrinsic::vp_sext:
        ExtValue = -1;
        break;
      case Intrinsic::vp_uitofp:
        ToType = DstType.changeVectorElementTypeToInteger();
        CvtIntNo = Intrinsic::epi_vfcvt_f_xu;
        LLVM_FALLTHROUGH;
      case Intrinsic::vp_zext:
        ExtValue = 1;
        break;
      }
      // NOTE: we do not handle the masked cases because they are not trivial
      // (and the unmasked cases generate a correct result anyway)
      SmallVector<SDValue, 3> VMVOps;
      VMVOps.push_back(DAG.getTargetConstant(Intrinsic::epi_vmv_v_x, DL, MVT::i64));
      VMVOps.push_back(DAG.getConstant(0, DL, MVT::i64));
      VMVOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                   Op.getOperand(EVLOpNo)));
      SDValue VMV = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, ToType, VMVOps);

      SmallVector<SDValue, 5> VMERGEOps;
      VMERGEOps.push_back(DAG.getTargetConstant(Intrinsic::epi_vmerge, DL, MVT::i64));
      VMERGEOps.push_back(VMV);
      VMERGEOps.push_back(DAG.getConstant(ExtValue, DL, MVT::i64));
      VMERGEOps.push_back(AdjustMaskElements(ToType, SrcOp, Subtarget, DAG));
      VMERGEOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                   Op.getOperand(EVLOpNo)));
      SDValue VMERGE = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, ToType, VMERGEOps);

      if (CvtIntNo == Intrinsic::not_intrinsic)
        return VMERGE;

      SmallVector<SDValue, 3> VFCVTOps;
      VFCVTOps.push_back(DAG.getTargetConstant(CvtIntNo, DL, MVT::i64));
      VFCVTOps.push_back(VMERGE);
      VFCVTOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                   Op.getOperand(EVLOpNo)));

      return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, DstType, VFCVTOps);
    }

    if (DstType.getVectorElementType() == MVT::i1) {
      switch (IntNo) {
      default:
        llvm_unreachable("DstType == i1 not valid for this intrinsic");
      case Intrinsic::vp_fptosi:
      case Intrinsic::vp_fptoui: {
        unsigned VMFNEIntNo = IsMasked ? Intrinsic::epi_vmfne_mask : Intrinsic::epi_vmfne;

        SmallVector<SDValue, 6> VMFNEOps;
        VMFNEOps.push_back(DAG.getTargetConstant(VMFNEIntNo, DL, MVT::i64));
        if (IsMasked)
          VMFNEOps.push_back(DAG.getNode(ISD::UNDEF, DL, DstType)); // Merge
        VMFNEOps.push_back(SrcOp);
        VMFNEOps.push_back(DAG.getConstantFP(0, DL, SrcType.getVectorElementType()));
        if (IsMasked)
          VMFNEOps.push_back(Op.getOperand(MaskOpNo)); // Mask.
        VMFNEOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                     Op.getOperand(EVLOpNo)));

        return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, DstType, VMFNEOps);
      }
      // Use the same lowering as the llvm IR opcode
      case Intrinsic::vp_trunc: {
        unsigned VANDIntNo = IsMasked ? Intrinsic::epi_vand_mask : Intrinsic::epi_vand;
        unsigned VMSNEIntNo = IsMasked ? Intrinsic::epi_vmsne_mask : Intrinsic::epi_vmsne;

        SmallVector<SDValue, 6> VANDOps;
        VANDOps.push_back(DAG.getTargetConstant(VANDIntNo, DL, MVT::i64));
        if (IsMasked)
          VANDOps.push_back(DAG.getNode(ISD::UNDEF, DL, SrcType)); // Merge
        VANDOps.push_back(SrcOp);
        VANDOps.push_back(DAG.getConstant(1, DL, MVT::i64));
        if (IsMasked)
          VANDOps.push_back(Op.getOperand(MaskOpNo)); // Mask.
        VANDOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                     Op.getOperand(EVLOpNo)));
        SDValue VAND = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, SrcType, VANDOps);

        SmallVector<SDValue, 6> VMSNEOps;
        VMSNEOps.push_back(DAG.getTargetConstant(VMSNEIntNo, DL, MVT::i64));
        if (IsMasked)
          VMSNEOps.push_back(DAG.getNode(ISD::UNDEF, DL, DstType)); // Merge
        VMSNEOps.push_back(VAND);
        VMSNEOps.push_back(DAG.getConstant(0, DL, MVT::i64));
        if (IsMasked)
          VMSNEOps.push_back(Op.getOperand(MaskOpNo)); // Mask.
        VMSNEOps.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                     Op.getOperand(EVLOpNo)));

        return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, DstType, VMSNEOps);
      }
      }
    }
  }

  // Second intrinsic is used to correctly widen/narrow the source operand
  unsigned EPIIntNo;
  unsigned FurtherEPIIntNo = Intrinsic::not_intrinsic;
  switch (IntNo) {
  default:
    llvm_unreachable("Unexpected intrinsic");
    break;
  case Intrinsic::vp_sitofp:
    if (Ratio == 1) {
      EPIIntNo =
          IsMasked ? Intrinsic::epi_vfcvt_f_x_mask : Intrinsic::epi_vfcvt_f_x;
    } else {
      if (DstTypeSize > SrcTypeSize) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfwcvt_f_x_mask
                            : Intrinsic::epi_vfwcvt_f_x;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vwadd_mask : Intrinsic::epi_vwadd;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfncvt_f_x_mask
                            : Intrinsic::epi_vfncvt_f_x;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vnsrl_mask : Intrinsic::epi_vnsrl;
      }
    }
    break;
  case Intrinsic::vp_uitofp:
    if (Ratio == 1) {
      EPIIntNo =
          IsMasked ? Intrinsic::epi_vfcvt_f_xu_mask : Intrinsic::epi_vfcvt_f_xu;
    } else {
      if (DstTypeSize > SrcTypeSize) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfwcvt_f_xu_mask
                            : Intrinsic::epi_vfwcvt_f_xu;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vwaddu_mask : Intrinsic::epi_vwaddu;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfncvt_f_xu_mask
                            : Intrinsic::epi_vfncvt_f_xu;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vnsrl_mask : Intrinsic::epi_vnsrl;
      }
    }
    break;
  case Intrinsic::vp_fptosi:
    if (Ratio == 1) {
      EPIIntNo =
          IsMasked ? Intrinsic::epi_vfcvt_x_f_mask : Intrinsic::epi_vfcvt_x_f;
    } else {
      if (DstTypeSize > SrcTypeSize) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfwcvt_x_f_mask
                            : Intrinsic::epi_vfwcvt_x_f;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vwadd_mask : Intrinsic::epi_vwadd;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfncvt_x_f_mask
                            : Intrinsic::epi_vfncvt_x_f;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vnsrl_mask : Intrinsic::epi_vnsrl;
      }
    }
    break;
  case Intrinsic::vp_fptoui:
    if (Ratio == 1) {
      EPIIntNo =
          IsMasked ? Intrinsic::epi_vfcvt_xu_f_mask : Intrinsic::epi_vfcvt_xu_f;
    } else {
      if (DstTypeSize > SrcTypeSize) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfwcvt_xu_f_mask
                            : Intrinsic::epi_vfwcvt_xu_f;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vwaddu_mask : Intrinsic::epi_vwaddu;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vfncvt_xu_f_mask
                            : Intrinsic::epi_vfncvt_xu_f;
        FurtherEPIIntNo =
            IsMasked ? Intrinsic::epi_vnsrl_mask : Intrinsic::epi_vnsrl;
      }
    }
    break;
  case Intrinsic::vp_sext:
    assert(DstTypeSize > SrcTypeSize && "vp_sext can be used only when widening");
    assert(Ratio > 1 && "Ratio must be bigger than 1 to invoke a widening");
    EPIIntNo = IsMasked ? Intrinsic::epi_vwadd_mask : Intrinsic::epi_vwadd;
    FurtherEPIIntNo = EPIIntNo;
    break;
  case Intrinsic::vp_zext:
    assert(DstTypeSize > SrcTypeSize && "vp_zext can be used only when widening");
    assert(Ratio > 1 && "Ratio must be bigger than 1 to invoke a widening");
    EPIIntNo = IsMasked ? Intrinsic::epi_vwaddu_mask : Intrinsic::epi_vwaddu;
    FurtherEPIIntNo = EPIIntNo;
    break;
  case Intrinsic::vp_trunc:
    assert(DstTypeSize < SrcTypeSize && "vp_trunc can be used only when narrowing");
    assert(Ratio > 1 && "Ratio must be bigger than 1 to invoke a narrowing");
    EPIIntNo = IsMasked ? Intrinsic::epi_vnsrl_mask : Intrinsic::epi_vnsrl;
    FurtherEPIIntNo = EPIIntNo;
    break;
  case Intrinsic::vp_fpext:
    assert(DstTypeSize > SrcTypeSize && "vp_fpext can be used only when widening");
    // NOTE: to be modified when adding support for f16
    assert(Ratio == 2 && "No widening possible for floats with ratio not equal to 2");
    EPIIntNo =
        IsMasked ? Intrinsic::epi_vfwcvt_f_f_mask : Intrinsic::epi_vfwcvt_f_f;
    // FurtherEPIIntNo not used since Ratio == 2
    break;
  case Intrinsic::vp_fptrunc:
    assert(DstTypeSize < SrcTypeSize && "vp_fptrunc can be used only when narrowing");
    // NOTE: to be modified when adding support for f16
    assert(Ratio == 2 && "No narrowing possible for floats with ratio not equal to 2");
    EPIIntNo =
        IsMasked ? Intrinsic::epi_vfncvt_f_f_mask : Intrinsic::epi_vfncvt_f_f;
    // FurtherEPIIntNo not used since Ratio == 2
    break;
  }

  SDValue FromOperand = SrcOp;
  // NOTE: widenIntegerVectorElementType can be used because we do not need to
  // expand floats with a Ratio > 2; when we add in EPI support for f16,
  // then we need to use another (custom) method
  // (like we already do for narrowing, see below)
  if (Ratio > 2 && DstTypeSize > SrcTypeSize) {
    // Invoke the FurtherEPIIntNo intrinsic to widen the source operand
    // as many times as needed, without changing the type
    EVT FromType = OrigSrcType;
    for (int i = 0; i < Ratio/4; i++) {
      EVT ToType = FromType.widenIntegerVectorElementType(*DAG.getContext());
      std::vector<SDValue> Operands;
      Operands.reserve(4 + IsMasked * 2);
      Operands.push_back(DAG.getTargetConstant(FurtherEPIIntNo, DL, MVT::i64));
      if (IsMasked)
        Operands.push_back(
            DAG.getNode(ISD::UNDEF, DL, ToType)); // Merge.
      Operands.push_back(FromOperand);
      Operands.push_back(DAG.getConstant(0, DL, MVT::i64));
      if (IsMasked) {
        SDValue MaskOp = Op.getOperand(MaskOpNo);
        if (Subtarget.onlyLMUL1()) {
          MaskOp = AdjustMaskElements(FromOperand.getValueType(), MaskOp,
                                      Subtarget, DAG);
        }
        Operands.push_back(MaskOp); // Mask.
      }
      Operands.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                     Op.getOperand(EVLOpNo))); // EVL.

      EVT ActualToType = ToType;
      if (Lowering.getPreferredVectorAction(ToType.getSimpleVT()) ==
          RISCVTargetLowering::TypeWidenVector) {
        ActualToType = Lowering.getTypeToTransformTo(*DAG.getContext(), ToType);
      }
      FromOperand = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, ActualToType, Operands);

      FromType = ToType;
    }
  }

  auto NarrowVectorElementType = [&](EVT SrcVT) -> EVT {
    EVT SrcEltVT = SrcVT.getVectorElementType();
    const ElementCount EltCount = SrcVT.getVectorElementCount();
    MVT ToEltVT;
    // When narrowing AND converting, the first instruction is the conversion
    // (together with the first narrowing, if possible), so ToType always has
    // the same type as the Dst operand
    if (DstType.isInteger()) {
      ToEltVT = MVT::getIntegerVT(SrcEltVT.getSizeInBits() / 2);
    } else if (SrcEltVT.isFloatingPoint()) {
      ToEltVT = MVT::getFloatingPointVT(SrcEltVT.getSizeInBits() / 2);
    } else {
      llvm_unreachable("Vector Element type must be Integer of Floating Point");
    }
    return EVT::getVectorVT(*DAG.getContext(), ToEltVT, EltCount);
  };

  // Invoke EPIIntNo intrinsic
  EVT ToType = DstType;
  if (Ratio > 2 && DstTypeSize < SrcTypeSize)
    ToType = NarrowVectorElementType(OrigSrcType);
  std::vector<SDValue> Operands;
  Operands.reserve(4 + IsMasked * 2);
  Operands.push_back(DAG.getTargetConstant(EPIIntNo, DL, MVT::i64));
  if (IsMasked)
    Operands.push_back(
        DAG.getNode(ISD::UNDEF, DL, ToType)); // Merge.
  Operands.push_back(FromOperand);
  // Special case because there is no unary narrowing instruction.
  if (IntNo == Intrinsic::vp_trunc || IntNo == Intrinsic::vp_zext ||
      IntNo == Intrinsic::vp_sext) {
    Operands.push_back(DAG.getConstant(0, DL, MVT::i64));
  }
  if (IsMasked) {
    SDValue MaskOp = Op.getOperand(MaskOpNo);
    if (Subtarget.onlyLMUL1()) {
      MaskOp = AdjustMaskElements(FromOperand.getValueType(), MaskOp, Subtarget,
                                  DAG);
    }
    Operands.push_back(MaskOp); // Mask.
  }
  Operands.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                 Op.getOperand(EVLOpNo))); // EVL.

  EVT ActualToType = ToType;
  if (Lowering.getPreferredVectorAction(ToType.getSimpleVT()) ==
      RISCVTargetLowering::TypeWidenVector) {
    ActualToType = Lowering.getTypeToTransformTo(*DAG.getContext(), ToType);
  }
  SDValue Result =
      DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, ActualToType, Operands);

  if (Ratio > 2 && DstTypeSize < SrcTypeSize) {
    // Invoke the FurtherEPIIntNo intrinsic to narrow the Result operand
    // as many times as needed, without changing the type
    EVT ResultType = ToType;
    for (int i = 0; i < Ratio / 4; i++) {
      ToType = NarrowVectorElementType(ResultType);
      std::vector<SDValue> Operands;
      Operands.reserve(4 + IsMasked * 2);
      Operands.push_back(DAG.getTargetConstant(FurtherEPIIntNo, DL, MVT::i64));
      if (IsMasked)
        Operands.push_back(DAG.getNode(ISD::UNDEF, DL, ToType)); // Merge.
      Operands.push_back(Result);
      Operands.push_back(DAG.getConstant(0, DL, MVT::i64));
      if (IsMasked) {
        SDValue MaskOp = Op.getOperand(MaskOpNo);
        if (Subtarget.onlyLMUL1()) {
          MaskOp = AdjustMaskElements(FromOperand.getValueType(), MaskOp,
                                      Subtarget, DAG);
        }
        Operands.push_back(MaskOp); // Mask.
      }
      Operands.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                     Op.getOperand(EVLOpNo))); // EVL.

      EVT ActualToType = ToType;
      if (Lowering.getPreferredVectorAction(ToType.getSimpleVT()) ==
          RISCVTargetLowering::TypeWidenVector) {
        ActualToType = Lowering.getTypeToTransformTo(*DAG.getContext(), ToType);
      }
      Result = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, ActualToType, Operands);

      ResultType = ToType;
    }
  }

  // Now make sure the number of elements match, so we don't break anything.
  Result = GetExtractOrInsertSubvector(Result, DstType, DAG, Subtarget);

  return Result;
}

// This exists to tame complexity when lowering VP intrinsics to EPI. Not ideal.
// This will probably go away when we handle them earlier in the pipeline.
#define VP_INTRINSIC_WO_CHAIN_SET                                              \
  VP_INTRINSIC(vp_add, 3)                                                      \
  VP_INTRINSIC(vp_sub, 3)                                                      \
  VP_INTRINSIC(vp_mul, 3)                                                      \
  VP_INTRINSIC(vp_sdiv, 3)                                                     \
  VP_INTRINSIC(vp_srem, 3)                                                     \
  VP_INTRINSIC(vp_udiv, 3)                                                     \
  VP_INTRINSIC(vp_urem, 3)                                                     \
  VP_INTRINSIC(vp_and, 3)                                                      \
  VP_INTRINSIC(vp_or, 3)                                                       \
  VP_INTRINSIC(vp_xor, 3)                                                      \
  VP_INTRINSIC(vp_ashr, 3)                                                     \
  VP_INTRINSIC(vp_lshr, 3)                                                     \
  VP_INTRINSIC(vp_shl, 3)                                                      \
  VP_INTRINSIC(vp_fadd, 3)                                                     \
  VP_INTRINSIC(vp_fsub, 3)                                                     \
  VP_INTRINSIC(vp_fmul, 3)                                                     \
  VP_INTRINSIC(vp_fdiv, 3)                                                     \
  VP_INTRINSIC(vp_frem, 3)                                                     \
  VP_INTRINSIC(vp_fma, 4)                                                      \
  VP_INTRINSIC(vp_fneg, 2)                                                     \
  VP_INTRINSIC(vp_icmp, 3)                                                     \
  VP_INTRINSIC(vp_fcmp, 3)                                                     \
  VP_INTRINSIC(vp_select, 4)                                                   \
  VP_INTRINSIC(vp_bitcast, 2)                                                  \
  VP_INTRINSIC(vp_sitofp, 2)                                                   \
  VP_INTRINSIC(vp_uitofp, 2)                                                   \
  VP_INTRINSIC(vp_fptosi, 2)                                                   \
  VP_INTRINSIC(vp_fptoui, 2)                                                   \
  VP_INTRINSIC(vp_fpext, 2)                                                    \
  VP_INTRINSIC(vp_fptrunc, 2)                                                  \
  VP_INTRINSIC(vp_trunc, 2)                                                    \
  VP_INTRINSIC(vp_zext, 2)                                                     \
  VP_INTRINSIC(vp_sext, 2)                                                     \
  VP_INTRINSIC(vp_ptrtoint, 2)                                                 \
  VP_INTRINSIC(vp_inttoptr, 2)

#define VP_INTRINSIC_W_CHAIN_SET                                               \
  VP_INTRINSIC(vp_load, 4)                                                     \
  VP_INTRINSIC(vp_strided_load, 4)                                             \
  VP_INTRINSIC(vp_gather, 4)

#define VP_INTRINSIC_VOID_SET                                                  \
  VP_INTRINSIC(vp_store, 5)                                                    \
  VP_INTRINSIC(vp_strided_store, 5)                                            \
  VP_INTRINSIC(vp_scatter, 5)

static unsigned VPGetMaskIdx(unsigned IntNo) {
  switch (IntNo) {
  default:
    llvm_unreachable("Unhandled intrinsic");
#define VP_INTRINSIC(vp_id, mask_idx)                                          \
  case Intrinsic::vp_id:                                                       \
    return mask_idx;
    VP_INTRINSIC_WO_CHAIN_SET
    VP_INTRINSIC_W_CHAIN_SET
    VP_INTRINSIC_VOID_SET
#undef VP_INTRINSIC
  }
}

static SDValue LowerVPINTRINSIC_WO_CHAIN(SDValue Op, SelectionDAG &DAG,
                                         const RISCVTargetLowering &Lowering,
                                         const RISCVSubtarget &Subtarget) {
  SmallVector<SDValue, 4> InOps(Op->op_begin(), Op->op_end());
  unsigned IntNo = cast<ConstantSDNode>(InOps[0])->getZExtValue();
  SDLoc DL(Op);

  // Many instructions allow commuting the second operand with the first one.
  // This is beneficial when we can use a scalar in the second operand as way
  // to fold a vector splat.
  auto GetCanonicalCommutativePerm = [&](SmallVector<unsigned, 3> VOpsPerm) {
    if (VOpsPerm.size() < 2)
      return VOpsPerm;

    SDValue Operand0 = InOps[VOpsPerm[0]];
    SDValue Operand1 = InOps[VOpsPerm[1]];

    if (Operand0.getOpcode() == ISD::SPLAT_VECTOR &&
        Operand1.getOpcode() != ISD::SPLAT_VECTOR) {
      SmallVector<unsigned, 3> CanonicalVOpsPerm = {VOpsPerm[1], VOpsPerm[0]};

      for (unsigned i = 2; i < VOpsPerm.size(); ++i) {
        CanonicalVOpsPerm.push_back(VOpsPerm[i]);
      }

      return CanonicalVOpsPerm;
    }

    return VOpsPerm;
  };

  auto SameEVL = [](SDValue Op1, SDValue Op2) -> bool {
    if (Op1 == Op2)
      return true;

    // We only care about truncate here.
    if (Op1->getOpcode() == ISD::TRUNCATE && Op1->getOperand(0) == Op2)
      return true;
    if (Op2->getOpcode() == ISD::TRUNCATE && Op2->getOperand(0) == Op1)
      return true;

    return false;
  };

  auto IsNegation = [&](SDValue Operand, bool IsMasked, SDValue Mask,
                        SDValue EVL) -> bool {
    if (Operand->getOpcode() != ISD::INTRINSIC_WO_CHAIN)
      return false;
    unsigned OpIntNo =
        cast<ConstantSDNode>(Operand->getOperand(0))->getZExtValue();
    // We check that this is an eligible negation and not some other thing.
    switch (OpIntNo) {
    default:
      return false;
    case Intrinsic::vp_fneg: {
      size_t MaskOpNo = 2;
      size_t EVLOpNo = 3;
      if (IsMasked && Operand.getOperand(MaskOpNo) != Mask)
        return false;
      if (!SameEVL(EVL, Operand.getOperand(EVLOpNo)))
        return false;
      break;
    }
    case Intrinsic::epi_vfsgnjn: {
      if (IsMasked)
        return false;
      if (Operand.getOperand(1) != Operand.getOperand(2))
        return false;
      if (!SameEVL(EVL, Operand.getOperand(3)))
        return false;
      break;
    }
    case Intrinsic::epi_vfsgnjn_mask: {
      if (!IsMasked)
        return false;
      if (Operand.getOperand(1) != Operand.getOperand(2))
        return false;
      if (Mask != Operand.getOperand(3))
        return false;
      if (!SameEVL(EVL, Operand.getOperand(4)))
        return false;
      break;
    }
    }
    return true;
  };

  auto GetNegatedOperand = [](SDValue Operand) {
    assert (Operand->getOpcode() == ISD::INTRINSIC_WO_CHAIN);
    unsigned OpIntNo =
        cast<ConstantSDNode>(Operand->getOperand(0))->getZExtValue();
    // We check that this is an eligible negation and not some other thing.
    switch (OpIntNo) {
    default:
      llvm_unreachable("Unexpected intrinsic");
    case Intrinsic::vp_fneg:
    case Intrinsic::epi_vfsgnjn:
    case Intrinsic::epi_vfsgnjn_mask: {
      return Operand.getOperand(1);
    }
    }
  };

  EVT OrigOpVT = Op.getValueType();
  EVT OpVT = Op.getValueType();

  auto AdjustRet = [&OpVT, &OrigOpVT, &DAG, &Subtarget](SDValue Ret) {
    if (OpVT != OrigOpVT)
      Ret = GetExtractOrInsertSubvector(Ret, OrigOpVT, DAG, Subtarget);
    return Ret;
  };

  SmallVector<unsigned, 3> VOpsPerm;
  unsigned ScalarOpNo = 0;
  unsigned MaskOpNo;
  unsigned EVLOpNo;
  bool IsMasked;
  unsigned EPIIntNo;
  bool IsLogical = OpVT.isVector() &&
                   OpVT.getVectorElementType() == MVT::i1;
  switch (IntNo) {
  default:
    llvm_unreachable("Unexpected intrinsic");
  case Intrinsic::vp_add:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vadd_mask : Intrinsic::epi_vadd;
    break;
  case Intrinsic::vp_sub:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vsub_mask : Intrinsic::epi_vsub;
    break;
  case Intrinsic::vp_mul:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vmul_mask : Intrinsic::epi_vmul;
    break;
  case Intrinsic::vp_sdiv:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vdiv_mask : Intrinsic::epi_vdiv;
    break;
  case Intrinsic::vp_srem:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vrem_mask : Intrinsic::epi_vrem;
    break;
  case Intrinsic::vp_udiv:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vdivu_mask : Intrinsic::epi_vdivu;
    break;
  case Intrinsic::vp_urem:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vremu_mask : Intrinsic::epi_vremu;
    break;
  case Intrinsic::vp_and:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    if (IsLogical) {
      ScalarOpNo = 0;
      EPIIntNo = Intrinsic::epi_vmand;
      // For masks operations, we ignore the mask (if present)
      IsMasked = false;
    } else
      EPIIntNo = IsMasked ? Intrinsic::epi_vand_mask : Intrinsic::epi_vand;
    break;
  case Intrinsic::vp_or:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    if (IsLogical) {
      ScalarOpNo = 0;
      EPIIntNo = Intrinsic::epi_vmor;
      // For masks operations, we ignore the mask (if present)
      IsMasked = false;
    } else
      EPIIntNo = IsMasked ? Intrinsic::epi_vor_mask : Intrinsic::epi_vor;
    break;
  case Intrinsic::vp_xor:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    if (IsLogical) {
      ScalarOpNo = 0;
      EPIIntNo = Intrinsic::epi_vmxor;
      // For masks operations, we ignore the mask (if present)
      IsMasked = false;
    } else
      EPIIntNo = IsMasked ? Intrinsic::epi_vxor_mask : Intrinsic::epi_vxor;
    break;
  case Intrinsic::vp_ashr:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vsra_mask : Intrinsic::epi_vsra;
    break;
  case Intrinsic::vp_lshr:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vsrl_mask : Intrinsic::epi_vsrl;
    break;
  case Intrinsic::vp_shl:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vsll_mask : Intrinsic::epi_vsll;
    break;
  case Intrinsic::vp_fadd:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vfadd_mask : Intrinsic::epi_vfadd;
    break;
  case Intrinsic::vp_fsub:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vfsub_mask : Intrinsic::epi_vfsub;
    break;
  case Intrinsic::vp_fmul:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2});
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vfmul_mask : Intrinsic::epi_vfmul;
    break;
  case Intrinsic::vp_fdiv:
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 3;
    EVLOpNo = 4;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo = IsMasked ? Intrinsic::epi_vfdiv_mask : Intrinsic::epi_vfdiv;
    break;
  case Intrinsic::vp_frem: {
    RISCVTargetLowering::VTToLibCall VTToLC[] = {
        {MVT::nxv1f64, RTLIB::VP_FREM_NXV1F64},
        {MVT::nxv2f64, RTLIB::VP_FREM_NXV2F64},
        {MVT::nxv4f64, RTLIB::VP_FREM_NXV4F64},
        {MVT::nxv8f64, RTLIB::VP_FREM_NXV8F64},
        {MVT::nxv2f32, RTLIB::VP_FREM_NXV2F32},
        {MVT::nxv4f32, RTLIB::VP_FREM_NXV4F32},
        {MVT::nxv8f32, RTLIB::VP_FREM_NXV8F32},
        {MVT::nxv16f32, RTLIB::VP_FREM_NXV16F32},
    };
    SDValue Ops[] = {InOps[1], InOps[2],
                     InOps[3], InOps[4]};
    return Lowering.lowerVECLIBCALL(Op.getValueType(), DL, Ops, DAG, VTToLC);
  }
  case Intrinsic::vp_fma:
    VOpsPerm = GetCanonicalCommutativePerm({1, 2, 3});
    ScalarOpNo = 2;
    MaskOpNo = 4;
    EVLOpNo = 5;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    // Try to select the best FMA version folding negations.
    if (EnableFoldingFMA &&
        IsNegation(InOps[3], IsMasked, InOps[MaskOpNo], InOps[EVLOpNo])) {
      if (IsNegation(InOps[1], IsMasked, InOps[MaskOpNo], InOps[EVLOpNo])) {
        InOps[1] = GetNegatedOperand(InOps[1]);
        InOps[3] = GetNegatedOperand(InOps[3]);
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vfnmadd_mask : Intrinsic::epi_vfnmadd;
      } else {
        InOps[3] = GetNegatedOperand(InOps[3]);
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vfmsub_mask : Intrinsic::epi_vfmsub;
      }
    } else if (EnableFoldingFMA &&
               IsNegation(InOps[1], IsMasked, InOps[MaskOpNo],
                          InOps[EVLOpNo])) {
      InOps[1] = GetNegatedOperand(InOps[1]);
      EPIIntNo =
          IsMasked ? Intrinsic::epi_vfnmsub_mask : Intrinsic::epi_vfnmsub;
    } else {
      // Fallback to vfmadd.
      EPIIntNo = IsMasked ? Intrinsic::epi_vfmadd_mask : Intrinsic::epi_vfmadd;
    }
    break;
  case Intrinsic::vp_fneg:
    VOpsPerm = {1, 1};
    MaskOpNo = 2;
    EVLOpNo = 3;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);
    EPIIntNo =
        IsMasked ? Intrinsic::epi_vfsgnjn_mask : Intrinsic::epi_vfsgnjn;
    break;
  case Intrinsic::vp_icmp: {
    OpVT = ToMaskVT(Lowering.getTypeToTransformTo(
                        *DAG.getContext(), InOps[1].getValueType()),
                    OrigOpVT, DAG);
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 4;
    EVLOpNo = 5;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);

    unsigned Cmp = cast<ConstantSDNode>(InOps[3])->getZExtValue();
    switch (Cmp) {
    case CmpInst::ICMP_EQ:
      VOpsPerm = GetCanonicalCommutativePerm({1, 2});
      EPIIntNo = IsMasked ? Intrinsic::epi_vmseq_mask : Intrinsic::epi_vmseq;
      break;
    case CmpInst::ICMP_NE:
      VOpsPerm = GetCanonicalCommutativePerm({1, 2});
      EPIIntNo = IsMasked ? Intrinsic::epi_vmsne_mask : Intrinsic::epi_vmsne;
      break;
    case CmpInst::ICMP_UGT: {
      SDValue RHS = InOps[2];
      if (RHS.getOpcode() == ISD::SPLAT_VECTOR) {
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vmsgtu_mask : Intrinsic::epi_vmsgtu;
      } else {
        VOpsPerm = {2, 1};
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vmsltu_mask : Intrinsic::epi_vmsltu;
      }
      break;
    }
    case CmpInst::ICMP_UGE:
      // Note: The ISA does not provide vmsgeu.vx to fold a scalar.
      VOpsPerm = {2, 1};
      EPIIntNo = IsMasked ? Intrinsic::epi_vmsleu_mask : Intrinsic::epi_vmsleu;
      break;
    case CmpInst::ICMP_ULT: {
      SDValue LHS = InOps[1];
      if (LHS.getOpcode() == ISD::SPLAT_VECTOR) {
        VOpsPerm = {2, 1};
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vmsgtu_mask : Intrinsic::epi_vmsgtu;
      } else {
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vmsltu_mask : Intrinsic::epi_vmsltu;
      }
      break;
    }
    case CmpInst::ICMP_ULE:
      // Note: The ISA does not provide vmsgeu.vx so we can't flip the operands
      // to fold a scalar.
      EPIIntNo = IsMasked ? Intrinsic::epi_vmsleu_mask : Intrinsic::epi_vmsleu;
      break;
    case CmpInst::ICMP_SGT: {
      SDValue RHS = InOps[2];
      if (RHS.getOpcode() == ISD::SPLAT_VECTOR) {
        EPIIntNo =
            IsMasked ? Intrinsic::epi_vmsgt_mask : Intrinsic::epi_vmsgt;
      } else {
        VOpsPerm = {2, 1};
        EPIIntNo = IsMasked ? Intrinsic::epi_vmslt_mask : Intrinsic::epi_vmslt;
      }
      break;
    }
    case CmpInst::ICMP_SGE:
      // Note: The ISA does not provide vmsge.vx to fold a scalar.
      VOpsPerm = {2, 1};
      EPIIntNo = IsMasked ? Intrinsic::epi_vmsle_mask : Intrinsic::epi_vmsle;
      break;
    case CmpInst::ICMP_SLT:
      EPIIntNo = IsMasked ? Intrinsic::epi_vmslt_mask : Intrinsic::epi_vmslt;
      break;
    case CmpInst::ICMP_SLE:
      // Note: The ISA does not provide vmsge.vx so we can't flip the operands
      // to fold a scalar.
      EPIIntNo = IsMasked ? Intrinsic::epi_vmsle_mask : Intrinsic::epi_vmsle;
      break;
    }
    break;
  }
  case Intrinsic::vp_fcmp: {
    OpVT = ToMaskVT(Lowering.getTypeToTransformTo(
                        *DAG.getContext(), InOps[1].getValueType()),
                    OrigOpVT, DAG);
    VOpsPerm = {1, 2};
    ScalarOpNo = 2;
    MaskOpNo = 4;
    EVLOpNo = 5;
    IsMasked = !IsSplatOfOne(InOps[MaskOpNo]);

    unsigned FCmp = cast<ConstantSDNode>(InOps[3])->getZExtValue();
    switch (FCmp) {
    case FCmpInst::FCMP_FALSE: {
      SDValue Zero = DAG.getTargetConstant(0, DL, MVT::i64);

      return DAG.getNode(ISD::SPLAT_VECTOR, DL, OpVT, Zero);
    }
    case FCmpInst::FCMP_OEQ:
      VOpsPerm = GetCanonicalCommutativePerm({1, 2});
      EPIIntNo = IsMasked ? Intrinsic::epi_vmfeq_mask : Intrinsic::epi_vmfeq;
      break;
    case FCmpInst::FCMP_OGT: {
      SDValue RHS = InOps[2];
      if (RHS.getOpcode() == ISD::SPLAT_VECTOR) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfgt_mask : Intrinsic::epi_vmfgt;
      } else {
        VOpsPerm = {2, 1};
        EPIIntNo = IsMasked ? Intrinsic::epi_vmflt_mask : Intrinsic::epi_vmflt;
      }
      break;
    }
    case FCmpInst::FCMP_OGE: {
      SDValue RHS = InOps[2];
      if (RHS.getOpcode() == ISD::SPLAT_VECTOR) {
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfge_mask : Intrinsic::epi_vmfge;
      } else {
        VOpsPerm = {2, 1};
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfle_mask : Intrinsic::epi_vmfle;
      }
      break;
    }
    case FCmpInst::FCMP_OLT: {
      SDValue LHS = InOps[1];
      if (LHS.getOpcode() == ISD::SPLAT_VECTOR) {
        VOpsPerm = {2, 1};
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfgt_mask : Intrinsic::epi_vmfgt;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vmflt_mask : Intrinsic::epi_vmflt;
      }
      break;
    }
    case FCmpInst::FCMP_OLE: {
      SDValue LHS = InOps[1];
      if (LHS.getOpcode() == ISD::SPLAT_VECTOR) {
        VOpsPerm = {2, 1};
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfge_mask : Intrinsic::epi_vmfge;
      } else {
        EPIIntNo = IsMasked ? Intrinsic::epi_vmfle_mask : Intrinsic::epi_vmfle;
      }
      break;
    }
    case FCmpInst::FCMP_ONE: {
      assert(InOps[EVLOpNo].getValueType() == MVT::i32 &&
             "Unexpected operand");
      SDValue AnyExtEVL =
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, InOps[EVLOpNo]);

      SDValue FeqOp1Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[1], InOps[1], AnyExtEVL};
      SDValue FeqOp2Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[2], InOps[2], AnyExtEVL};
      SDValue FeqOp1 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp1Operands);
      SDValue FeqOp2 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp2Operands);

      SDValue FirstAndOperands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmand, DL, MVT::i64), FeqOp1,
          FeqOp2, AnyExtEVL};
      SDValue And =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FirstAndOperands);

      SDValue FCmpOperands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfne_mask, DL, MVT::i64),
          DAG.getNode(ISD::UNDEF, DL, OpVT), // Merge.
          InOps[1],
          InOps[2],
          And,
          AnyExtEVL};
      SDValue FCmp =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FCmpOperands);

      SDValue SecondAndOperands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmand, DL, MVT::i64), FCmp, And,
          AnyExtEVL};
      return AdjustRet(
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, SecondAndOperands));
    }
    case FCmpInst::FCMP_ORD: {
      assert(InOps[EVLOpNo].getValueType() == MVT::i32 &&
             "Unexpected operand");
      SDValue AnyExtEVL =
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, InOps[EVLOpNo]);

      SDValue FeqOp1Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[1], InOps[1], AnyExtEVL};
      SDValue FeqOp2Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[2], InOps[2], AnyExtEVL};
      SDValue FeqOp1 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp1Operands);
      SDValue FeqOp2 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp2Operands);

      SDValue FirstAndOperands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmand, DL, MVT::i64), FeqOp1,
          FeqOp2, AnyExtEVL};
      return AdjustRet(
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FirstAndOperands));
    }
    // FIXME: Fold scalar operands also in unordered comparisons.
    case FCmpInst::FCMP_UEQ:
      return AdjustRet(LowerVPUnorderedFCmp(
          Intrinsic::epi_vmfeq_mask, InOps[1], InOps[2],
          InOps[EVLOpNo], OpVT, DAG, DL));
    case FCmpInst::FCMP_UGT:
      return AdjustRet(LowerVPUnorderedFCmp(
          Intrinsic::epi_vmfgt_mask, InOps[1], InOps[2],
          InOps[EVLOpNo], OpVT, DAG, DL));
    case FCmpInst::FCMP_UGE:
      return AdjustRet(LowerVPUnorderedFCmp(
          Intrinsic::epi_vmfge_mask, InOps[1], InOps[2],
          InOps[EVLOpNo], OpVT, DAG, DL));
    case FCmpInst::FCMP_ULT:
      return AdjustRet(LowerVPUnorderedFCmp(
          Intrinsic::epi_vmflt_mask, InOps[1], InOps[2],
          InOps[EVLOpNo], OpVT, DAG, DL));
    case FCmpInst::FCMP_ULE:
      return AdjustRet(LowerVPUnorderedFCmp(
          Intrinsic::epi_vmfle_mask, InOps[1], InOps[2],
          InOps[EVLOpNo], OpVT, DAG, DL));
    case FCmpInst::FCMP_UNE:
      VOpsPerm = GetCanonicalCommutativePerm({1, 2});
      EPIIntNo = IsMasked ? Intrinsic::epi_vmfne_mask : Intrinsic::epi_vmfne;
      break;
    case FCmpInst::FCMP_UNO: {
      assert(InOps[EVLOpNo].getValueType() == MVT::i32 &&
             "Unexpected operand");
      SDValue AnyExtEVL =
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, InOps[EVLOpNo]);

      SDValue FeqOp1Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[1], InOps[1], AnyExtEVL};
      SDValue FeqOp2Operands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmfeq, DL, MVT::i64),
          InOps[2], InOps[2], AnyExtEVL};
      SDValue FeqOp1 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp1Operands);
      SDValue FeqOp2 =
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, FeqOp2Operands);

      SDValue NandOperands[] = {
          DAG.getTargetConstant(Intrinsic::epi_vmnand, DL, MVT::i64), FeqOp1,
          FeqOp2, AnyExtEVL};
      return AdjustRet(
          DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, NandOperands));
    }
    case FCmpInst::FCMP_TRUE: {
      SDValue One = DAG.getTargetConstant(1, DL, MVT::i64);
      return AdjustRet(DAG.getNode(ISD::SPLAT_VECTOR, DL, OpVT, One));
    }
    }
    break;
  }
  case Intrinsic::vp_select: {
    VOpsPerm = {3, 2, 1};
    ScalarOpNo = 2;
    MaskOpNo = -1;
    EVLOpNo = 4;
    IsMasked = false;

    const EVT &ElementType = OpVT.getVectorElementType();
    if (ElementType.isFloatingPoint()) {
      EPIIntNo = Intrinsic::epi_vfmerge;
      break;
    } else if (ElementType != MVT::i1) {
      EPIIntNo = Intrinsic::epi_vmerge;
      break;
    }

    // llvm.vp.select applied to mask types.
    // vp.select computes a masked merge from two values. This can be naively
    // computed doing: (a & mask) | (b & ~mask)
    // However, the bithack described in
    // https://graphics.stanford.edu/~seander/bithacks.html#MaskedMerge shows
    // how it can be optimized to: b ^ ((b ^ a) & mask)

    assert(InOps[EVLOpNo].getValueType() == MVT::i32 &&
           "Unexpected operand");
    SDValue EVL =
        DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, InOps[EVLOpNo]);

    const SDValue &OpA = InOps[2];
    const SDValue &OpB = InOps[3];
    const SDValue &Mask = InOps[1];

    SDValue BXorA =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT,
                    {DAG.getTargetConstant(Intrinsic::epi_vmxor, DL, MVT::i64),
                     OpB, OpA, EVL});

    SDValue XorAndMask =
        DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT,
                    {DAG.getTargetConstant(Intrinsic::epi_vmand, DL, MVT::i64),
                     BXorA, Mask, EVL});

    return AdjustRet(DAG.getNode(
        ISD::INTRINSIC_WO_CHAIN, DL, OpVT,
        {DAG.getTargetConstant(Intrinsic::epi_vmxor, DL, MVT::i64), OpB,
         XorAndMask, EVL}));
  }
  case Intrinsic::vp_bitcast: {
    assert(OpVT.getSizeInBits() ==
               InOps[1].getValueType().getSizeInBits() &&
           "Unable to bitcast values of unmatching sizes");
    return DAG.getNode(ISD::BITCAST, DL, OpVT, InOps[1]);
  }
  }

  std::vector<SDValue> Operands;
  Operands.reserve(2 + VOpsPerm.size() + IsMasked * 2);

  Operands.push_back(DAG.getTargetConstant(EPIIntNo, DL, MVT::i64));

  if (IsMasked && IntNo != Intrinsic::vp_fma)
    Operands.push_back(
        DAG.getNode(ISD::UNDEF, DL, OpVT)); // Merge.

  for (auto VOpI = VOpsPerm.begin(), VOpE = VOpsPerm.end(), VOpStart = VOpI;
       VOpI != VOpE; VOpI++) {
    SDValue Operand = InOps[*VOpI];
    // +1 because we skip the IntrinsicID
    unsigned OpIdx = (VOpI - VOpStart) + 1;
    if ((OpIdx == ScalarOpNo) && (Operand.getOpcode() == ISD::SPLAT_VECTOR))
      Operand = Operand.getOperand(0);
    Operands.push_back(Operand);
  }

  if (IsMasked) {
    SDValue MaskOp = InOps[MaskOpNo];
    EVT OrigMaskVT = MaskOp.getValueType();
    EVT MaskVT = ToMaskVT(OpVT, OrigMaskVT, DAG);

    MaskOp = GetExtractOrInsertSubvector(MaskOp, MaskVT, DAG, Subtarget);

    Operands.push_back(MaskOp); // Mask.
  }

  assert(InOps[EVLOpNo].getValueType() == MVT::i32 &&
         "Unexpected operand");
  Operands.push_back(DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                 InOps[EVLOpNo])); // EVL.

  SDValue Ret = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, OpVT, Operands);

  Ret = AdjustRet(Ret);
  return Ret;
}

// Decomposes a vector of addresses into a base address plus a vector of
// offsets.
static void GetBaseAddressAndOffsets(const SDValue &Addresses, EVT OffsetsVT,
                                     SDLoc DL, SelectionDAG &DAG,
                                     SDValue &BaseAddr, SDValue &Offsets) {
  unsigned Opcode = Addresses.getOpcode();
  if (Opcode == ISD::SPLAT_VECTOR) {
    // Addresses is a splat vector. Set the BaseAddr as the splatted value
    // and the offsets to zero.
    BaseAddr = Addresses.getOperand(0);
    Offsets =
        DAG.getNode(ISD::SPLAT_VECTOR, DL, OffsetsVT,
                    DAG.getConstant(0, DL, OffsetsVT.getVectorElementType()));
    return;
  }

  if (Opcode == ISD::ADD) {
    // Addresses is either (add a, (splat b)) or (add (splat a), b). Compute the
    // base address as int the previous case and use the addend as offsets.
    SDValue Op0 = Addresses.getOperand(0);
    SDValue Op1 = Addresses.getOperand(1);
    if (Op0.getOpcode() == ISD::SPLAT_VECTOR ||
        Op1.getOpcode() == ISD::SPLAT_VECTOR) {
      if (Op0.getOpcode() == ISD::SPLAT_VECTOR) {
        BaseAddr = Op0.getOperand(0);
        Offsets = Op1;
      } else {
        BaseAddr = Op1.getOperand(0);
        Offsets = Op0;
      }
      assert(OffsetsVT == Offsets.getValueType() &&
             "Unexpected type for the offsets vector");
      return;
    }
  }

  if (Opcode == ISD::INTRINSIC_WO_CHAIN) {
    // When the addresses come from the result of a GEP to VP operands
    // transformation, what we have is a vector sum of the base address and the
    // (already scaled) indices.
    unsigned IntNo =
        cast<ConstantSDNode>(Addresses.getOperand(0))->getZExtValue();
    if (IntNo == Intrinsic::epi_vadd) {
      // FIXME: here checking for ISD::CopyFromReg, as the
      // base address operand always come from a different BB. Ideally
      // we'd deal with this in SelectionDAGBuilder but we believe this should
      // do. For robustness purposes, we expect a scalar value for the base
      // pointer address that due to the way we sink scalar operands, will
      // always be in the RHS.
      SDValue Op1 = Addresses.getOperand(1);
      SDValue Op2 = Addresses.getOperand(2);
      if (Op2.getValueType().isScalarInteger() &&
          Op2.getOpcode() == ISD::CopyFromReg) {
        BaseAddr = Op2;
        Offsets = Op1;
        assert(OffsetsVT == Offsets.getValueType() &&
               "Unexpected type for the offsets vector");
        return;
      }
    }
  }

  // Fallback to setting the base address to zero and the offsets to the
  // Addresses vector.
  assert(OffsetsVT == Addresses.getValueType() &&
         "Unexpected type for the offsets vector");
  BaseAddr = DAG.getConstant(0, DL, MVT::i64);
  Offsets = Addresses;
}


static SDValue LowerVPINTRINSIC_W_CHAIN(SDValue Op, SelectionDAG &DAG) {
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(1))->getZExtValue();
  SDLoc DL(Op);

  switch (IntNo) {
  default:
    llvm_unreachable("Unexpected intrinsic");
  case Intrinsic::vp_load: {
    // NOTE: Address alignment operand (3) ignored.
    assert(Op.getOperand(5).getValueType() == MVT::i32 && "Unexpected operand");

    std::vector<SDValue> Operands;
    const SDValue &MaskOp = Op.getOperand(4);
    if (IsSplatOfOne(MaskOp))
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vload, DL, MVT::i64),
          Op.getOperand(2), // Address.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(5)) // EVL.
      };
    else
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vload_mask, DL, MVT::i64),
          DAG.getNode(ISD::UNDEF, DL, Op.getValueType()), // Merge.
          Op.getOperand(2),                               // Address.
          Op.getOperand(4), // Mask.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(5)) // EVL.
      };

    SDValue Result =
        DAG.getNode(ISD::INTRINSIC_W_CHAIN, DL, Op->getVTList(), Operands);

    return DAG.getMergeValues({Result, Result.getValue(1)}, DL);
  }
  case Intrinsic::vp_strided_load: {
    assert(Op.getOperand(5).getValueType() == MVT::i32 && "Unexpected operand");

    std::vector<SDValue> Operands;
    const SDValue &MaskOp = Op.getOperand(4);
    if (IsSplatOfOne(MaskOp))
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vload_strided, DL, MVT::i64),
          Op.getOperand(2), // Address.
          Op.getOperand(3), // Stride.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(5)) // EVL.
      };
    else
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vload_strided_mask, DL,
                                MVT::i64),
          DAG.getNode(ISD::UNDEF, DL, Op.getValueType()), // Merge.
          Op.getOperand(2),                               // Address.
          Op.getOperand(3),                               // Stride.
          MaskOp,                                         // Mask.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(5)) // EVL.
      };

    SDValue Result =
        DAG.getNode(ISD::INTRINSIC_W_CHAIN, DL, Op->getVTList(), Operands);

    return DAG.getMergeValues({Result, Result.getValue(1)}, DL);
  }
  case Intrinsic::vp_gather: {
    // NOTE: Address alignment operand (3) ignored.
    EVT VT = Op.getValueType();
    EVT OffsetsVT = VT.changeVectorElementTypeToInteger();

    SDValue BaseAddr;
    SDValue Offsets;
    SDValue Addresses = Op.getOperand(2);
    GetBaseAddressAndOffsets(Addresses, OffsetsVT, DL, DAG, BaseAddr, Offsets);

    SDValue VL = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, Op.getOperand(5));

    std::vector<SDValue> Operands;
    const SDValue &MaskOp = Op.getOperand(4);
    if (IsSplatOfOne(MaskOp)) {
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vload_indexed, DL, MVT::i64),
          BaseAddr, Offsets, VL};
    } else {
      Operands = {Op.getOperand(0), // Chain.
                  DAG.getTargetConstant(Intrinsic::epi_vload_indexed_mask, DL,
                                        MVT::i64),
                  DAG.getNode(ISD::UNDEF, DL, VT), // Merge.
                  BaseAddr,
                  Offsets,
                  MaskOp,
                  VL};
    }
    SDValue Result =
        DAG.getNode(ISD::INTRINSIC_W_CHAIN, DL, Op->getVTList(), Operands);

    return Result;
  }
  }
}

static SDValue LowerVPINTRINSIC_VOID(SDValue Op, SelectionDAG &DAG,
                                     const RISCVTargetLowering &Lowering,
                                     const RISCVSubtarget &Subtarget) {
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(1))->getZExtValue();
  SDLoc DL(Op);

  switch (IntNo) {
  default:
    llvm_unreachable("Unexpected intrinsic");
  case Intrinsic::vp_store: {
    // NOTE: Address alignment operand (4) ignored.
    assert(Op.getOperand(6).getValueType() == MVT::i32 && "Unexpected operand");

    std::vector<SDValue> Operands;
    SDValue MaskOp = Op.getOperand(5);
    if (IsSplatOfOne(MaskOp))
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vstore, DL, MVT::i64),
          Op.getOperand(2), // Value.
          Op.getOperand(3), // Address.
          // FIXME Alignment ignored.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(6)), // EVL.
      };
    else {
      EVT WidenVT = Lowering.getTypeToTransformTo(
          *DAG.getContext(), Op.getOperand(2).getValueType());
      MaskOp = AdjustMaskElements(WidenVT, MaskOp, Subtarget, DAG);

      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vstore_mask, DL, MVT::i64),
          Op.getOperand(2), // Value.
          Op.getOperand(3), // Address.
          // FIXME Alignment ignored.
          MaskOp, // Mask.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(6)), // EVL.
      };
    }

    return DAG.getNode(ISD::INTRINSIC_VOID, DL, Op->getVTList(), Operands);
    break;
  }
  case Intrinsic::vp_strided_store: {
    assert(Op.getOperand(6).getValueType() == MVT::i32 && "Unexpected operand");

    std::vector<SDValue> Operands;
    SDValue MaskOp = Op.getOperand(5);
    if (IsSplatOfOne(MaskOp))
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vstore_strided, DL, MVT::i64),
          Op.getOperand(2), // Value.
          Op.getOperand(3), // Address.
          Op.getOperand(4), // Stride.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(6)), // EVL.
      };
    else {
      EVT WidenVT = Lowering.getTypeToTransformTo(
          *DAG.getContext(), Op.getOperand(2).getValueType());
      MaskOp = AdjustMaskElements(WidenVT, MaskOp, Subtarget, DAG);

      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vstore_strided_mask, DL,
                                MVT::i64),
          Op.getOperand(2), // Value.
          Op.getOperand(3), // Address.
          Op.getOperand(4), // Stride.
          MaskOp,           // Mask.
          DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                      Op.getOperand(6)), // EVL.
      };
    }

    return DAG.getNode(ISD::INTRINSIC_VOID, DL, Op->getVTList(), Operands);
    break;
  }
  case Intrinsic::vp_scatter: {
    // NOTE: Address alignment operand (4) ignored.
    SDValue Data = Op.getOperand(2);
    EVT OffsetsVT = Data.getValueType().changeVectorElementTypeToInteger();

    SDValue BaseAddr;
    SDValue Offsets;
    SDValue Addresses = Op.getOperand(3);
    GetBaseAddressAndOffsets(Addresses, OffsetsVT, DL, DAG, BaseAddr, Offsets);

    SDValue VL = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, Op.getOperand(6));

    std::vector<SDValue> Operands;
    SDValue MaskOp = Op.getOperand(5);
    if (IsSplatOfOne(MaskOp)) {
      // Unmasked.
      Operands = {
          Op.getOperand(0), // Chain.
          DAG.getTargetConstant(Intrinsic::epi_vstore_indexed, DL, MVT::i64),
          Data,
          BaseAddr,
          Offsets,
          VL};
    } else {
      EVT WidenVT =
          Lowering.getTypeToTransformTo(*DAG.getContext(), Data.getValueType());
      MaskOp = AdjustMaskElements(WidenVT, MaskOp, Subtarget, DAG);

      Operands = {Op.getOperand(0), // Chain.
                  DAG.getTargetConstant(Intrinsic::epi_vstore_indexed_mask, DL,
                                        MVT::i64),
                  Data,
                  BaseAddr,
                  Offsets,
                  MaskOp, // Mask.
                  VL};
    }

    SDValue Result =
        DAG.getNode(ISD::INTRINSIC_VOID, DL, Op->getVTList(), Operands);
    return Result;
  }
  }
}

static SDValue
LowerVPIntegerPointerConversion(SDValue Op, const RISCVTargetLowering &Lowering,
                                const RISCVSubtarget &Subtarget,
                                SelectionDAG &DAG) {
  EVT DstType = Op.getValueType();
  uint64_t DstTypeSize = DstType.getScalarSizeInBits();
  SDValue SrcOp = Op.getOperand(1);
  EVT SrcType = SrcOp.getValueType();
  uint64_t SrcTypeSize = SrcType.getScalarSizeInBits();
  assert(isPowerOf2_64(DstTypeSize) && isPowerOf2_64(SrcTypeSize) &&
         "Types must be powers of two");

  if (DstTypeSize == SrcTypeSize)
    return SrcOp;

  return LowerVPIntrinsicConversion(Op, Lowering, Subtarget, DAG);
}

// This is just to make sure we properly dispatch all the VP intrinsics
// of VP_INTRINSIC_WO_CHAIN_SET in LowerVPIntrinsic

enum VPIntrinsicsSubset {
#define VP_INTRINSIC(X, ...) Intrinsic__##X = Intrinsic::X,
  VP_INTRINSIC_WO_CHAIN_SET VP_INTRINSIC_W_CHAIN_SET VP_INTRINSIC_VOID_SET
#undef VP_INTRINSIC
};

static SDValue LowerVPIntrinsic(unsigned IntNo, SDValue Op, SelectionDAG &DAG,
    const RISCVTargetLowering &Lowering, const RISCVSubtarget &Subtarget) {
  VPIntrinsicsSubset VPIntNo = static_cast<VPIntrinsicsSubset>(IntNo);
  SDLoc DL(Op);

  switch (VPIntNo) {
    // Note the use of __ instead of ::
  case Intrinsic__vp_add:
  case Intrinsic__vp_sub:
  case Intrinsic__vp_mul:
  case Intrinsic__vp_sdiv:
  case Intrinsic__vp_srem:
  case Intrinsic__vp_udiv:
  case Intrinsic__vp_urem:
  case Intrinsic__vp_and:
  case Intrinsic__vp_or:
  case Intrinsic__vp_xor:
  case Intrinsic__vp_ashr:
  case Intrinsic__vp_lshr:
  case Intrinsic__vp_shl:
  case Intrinsic__vp_fadd:
  case Intrinsic__vp_fsub:
  case Intrinsic__vp_fmul:
  case Intrinsic__vp_fdiv:
  case Intrinsic__vp_frem:
  case Intrinsic__vp_fma:
  case Intrinsic__vp_fneg:
  case Intrinsic__vp_icmp:
  case Intrinsic__vp_fcmp:
  case Intrinsic__vp_select:
  case Intrinsic__vp_bitcast:
    return LowerVPINTRINSIC_WO_CHAIN(Op, DAG, Lowering, Subtarget);
  case Intrinsic__vp_sitofp:
  case Intrinsic__vp_uitofp:
  case Intrinsic__vp_fptosi:
  case Intrinsic__vp_fptoui:
  case Intrinsic__vp_fpext:
  case Intrinsic__vp_fptrunc:
  case Intrinsic__vp_trunc:
  case Intrinsic__vp_zext:
  case Intrinsic__vp_sext:
    return LowerVPIntrinsicConversion(Op, Lowering, Subtarget, DAG);
  case Intrinsic__vp_ptrtoint:
  case Intrinsic__vp_inttoptr:
    return LowerVPIntegerPointerConversion(Op, Lowering, Subtarget, DAG);
  case Intrinsic__vp_load:
  case Intrinsic__vp_strided_load:
  case Intrinsic__vp_gather:
    return LowerVPINTRINSIC_W_CHAIN(Op, DAG);
  case Intrinsic__vp_store:
  case Intrinsic__vp_strided_store:
  case Intrinsic__vp_scatter:
    return LowerVPINTRINSIC_VOID(Op, DAG, Lowering, Subtarget);
  }
}

SDValue RISCVTargetLowering::LowerINTRINSIC_WO_CHAIN(SDValue Op,
                                                     SelectionDAG &DAG) const {
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(0))->getZExtValue();
  SDLoc DL(Op);

  if (Subtarget.hasStdExtV()) {
    if (const RISCVEPIIntrinsicsTable::EPIIntrinsicInfo *EII =
            RISCVEPIIntrinsicsTable::getEPIIntrinsicInfo(IntNo)) {
      // Widen vector operands.
      std::vector<SDValue> Operands(Op->op_begin(), Op->op_end());
      bool Changed = false;
      for (SDValue &Operand : Operands) {
        if (Operand.getValueType().isScalableVector() &&
            getPreferredVectorAction(Operand.getValueType().getSimpleVT()) ==
                TypeWidenVector) {
          EVT WidenVT =
              getTypeToTransformTo(*DAG.getContext(), Operand.getValueType());
          Operand =
              DAG.getNode(ISD::INSERT_SUBVECTOR, DL, WidenVT,
                          DAG.getNode(ISD::UNDEF, DL, WidenVT), Operand,
                          DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
          Changed = true;
        }
      }

      // Some EPI intrinsics may claim that they want an integer operand to be
      // extended.
      if (EII->ExtendedOperand) {
        assert(EII->ExtendedOperand < Op.getNumOperands());
        SDValue &ScalarOp = Operands[EII->ExtendedOperand];
        if (ScalarOp.getValueType() == MVT::i32 ||
            ScalarOp.getValueType() == MVT::i16 ||
            ScalarOp.getValueType() == MVT::i8) {
          ScalarOp = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, ScalarOp);
          Changed = true;
        }
        EVT ScalarVT = ScalarOp.getValueType();

        if (ScalarVT == MVT::f8_143 || ScalarVT == MVT::f8_152 ||
            ScalarVT == MVT::f16 || ScalarVT == MVT::bf16) {

          auto BitcastVT = (ScalarVT == MVT::f16 || ScalarVT == MVT::bf16)
                               ? MVT::i16
                               : MVT::i8;
          ScalarOp = DAG.getNode(ISD::BITCAST, DL, BitcastVT, ScalarOp);
          ScalarOp = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, ScalarOp);
          ScalarOp = DAG.getNode(RISCVISD::FP_TO_FP_REG, DL, MVT::f32, ScalarOp,
                                 DAG.getValueType(ScalarVT));
          Changed = true;
        }
      }

      if (Changed)
        return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, Op.getValueType(),
                           Operands);
    }
  }
  switch (IntNo) {
  default:
    return SDValue(); // Don't custom lower most intrinsics.
  case Intrinsic::thread_pointer: {
    EVT PtrVT = getPointerTy(DAG.getDataLayout());
    return DAG.getRegister(RISCV::X4, PtrVT);
  }
  case Intrinsic::epi_vzip2:
  case Intrinsic::epi_vunzip2:
  case Intrinsic::epi_vtrn: {
    SDVTList VTList = Op->getVTList();
    assert(VTList.NumVTs == 2);
    EVT VT = VTList.VTs[0];

    unsigned TupleOpcode;
    switch (IntNo) {
    default:
      llvm_unreachable("Invalid opcode");
      break;
    case Intrinsic::epi_vzip2:
      TupleOpcode = RISCVISD::VZIP2;
      break;
    case Intrinsic::epi_vunzip2:
      TupleOpcode = RISCVISD::VUNZIP2;
      break;
    case Intrinsic::epi_vtrn:
      TupleOpcode = RISCVISD::VTRN;
      break;
    }

    SDValue TupleNode =
        DAG.getNode(TupleOpcode, DL, MVT::Untyped, Op->getOperand(1),
                    Op->getOperand(2), Op->getOperand(3));
    SDValue SubRegFirst = DAG.getTargetConstant(RISCV::vtfirst, DL, MVT::i32);
    MachineSDNode *FirstNode = DAG.getMachineNode(
        TargetOpcode::EXTRACT_SUBREG, DL, VT, TupleNode, SubRegFirst);

    SDValue SubRegSecond = DAG.getTargetConstant(RISCV::vtsecond, DL, MVT::i32);
    MachineSDNode *SecondNode = DAG.getMachineNode(
        TargetOpcode::EXTRACT_SUBREG, DL, VT, TupleNode, SubRegSecond);

    SDValue ExtractedOps[] = {SDValue(FirstNode, 0), SDValue(SecondNode, 0)};
    return DAG.getNode(ISD::MERGE_VALUES, DL, VTList, ExtractedOps);
  }
  case Intrinsic::experimental_vector_vp_slideleftfill:
  case Intrinsic::experimental_vector_slideleftfill: {
    SmallVector<SDValue, 8> Operands(Op->op_begin(), Op->op_end());
    SmallVector<unsigned, 8> OpIdxs;
    if (IntNo == Intrinsic::experimental_vector_vp_slideleftfill) {
      OpIdxs = {3, 4, 5};
    } else if (IntNo == Intrinsic::experimental_vector_slideleftfill) {
      OpIdxs = {3};
    } else {
      llvm_unreachable("Unexpected intrinsic");
    }
    EVT Ty = Operands[1].getValueType();
    if (getPreferredVectorAction(Ty.getSimpleVT()) == TypeWidenVector) {
      EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Ty);
      Operands[1] =
          DAG.getNode(ISD::INSERT_SUBVECTOR, DL, WidenVT,
                      DAG.getNode(ISD::UNDEF, DL, WidenVT), Operands[1],
                      DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
      Operands[2] =
          DAG.getNode(ISD::INSERT_SUBVECTOR, DL, WidenVT,
                      DAG.getNode(ISD::UNDEF, DL, WidenVT), Operands[2],
                      DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
    }
    // offset, evl1, evl2
    for (auto OpIdx : OpIdxs) {
      SDValue &ScalarOp = Operands[OpIdx];
      EVT OpVT = ScalarOp.getValueType();
      if (OpVT == MVT::i8 || OpVT == MVT::i16 ||
          (OpVT == MVT::i32 && Subtarget.is64Bit())) {
        ScalarOp =
            DAG.getNode(ISD::ANY_EXTEND, DL, Subtarget.getXLenVT(), ScalarOp);
      }
    }
    return DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, Op.getValueType(),
                       Operands);
    break;
  }
  // We handle VP Intrinsics elsewhere.
#define VP_INTRINSIC(X, ...) case Intrinsic::X:
    VP_INTRINSIC_WO_CHAIN_SET
#undef VP_INTRINSIC
    return LowerVPIntrinsic(IntNo, Op, DAG, *this, Subtarget);
  }
}

SDValue RISCVTargetLowering::LowerINTRINSIC_W_CHAIN(SDValue Op,
                                                    SelectionDAG &DAG) const {
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(1))->getZExtValue();
  SDLoc DL(Op);
  switch (IntNo) {
    // By default we do not lower any intrinsic.
  default:
    break;
#define VP_INTRINSIC(X, ...) case Intrinsic::X:
    VP_INTRINSIC_W_CHAIN_SET
#undef VP_INTRINSIC
    return LowerVPIntrinsic(IntNo, Op, DAG, *this, Subtarget);
  }

  return SDValue();
}

SDValue RISCVTargetLowering::LowerINTRINSIC_VOID(SDValue Op,
                                                 SelectionDAG &DAG) const {
  unsigned IntNo = cast<ConstantSDNode>(Op.getOperand(1))->getZExtValue();
  SDLoc DL(Op);

  if (Subtarget.hasStdExtV()) {
    if (const RISCVEPIIntrinsicsTable::EPIIntrinsicInfo *EII =
            RISCVEPIIntrinsicsTable::getEPIIntrinsicInfo(IntNo)) {
      // Widen vector operands.
      std::vector<SDValue> Operands(Op->op_begin(), Op->op_end());
      bool Changed = false;
      for (SDValue &Operand : Operands) {
        if (Operand.getValueType().isScalableVector() &&
            getPreferredVectorAction(Operand.getValueType().getSimpleVT()) ==
                TypeWidenVector) {
          EVT WidenVT =
              getTypeToTransformTo(*DAG.getContext(), Operand.getValueType());
          Operand =
              DAG.getNode(ISD::INSERT_SUBVECTOR, DL, WidenVT,
                          DAG.getNode(ISD::UNDEF, DL, WidenVT), Operand,
                          DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
          Changed = true;
        }
      }

      if (Changed)
        return DAG.getNode(ISD::INTRINSIC_VOID, DL, Op->getVTList(), Operands);
    }
  }

  switch (IntNo) {
    // By default we do not lower any intrinsic.
  default:
    break;
  // We handle VP Intrinsics elsewhere.
#define VP_INTRINSIC(X, ...) case Intrinsic::X:
    VP_INTRINSIC_VOID_SET
#undef VP_INTRINSIC
    return LowerVPIntrinsic(IntNo, Op, DAG, *this, Subtarget);
  }

  return SDValue();
}

// Returns the opcode of the target-specific SDNode that implements the 32-bit
// form of the given Opcode.
static RISCVISD::NodeType getRISCVWOpcode(unsigned Opcode) {
  switch (Opcode) {
  default:
    llvm_unreachable("Unexpected opcode");
  case ISD::SHL:
    return RISCVISD::SLLW;
  case ISD::SRA:
    return RISCVISD::SRAW;
  case ISD::SRL:
    return RISCVISD::SRLW;
  case ISD::SDIV:
    return RISCVISD::DIVW;
  case ISD::UDIV:
    return RISCVISD::DIVUW;
  case ISD::UREM:
    return RISCVISD::REMUW;
  case ISD::ROTL:
    return RISCVISD::ROLW;
  case ISD::ROTR:
    return RISCVISD::RORW;
  case RISCVISD::GREVI:
    return RISCVISD::GREVIW;
  case RISCVISD::GORCI:
    return RISCVISD::GORCIW;
  }
}

// Converts the given 32-bit operation to a target-specific SelectionDAG node.
// Because i32 isn't a legal type for RV64, these operations would otherwise
// be promoted to i64, making it difficult to select the SLLW/DIVUW/.../*W
// later one because the fact the operation was originally of type i32 is
// lost.
static SDValue customLegalizeToWOp(SDNode *N, SelectionDAG &DAG) {
  SDLoc DL(N);
  RISCVISD::NodeType WOpcode = getRISCVWOpcode(N->getOpcode());
  SDValue NewOp0 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(0));
  SDValue NewOp1 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(1));
  SDValue NewRes = DAG.getNode(WOpcode, DL, MVT::i64, NewOp0, NewOp1);
  // ReplaceNodeResults requires we maintain the same type for the return value.
  return DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, NewRes);
}

// Converts the given 32-bit operation to a i64 operation with signed extension
// semantic to reduce the signed extension instructions.
static SDValue customLegalizeToWOpWithSExt(SDNode *N, SelectionDAG &DAG) {
  SDLoc DL(N);
  SDValue NewOp0 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(0));
  SDValue NewOp1 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(1));
  SDValue NewWOp = DAG.getNode(N->getOpcode(), DL, MVT::i64, NewOp0, NewOp1);
  SDValue NewRes = DAG.getNode(ISD::SIGN_EXTEND_INREG, DL, MVT::i64, NewWOp,
                               DAG.getValueType(MVT::i32));
  return DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, NewRes);
}

void RISCVTargetLowering::ReplaceNodeResults(SDNode *N,
                                             SmallVectorImpl<SDValue> &Results,
                                             SelectionDAG &DAG) const {
  SDLoc DL(N);
  switch (N->getOpcode()) {
  default:
    llvm_unreachable("Don't know how to custom type legalize this operation!");
  case ISD::STRICT_FP_TO_SINT:
  case ISD::STRICT_FP_TO_UINT:
  case ISD::FP_TO_SINT:
  case ISD::FP_TO_UINT: {
    bool IsStrict = N->isStrictFPOpcode();
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           "Unexpected custom legalisation");
    SDValue Op0 = IsStrict ? N->getOperand(1) : N->getOperand(0);
    // If the FP type needs to be softened, emit a library call using the 'si'
    // version. If we left it to default legalization we'd end up with 'di'. If
    // the FP type doesn't need to be softened just let generic type
    // legalization promote the result type.
    if (getTypeAction(*DAG.getContext(), Op0.getValueType()) !=
        TargetLowering::TypeSoftenFloat)
      return;
    RTLIB::Libcall LC;
    if (N->getOpcode() == ISD::FP_TO_SINT ||
        N->getOpcode() == ISD::STRICT_FP_TO_SINT)
      LC = RTLIB::getFPTOSINT(Op0.getValueType(), N->getValueType(0));
    else
      LC = RTLIB::getFPTOUINT(Op0.getValueType(), N->getValueType(0));
    MakeLibCallOptions CallOptions;
    EVT OpVT = Op0.getValueType();
    CallOptions.setTypeListBeforeSoften(OpVT, N->getValueType(0), true);
    SDValue Chain = IsStrict ? N->getOperand(0) : SDValue();
    SDValue Result;
    std::tie(Result, Chain) =
        makeLibCall(DAG, LC, N->getValueType(0), Op0, CallOptions, DL, Chain);
    Results.push_back(Result);
    if (IsStrict)
      Results.push_back(Chain);
    break;
  }
  case ISD::READCYCLECOUNTER: {
    assert(!Subtarget.is64Bit() &&
           "READCYCLECOUNTER only has custom type legalization on riscv32");

    SDVTList VTs = DAG.getVTList(MVT::i32, MVT::i32, MVT::Other);
    SDValue RCW =
        DAG.getNode(RISCVISD::READ_CYCLE_WIDE, DL, VTs, N->getOperand(0));

    Results.push_back(
        DAG.getNode(ISD::BUILD_PAIR, DL, MVT::i64, RCW, RCW.getValue(1)));
    Results.push_back(RCW.getValue(2));
    break;
  }
  case ISD::ADD:
  case ISD::SUB:
  case ISD::MUL:
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           "Unexpected custom legalisation");
    Results.push_back(customLegalizeToWOpWithSExt(N, DAG));
    break;
  case ISD::SHL:
  case ISD::SRA:
  case ISD::SRL:
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           "Unexpected custom legalisation");
    if (N->getOperand(1).getOpcode() == ISD::Constant)
      return;
    Results.push_back(customLegalizeToWOp(N, DAG));
    break;
  case ISD::ROTL:
  case ISD::ROTR:
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           "Unexpected custom legalisation");
    Results.push_back(customLegalizeToWOp(N, DAG));
    break;
  case ISD::SDIV:
  case ISD::UDIV:
  case ISD::UREM:
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           Subtarget.hasStdExtM() && "Unexpected custom legalisation");
    if (N->getOperand(0).getOpcode() == ISD::Constant ||
        N->getOperand(1).getOpcode() == ISD::Constant)
      return;
    Results.push_back(customLegalizeToWOp(N, DAG));
    break;
  case ISD::BITCAST: {
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           Subtarget.hasStdExtF() && "Unexpected custom legalisation");
    SDValue Op0 = N->getOperand(0);
    if (Op0.getValueType() != MVT::f32)
      return;
    SDValue FPConv =
        DAG.getNode(RISCVISD::FMV_X_ANYEXTW_RV64, DL, MVT::i64, Op0);
    Results.push_back(DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, FPConv));
    break;
  }
  case RISCVISD::GREVI:
  case RISCVISD::GORCI: {
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           "Unexpected custom legalisation");
    // This is similar to customLegalizeToWOp, except that we pass the second
    // operand (a TargetConstant) straight through: it is already of type
    // XLenVT.
    SDLoc DL(N);
    RISCVISD::NodeType WOpcode = getRISCVWOpcode(N->getOpcode());
    SDValue NewOp0 =
        DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(0));
    SDValue NewRes =
        DAG.getNode(WOpcode, DL, MVT::i64, NewOp0, N->getOperand(1));
    // ReplaceNodeResults requires we maintain the same type for the return
    // value.
    Results.push_back(DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, NewRes));
    break;
  }
  case ISD::BSWAP:
  case ISD::BITREVERSE: {
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           Subtarget.hasStdExtZbp() && "Unexpected custom legalisation");
    SDValue NewOp0 = DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64,
                                 N->getOperand(0));
    unsigned Imm = N->getOpcode() == ISD::BITREVERSE ? 31 : 24;
    SDValue GREVIW = DAG.getNode(RISCVISD::GREVIW, DL, MVT::i64, NewOp0,
                                 DAG.getTargetConstant(Imm, DL,
                                                       Subtarget.getXLenVT()));
    // ReplaceNodeResults requires we maintain the same type for the return
    // value.
    Results.push_back(DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, GREVIW));
    break;
  }
  case ISD::FSHL:
  case ISD::FSHR: {
    assert(N->getValueType(0) == MVT::i32 && Subtarget.is64Bit() &&
           Subtarget.hasStdExtZbt() && "Unexpected custom legalisation");
    SDValue NewOp0 =
        DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(0));
    SDValue NewOp1 =
        DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(1));
    SDValue NewOp2 =
        DAG.getNode(ISD::ANY_EXTEND, DL, MVT::i64, N->getOperand(2));
    // FSLW/FSRW take a 6 bit shift amount but i32 FSHL/FSHR only use 5 bits.
    // Mask the shift amount to 5 bits.
    NewOp2 = DAG.getNode(ISD::AND, DL, MVT::i64, NewOp2,
                         DAG.getConstant(0x1f, DL, MVT::i64));
    unsigned Opc =
        N->getOpcode() == ISD::FSHL ? RISCVISD::FSLW : RISCVISD::FSRW;
    SDValue NewOp = DAG.getNode(Opc, DL, MVT::i64, NewOp0, NewOp1, NewOp2);
    Results.push_back(DAG.getNode(ISD::TRUNCATE, DL, MVT::i32, NewOp));
    break;
  }
  case ISD::INTRINSIC_WO_CHAIN: {
    unsigned IntNo = cast<ConstantSDNode>(N->getOperand(0))->getZExtValue();
    switch (IntNo) {
    default:
      llvm_unreachable(
          "Don't know how to custom type legalize this intrinsic!");
    case Intrinsic::vscale: {
      EVT Ty = N->getValueType(0);
      switch (Ty.getSimpleVT().SimpleTy) {
      default:
        llvm_unreachable("Unexpected result type to legalize");
      case MVT::i32:
      case MVT::i16:
      case MVT::i8:
        SDValue Promoted = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, MVT::i64,
                                       DAG.getConstant(IntNo, DL, MVT::i64));
        SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, Ty, Promoted);
        Results.push_back(Trunc);
        break;
      }
      break;
    }
    case Intrinsic::epi_vfmv_f_s: {
      auto NodeVT = N->getValueType(0);

      SDValue Extract64 =
          DAG.getNode(RISCVISD::VEXT_X_V, DL, MVT::i64, N->getOperand(1),
                      DAG.getConstant(0, DL, MVT::i64));

      assert((NodeVT == MVT::f16 || NodeVT == MVT::bf16 || NodeVT == MVT::f8_143 || NodeVT == MVT::f8_152) && "Node type error for intrinsic epi_vfmv_f_s");
      auto TruncVT =
          (NodeVT == MVT::f16 || NodeVT == MVT::bf16) ? MVT::i16 : MVT::i8;
      SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, TruncVT, Extract64);

      SDValue Bitcast = DAG.getNode(ISD::BITCAST, DL, N->getValueType(0), Trunc);

      Results.push_back(Bitcast);

      break;
    }
    case Intrinsic::epi_vext_x_v: {
      EVT Ty = N->getValueType(0);
      MVT::SimpleValueType SimpleVT = Ty.getSimpleVT().SimpleTy;
      assert(SimpleVT == MVT::i8 || SimpleVT == MVT::i16 ||
             SimpleVT == MVT::i32);

      SDValue Extract64 = DAG.getNode(RISCVISD::VEXT_X_V, DL, MVT::i64,
                                      N->getOperand(1), N->getOperand(2));
      SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, Ty, Extract64);
      Results.push_back(Trunc);
      break;
    }
    case Intrinsic::experimental_vector_stepvector: {
      EVT Ty = N->getValueType(0);
      if (getPreferredVectorAction(Ty.getSimpleVT()) == TypeWidenVector) {
        EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Ty);
        SDValue NewIntrinsic =
            DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, WidenVT, N->ops());
        SDValue Extract =
            DAG.getNode(ISD::EXTRACT_SUBVECTOR, DL, Ty, NewIntrinsic,
                        DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
        Results.push_back(Extract);
      }
      break;
    }
    case Intrinsic::experimental_vector_vp_slideleftfill: {
      assert(Subtarget.onlyLMUL1() && "This is expected only lmul1");
      EVT Ty = N->getValueType(0);
      assert(Ty.isScalableVector() && "Expecting a scalable type");
      if (getPreferredVectorAction(Ty.getSimpleVT()) == TypeWidenVector) {
        EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Ty);

        SmallVector<SDValue, 4> WidenOps{N->op_values()};
        SDValue NewIntrinsic =
            DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, WidenVT, WidenOps);
        SDValue Extract =
            DAG.getNode(ISD::EXTRACT_SUBVECTOR, DL, Ty, NewIntrinsic,
                        DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
        Results.push_back(Extract);
      }
      break;
    }
#define VP_INTRINSIC(X, ...) case Intrinsic::X:
      VP_INTRINSIC_WO_CHAIN_SET
#undef VP_INTRINSIC
      // VP intrinsics
      {
        EVT Ty = N->getValueType(0);
        assert(Ty.isScalableVector() && "Expecting a scalable type");

        // Trunc is complicated, so decompose it first, if needed.
        if (IntNo == Intrinsic::vp_trunc &&
            getPreferredVectorAction(Ty.getSimpleVT()) == TypeWidenVector) {
          EVT DstType = N->getValueType(0);
          uint64_t DstTypeSize = DstType.getScalarSizeInBits();
          SDValue SrcOp = N->getOperand(1);
          EVT SrcType = SrcOp.getValueType();
          uint64_t SrcTypeSize = SrcType.getScalarSizeInBits();
          int Ratio = SrcTypeSize / DstTypeSize;
          // Decompose in several vp_trunc
          if (Ratio > 2) {
            auto NarrowVectorElementType = [&](EVT SrcVT) -> EVT {
              EVT SrcEltVT = SrcVT.getVectorElementType();
              const ElementCount EltCount = SrcVT.getVectorElementCount();
              MVT ToEltVT;
              if (DstType.isInteger()) {
                ToEltVT = MVT::getIntegerVT(SrcEltVT.getSizeInBits() / 2);
              } else {
                llvm_unreachable(
                    "Vector Element type must be Integer");
              }
              return EVT::getVectorVT(*DAG.getContext(), ToEltVT, EltCount);
            };
            SDValue Trunc = N->getOperand(1);
            EVT NarrowType = Trunc.getValueType();

            while (Ratio >= 2) {
              NarrowType = NarrowVectorElementType(NarrowType);
              Trunc = DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, NarrowType,
                                  {N->getOperand(0), Trunc, N->getOperand(2),
                                   N->getOperand(3)});
              Ratio /= 2;
            }

            Results.push_back(Trunc);
            // We are done now.
            break;
          }
        }

        if (getPreferredVectorAction(Ty.getSimpleVT()) ==
                   TypeWidenVector) {
          EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Ty);

          SmallVector<SDValue, 4> WidenOps{N->op_values()};
          SDValue &Mask = WidenOps[VPGetMaskIdx(IntNo)];
          Mask = AdjustMaskElements(WidenVT, Mask, Subtarget, DAG);

          SDValue NewIntrinsic = LowerINTRINSIC_WO_CHAIN(
              DAG.getNode(ISD::INTRINSIC_WO_CHAIN, DL, WidenVT, WidenOps), DAG);
          SDValue Extract =
              DAG.getNode(ISD::EXTRACT_SUBVECTOR, DL, Ty, NewIntrinsic,
                          DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
          Results.push_back(Extract);
        }
        break;
      }
    }
    break;
  }
  case ISD::EXTRACT_VECTOR_ELT: {
    EVT Ty = N->getValueType(0);
    MVT::SimpleValueType SimpleVT = Ty.getSimpleVT().SimpleTy;
    assert(SimpleVT == MVT::i8 || SimpleVT == MVT::i16 || SimpleVT == MVT::i32);
    SDValue Op = N->getOperand(0);
    if (getPreferredVectorAction(Op.getValueType().getSimpleVT()) ==
        TypeWidenVector) {
      EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Op.getValueType());
      Op = DAG.getNode(ISD::INSERT_SUBVECTOR, DL, WidenVT,
                       DAG.getNode(ISD::UNDEF, DL, WidenVT), Op,
                       DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));
    }
    SDValue Extract64 = DAG.getNode(RISCVISD::EXTRACT_VECTOR_ELT, DL, MVT::i64,
                                    Op, N->getOperand(1));
    SDValue Trunc = DAG.getNode(ISD::TRUNCATE, DL, Ty, Extract64);
    Results.push_back(Trunc);
    break;
  }
  case ISD::EXTRACT_SUBVECTOR: {
    unsigned Idx = cast<ConstantSDNode>(N->getOperand(1))->getZExtValue();
    if (Idx == 0)
      return;
    // Original input type (might be invalid)
    EVT InTy = N->getOperand(0)->getValueType(0);
    EVT Ty = N->getValueType(0);
    assert(Ty.getVectorElementType() != MVT::i1 && "Not implemented yet");

    SDValue InputVector = N->getOperand(0);
    EVT SlideTy = InTy;
    if (getPreferredVectorAction(InTy.getSimpleVT()) == TypeWidenVector) {
      // Legalize the input for the slide operation if needed.
      SlideTy = getTypeToTransformTo(*DAG.getContext(), InTy);
      InputVector =
          GetExtractOrInsertSubvector(InputVector, SlideTy, DAG, Subtarget);
    }

    // Slide down as many positions as needed.
    SDValue VLMaxOut =
        DAG.getVScale(DL, MVT::i64, APInt(64, Ty.getVectorMinNumElements()));
    assert(Idx % Ty.getVectorMinNumElements() == 0 &&
            "Invalid Idx for EXTRACT_SUBVECTOR");
    // Scale down the index based on the result type.
    Idx /= Ty.getVectorMinNumElements();
    SDValue Offset =
        DAG.getNode(ISD::MUL, DL, MVT::i64,
                    {VLMaxOut, DAG.getTargetConstant(Idx, DL, MVT::i64)});
    SDValue VLMaxIn =
        DAG.getVScale(DL, MVT::i64, APInt(64, InTy.getVectorMinNumElements()));
    SDValue SlideDown = DAG.getNode(
        ISD::INTRINSIC_WO_CHAIN, DL, SlideTy,
        {DAG.getTargetConstant(Intrinsic::epi_vslidedown, DL, MVT::i64),
         InputVector, Offset, VLMaxIn});

    // Now extract the 0th subvector.
    SlideDown = GetExtractOrInsertSubvector(SlideDown, Ty, DAG, Subtarget);

    Results.push_back(SlideDown);
    break;
  }
  case ISD::INTRINSIC_W_CHAIN: {
    unsigned IntNo = cast<ConstantSDNode>(N->getOperand(1))->getZExtValue();
    switch (IntNo) {
    case Intrinsic::vp_load:
    case Intrinsic::vp_strided_load: {
      EVT Ty = N->getValueType(0);
      EVT ChainTy = N->getValueType(1);
      assert(Ty.isScalableVector() && "Expecting a scalable type");
      if (getPreferredVectorAction(Ty.getSimpleVT()) == TypeWidenVector) {
        EVT WidenVT = getTypeToTransformTo(*DAG.getContext(), Ty);

        SmallVector<SDValue, 4> ops(N->op_begin(), N->op_end());
        SDValue &Mask = ops[VPGetMaskIdx(IntNo)];
        Mask = AdjustMaskElements(WidenVT, Mask, Subtarget, DAG);

        SDValue NewIntrinsic = LowerINTRINSIC_W_CHAIN(
            DAG.getNode(ISD::INTRINSIC_W_CHAIN, DL,
                        DAG.getVTList(WidenVT, ChainTy), ops),
            DAG);

        SDValue Extract =
            DAG.getNode(ISD::EXTRACT_SUBVECTOR, DL, Ty, NewIntrinsic,
                        DAG.getTargetConstant(0, DL, Subtarget.getXLenVT()));

        Results.push_back(Extract);
        Results.push_back(NewIntrinsic.getValue(1));
      }
      break;
    }
    default:
      llvm_unreachable("Don't know how to custom type legalize the result of "
                       "this intrinsic!");
    }
    break;
  }
  }
}

// A structure to hold one of the bit-manipulation patterns below. Together, a
// SHL and non-SHL pattern may form a bit-manipulation pair on a single source:
//   (or (and (shl x, 1), 0xAAAAAAAA),
//       (and (srl x, 1), 0x55555555))
struct RISCVBitmanipPat {
  SDValue Op;
  unsigned ShAmt;
  bool IsSHL;

  bool formsPairWith(const RISCVBitmanipPat &Other) const {
    return Op == Other.Op && ShAmt == Other.ShAmt && IsSHL != Other.IsSHL;
  }
};

// Matches any of the following bit-manipulation patterns:
//   (and (shl x, 1), (0x55555555 << 1))
//   (and (srl x, 1), 0x55555555)
//   (shl (and x, 0x55555555), 1)
//   (srl (and x, (0x55555555 << 1)), 1)
// where the shift amount and mask may vary thus:
//   [1]  = 0x55555555 / 0xAAAAAAAA
//   [2]  = 0x33333333 / 0xCCCCCCCC
//   [4]  = 0x0F0F0F0F / 0xF0F0F0F0
//   [8]  = 0x00FF00FF / 0xFF00FF00
//   [16] = 0x0000FFFF / 0xFFFFFFFF
//   [32] = 0x00000000FFFFFFFF / 0xFFFFFFFF00000000 (for RV64)
static Optional<RISCVBitmanipPat> matchRISCVBitmanipPat(SDValue Op) {
  Optional<uint64_t> Mask;
  // Optionally consume a mask around the shift operation.
  if (Op.getOpcode() == ISD::AND && isa<ConstantSDNode>(Op.getOperand(1))) {
    Mask = Op.getConstantOperandVal(1);
    Op = Op.getOperand(0);
  }
  if (Op.getOpcode() != ISD::SHL && Op.getOpcode() != ISD::SRL)
    return None;
  bool IsSHL = Op.getOpcode() == ISD::SHL;

  if (!isa<ConstantSDNode>(Op.getOperand(1)))
    return None;
  auto ShAmt = Op.getConstantOperandVal(1);

  if (!isPowerOf2_64(ShAmt))
    return None;

  // These are the unshifted masks which we use to match bit-manipulation
  // patterns. They may be shifted left in certain circumstances.
  static const uint64_t BitmanipMasks[] = {
      0x5555555555555555ULL, 0x3333333333333333ULL, 0x0F0F0F0F0F0F0F0FULL,
      0x00FF00FF00FF00FFULL, 0x0000FFFF0000FFFFULL, 0x00000000FFFFFFFFULL,
  };

  unsigned MaskIdx = Log2_64(ShAmt);
  if (MaskIdx >= array_lengthof(BitmanipMasks))
    return None;

  auto Src = Op.getOperand(0);

  unsigned Width = Op.getValueType() == MVT::i64 ? 64 : 32;
  auto ExpMask = BitmanipMasks[MaskIdx] & maskTrailingOnes<uint64_t>(Width);

  // The expected mask is shifted left when the AND is found around SHL
  // patterns.
  //   ((x >> 1) & 0x55555555)
  //   ((x << 1) & 0xAAAAAAAA)
  bool SHLExpMask = IsSHL;

  if (!Mask) {
    // Sometimes LLVM keeps the mask as an operand of the shift, typically when
    // the mask is all ones: consume that now.
    if (Src.getOpcode() == ISD::AND && isa<ConstantSDNode>(Src.getOperand(1))) {
      Mask = Src.getConstantOperandVal(1);
      Src = Src.getOperand(0);
      // The expected mask is now in fact shifted left for SRL, so reverse the
      // decision.
      //   ((x & 0xAAAAAAAA) >> 1)
      //   ((x & 0x55555555) << 1)
      SHLExpMask = !SHLExpMask;
    } else {
      // Use a default shifted mask of all-ones if there's no AND, truncated
      // down to the expected width. This simplifies the logic later on.
      Mask = maskTrailingOnes<uint64_t>(Width);
      *Mask &= (IsSHL ? *Mask << ShAmt : *Mask >> ShAmt);
    }
  }

  if (SHLExpMask)
    ExpMask <<= ShAmt;

  if (Mask != ExpMask)
    return None;

  return RISCVBitmanipPat{Src, (unsigned)ShAmt, IsSHL};
}

// Match the following pattern as a GREVI(W) operation
//   (or (BITMANIP_SHL x), (BITMANIP_SRL x))
static SDValue combineORToGREV(SDValue Op, SelectionDAG &DAG,
                               const RISCVSubtarget &Subtarget) {
  EVT VT = Op.getValueType();

  if (VT == Subtarget.getXLenVT() || (Subtarget.is64Bit() && VT == MVT::i32)) {
    auto LHS = matchRISCVBitmanipPat(Op.getOperand(0));
    auto RHS = matchRISCVBitmanipPat(Op.getOperand(1));
    if (LHS && RHS && LHS->formsPairWith(*RHS)) {
      SDLoc DL(Op);
      return DAG.getNode(
          RISCVISD::GREVI, DL, VT, LHS->Op,
          DAG.getTargetConstant(LHS->ShAmt, DL, Subtarget.getXLenVT()));
    }
  }
  return SDValue();
}

// Matches any the following pattern as a GORCI(W) operation
// 1.  (or (GREVI x, shamt), x)
// 2.  (or x, (GREVI x, shamt))
// 3.  (or (or (BITMANIP_SHL x), x), (BITMANIP_SRL x))
// Note that with the variant of 3.,
//     (or (or (BITMANIP_SHL x), (BITMANIP_SRL x)), x)
// the inner pattern will first be matched as GREVI and then the outer
// pattern will be matched to GORC via the first rule above.
static SDValue combineORToGORC(SDValue Op, SelectionDAG &DAG,
                               const RISCVSubtarget &Subtarget) {
  EVT VT = Op.getValueType();

  if (VT == Subtarget.getXLenVT() || (Subtarget.is64Bit() && VT == MVT::i32)) {
    SDLoc DL(Op);
    SDValue Op0 = Op.getOperand(0);
    SDValue Op1 = Op.getOperand(1);

    // Check for either commutable permutation of (or (GREVI x, shamt), x)
    for (const auto &OpPair :
         {std::make_pair(Op0, Op1), std::make_pair(Op1, Op0)}) {
      if (OpPair.first.getOpcode() == RISCVISD::GREVI &&
          OpPair.first.getOperand(0) == OpPair.second)
        return DAG.getNode(RISCVISD::GORCI, DL, VT, OpPair.second,
                           OpPair.first.getOperand(1));
    }

    // OR is commutable so canonicalize its OR operand to the left
    if (Op0.getOpcode() != ISD::OR && Op1.getOpcode() == ISD::OR)
      std::swap(Op0, Op1);
    if (Op0.getOpcode() != ISD::OR)
      return SDValue();
    SDValue OrOp0 = Op0.getOperand(0);
    SDValue OrOp1 = Op0.getOperand(1);
    auto LHS = matchRISCVBitmanipPat(OrOp0);
    // OR is commutable so swap the operands and try again: x might have been
    // on the left
    if (!LHS) {
      std::swap(OrOp0, OrOp1);
      LHS = matchRISCVBitmanipPat(OrOp0);
    }
    auto RHS = matchRISCVBitmanipPat(Op1);
    if (LHS && RHS && LHS->formsPairWith(*RHS) && LHS->Op == OrOp1) {
      return DAG.getNode(
          RISCVISD::GORCI, DL, VT, LHS->Op,
          DAG.getTargetConstant(LHS->ShAmt, DL, Subtarget.getXLenVT()));
    }
  }
  return SDValue();
}

SDValue RISCVTargetLowering::PerformDAGCombine(SDNode *N,
                                               DAGCombinerInfo &DCI) const {
  SelectionDAG &DAG = DCI.DAG;

  switch (N->getOpcode()) {
  default:
    break;
  case RISCVISD::SplitF64: {
    SDValue Op0 = N->getOperand(0);
    // If the input to SplitF64 is just BuildPairF64 then the operation is
    // redundant. Instead, use BuildPairF64's operands directly.
    if (Op0->getOpcode() == RISCVISD::BuildPairF64)
      return DCI.CombineTo(N, Op0.getOperand(0), Op0.getOperand(1));

    SDLoc DL(N);

    // It's cheaper to materialise two 32-bit integers than to load a double
    // from the constant pool and transfer it to integer registers through the
    // stack.
    if (ConstantFPSDNode *C = dyn_cast<ConstantFPSDNode>(Op0)) {
      APInt V = C->getValueAPF().bitcastToAPInt();
      SDValue Lo = DAG.getConstant(V.trunc(32), DL, MVT::i32);
      SDValue Hi = DAG.getConstant(V.lshr(32).trunc(32), DL, MVT::i32);
      return DCI.CombineTo(N, Lo, Hi);
    }

    // This is a target-specific version of a DAGCombine performed in
    // DAGCombiner::visitBITCAST. It performs the equivalent of:
    // fold (bitconvert (fneg x)) -> (xor (bitconvert x), signbit)
    // fold (bitconvert (fabs x)) -> (and (bitconvert x), (not signbit))
    if (!(Op0.getOpcode() == ISD::FNEG || Op0.getOpcode() == ISD::FABS) ||
        !Op0.getNode()->hasOneUse())
      break;
    SDValue NewSplitF64 =
        DAG.getNode(RISCVISD::SplitF64, DL, DAG.getVTList(MVT::i32, MVT::i32),
                    Op0.getOperand(0));
    SDValue Lo = NewSplitF64.getValue(0);
    SDValue Hi = NewSplitF64.getValue(1);
    APInt SignBit = APInt::getSignMask(32);
    if (Op0.getOpcode() == ISD::FNEG) {
      SDValue NewHi = DAG.getNode(ISD::XOR, DL, MVT::i32, Hi,
                                  DAG.getConstant(SignBit, DL, MVT::i32));
      return DCI.CombineTo(N, Lo, NewHi);
    }
    assert(Op0.getOpcode() == ISD::FABS);
    SDValue NewHi = DAG.getNode(ISD::AND, DL, MVT::i32, Hi,
                                DAG.getConstant(~SignBit, DL, MVT::i32));
    return DCI.CombineTo(N, Lo, NewHi);
  }
  case RISCVISD::SLLW:
  case RISCVISD::SRAW:
  case RISCVISD::SRLW:
  case RISCVISD::ROLW:
  case RISCVISD::RORW: {
    // Only the lower 32 bits of LHS and lower 5 bits of RHS are read.
    SDValue LHS = N->getOperand(0);
    SDValue RHS = N->getOperand(1);
    APInt LHSMask = APInt::getLowBitsSet(LHS.getValueSizeInBits(), 32);
    APInt RHSMask = APInt::getLowBitsSet(RHS.getValueSizeInBits(), 5);
    if (SimplifyDemandedBits(N->getOperand(0), LHSMask, DCI) ||
        SimplifyDemandedBits(N->getOperand(1), RHSMask, DCI)) {
      if (N->getOpcode() != ISD::DELETED_NODE)
        DCI.AddToWorklist(N);
      return SDValue(N, 0);
    }
    break;
  }
  case RISCVISD::FSLW:
  case RISCVISD::FSRW: {
    // Only the lower 32 bits of Values and lower 6 bits of shift amount are
    // read.
    SDValue Op0 = N->getOperand(0);
    SDValue Op1 = N->getOperand(1);
    SDValue ShAmt = N->getOperand(2);
    APInt OpMask = APInt::getLowBitsSet(Op0.getValueSizeInBits(), 32);
    APInt ShAmtMask = APInt::getLowBitsSet(ShAmt.getValueSizeInBits(), 6);
    if (SimplifyDemandedBits(Op0, OpMask, DCI) ||
        SimplifyDemandedBits(Op1, OpMask, DCI) ||
        SimplifyDemandedBits(ShAmt, ShAmtMask, DCI)) {
      if (N->getOpcode() != ISD::DELETED_NODE)
        DCI.AddToWorklist(N);
      return SDValue(N, 0);
    }
    break;
  }
  case RISCVISD::GREVIW:
  case RISCVISD::GORCIW: {
    // Only the lower 32 bits of the first operand are read
    SDValue Op0 = N->getOperand(0);
    APInt Mask = APInt::getLowBitsSet(Op0.getValueSizeInBits(), 32);
    if (SimplifyDemandedBits(Op0, Mask, DCI)) {
      if (N->getOpcode() != ISD::DELETED_NODE)
        DCI.AddToWorklist(N);
      return SDValue(N, 0);
    }
    break;
  }
  case RISCVISD::FMV_X_ANYEXTW_RV64: {
    SDLoc DL(N);
    SDValue Op0 = N->getOperand(0);
    // If the input to FMV_X_ANYEXTW_RV64 is just FMV_W_X_RV64 then the
    // conversion is unnecessary and can be replaced with an ANY_EXTEND
    // of the FMV_W_X_RV64 operand.
    if (Op0->getOpcode() == RISCVISD::FMV_W_X_RV64) {
      assert(Op0.getOperand(0).getValueType() == MVT::i64 &&
             "Unexpected value type!");
      return Op0.getOperand(0);
    }

    // This is a target-specific version of a DAGCombine performed in
    // DAGCombiner::visitBITCAST. It performs the equivalent of:
    // fold (bitconvert (fneg x)) -> (xor (bitconvert x), signbit)
    // fold (bitconvert (fabs x)) -> (and (bitconvert x), (not signbit))
    if (!(Op0.getOpcode() == ISD::FNEG || Op0.getOpcode() == ISD::FABS) ||
        !Op0.getNode()->hasOneUse())
      break;
    SDValue NewFMV = DAG.getNode(RISCVISD::FMV_X_ANYEXTW_RV64, DL, MVT::i64,
                                 Op0.getOperand(0));
    APInt SignBit = APInt::getSignMask(32).sext(64);
    if (Op0.getOpcode() == ISD::FNEG)
      return DAG.getNode(ISD::XOR, DL, MVT::i64, NewFMV,
                         DAG.getConstant(SignBit, DL, MVT::i64));

    assert(Op0.getOpcode() == ISD::FABS);
    return DAG.getNode(ISD::AND, DL, MVT::i64, NewFMV,
                       DAG.getConstant(~SignBit, DL, MVT::i64));
  }
  case RISCVISD::GREVI: {
    // Combine (GREVI (GREVI x, C2), C1) -> (GREVI x, C1^C2) when C1^C2 is
    // non-zero, and to x when it is. Any repeated GREVI stage undoes itself.
    SDLoc DL(N);
    auto GREVSrc = N->getOperand(0);
    uint64_t ShAmt1 = N->getConstantOperandVal(1);
    if (GREVSrc->getOpcode() != RISCVISD::GREVI)
      break;
    uint64_t ShAmt2 = GREVSrc.getConstantOperandVal(1);
    GREVSrc = GREVSrc->getOperand(0);
    uint64_t CombinedShAmt = ShAmt1 ^ ShAmt2;
    if (CombinedShAmt == 0)
      return GREVSrc;
    return DAG.getNode(
        RISCVISD::GREVI, DL, N->getValueType(0), GREVSrc,
        DAG.getTargetConstant(CombinedShAmt, DL, Subtarget.getXLenVT()));
  }
  case ISD::OR:
    if (auto GREV = combineORToGREV(SDValue(N, 0), DCI.DAG, Subtarget))
      return GREV;
    if (auto GORC = combineORToGORC(SDValue(N, 0), DCI.DAG, Subtarget))
      return GORC;
    break;
  }

  return SDValue();
}

bool RISCVTargetLowering::isDesirableToCommuteWithShift(
    const SDNode *N, CombineLevel Level) const {
  // The following folds are only desirable if `(OP _, c1 << c2)` can be
  // materialised in fewer instructions than `(OP _, c1)`:
  //
  //   (shl (add x, c1), c2) -> (add (shl x, c2), c1 << c2)
  //   (shl (or x, c1), c2) -> (or (shl x, c2), c1 << c2)
  SDValue N0 = N->getOperand(0);
  EVT Ty = N0.getValueType();
  if (Ty.isScalarInteger() &&
      (N0.getOpcode() == ISD::ADD || N0.getOpcode() == ISD::OR)) {
    auto *C1 = dyn_cast<ConstantSDNode>(N0->getOperand(1));
    auto *C2 = dyn_cast<ConstantSDNode>(N->getOperand(1));
    if (C1 && C2) {
      APInt C1Int = C1->getAPIntValue();
      APInt ShiftedC1Int = C1Int << C2->getAPIntValue();

      // We can materialise `c1 << c2` into an add immediate, so it's "free",
      // and the combine should happen, to potentially allow further combines
      // later.
      if (ShiftedC1Int.getMinSignedBits() <= 64 &&
          isLegalAddImmediate(ShiftedC1Int.getSExtValue()))
        return true;

      // We can materialise `c1` in an add immediate, so it's "free", and the
      // combine should be prevented.
      if (C1Int.getMinSignedBits() <= 64 &&
          isLegalAddImmediate(C1Int.getSExtValue()))
        return false;

      // Neither constant will fit into an immediate, so find materialisation
      // costs.
      int C1Cost = RISCVMatInt::getIntMatCost(C1Int, Ty.getSizeInBits(),
                                              Subtarget.is64Bit());
      int ShiftedC1Cost = RISCVMatInt::getIntMatCost(
          ShiftedC1Int, Ty.getSizeInBits(), Subtarget.is64Bit());

      // Materialising `c1` is cheaper than materialising `c1 << c2`, so the
      // combine should be prevented.
      if (C1Cost < ShiftedC1Cost)
        return false;
    }
  }
  return true;
}

unsigned RISCVTargetLowering::ComputeNumSignBitsForTargetNode(
    SDValue Op, const APInt &DemandedElts, const SelectionDAG &DAG,
    unsigned Depth) const {
  switch (Op.getOpcode()) {
  default:
    break;
  case RISCVISD::SLLW:
  case RISCVISD::SRAW:
  case RISCVISD::SRLW:
  case RISCVISD::DIVW:
  case RISCVISD::DIVUW:
  case RISCVISD::REMUW:
  case RISCVISD::ROLW:
  case RISCVISD::RORW:
  case RISCVISD::GREVIW:
  case RISCVISD::GORCIW:
  case RISCVISD::FSLW:
  case RISCVISD::FSRW:
    // TODO: As the result is sign-extended, this is conservatively correct. A
    // more precise answer could be calculated for SRAW depending on known
    // bits in the shift amount.
    return 33;
  case RISCVISD::VEXT_X_V:
  case RISCVISD::EXTRACT_VECTOR_ELT:
    unsigned XLen = DAG.getDataLayout().getLargestLegalIntTypeSizeInBits();
    // The number of sign bits of the scalar result is computed by obtaining the
    // element type of the input vector operand, substracting its width from the
    // XLEN, and then adding one (sign bit within the element type).
    return XLen -
           Op->getOperand(0)
               .getValueType()
               .getVectorElementType()
               .getSizeInBits() +
           1;
  }

  return 1;
}

static MachineBasicBlock *emitReadCycleWidePseudo(MachineInstr &MI,
                                                  MachineBasicBlock *BB) {
  assert(MI.getOpcode() == RISCV::ReadCycleWide && "Unexpected instruction");

  // To read the 64-bit cycle CSR on a 32-bit target, we read the two halves.
  // Should the count have wrapped while it was being read, we need to try
  // again.
  // ...
  // read:
  // rdcycleh x3 # load high word of cycle
  // rdcycle  x2 # load low word of cycle
  // rdcycleh x4 # load high word of cycle
  // bne x3, x4, read # check if high word reads match, otherwise try again
  // ...

  MachineFunction &MF = *BB->getParent();
  const BasicBlock *LLVM_BB = BB->getBasicBlock();
  MachineFunction::iterator It = ++BB->getIterator();

  MachineBasicBlock *LoopMBB = MF.CreateMachineBasicBlock(LLVM_BB);
  MF.insert(It, LoopMBB);

  MachineBasicBlock *DoneMBB = MF.CreateMachineBasicBlock(LLVM_BB);
  MF.insert(It, DoneMBB);

  // Transfer the remainder of BB and its successor edges to DoneMBB.
  DoneMBB->splice(DoneMBB->begin(), BB,
                  std::next(MachineBasicBlock::iterator(MI)), BB->end());
  DoneMBB->transferSuccessorsAndUpdatePHIs(BB);

  BB->addSuccessor(LoopMBB);

  MachineRegisterInfo &RegInfo = MF.getRegInfo();
  Register ReadAgainReg = RegInfo.createVirtualRegister(&RISCV::GPRRegClass);
  Register LoReg = MI.getOperand(0).getReg();
  Register HiReg = MI.getOperand(1).getReg();
  DebugLoc DL = MI.getDebugLoc();

  const TargetInstrInfo *TII = MF.getSubtarget().getInstrInfo();
  BuildMI(LoopMBB, DL, TII->get(RISCV::CSRRS), HiReg)
      .addImm(RISCVSysReg::lookupSysRegByName("CYCLEH")->Encoding)
      .addReg(RISCV::X0);
  BuildMI(LoopMBB, DL, TII->get(RISCV::CSRRS), LoReg)
      .addImm(RISCVSysReg::lookupSysRegByName("CYCLE")->Encoding)
      .addReg(RISCV::X0);
  BuildMI(LoopMBB, DL, TII->get(RISCV::CSRRS), ReadAgainReg)
      .addImm(RISCVSysReg::lookupSysRegByName("CYCLEH")->Encoding)
      .addReg(RISCV::X0);

  BuildMI(LoopMBB, DL, TII->get(RISCV::BNE))
      .addReg(HiReg)
      .addReg(ReadAgainReg)
      .addMBB(LoopMBB);

  LoopMBB->addSuccessor(LoopMBB);
  LoopMBB->addSuccessor(DoneMBB);

  MI.eraseFromParent();

  return DoneMBB;
}

static MachineBasicBlock *emitSplitF64Pseudo(MachineInstr &MI,
                                             MachineBasicBlock *BB) {
  assert(MI.getOpcode() == RISCV::SplitF64Pseudo && "Unexpected instruction");

  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  const TargetRegisterInfo *RI = MF.getSubtarget().getRegisterInfo();
  Register LoReg = MI.getOperand(0).getReg();
  Register HiReg = MI.getOperand(1).getReg();
  Register SrcReg = MI.getOperand(2).getReg();
  const TargetRegisterClass *SrcRC = &RISCV::FPR64RegClass;
  int FI = MF.getInfo<RISCVMachineFunctionInfo>()->getMoveF64FrameIndex(MF);

  TII.storeRegToStackSlot(*BB, MI, SrcReg, MI.getOperand(2).isKill(), FI, SrcRC,
                          RI);
  MachinePointerInfo MPI = MachinePointerInfo::getFixedStack(MF, FI);
  MachineMemOperand *MMOLo =
      MF.getMachineMemOperand(MPI, MachineMemOperand::MOLoad, 4, Align(8));
  MachineMemOperand *MMOHi = MF.getMachineMemOperand(
      MPI.getWithOffset(4), MachineMemOperand::MOLoad, 4, Align(8));
  BuildMI(*BB, MI, DL, TII.get(RISCV::LW), LoReg)
      .addFrameIndex(FI)
      .addImm(0)
      .addMemOperand(MMOLo);
  BuildMI(*BB, MI, DL, TII.get(RISCV::LW), HiReg)
      .addFrameIndex(FI)
      .addImm(4)
      .addMemOperand(MMOHi);
  MI.eraseFromParent(); // The pseudo instruction is gone now.
  return BB;
}

static MachineBasicBlock *emitBuildPairF64Pseudo(MachineInstr &MI,
                                                 MachineBasicBlock *BB) {
  assert(MI.getOpcode() == RISCV::BuildPairF64Pseudo &&
         "Unexpected instruction");

  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();
  const TargetRegisterInfo *RI = MF.getSubtarget().getRegisterInfo();
  Register DstReg = MI.getOperand(0).getReg();
  Register LoReg = MI.getOperand(1).getReg();
  Register HiReg = MI.getOperand(2).getReg();
  const TargetRegisterClass *DstRC = &RISCV::FPR64RegClass;
  int FI = MF.getInfo<RISCVMachineFunctionInfo>()->getMoveF64FrameIndex(MF);

  MachinePointerInfo MPI = MachinePointerInfo::getFixedStack(MF, FI);
  MachineMemOperand *MMOLo =
      MF.getMachineMemOperand(MPI, MachineMemOperand::MOStore, 4, Align(8));
  MachineMemOperand *MMOHi = MF.getMachineMemOperand(
      MPI.getWithOffset(4), MachineMemOperand::MOStore, 4, Align(8));
  BuildMI(*BB, MI, DL, TII.get(RISCV::SW))
      .addReg(LoReg, getKillRegState(MI.getOperand(1).isKill()))
      .addFrameIndex(FI)
      .addImm(0)
      .addMemOperand(MMOLo);
  BuildMI(*BB, MI, DL, TII.get(RISCV::SW))
      .addReg(HiReg, getKillRegState(MI.getOperand(2).isKill()))
      .addFrameIndex(FI)
      .addImm(4)
      .addMemOperand(MMOHi);
  TII.loadRegFromStackSlot(*BB, MI, DstReg, FI, DstRC, RI);
  MI.eraseFromParent(); // The pseudo instruction is gone now.
  return BB;
}

static bool isSelectPseudo(MachineInstr &MI) {
  switch (MI.getOpcode()) {
  default:
    return false;
  case RISCV::Select_GPR_Using_CC_GPR:
  case RISCV::Select_FPR32_Using_CC_GPR:
  case RISCV::Select_FPR64_Using_CC_GPR:
    return true;
  }
}

static MachineBasicBlock *emitSelectPseudo(MachineInstr &MI,
                                           MachineBasicBlock *BB) {
  // To "insert" Select_* instructions, we actually have to insert the triangle
  // control-flow pattern.  The incoming instructions know the destination vreg
  // to set, the condition code register to branch on, the true/false values to
  // select between, and the condcode to use to select the appropriate branch.
  //
  // We produce the following control flow:
  //     HeadMBB
  //     |  \
  //     |  IfFalseMBB
  //     | /
  //    TailMBB
  //
  // When we find a sequence of selects we attempt to optimize their emission
  // by sharing the control flow. Currently we only handle cases where we have
  // multiple selects with the exact same condition (same LHS, RHS and CC).
  // The selects may be interleaved with other instructions if the other
  // instructions meet some requirements we deem safe:
  // - They are debug instructions. Otherwise,
  // - They do not have side-effects, do not access memory and their inputs do
  //   not depend on the results of the select pseudo-instructions.
  // The TrueV/FalseV operands of the selects cannot depend on the result of
  // previous selects in the sequence.
  // These conditions could be further relaxed. See the X86 target for a
  // related approach and more information.
  Register LHS = MI.getOperand(1).getReg();
  Register RHS = MI.getOperand(2).getReg();
  auto CC = static_cast<ISD::CondCode>(MI.getOperand(3).getImm());

  SmallVector<MachineInstr *, 4> SelectDebugValues;
  SmallSet<Register, 4> SelectDests;
  SelectDests.insert(MI.getOperand(0).getReg());

  MachineInstr *LastSelectPseudo = &MI;

  for (auto E = BB->end(), SequenceMBBI = MachineBasicBlock::iterator(MI);
       SequenceMBBI != E; ++SequenceMBBI) {
    if (SequenceMBBI->isDebugInstr())
      continue;
    else if (isSelectPseudo(*SequenceMBBI)) {
      if (SequenceMBBI->getOperand(1).getReg() != LHS ||
          SequenceMBBI->getOperand(2).getReg() != RHS ||
          SequenceMBBI->getOperand(3).getImm() != CC ||
          SelectDests.count(SequenceMBBI->getOperand(4).getReg()) ||
          SelectDests.count(SequenceMBBI->getOperand(5).getReg()))
        break;
      LastSelectPseudo = &*SequenceMBBI;
      SequenceMBBI->collectDebugValues(SelectDebugValues);
      SelectDests.insert(SequenceMBBI->getOperand(0).getReg());
    } else {
      if (SequenceMBBI->hasUnmodeledSideEffects() ||
          SequenceMBBI->mayLoadOrStore())
        break;
      if (llvm::any_of(SequenceMBBI->operands(), [&](MachineOperand &MO) {
            return MO.isReg() && MO.isUse() && SelectDests.count(MO.getReg());
          }))
        break;
    }
  }

  const TargetInstrInfo &TII = *BB->getParent()->getSubtarget().getInstrInfo();
  const BasicBlock *LLVM_BB = BB->getBasicBlock();
  DebugLoc DL = MI.getDebugLoc();
  MachineFunction::iterator I = ++BB->getIterator();

  MachineBasicBlock *HeadMBB = BB;
  MachineFunction *F = BB->getParent();
  MachineBasicBlock *TailMBB = F->CreateMachineBasicBlock(LLVM_BB);
  MachineBasicBlock *IfFalseMBB = F->CreateMachineBasicBlock(LLVM_BB);

  F->insert(I, IfFalseMBB);
  F->insert(I, TailMBB);

  // Transfer debug instructions associated with the selects to TailMBB.
  for (MachineInstr *DebugInstr : SelectDebugValues) {
    TailMBB->push_back(DebugInstr->removeFromParent());
  }

  // Move all instructions after the sequence to TailMBB.
  TailMBB->splice(TailMBB->end(), HeadMBB,
                  std::next(LastSelectPseudo->getIterator()), HeadMBB->end());
  // Update machine-CFG edges by transferring all successors of the current
  // block to the new block which will contain the Phi nodes for the selects.
  TailMBB->transferSuccessorsAndUpdatePHIs(HeadMBB);
  // Set the successors for HeadMBB.
  HeadMBB->addSuccessor(IfFalseMBB);
  HeadMBB->addSuccessor(TailMBB);

  // Insert appropriate branch.
  unsigned Opcode = getBranchOpcodeForIntCondCode(CC);

  BuildMI(HeadMBB, DL, TII.get(Opcode)).addReg(LHS).addReg(RHS).addMBB(TailMBB);

  // IfFalseMBB just falls through to TailMBB.
  IfFalseMBB->addSuccessor(TailMBB);

  // Create PHIs for all of the select pseudo-instructions.
  auto SelectMBBI = MI.getIterator();
  auto SelectEnd = std::next(LastSelectPseudo->getIterator());
  auto InsertionPoint = TailMBB->begin();
  while (SelectMBBI != SelectEnd) {
    auto Next = std::next(SelectMBBI);
    if (isSelectPseudo(*SelectMBBI)) {
      // %Result = phi [ %TrueValue, HeadMBB ], [ %FalseValue, IfFalseMBB ]
      BuildMI(*TailMBB, InsertionPoint, SelectMBBI->getDebugLoc(),
              TII.get(RISCV::PHI), SelectMBBI->getOperand(0).getReg())
          .addReg(SelectMBBI->getOperand(4).getReg())
          .addMBB(HeadMBB)
          .addReg(SelectMBBI->getOperand(5).getReg())
          .addMBB(IfFalseMBB);
      SelectMBBI->eraseFromParent();
    }
    SelectMBBI = Next;
  }

  F->getProperties().reset(MachineFunctionProperties::Property::NoPHIs);
  return TailMBB;
}

static MachineBasicBlock *emitComputeVSCALE(MachineInstr &MI,
                                            MachineBasicBlock *BB) {
  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();

  Register DestReg = MI.getOperand(0).getReg();

  // VSCALE can be computed as VLMAX of ELEN, given that the scaling factor for
  // ELEN is '1'.
  MachineInstr &I =
      *BuildMI(*BB, MI, DL, TII.get(RISCV::PseudoVSETVLI), DestReg)
           .addReg(RISCV::X0)
           // FIXME - ELEN hardcoded to SEW=64.
           .addImm(/* e64,m1 */ 3 << 2);
  // Set VTYPE and VL as dead.
  I.getOperand(3).setIsDead();
  I.getOperand(4).setIsDead();

  // The pseudo instruction is gone now.
  MI.eraseFromParent();
  return BB;
}

static MachineBasicBlock *emitComputeVMSET(MachineInstr &MI,
                                           MachineBasicBlock *BB) {
  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();

  Register DestReg = MI.getOperand(0).getReg();
  unsigned SEW = MI.getOperand(2).getImm();

  BuildMI(*BB, MI, DL, TII.get(RISCV::PseudoVMXNOR_MM_M1), DestReg)
      .addReg(DestReg, RegState::Undef)
      .addReg(DestReg, RegState::Undef)
      .addReg(MI.getOperand(1).getReg())
      .addImm(SEW);

  // The pseudo instruction is gone now.
  MI.eraseFromParent();

  return BB;
}

static MachineBasicBlock *emitComputeVMCLR(MachineInstr &MI,
                                           MachineBasicBlock *BB) {
  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();

  Register DestReg = MI.getOperand(0).getReg();
  unsigned SEW = MI.getOperand(2).getImm();

  BuildMI(*BB, MI, DL, TII.get(RISCV::PseudoVMXOR_MM_M1), DestReg)
      .addReg(DestReg, RegState::Undef)
      .addReg(DestReg, RegState::Undef)
      .addReg(MI.getOperand(1).getReg())
      .addImm(SEW);

  // The pseudo instruction is gone now.
  MI.eraseFromParent();

  return BB;
}

static MachineBasicBlock *emitImplicitVRTuple(MachineInstr &MI,
                                              MachineBasicBlock *BB) {
  MachineFunction &MF = *BB->getParent();
  DebugLoc DL = MI.getDebugLoc();
  const TargetInstrInfo &TII = *MF.getSubtarget().getInstrInfo();

  Register DestReg = MI.getOperand(0).getReg();
  BuildMI(*BB, MI, DL, TII.get(RISCV::IMPLICIT_DEF), DestReg);

  // The pseudo instruction is gone now.
  MI.eraseFromParent();
  return BB;
}

MachineBasicBlock *
RISCVTargetLowering::EmitInstrWithCustomInserter(MachineInstr &MI,
                                                 MachineBasicBlock *BB) const {
  switch (MI.getOpcode()) {
  default:
    break;
  case RISCV::PseudoVSCALE:
    return emitComputeVSCALE(MI, BB);
  case RISCV::PseudoVMSET_M1:
  case RISCV::PseudoVMSET_M2:
  case RISCV::PseudoVMSET_M4:
  case RISCV::PseudoVMSET_M8:
    return emitComputeVMSET(MI, BB);
  case RISCV::PseudoVMCLR_M1:
  case RISCV::PseudoVMCLR_M2:
  case RISCV::PseudoVMCLR_M4:
  case RISCV::PseudoVMCLR_M8:
    return emitComputeVMCLR(MI, BB);
  case RISCV::PseudoImplicitVRTuple:
    return emitImplicitVRTuple(MI, BB);
  }

  switch (MI.getOpcode()) {
  default:
    llvm_unreachable("Unexpected instr type to insert");
  case RISCV::ReadCycleWide:
    assert(!Subtarget.is64Bit() &&
           "ReadCycleWrite is only to be used on riscv32");
    return emitReadCycleWidePseudo(MI, BB);
  case RISCV::Select_GPR_Using_CC_GPR:
  case RISCV::Select_FPR32_Using_CC_GPR:
  case RISCV::Select_FPR64_Using_CC_GPR:
    return emitSelectPseudo(MI, BB);
  case RISCV::BuildPairF64Pseudo:
    return emitBuildPairF64Pseudo(MI, BB);
  case RISCV::SplitF64Pseudo:
    return emitSplitF64Pseudo(MI, BB);
  }
}

// Calling Convention Implementation.
// The expectations for frontend ABI lowering vary from target to target.
// Ideally, an LLVM frontend would be able to avoid worrying about many ABI
// details, but this is a longer term goal. For now, we simply try to keep the
// role of the frontend as simple and well-defined as possible. The rules can
// be summarised as:
// * Never split up large scalar arguments. We handle them here.
// * If a hardfloat calling convention is being used, and the struct may be
// passed in a pair of registers (fp+fp, int+fp), and both registers are
// available, then pass as two separate arguments. If either the GPRs or FPRs
// are exhausted, then pass according to the rule below.
// * If a struct could never be passed in registers or directly in a stack
// slot (as it is larger than 2*XLEN and the floating point rules don't
// apply), then pass it using a pointer with the byval attribute.
// * If a struct is less than 2*XLEN, then coerce to either a two-element
// word-sized array or a 2*XLEN scalar (depending on alignment).
// * The frontend can determine whether a struct is returned by reference or
// not based on its size and fields. If it will be returned by reference, the
// frontend must modify the prototype so a pointer with the sret annotation is
// passed as the first argument. This is not necessary for large scalar
// returns.
// * Struct return values and varargs should be coerced to structs containing
// register-size fields in the same situations they would be for fixed
// arguments.

static const MCPhysReg ArgGPRs[] = {
  RISCV::X10, RISCV::X11, RISCV::X12, RISCV::X13,
  RISCV::X14, RISCV::X15, RISCV::X16, RISCV::X17
};
static const MCPhysReg ArgFPR32s[] = {
  RISCV::F10_F, RISCV::F11_F, RISCV::F12_F, RISCV::F13_F,
  RISCV::F14_F, RISCV::F15_F, RISCV::F16_F, RISCV::F17_F
};
static const MCPhysReg ArgFPR64s[] = {
  RISCV::F10_D, RISCV::F11_D, RISCV::F12_D, RISCV::F13_D,
  RISCV::F14_D, RISCV::F15_D, RISCV::F16_D, RISCV::F17_D
};

static const MCPhysReg ArgVRs[] = {RISCV::V16, RISCV::V17, RISCV::V18,
                                    RISCV::V19, RISCV::V20, RISCV::V21,
                                    RISCV::V22, RISCV::V23};
static const MCPhysReg ArgVRM2s[] = {
    RISCV::V16M2,
    RISCV::V18M2,
    RISCV::V20M2,
    RISCV::V22M2,
};
static const MCPhysReg ArgVRM4s[] = {RISCV::V16M4, RISCV::V20M4};
static const MCPhysReg ArgVRM8s[] = {RISCV::V16M8};

// Pass a 2*XLEN argument that has been split into two XLEN values through
// registers or the stack as necessary.
static bool CC_RISCVAssign2XLen(unsigned XLen, CCState &State, CCValAssign VA1,
                                ISD::ArgFlagsTy ArgFlags1, unsigned ValNo2,
                                MVT ValVT2, MVT LocVT2,
                                ISD::ArgFlagsTy ArgFlags2) {
  unsigned XLenInBytes = XLen / 8;
  if (Register Reg = State.AllocateReg(ArgGPRs)) {
    // At least one half can be passed via register.
    State.addLoc(CCValAssign::getReg(VA1.getValNo(), VA1.getValVT(), Reg,
                                     VA1.getLocVT(), CCValAssign::Full));
  } else {
    // Both halves must be passed on the stack, with proper alignment.
    Align StackAlign =
        std::max(Align(XLenInBytes), ArgFlags1.getNonZeroOrigAlign());
    State.addLoc(
        CCValAssign::getMem(VA1.getValNo(), VA1.getValVT(),
                            State.AllocateStack(XLenInBytes, StackAlign),
                            VA1.getLocVT(), CCValAssign::Full));
    State.addLoc(CCValAssign::getMem(
        ValNo2, ValVT2, State.AllocateStack(XLenInBytes, Align(XLenInBytes)),
        LocVT2, CCValAssign::Full));
    return false;
  }

  if (Register Reg = State.AllocateReg(ArgGPRs)) {
    // The second half can also be passed via register.
    State.addLoc(
        CCValAssign::getReg(ValNo2, ValVT2, Reg, LocVT2, CCValAssign::Full));
  } else {
    // The second half is passed via the stack, without additional alignment.
    State.addLoc(CCValAssign::getMem(
        ValNo2, ValVT2, State.AllocateStack(XLenInBytes, Align(XLenInBytes)),
        LocVT2, CCValAssign::Full));
  }

  return false;
}

// Implements the RISC-V calling convention. Returns true upon failure.
static bool CC_RISCV(const DataLayout &DL, RISCVABI::ABI ABI, unsigned ValNo,
                     MVT ValVT, MVT LocVT, CCValAssign::LocInfo LocInfo,
                     ISD::ArgFlagsTy ArgFlags, CCState &State, bool IsFixed,
                     bool IsRet, Type *OrigTy, const RISCVTargetLowering *TLI,
                     Optional<unsigned> FirstMaskArgument) {
  unsigned XLen = DL.getLargestLegalIntTypeSizeInBits();
  assert(XLen == 32 || XLen == 64);
  MVT XLenVT = XLen == 32 ? MVT::i32 : MVT::i64;

  // Any return value split in to more than two values can't be returned
  // directly.
  if (IsRet && ValNo > 1)
    return true;

  // UseGPRForF32 if targeting one of the soft-float ABIs, if passing a
  // variadic argument, or if no F32 argument registers are available.
  bool UseGPRForF32 = true;
  // UseGPRForF64 if targeting soft-float ABIs or an FLEN=32 ABI, if passing a
  // variadic argument, or if no F64 argument registers are available.
  bool UseGPRForF64 = true;

  switch (ABI) {
  default:
    llvm_unreachable("Unexpected ABI");
  case RISCVABI::ABI_ILP32:
  case RISCVABI::ABI_LP64:
    break;
  case RISCVABI::ABI_ILP32F:
  case RISCVABI::ABI_LP64F:
    UseGPRForF32 = !IsFixed;
    break;
  case RISCVABI::ABI_ILP32D:
  case RISCVABI::ABI_LP64D:
    UseGPRForF32 = !IsFixed;
    UseGPRForF64 = !IsFixed;
    break;
  }

  if (State.getFirstUnallocated(ArgFPR32s) == array_lengthof(ArgFPR32s))
    UseGPRForF32 = true;
  if (State.getFirstUnallocated(ArgFPR64s) == array_lengthof(ArgFPR64s))
    UseGPRForF64 = true;

  // From this point on, rely on UseGPRForF32, UseGPRForF64 and similar local
  // variables rather than directly checking against the target ABI.

  if (UseGPRForF32 && ValVT == MVT::f32) {
    LocVT = XLenVT;
    LocInfo = CCValAssign::BCvt;
  } else if (UseGPRForF64 && XLen == 64 && ValVT == MVT::f64) {
    LocVT = MVT::i64;
    LocInfo = CCValAssign::BCvt;
  }

  // If this is a variadic argument, the RISC-V calling convention requires
  // that it is assigned an 'even' or 'aligned' register if it has 8-byte
  // alignment (RV32) or 16-byte alignment (RV64). An aligned register should
  // be used regardless of whether the original argument was split during
  // legalisation or not. The argument will not be passed by registers if the
  // original type is larger than 2*XLEN, so the register alignment rule does
  // not apply.
  unsigned TwoXLenInBytes = (2 * XLen) / 8;
  if (!IsFixed && ArgFlags.getNonZeroOrigAlign() == TwoXLenInBytes &&
      DL.getTypeAllocSize(OrigTy) == TwoXLenInBytes) {
    unsigned RegIdx = State.getFirstUnallocated(ArgGPRs);
    // Skip 'odd' register if necessary.
    if (RegIdx != array_lengthof(ArgGPRs) && RegIdx % 2 == 1)
      State.AllocateReg(ArgGPRs);
  }

  SmallVectorImpl<CCValAssign> &PendingLocs = State.getPendingLocs();
  SmallVectorImpl<ISD::ArgFlagsTy> &PendingArgFlags =
      State.getPendingArgFlags();

  assert(PendingLocs.size() == PendingArgFlags.size() &&
         "PendingLocs and PendingArgFlags out of sync");

  // Handle passing f64 on RV32D with a soft float ABI or when floating point
  // registers are exhausted.
  if (UseGPRForF64 && XLen == 32 && ValVT == MVT::f64) {
    assert(!ArgFlags.isSplit() && PendingLocs.empty() &&
           "Can't lower f64 if it is split");
    // Depending on available argument GPRS, f64 may be passed in a pair of
    // GPRs, split between a GPR and the stack, or passed completely on the
    // stack. LowerCall/LowerFormalArguments/LowerReturn must recognise these
    // cases.
    Register Reg = State.AllocateReg(ArgGPRs);
    LocVT = MVT::i32;
    if (!Reg) {
      unsigned StackOffset = State.AllocateStack(8, Align(8));
      State.addLoc(
          CCValAssign::getMem(ValNo, ValVT, StackOffset, LocVT, LocInfo));
      return false;
    }
    if (!State.AllocateReg(ArgGPRs))
      State.AllocateStack(4, Align(4));
    State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
    return false;
  }

  // Split arguments might be passed indirectly, so keep track of the pending
  // values.
  if (ArgFlags.isSplit() || !PendingLocs.empty()) {
    LocVT = XLenVT;
    LocInfo = CCValAssign::Indirect;
    PendingLocs.push_back(
        CCValAssign::getPending(ValNo, ValVT, LocVT, LocInfo));
    PendingArgFlags.push_back(ArgFlags);
    if (!ArgFlags.isSplitEnd()) {
      return false;
    }
  }

  // If the split argument only had two elements, it should be passed directly
  // in registers or on the stack.
  if (ArgFlags.isSplitEnd() && PendingLocs.size() <= 2) {
    assert(PendingLocs.size() == 2 && "Unexpected PendingLocs.size()");
    // Apply the normal calling convention rules to the first half of the
    // split argument.
    CCValAssign VA = PendingLocs[0];
    ISD::ArgFlagsTy AF = PendingArgFlags[0];
    PendingLocs.clear();
    PendingArgFlags.clear();
    return CC_RISCVAssign2XLen(XLen, State, VA, AF, ValNo, ValVT, LocVT,
                               ArgFlags);
  }

  // Allocate to a register if possible, or else a stack slot.
  Register Reg;
  if (ValVT == MVT::f32 && !UseGPRForF32)
    Reg = State.AllocateReg(ArgFPR32s);
  else if (ValVT == MVT::f64 && !UseGPRForF64)
    Reg = State.AllocateReg(ArgFPR64s);
  else if (ValVT.isScalableVector()) {
    const TargetRegisterClass *RC = TLI->getRegClassFor(ValVT);
    if (RC->hasSuperClassEq(&RISCV::VRRegClass)) {
      if (FirstMaskArgument.hasValue() &&
          ValNo == FirstMaskArgument.getValue()) {
        Reg = RISCV::V0;
      } else {
        Reg = State.AllocateReg(ArgVRs);
      }
    } else if (RC->hasSuperClassEq(&RISCV::VRM2RegClass)) {
      Reg = State.AllocateReg(ArgVRM2s);
    } else if (RC->hasSuperClassEq(&RISCV::VRM4RegClass)) {
      Reg = State.AllocateReg(ArgVRM4s);
    } else if (RC->hasSuperClassEq(&RISCV::VRM8RegClass)) {
      Reg = State.AllocateReg(ArgVRM8s);
    } else {
      llvm_unreachable("Unhandled class register for ValueType");
    }
    if (!Reg) {
      LocInfo = CCValAssign::Indirect;
      // Try using a GPR to pass the address
      Reg = State.AllocateReg(ArgGPRs);
      LocVT = XLenVT;
    }
  } else
    Reg = State.AllocateReg(ArgGPRs);
  unsigned StackOffset =
      Reg ? 0 : State.AllocateStack(XLen / 8, Align(XLen / 8));

  // If we reach this point and PendingLocs is non-empty, we must be at the
  // end of a split argument that must be passed indirectly.
  if (!PendingLocs.empty()) {
    assert(ArgFlags.isSplitEnd() && "Expected ArgFlags.isSplitEnd()");
    assert(PendingLocs.size() > 2 && "Unexpected PendingLocs.size()");

    for (auto &It : PendingLocs) {
      if (Reg)
        It.convertToReg(Reg);
      else
        It.convertToMem(StackOffset);
      State.addLoc(It);
    }
    PendingLocs.clear();
    PendingArgFlags.clear();
    return false;
  }

  assert((!UseGPRForF32 || !UseGPRForF64 ||
          (TLI->getSubtarget().hasStdExtV() && ValVT.isScalableVector()) ||
          LocVT == XLenVT) &&
         "Expected an XLenVT at this stage");

  if (Reg) {
    State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
    return false;
  }

  // When an f32 or f64 is passed on the stack, no bit-conversion is needed.
  if (ValVT == MVT::f32 || ValVT == MVT::f64) {
    LocVT = ValVT;
    LocInfo = CCValAssign::Full;
  }
  State.addLoc(CCValAssign::getMem(ValNo, ValVT, StackOffset, LocVT, LocInfo));
  return false;
}

template <typename ArgTy>
static void PreAssignMask(const ArgTy &Args,
                          Optional<unsigned> &FirstMaskArgument,
                          CCState &CCInfo) {
  unsigned NumArgs = Args.size();
  for (unsigned i = 0; i != NumArgs; ++i) {
    MVT ArgVT = Args[i].VT;
    if (!ArgVT.isScalableVector() ||
        ArgVT.getVectorElementType().SimpleTy != MVT::i1)
      continue;

    FirstMaskArgument = i;
    CCInfo.AllocateReg(RISCV::V0);
    break;
  }
}

void RISCVTargetLowering::analyzeInputArgs(
    MachineFunction &MF, CCState &CCInfo,
    const SmallVectorImpl<ISD::InputArg> &Ins, bool IsRet) const {
  unsigned NumArgs = Ins.size();
  FunctionType *FType = MF.getFunction().getFunctionType();

  Optional<unsigned> FirstMaskArgument;
  if (Subtarget.hasStdExtV()) {
    PreAssignMask(Ins, FirstMaskArgument, CCInfo);
  }

  for (unsigned i = 0; i != NumArgs; ++i) {
    MVT ArgVT = Ins[i].VT;
    ISD::ArgFlagsTy ArgFlags = Ins[i].Flags;

    Type *ArgTy = nullptr;
    if (IsRet)
      ArgTy = FType->getReturnType();
    else if (Ins[i].isOrigArg())
      ArgTy = FType->getParamType(Ins[i].getOrigArgIndex());

    RISCVABI::ABI ABI = MF.getSubtarget<RISCVSubtarget>().getTargetABI();
    if (CC_RISCV(MF.getDataLayout(), ABI, i, ArgVT, ArgVT, CCValAssign::Full,
                 ArgFlags, CCInfo, /*IsFixed=*/true, IsRet, ArgTy, this,
                 FirstMaskArgument)) {
      LLVM_DEBUG(dbgs() << "InputArg #" << i << " has unhandled type "
                        << EVT(ArgVT).getEVTString() << '\n');
      llvm_unreachable(nullptr);
    }
  }
}

void RISCVTargetLowering::analyzeOutputArgs(
    MachineFunction &MF, CCState &CCInfo,
    const SmallVectorImpl<ISD::OutputArg> &Outs, bool IsRet,
    CallLoweringInfo *CLI) const {
  unsigned NumArgs = Outs.size();

  Optional<unsigned> FirstMaskArgument;
  if (Subtarget.hasStdExtV()) {
    PreAssignMask(Outs, FirstMaskArgument, CCInfo);
  }

  for (unsigned i = 0; i != NumArgs; i++) {
    MVT ArgVT = Outs[i].VT;
    ISD::ArgFlagsTy ArgFlags = Outs[i].Flags;
    Type *OrigTy = CLI ? CLI->getArgs()[Outs[i].OrigArgIndex].Ty : nullptr;

    RISCVABI::ABI ABI = MF.getSubtarget<RISCVSubtarget>().getTargetABI();
    if (CC_RISCV(MF.getDataLayout(), ABI, i, ArgVT, ArgVT, CCValAssign::Full,
                 ArgFlags, CCInfo, Outs[i].IsFixed, IsRet, OrigTy, this,
                 FirstMaskArgument)) {
      LLVM_DEBUG(dbgs() << "OutputArg #" << i << " has unhandled type "
                        << EVT(ArgVT).getEVTString() << "\n");
      llvm_unreachable(nullptr);
    }
  }
}

// Convert Val to a ValVT. Should not be called for CCValAssign::Indirect
// values.
static SDValue convertLocVTToValVT(SelectionDAG &DAG, SDValue Val,
                                   const CCValAssign &VA, const SDLoc &DL) {
  switch (VA.getLocInfo()) {
  default:
    llvm_unreachable("Unexpected CCValAssign::LocInfo");
  case CCValAssign::Full:
    break;
  case CCValAssign::BCvt:
    if (VA.getLocVT() == MVT::i64 && VA.getValVT() == MVT::f32) {
      Val = DAG.getNode(RISCVISD::FMV_W_X_RV64, DL, MVT::f32, Val);
      break;
    }
    Val = DAG.getNode(ISD::BITCAST, DL, VA.getValVT(), Val);
    break;
  }
  return Val;
}

// The caller is responsible for loading the full value if the argument is
// passed with CCValAssign::Indirect.
static SDValue unpackFromRegLoc(SelectionDAG &DAG, SDValue Chain,
                                const CCValAssign &VA, const SDLoc &DL,
                                const RISCVTargetLowering *TLI) {
  MachineFunction &MF = DAG.getMachineFunction();
  MachineRegisterInfo &RegInfo = MF.getRegInfo();
  EVT LocVT = VA.getLocVT();
  SDValue Val;
  const TargetRegisterClass *RC;

  if (LocVT.getSimpleVT().isScalableVector()) {
    RC = TLI->getRegClassFor(LocVT.getSimpleVT());
  } else {
    switch (LocVT.getSimpleVT().SimpleTy) {
    default:
      llvm_unreachable("Unexpected register type");
    case MVT::i32:
    case MVT::i64:
      RC = &RISCV::GPRRegClass;
      break;
    case MVT::f32:
      RC = &RISCV::FPR32RegClass;
      break;
    case MVT::f64:
      RC = &RISCV::FPR64RegClass;
      break;
    }
  }

  Register VReg = RegInfo.createVirtualRegister(RC);
  RegInfo.addLiveIn(VA.getLocReg(), VReg);
  Val = DAG.getCopyFromReg(Chain, DL, VReg, LocVT);

  if (VA.getLocInfo() == CCValAssign::Indirect)
    return Val;

  return convertLocVTToValVT(DAG, Val, VA, DL);
}

static SDValue convertValVTToLocVT(SelectionDAG &DAG, SDValue Val,
                                   const CCValAssign &VA, const SDLoc &DL) {
  EVT LocVT = VA.getLocVT();

  switch (VA.getLocInfo()) {
  default:
    llvm_unreachable("Unexpected CCValAssign::LocInfo");
  case CCValAssign::Full:
    break;
  case CCValAssign::BCvt:
    if (VA.getLocVT() == MVT::i64 && VA.getValVT() == MVT::f32) {
      Val = DAG.getNode(RISCVISD::FMV_X_ANYEXTW_RV64, DL, MVT::i64, Val);
      break;
    }
    Val = DAG.getNode(ISD::BITCAST, DL, LocVT, Val);
    break;
  }
  return Val;
}

// The caller is responsible for loading the full value if the argument is
// passed with CCValAssign::Indirect.
static SDValue unpackFromMemLoc(SelectionDAG &DAG, SDValue Chain,
                                const CCValAssign &VA, const SDLoc &DL) {
  MachineFunction &MF = DAG.getMachineFunction();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  EVT LocVT = VA.getLocVT();
  EVT ValVT = VA.getValVT();
  EVT PtrVT = MVT::getIntegerVT(DAG.getDataLayout().getPointerSizeInBits(0));
  int FI = MFI.CreateFixedObject(ValVT.getSizeInBits().getKnownMinSize() / 8,
                                 VA.getLocMemOffset(), /*Immutable=*/true);
  SDValue FIN = DAG.getFrameIndex(FI, PtrVT);
  SDValue Val;

  ISD::LoadExtType ExtType;
  switch (VA.getLocInfo()) {
  default:
    llvm_unreachable("Unexpected CCValAssign::LocInfo");
  case CCValAssign::Indirect:
    if (ValVT.isScalableVector()) {
      // Indirect load of the vector value
      SDValue Ptr = DAG.getLoad(
          LocVT, DL, Chain, FIN,
          MachinePointerInfo::getFixedStack(DAG.getMachineFunction(), FI));
      return Ptr;
    }
    LLVM_FALLTHROUGH;
  case CCValAssign::Full:
  case CCValAssign::BCvt:
    ExtType = ISD::NON_EXTLOAD;
    break;
  }
  Val = DAG.getExtLoad(
      ExtType, DL, LocVT, Chain, FIN,
      MachinePointerInfo::getFixedStack(DAG.getMachineFunction(), FI), ValVT);
  return Val;
}

static SDValue unpackF64OnRV32DSoftABI(SelectionDAG &DAG, SDValue Chain,
                                       const CCValAssign &VA, const SDLoc &DL) {
  assert(VA.getLocVT() == MVT::i32 && VA.getValVT() == MVT::f64 &&
         "Unexpected VA");
  MachineFunction &MF = DAG.getMachineFunction();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  MachineRegisterInfo &RegInfo = MF.getRegInfo();

  if (VA.isMemLoc()) {
    // f64 is passed on the stack.
    int FI = MFI.CreateFixedObject(8, VA.getLocMemOffset(), /*Immutable=*/true);
    SDValue FIN = DAG.getFrameIndex(FI, MVT::i32);
    return DAG.getLoad(MVT::f64, DL, Chain, FIN,
                       MachinePointerInfo::getFixedStack(MF, FI));
  }

  assert(VA.isRegLoc() && "Expected register VA assignment");

  Register LoVReg = RegInfo.createVirtualRegister(&RISCV::GPRRegClass);
  RegInfo.addLiveIn(VA.getLocReg(), LoVReg);
  SDValue Lo = DAG.getCopyFromReg(Chain, DL, LoVReg, MVT::i32);
  SDValue Hi;
  if (VA.getLocReg() == RISCV::X17) {
    // Second half of f64 is passed on the stack.
    int FI = MFI.CreateFixedObject(4, 0, /*Immutable=*/true);
    SDValue FIN = DAG.getFrameIndex(FI, MVT::i32);
    Hi = DAG.getLoad(MVT::i32, DL, Chain, FIN,
                     MachinePointerInfo::getFixedStack(MF, FI));
  } else {
    // Second half of f64 is passed in another GPR.
    Register HiVReg = RegInfo.createVirtualRegister(&RISCV::GPRRegClass);
    RegInfo.addLiveIn(VA.getLocReg() + 1, HiVReg);
    Hi = DAG.getCopyFromReg(Chain, DL, HiVReg, MVT::i32);
  }
  return DAG.getNode(RISCVISD::BuildPairF64, DL, MVT::f64, Lo, Hi);
}

static EVT MaskVTToMemVT(EVT VT) {
  assert(VT.isScalableVector() && "Must be a scalable vector");
  assert(VT.getVectorElementType() == MVT::i1 && "Must be a mask");
  // FIXME - Assumes ELEN=64
  EVT MemVT =
      MVT::getVectorVT(MVT::getIntegerVT(64 / VT.getVectorMinNumElements()),
                       VT.getVectorMinNumElements(),
                       /* IsScalable */ true);
  return MemVT;
}

// FastCC has less than 1% performance improvement for some particular
// benchmark. But theoretically, it may has benenfit for some cases.
static bool CC_RISCV_FastCC(unsigned ValNo, MVT ValVT, MVT LocVT,
                            CCValAssign::LocInfo LocInfo,
                            ISD::ArgFlagsTy ArgFlags, CCState &State) {

  if (LocVT == MVT::i32 || LocVT == MVT::i64) {
    // X5 and X6 might be used for save-restore libcall.
    static const MCPhysReg GPRList[] = {
        RISCV::X10, RISCV::X11, RISCV::X12, RISCV::X13, RISCV::X14,
        RISCV::X15, RISCV::X16, RISCV::X17, RISCV::X7,  RISCV::X28,
        RISCV::X29, RISCV::X30, RISCV::X31};
    if (unsigned Reg = State.AllocateReg(GPRList)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  if (LocVT == MVT::f32) {
    static const MCPhysReg FPR32List[] = {
        RISCV::F10_F, RISCV::F11_F, RISCV::F12_F, RISCV::F13_F, RISCV::F14_F,
        RISCV::F15_F, RISCV::F16_F, RISCV::F17_F, RISCV::F0_F,  RISCV::F1_F,
        RISCV::F2_F,  RISCV::F3_F,  RISCV::F4_F,  RISCV::F5_F,  RISCV::F6_F,
        RISCV::F7_F,  RISCV::F28_F, RISCV::F29_F, RISCV::F30_F, RISCV::F31_F};
    if (unsigned Reg = State.AllocateReg(FPR32List)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  if (LocVT == MVT::f64) {
    static const MCPhysReg FPR64List[] = {
        RISCV::F10_D, RISCV::F11_D, RISCV::F12_D, RISCV::F13_D, RISCV::F14_D,
        RISCV::F15_D, RISCV::F16_D, RISCV::F17_D, RISCV::F0_D,  RISCV::F1_D,
        RISCV::F2_D,  RISCV::F3_D,  RISCV::F4_D,  RISCV::F5_D,  RISCV::F6_D,
        RISCV::F7_D,  RISCV::F28_D, RISCV::F29_D, RISCV::F30_D, RISCV::F31_D};
    if (unsigned Reg = State.AllocateReg(FPR64List)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  if (LocVT == MVT::i32 || LocVT == MVT::f32) {
    unsigned Offset4 = State.AllocateStack(4, Align(4));
    State.addLoc(CCValAssign::getMem(ValNo, ValVT, Offset4, LocVT, LocInfo));
    return false;
  }

  if (LocVT == MVT::i64 || LocVT == MVT::f64) {
    unsigned Offset5 = State.AllocateStack(8, Align(8));
    State.addLoc(CCValAssign::getMem(ValNo, ValVT, Offset5, LocVT, LocInfo));
    return false;
  }

  return true; // CC didn't match.
}

static bool CC_RISCV_GHC(unsigned ValNo, MVT ValVT, MVT LocVT,
                         CCValAssign::LocInfo LocInfo,
                         ISD::ArgFlagsTy ArgFlags, CCState &State) {

  if (LocVT == MVT::i32 || LocVT == MVT::i64) {
    // Pass in STG registers: Base, Sp, Hp, R1, R2, R3, R4, R5, R6, R7, SpLim
    //                        s1    s2  s3  s4  s5  s6  s7  s8  s9  s10 s11
    static const MCPhysReg GPRList[] = {
        RISCV::X9, RISCV::X18, RISCV::X19, RISCV::X20, RISCV::X21, RISCV::X22,
        RISCV::X23, RISCV::X24, RISCV::X25, RISCV::X26, RISCV::X27};
    if (unsigned Reg = State.AllocateReg(GPRList)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  if (LocVT == MVT::f32) {
    // Pass in STG registers: F1, ..., F6
    //                        fs0 ... fs5
    static const MCPhysReg FPR32List[] = {RISCV::F8_F, RISCV::F9_F,
                                          RISCV::F18_F, RISCV::F19_F,
                                          RISCV::F20_F, RISCV::F21_F};
    if (unsigned Reg = State.AllocateReg(FPR32List)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  if (LocVT == MVT::f64) {
    // Pass in STG registers: D1, ..., D6
    //                        fs6 ... fs11
    static const MCPhysReg FPR64List[] = {RISCV::F22_D, RISCV::F23_D,
                                          RISCV::F24_D, RISCV::F25_D,
                                          RISCV::F26_D, RISCV::F27_D};
    if (unsigned Reg = State.AllocateReg(FPR64List)) {
      State.addLoc(CCValAssign::getReg(ValNo, ValVT, Reg, LocVT, LocInfo));
      return false;
    }
  }

  report_fatal_error("No registers left in GHC calling convention");
  return true;
}

// Transform physical registers into virtual registers.
SDValue RISCVTargetLowering::LowerFormalArguments(
    SDValue Chain, CallingConv::ID CallConv, bool IsVarArg,
    const SmallVectorImpl<ISD::InputArg> &Ins, const SDLoc &DL,
    SelectionDAG &DAG, SmallVectorImpl<SDValue> &InVals) const {

  MachineFunction &MF = DAG.getMachineFunction();

  switch (CallConv) {
  default:
    report_fatal_error("Unsupported calling convention");
  case CallingConv::C:
  case CallingConv::Fast:
    break;
  case CallingConv::GHC:
    if (!MF.getSubtarget().getFeatureBits()[RISCV::FeatureStdExtF] ||
        !MF.getSubtarget().getFeatureBits()[RISCV::FeatureStdExtD])
      report_fatal_error(
        "GHC calling convention requires the F and D instruction set extensions");
  }

  const Function &Func = MF.getFunction();
  if (Func.hasFnAttribute("interrupt")) {
    if (!Func.arg_empty())
      report_fatal_error(
        "Functions with the interrupt attribute cannot have arguments!");

    StringRef Kind =
      MF.getFunction().getFnAttribute("interrupt").getValueAsString();

    if (!(Kind == "user" || Kind == "supervisor" || Kind == "machine"))
      report_fatal_error(
        "Function interrupt attribute argument not supported!");
  }

  EVT PtrVT = getPointerTy(DAG.getDataLayout());
  MVT XLenVT = Subtarget.getXLenVT();
  unsigned XLenInBytes = Subtarget.getXLen() / 8;
  // Used with vargs to acumulate store chains.
  std::vector<SDValue> OutChains;

  // Assign locations to all of the incoming arguments.
  SmallVector<CCValAssign, 16> ArgLocs;
  CCState CCInfo(CallConv, IsVarArg, MF, ArgLocs, *DAG.getContext());

  // We do not want to use fastcc when returning scalable vectors because
  // we want to have the CC for them in a single place in the code for now.
  bool HasInsScalableVectors =
      std::any_of(Ins.begin(), Ins.end(),
                  [](ISD::InputArg In) { return In.VT.isScalableVector(); });

  if (CallConv == CallingConv::Fast && !HasInsScalableVectors)
    CCInfo.AnalyzeFormalArguments(Ins, CC_RISCV_FastCC);
  else if (CallConv == CallingConv::GHC)
    CCInfo.AnalyzeFormalArguments(Ins, CC_RISCV_GHC);
  else
    analyzeInputArgs(MF, CCInfo, Ins, /*IsRet=*/false);

  for (unsigned i = 0, e = ArgLocs.size(); i != e; ++i) {
    CCValAssign &VA = ArgLocs[i];
    SDValue ArgValue;
    // Passing f64 on RV32D with a soft float ABI must be handled as a special
    // case.
    if (VA.getLocVT() == MVT::i32 && VA.getValVT() == MVT::f64)
      ArgValue = unpackF64OnRV32DSoftABI(DAG, Chain, VA, DL);
    else if (VA.isRegLoc())
      ArgValue = unpackFromRegLoc(DAG, Chain, VA, DL, this);
    else
      ArgValue = unpackFromMemLoc(DAG, Chain, VA, DL);

    // Special case for mask vectors.
    if (VA.getLocInfo() == CCValAssign::Indirect &&
        VA.getValVT().isScalableVector() &&
        VA.getValVT().getVectorElementType() == MVT::i1) {
      EVT LocVT = MaskVTToMemVT(VA.getValVT());
      InVals.push_back(DAG.getNode(
          ISD::TRUNCATE, DL, VA.getValVT(),
          DAG.getLoad(LocVT, DL, Chain, ArgValue, MachinePointerInfo())));
      continue;
    } else if (VA.getLocInfo() == CCValAssign::Indirect) {
      // If the original argument was split and passed by reference (e.g. i128
      // on RV32), we need to load all parts of it here (using the same
      // address).
      InVals.push_back(DAG.getLoad(VA.getValVT(), DL, Chain, ArgValue,
                                   MachinePointerInfo()));
      unsigned ArgIndex = Ins[i].OrigArgIndex;
      assert(Ins[i].PartOffset == 0);
      while (i + 1 != e && Ins[i + 1].OrigArgIndex == ArgIndex) {
        CCValAssign &PartVA = ArgLocs[i + 1];
        unsigned PartOffset = Ins[i + 1].PartOffset;
        SDValue Address = DAG.getNode(ISD::ADD, DL, PtrVT, ArgValue,
                                      DAG.getIntPtrConstant(PartOffset, DL));
        InVals.push_back(DAG.getLoad(PartVA.getValVT(), DL, Chain, Address,
                                     MachinePointerInfo()));
        ++i;
      }
      continue;
    }
    InVals.push_back(ArgValue);
  }

  if (IsVarArg) {
    ArrayRef<MCPhysReg> ArgRegs = makeArrayRef(ArgGPRs);
    unsigned Idx = CCInfo.getFirstUnallocated(ArgRegs);
    const TargetRegisterClass *RC = &RISCV::GPRRegClass;
    MachineFrameInfo &MFI = MF.getFrameInfo();
    MachineRegisterInfo &RegInfo = MF.getRegInfo();
    RISCVMachineFunctionInfo *RVFI = MF.getInfo<RISCVMachineFunctionInfo>();

    // Offset of the first variable argument from stack pointer, and size of
    // the vararg save area. For now, the varargs save area is either zero or
    // large enough to hold a0-a7.
    int VaArgOffset, VarArgsSaveSize;

    // If all registers are allocated, then all varargs must be passed on the
    // stack and we don't need to save any argregs.
    if (ArgRegs.size() == Idx) {
      VaArgOffset = CCInfo.getNextStackOffset();
      VarArgsSaveSize = 0;
    } else {
      VarArgsSaveSize = XLenInBytes * (ArgRegs.size() - Idx);
      VaArgOffset = -VarArgsSaveSize;
    }

    // Record the frame index of the first variable argument
    // which is a value necessary to VASTART.
    int FI = MFI.CreateFixedObject(XLenInBytes, VaArgOffset, true);
    RVFI->setVarArgsFrameIndex(FI);

    // If saving an odd number of registers then create an extra stack slot to
    // ensure that the frame pointer is 2*XLEN-aligned, which in turn ensures
    // offsets to even-numbered registered remain 2*XLEN-aligned.
    if (Idx % 2) {
      MFI.CreateFixedObject(XLenInBytes, VaArgOffset - (int)XLenInBytes, true);
      VarArgsSaveSize += XLenInBytes;
    }

    // Copy the integer registers that may have been used for passing varargs
    // to the vararg save area.
    for (unsigned I = Idx; I < ArgRegs.size();
         ++I, VaArgOffset += XLenInBytes) {
      const Register Reg = RegInfo.createVirtualRegister(RC);
      RegInfo.addLiveIn(ArgRegs[I], Reg);
      SDValue ArgValue = DAG.getCopyFromReg(Chain, DL, Reg, XLenVT);
      FI = MFI.CreateFixedObject(XLenInBytes, VaArgOffset, true);
      SDValue PtrOff = DAG.getFrameIndex(FI, getPointerTy(DAG.getDataLayout()));
      SDValue Store = DAG.getStore(Chain, DL, ArgValue, PtrOff,
                                   MachinePointerInfo::getFixedStack(MF, FI));
      cast<StoreSDNode>(Store.getNode())
          ->getMemOperand()
          ->setValue((Value *)nullptr);
      OutChains.push_back(Store);
    }
    RVFI->setVarArgsSaveSize(VarArgsSaveSize);
  }

  // All stores are grouped in one node to allow the matching between
  // the size of Ins and InVals. This only happens for vararg functions.
  if (!OutChains.empty()) {
    OutChains.push_back(Chain);
    Chain = DAG.getNode(ISD::TokenFactor, DL, MVT::Other, OutChains);
  }

  return Chain;
}

/// isEligibleForTailCallOptimization - Check whether the call is eligible
/// for tail call optimization.
/// Note: This is modelled after ARM's IsEligibleForTailCallOptimization.
bool RISCVTargetLowering::isEligibleForTailCallOptimization(
    CCState &CCInfo, CallLoweringInfo &CLI, MachineFunction &MF,
    const SmallVector<CCValAssign, 16> &ArgLocs) const {

  auto &Callee = CLI.Callee;
  auto CalleeCC = CLI.CallConv;
  auto &Outs = CLI.Outs;
  auto &Caller = MF.getFunction();
  auto CallerCC = Caller.getCallingConv();

  // Exception-handling functions need a special set of instructions to
  // indicate a return to the hardware. Tail-calling another function would
  // probably break this.
  // TODO: The "interrupt" attribute isn't currently defined by RISC-V. This
  // should be expanded as new function attributes are introduced.
  if (Caller.hasFnAttribute("interrupt"))
    return false;

  // Do not tail call opt if the stack is used to pass parameters.
  if (CCInfo.getNextStackOffset() != 0)
    return false;

  // Do not tail call opt if any parameters need to be passed indirectly.
  // Since long doubles (fp128) and i128 are larger than 2*XLEN, they are
  // passed indirectly. So the address of the value will be passed in a
  // register, or if not available, then the address is put on the stack. In
  // order to pass indirectly, space on the stack often needs to be allocated
  // in order to store the value. In this case the CCInfo.getNextStackOffset()
  // != 0 check is not enough and we need to check if any CCValAssign ArgsLocs
  // are passed CCValAssign::Indirect.
  for (auto &VA : ArgLocs)
    if (VA.getLocInfo() == CCValAssign::Indirect)
      return false;

  // Do not tail call opt if either caller or callee uses struct return
  // semantics.
  auto IsCallerStructRet = Caller.hasStructRetAttr();
  auto IsCalleeStructRet = Outs.empty() ? false : Outs[0].Flags.isSRet();
  if (IsCallerStructRet || IsCalleeStructRet)
    return false;

  // Externally-defined functions with weak linkage should not be
  // tail-called. The behaviour of branch instructions in this situation (as
  // used for tail calls) is implementation-defined, so we cannot rely on the
  // linker replacing the tail call with a return.
  if (GlobalAddressSDNode *G = dyn_cast<GlobalAddressSDNode>(Callee)) {
    const GlobalValue *GV = G->getGlobal();
    if (GV->hasExternalWeakLinkage())
      return false;
  }

  // The callee has to preserve all registers the caller needs to preserve.
  const RISCVRegisterInfo *TRI = Subtarget.getRegisterInfo();
  const uint32_t *CallerPreserved = TRI->getCallPreservedMask(MF, CallerCC);
  if (CalleeCC != CallerCC) {
    const uint32_t *CalleePreserved = TRI->getCallPreservedMask(MF, CalleeCC);
    if (!TRI->regmaskSubsetEqual(CallerPreserved, CalleePreserved))
      return false;
  }

  // Byval parameters hand the function a pointer directly into the stack area
  // we want to reuse during a tail call. Working around this *is* possible
  // but less efficient and uglier in LowerCall.
  for (auto &Arg : Outs)
    if (Arg.Flags.isByVal())
      return false;

  return true;
}

// Lower a call to a callseq_start + CALL + callseq_end chain, and add input
// and output parameter nodes.
SDValue RISCVTargetLowering::LowerCall(CallLoweringInfo &CLI,
                                       SmallVectorImpl<SDValue> &InVals) const {
  SelectionDAG &DAG = CLI.DAG;
  SDLoc &DL = CLI.DL;
  SmallVectorImpl<ISD::OutputArg> &Outs = CLI.Outs;
  SmallVectorImpl<SDValue> &OutVals = CLI.OutVals;
  SmallVectorImpl<ISD::InputArg> &Ins = CLI.Ins;
  SDValue Chain = CLI.Chain;
  SDValue Callee = CLI.Callee;
  bool &IsTailCall = CLI.IsTailCall;
  CallingConv::ID CallConv = CLI.CallConv;
  bool IsVarArg = CLI.IsVarArg;
  EVT PtrVT = getPointerTy(DAG.getDataLayout());
  MVT XLenVT = Subtarget.getXLenVT();

  MachineFunction &MF = DAG.getMachineFunction();

  // Analyze the operands of the call, assigning locations to each operand.
  SmallVector<CCValAssign, 16> ArgLocs;
  CCState ArgCCInfo(CallConv, IsVarArg, MF, ArgLocs, *DAG.getContext());

  // We do not want to use fastcc when returning scalable vectors because
  // we want to have the CC for them in a single place in the code for now.
  bool ReturningScalableVectors =
      std::any_of(Outs.begin(), Outs.end(),
                  [](ISD::OutputArg Out) { return Out.VT.isScalableVector(); });

  if (CallConv == CallingConv::Fast && !ReturningScalableVectors)
    ArgCCInfo.AnalyzeCallOperands(Outs, CC_RISCV_FastCC);
  else if (CallConv == CallingConv::GHC)
    ArgCCInfo.AnalyzeCallOperands(Outs, CC_RISCV_GHC);
  else
    analyzeOutputArgs(MF, ArgCCInfo, Outs, /*IsRet=*/false, &CLI);

  // Check if it's really possible to do a tail call.
  if (IsTailCall)
    IsTailCall = isEligibleForTailCallOptimization(ArgCCInfo, CLI, MF, ArgLocs);

  if (IsTailCall)
    ++NumTailCalls;
  else if (CLI.CB && CLI.CB->isMustTailCall())
    report_fatal_error("failed to perform tail call elimination on a call "
                       "site marked musttail");

  // Get a count of how many bytes are to be pushed on the stack.
  unsigned NumBytes = ArgCCInfo.getNextStackOffset();

  // Create local copies for byval args
  SmallVector<SDValue, 8> ByValArgs;
  for (unsigned i = 0, e = Outs.size(); i != e; ++i) {
    ISD::ArgFlagsTy Flags = Outs[i].Flags;
    if (!Flags.isByVal())
      continue;

    SDValue Arg = OutVals[i];
    unsigned Size = Flags.getByValSize();
    Align Alignment = Flags.getNonZeroByValAlign();

    int FI =
        MF.getFrameInfo().CreateStackObject(Size, Alignment, /*isSS=*/false);
    SDValue FIPtr = DAG.getFrameIndex(FI, getPointerTy(DAG.getDataLayout()));
    SDValue SizeNode = DAG.getConstant(Size, DL, XLenVT);

    Chain = DAG.getMemcpy(Chain, DL, FIPtr, Arg, SizeNode, Alignment,
                          /*IsVolatile=*/false,
                          /*AlwaysInline=*/false, IsTailCall,
                          MachinePointerInfo(), MachinePointerInfo());
    ByValArgs.push_back(FIPtr);
  }

  if (!IsTailCall)
    Chain = DAG.getCALLSEQ_START(Chain, NumBytes, 0, CLI.DL);

  // Copy argument values to their designated locations.
  SmallVector<std::pair<Register, SDValue>, 8> RegsToPass;
  SmallVector<SDValue, 8> MemOpChains;
  SDValue StackPtr;
  for (unsigned i = 0, j = 0, e = ArgLocs.size(); i != e; ++i) {
    CCValAssign &VA = ArgLocs[i];
    SDValue ArgValue = OutVals[i];
    ISD::ArgFlagsTy Flags = Outs[i].Flags;

    // Handle passing f64 on RV32D with a soft float or hard float single ABI as
    // a special case.
    bool IsF64OnRV32DSoftABI =
        VA.getLocVT() == MVT::i32 && VA.getValVT() == MVT::f64;
    if (IsF64OnRV32DSoftABI && VA.isRegLoc()) {
      SDValue SplitF64 = DAG.getNode(
          RISCVISD::SplitF64, DL, DAG.getVTList(MVT::i32, MVT::i32), ArgValue);
      SDValue Lo = SplitF64.getValue(0);
      SDValue Hi = SplitF64.getValue(1);

      Register RegLo = VA.getLocReg();
      RegsToPass.push_back(std::make_pair(RegLo, Lo));

      if (RegLo == RISCV::X17) {
        // Second half of f64 is passed on the stack.
        // Work out the address of the stack slot.
        if (!StackPtr.getNode())
          StackPtr = DAG.getCopyFromReg(Chain, DL, RISCV::X2, PtrVT);
        // Emit the store.
        MemOpChains.push_back(
            DAG.getStore(Chain, DL, Hi, StackPtr, MachinePointerInfo()));
      } else {
        // Second half of f64 is passed in another GPR.
        assert(RegLo < RISCV::X31 && "Invalid register pair");
        Register RegHigh = RegLo + 1;
        RegsToPass.push_back(std::make_pair(RegHigh, Hi));
      }
      continue;
    }

    // IsF64OnRV32DSoftABI && VA.isMemLoc() is handled below in the same way
    // as any other MemLoc.

    // Promote the value if needed.
    // For now, only handle fully promoted and indirect arguments.
    if (VA.getLocInfo() == CCValAssign::Indirect) {
      if (VA.getValVT().isScalableVector()) {
        // Create a stack slot for the EPI register.
        SDValue SpillSlot = DAG.CreateStackTemporary(Outs[i].ArgVT);
        int FI = cast<FrameIndexSDNode>(SpillSlot)->getIndex();

        RISCVMachineFunctionInfo *RVFI = MF.getInfo<RISCVMachineFunctionInfo>();
        // Let know FrameLowering that we're spilling vector registers.
        RVFI->setHasSpilledVR();
        // Mark this spill as a vector spill.
        MF.getFrameInfo().setStackID(FI, TargetStackID::EPIVector);

        // We load an XLenVT from the spill slot because RISCVFrameLowering.cpp
        // will replace this slot from a vector type to an XLenVT.
        SDValue Ptr = DAG.getLoad(XLenVT, DL, Chain, SpillSlot,
                                  MachinePointerInfo::getFixedStack(MF, FI));

        // Special case for masks.
        if (VA.getValVT().getVectorElementType() == MVT::i1) {
          EVT LocVT = MaskVTToMemVT(VA.getValVT());
          ArgValue = DAG.getNode(ISD::ZERO_EXTEND, DL, LocVT, ArgValue);
        }

        MemOpChains.push_back(
            DAG.getStore(Chain, DL, ArgValue, Ptr, MachinePointerInfo()));
        ArgValue = Ptr;
      } else {
        // Store the argument in a stack slot and pass its address.
        SDValue SpillSlot = DAG.CreateStackTemporary(Outs[i].ArgVT);
        int FI = cast<FrameIndexSDNode>(SpillSlot)->getIndex();
        MemOpChains.push_back(
            DAG.getStore(Chain, DL, ArgValue, SpillSlot,
                         MachinePointerInfo::getFixedStack(MF, FI)));
        // If the original argument was split (e.g. i128), we need
        // to store all parts of it here (and pass just one address).
        unsigned ArgIndex = Outs[i].OrigArgIndex;
        assert(Outs[i].PartOffset == 0);
        while (i + 1 != e && Outs[i + 1].OrigArgIndex == ArgIndex) {
          SDValue PartValue = OutVals[i + 1];
          unsigned PartOffset = Outs[i + 1].PartOffset;
          SDValue Address = DAG.getNode(ISD::ADD, DL, PtrVT, SpillSlot,
                                        DAG.getIntPtrConstant(PartOffset, DL));
          MemOpChains.push_back(
              DAG.getStore(Chain, DL, PartValue, Address,
                           MachinePointerInfo::getFixedStack(MF, FI)));
          ++i;
        }
        ArgValue = SpillSlot;
      }
    } else {
      ArgValue = convertValVTToLocVT(DAG, ArgValue, VA, DL);
    }

    // Use local copy if it is a byval arg.
    if (Flags.isByVal())
      ArgValue = ByValArgs[j++];

    if (VA.isRegLoc()) {
      // Queue up the argument copies and emit them at the end.
      RegsToPass.push_back(std::make_pair(VA.getLocReg(), ArgValue));
    } else {
      assert(VA.isMemLoc() && "Argument not register or memory");
      assert(!IsTailCall && "Tail call not allowed if stack is used "
                            "for passing parameters");

      // Work out the address of the stack slot.
      if (!StackPtr.getNode())
        StackPtr = DAG.getCopyFromReg(Chain, DL, RISCV::X2, PtrVT);
      SDValue Address =
          DAG.getNode(ISD::ADD, DL, PtrVT, StackPtr,
                      DAG.getIntPtrConstant(VA.getLocMemOffset(), DL));

      // Emit the store.
      MemOpChains.push_back(
          DAG.getStore(Chain, DL, ArgValue, Address, MachinePointerInfo()));
    }
  }

  // Join the stores, which are independent of one another.
  if (!MemOpChains.empty())
    Chain = DAG.getNode(ISD::TokenFactor, DL, MVT::Other, MemOpChains);

  SDValue Glue;

  // Build a sequence of copy-to-reg nodes, chained and glued together.
  for (auto &Reg : RegsToPass) {
    Chain = DAG.getCopyToReg(Chain, DL, Reg.first, Reg.second, Glue);
    Glue = Chain.getValue(1);
  }

  // Validate that none of the argument registers have been marked as
  // reserved, if so report an error. Do the same for the return address if this
  // is not a tailcall.
  validateCCReservedRegs(RegsToPass, MF);
  if (!IsTailCall &&
      MF.getSubtarget<RISCVSubtarget>().isRegisterReservedByUser(RISCV::X1))
    MF.getFunction().getContext().diagnose(DiagnosticInfoUnsupported{
        MF.getFunction(),
        "Return address register required, but has been reserved."});

  // If the callee is a GlobalAddress/ExternalSymbol node, turn it into a
  // TargetGlobalAddress/TargetExternalSymbol node so that legalize won't
  // split it and then direct call can be matched by PseudoCALL.
  if (GlobalAddressSDNode *S = dyn_cast<GlobalAddressSDNode>(Callee)) {
    const GlobalValue *GV = S->getGlobal();

    unsigned OpFlags = RISCVII::MO_CALL;
    if (!getTargetMachine().shouldAssumeDSOLocal(*GV->getParent(), GV))
      OpFlags = RISCVII::MO_PLT;

    Callee = DAG.getTargetGlobalAddress(GV, DL, PtrVT, 0, OpFlags);
  } else if (ExternalSymbolSDNode *S = dyn_cast<ExternalSymbolSDNode>(Callee)) {
    unsigned OpFlags = RISCVII::MO_CALL;

    if (!getTargetMachine().shouldAssumeDSOLocal(*MF.getFunction().getParent(),
                                                 nullptr))
      OpFlags = RISCVII::MO_PLT;

    Callee = DAG.getTargetExternalSymbol(S->getSymbol(), PtrVT, OpFlags);
  }

  // The first call operand is the chain and the second is the target address.
  SmallVector<SDValue, 8> Ops;
  Ops.push_back(Chain);
  Ops.push_back(Callee);

  // Add argument registers to the end of the list so that they are
  // known live into the call.
  for (auto &Reg : RegsToPass)
    Ops.push_back(DAG.getRegister(Reg.first, Reg.second.getValueType()));

  if (!IsTailCall) {
    // Add a register mask operand representing the call-preserved registers.
    const TargetRegisterInfo *TRI = Subtarget.getRegisterInfo();
    const uint32_t *Mask = TRI->getCallPreservedMask(MF, CallConv);
    assert(Mask && "Missing call preserved mask for calling convention");
    Ops.push_back(DAG.getRegisterMask(Mask));
  }

  // Glue the call to the argument copies, if any.
  if (Glue.getNode())
    Ops.push_back(Glue);

  // Emit the call.
  SDVTList NodeTys = DAG.getVTList(MVT::Other, MVT::Glue);

  if (IsTailCall) {
    MF.getFrameInfo().setHasTailCall();
    return DAG.getNode(RISCVISD::TAIL, DL, NodeTys, Ops);
  }

  Chain = DAG.getNode(RISCVISD::CALL, DL, NodeTys, Ops);
  DAG.addNoMergeSiteInfo(Chain.getNode(), CLI.NoMerge);
  Glue = Chain.getValue(1);

  // Mark the end of the call, which is glued to the call itself.
  Chain = DAG.getCALLSEQ_END(Chain,
                             DAG.getConstant(NumBytes, DL, PtrVT, true),
                             DAG.getConstant(0, DL, PtrVT, true),
                             Glue, DL);
  Glue = Chain.getValue(1);

  // Assign locations to each value returned by this call.
  SmallVector<CCValAssign, 16> RVLocs;
  CCState RetCCInfo(CallConv, IsVarArg, MF, RVLocs, *DAG.getContext());
  analyzeInputArgs(MF, RetCCInfo, Ins, /*IsRet=*/true);

  // Copy all of the result registers out of their specified physreg.
  for (auto &VA : RVLocs) {
    // Copy the value out
    SDValue RetValue =
        DAG.getCopyFromReg(Chain, DL, VA.getLocReg(), VA.getLocVT(), Glue);
    // Glue the RetValue to the end of the call sequence
    Chain = RetValue.getValue(1);
    Glue = RetValue.getValue(2);

    if (VA.getLocVT() == MVT::i32 && VA.getValVT() == MVT::f64) {
      assert(VA.getLocReg() == ArgGPRs[0] && "Unexpected reg assignment");
      SDValue RetValue2 =
          DAG.getCopyFromReg(Chain, DL, ArgGPRs[1], MVT::i32, Glue);
      Chain = RetValue2.getValue(1);
      Glue = RetValue2.getValue(2);
      RetValue = DAG.getNode(RISCVISD::BuildPairF64, DL, MVT::f64, RetValue,
                             RetValue2);
    }

    RetValue = convertLocVTToValVT(DAG, RetValue, VA, DL);

    InVals.push_back(RetValue);
  }

  return Chain;
}

bool RISCVTargetLowering::CanLowerReturn(
    CallingConv::ID CallConv, MachineFunction &MF, bool IsVarArg,
    const SmallVectorImpl<ISD::OutputArg> &Outs, LLVMContext &Context) const {
  SmallVector<CCValAssign, 16> RVLocs;
  CCState CCInfo(CallConv, IsVarArg, MF, RVLocs, Context);

  Optional<unsigned> FirstMaskArgument;
  if (Subtarget.hasStdExtV()) {
    PreAssignMask(Outs, FirstMaskArgument, CCInfo);
  }

  for (unsigned i = 0, e = Outs.size(); i != e; ++i) {
    MVT VT = Outs[i].VT;
    ISD::ArgFlagsTy ArgFlags = Outs[i].Flags;
    RISCVABI::ABI ABI = MF.getSubtarget<RISCVSubtarget>().getTargetABI();
    if (CC_RISCV(MF.getDataLayout(), ABI, i, VT, VT, CCValAssign::Full,
                 ArgFlags, CCInfo, /*IsFixed=*/true, /*IsRet=*/true, nullptr,
                 this, FirstMaskArgument))
      return false;
  }
  return true;
}

SDValue
RISCVTargetLowering::LowerReturn(SDValue Chain, CallingConv::ID CallConv,
                                 bool IsVarArg,
                                 const SmallVectorImpl<ISD::OutputArg> &Outs,
                                 const SmallVectorImpl<SDValue> &OutVals,
                                 const SDLoc &DL, SelectionDAG &DAG) const {
  const MachineFunction &MF = DAG.getMachineFunction();
  const RISCVSubtarget &STI = MF.getSubtarget<RISCVSubtarget>();

  // Stores the assignment of the return value to a location.
  SmallVector<CCValAssign, 16> RVLocs;

  // Info about the registers and stack slot.
  CCState CCInfo(CallConv, IsVarArg, DAG.getMachineFunction(), RVLocs,
                 *DAG.getContext());

  analyzeOutputArgs(DAG.getMachineFunction(), CCInfo, Outs, /*IsRet=*/true,
                    nullptr);

  if (CallConv == CallingConv::GHC && !RVLocs.empty())
    report_fatal_error("GHC functions return void only");

  SDValue Glue;
  SmallVector<SDValue, 4> RetOps(1, Chain);

  // Copy the result values into the output registers.
  for (unsigned i = 0, e = RVLocs.size(); i < e; ++i) {
    SDValue Val = OutVals[i];
    CCValAssign &VA = RVLocs[i];
    assert(VA.isRegLoc() && "Can only return in registers!");

    if (VA.getLocVT() == MVT::i32 && VA.getValVT() == MVT::f64) {
      // Handle returning f64 on RV32D with a soft float ABI.
      assert(VA.isRegLoc() && "Expected return via registers");
      SDValue SplitF64 = DAG.getNode(RISCVISD::SplitF64, DL,
                                     DAG.getVTList(MVT::i32, MVT::i32), Val);
      SDValue Lo = SplitF64.getValue(0);
      SDValue Hi = SplitF64.getValue(1);
      Register RegLo = VA.getLocReg();
      assert(RegLo < RISCV::X31 && "Invalid register pair");
      Register RegHi = RegLo + 1;

      if (STI.isRegisterReservedByUser(RegLo) ||
          STI.isRegisterReservedByUser(RegHi))
        MF.getFunction().getContext().diagnose(DiagnosticInfoUnsupported{
            MF.getFunction(),
            "Return value register required, but has been reserved."});

      Chain = DAG.getCopyToReg(Chain, DL, RegLo, Lo, Glue);
      Glue = Chain.getValue(1);
      RetOps.push_back(DAG.getRegister(RegLo, MVT::i32));
      Chain = DAG.getCopyToReg(Chain, DL, RegHi, Hi, Glue);
      Glue = Chain.getValue(1);
      RetOps.push_back(DAG.getRegister(RegHi, MVT::i32));
    } else {
      // Handle a 'normal' return.
      Val = convertValVTToLocVT(DAG, Val, VA, DL);
      Chain = DAG.getCopyToReg(Chain, DL, VA.getLocReg(), Val, Glue);

      if (STI.isRegisterReservedByUser(VA.getLocReg()))
        MF.getFunction().getContext().diagnose(DiagnosticInfoUnsupported{
            MF.getFunction(),
            "Return value register required, but has been reserved."});

      // Guarantee that all emitted copies are stuck together.
      Glue = Chain.getValue(1);
      RetOps.push_back(DAG.getRegister(VA.getLocReg(), VA.getLocVT()));
    }
  }

  RetOps[0] = Chain; // Update chain.

  // Add the glue node if we have it.
  if (Glue.getNode()) {
    RetOps.push_back(Glue);
  }

  // Interrupt service routines use different return instructions.
  const Function &Func = DAG.getMachineFunction().getFunction();
  if (Func.hasFnAttribute("interrupt")) {
    if (!Func.getReturnType()->isVoidTy())
      report_fatal_error(
          "Functions with the interrupt attribute must have void return type!");

    MachineFunction &MF = DAG.getMachineFunction();
    StringRef Kind =
      MF.getFunction().getFnAttribute("interrupt").getValueAsString();

    unsigned RetOpc;
    if (Kind == "user")
      RetOpc = RISCVISD::URET_FLAG;
    else if (Kind == "supervisor")
      RetOpc = RISCVISD::SRET_FLAG;
    else
      RetOpc = RISCVISD::MRET_FLAG;

    return DAG.getNode(RetOpc, DL, MVT::Other, RetOps);
  }

  return DAG.getNode(RISCVISD::RET_FLAG, DL, MVT::Other, RetOps);
}

void RISCVTargetLowering::validateCCReservedRegs(
    const SmallVectorImpl<std::pair<llvm::Register, llvm::SDValue>> &Regs,
    MachineFunction &MF) const {
  const Function &F = MF.getFunction();
  const RISCVSubtarget &STI = MF.getSubtarget<RISCVSubtarget>();

  if (std::any_of(std::begin(Regs), std::end(Regs), [&STI](auto Reg) {
        return STI.isRegisterReservedByUser(Reg.first);
      }))
    F.getContext().diagnose(DiagnosticInfoUnsupported{
        F, "Argument register required, but has been reserved."});
}

bool RISCVTargetLowering::mayBeEmittedAsTailCall(const CallInst *CI) const {
  return CI->isTailCall();
}

const char *RISCVTargetLowering::getTargetNodeName(unsigned Opcode) const {
#define NODE_NAME_CASE(NODE)                                                   \
  case RISCVISD::NODE:                                                         \
    return "RISCVISD::" #NODE;
  // clang-format off
  switch ((RISCVISD::NodeType)Opcode) {
  case RISCVISD::FIRST_NUMBER:
    break;
  NODE_NAME_CASE(RET_FLAG)
  NODE_NAME_CASE(URET_FLAG)
  NODE_NAME_CASE(SRET_FLAG)
  NODE_NAME_CASE(MRET_FLAG)
  NODE_NAME_CASE(CALL)
  NODE_NAME_CASE(SELECT_CC)
  NODE_NAME_CASE(BuildPairF64)
  NODE_NAME_CASE(SplitF64)
  NODE_NAME_CASE(TAIL)
  NODE_NAME_CASE(SLLW)
  NODE_NAME_CASE(SRAW)
  NODE_NAME_CASE(SRLW)
  NODE_NAME_CASE(DIVW)
  NODE_NAME_CASE(DIVUW)
  NODE_NAME_CASE(REMUW)
  NODE_NAME_CASE(ROLW)
  NODE_NAME_CASE(RORW)
  NODE_NAME_CASE(FSLW)
  NODE_NAME_CASE(FSRW)
  NODE_NAME_CASE(FMV_W_X_RV64)
  NODE_NAME_CASE(FMV_X_ANYEXTW_RV64)
  NODE_NAME_CASE(READ_CYCLE_WIDE)
  NODE_NAME_CASE(GREVI)
  NODE_NAME_CASE(GREVIW)
  NODE_NAME_CASE(GORCI)
  NODE_NAME_CASE(GORCIW)

  NODE_NAME_CASE(VEXT_X_V)
  NODE_NAME_CASE(EXTRACT_VECTOR_ELT)
  NODE_NAME_CASE(EXTRACT_VECTOR_ELT_FP)
  NODE_NAME_CASE(SIGN_EXTEND_VECTOR)
  NODE_NAME_CASE(ZERO_EXTEND_VECTOR)
  NODE_NAME_CASE(TRUNCATE_VECTOR)
  NODE_NAME_CASE(SHUFFLE_EXTEND)
  NODE_NAME_CASE(SIGN_EXTEND_BITS_INREG)
  NODE_NAME_CASE(ZERO_EXTEND_BITS_INREG)
  NODE_NAME_CASE(LOWER_PART)

  NODE_NAME_CASE(VZIP2)
  NODE_NAME_CASE(VUNZIP2)
  NODE_NAME_CASE(VTRN)

  NODE_NAME_CASE(REDUCE_AND_MASK)
  NODE_NAME_CASE(REDUCE_OR_MASK)
  NODE_NAME_CASE(REDUCE_XOR_MASK)

  NODE_NAME_CASE(FP_TO_FP_REG)
  }
  // clang-format on
  return nullptr;
#undef NODE_NAME_CASE
}

/// getConstraintType - Given a constraint letter, return the type of
/// constraint it is for this target.
RISCVTargetLowering::ConstraintType
RISCVTargetLowering::getConstraintType(StringRef Constraint) const {
  if (Constraint.size() == 1) {
    switch (Constraint[0]) {
    default:
      break;
    case 'f':
    case 'v':
      return C_RegisterClass;
    case 'I':
    case 'J':
    case 'K':
      return C_Immediate;
    case 'A':
      return C_Memory;
    }
  }
  return TargetLowering::getConstraintType(Constraint);
}

std::pair<unsigned, const TargetRegisterClass *>
RISCVTargetLowering::getRegForInlineAsmConstraint(const TargetRegisterInfo *TRI,
                                                  StringRef Constraint,
                                                  MVT VT) const {
  // First, see if this is a constraint that directly corresponds to a
  // RISCV register class.
  if (Constraint.size() == 1) {
    switch (Constraint[0]) {
    case 'r':
      return std::make_pair(0U, &RISCV::GPRRegClass);
    case 'f':
      if (Subtarget.hasStdExtF() && VT == MVT::f32)
        return std::make_pair(0U, &RISCV::FPR32RegClass);
      if (Subtarget.hasStdExtD() && VT == MVT::f64)
        return std::make_pair(0U, &RISCV::FPR64RegClass);
      break;
    case 'v':
      for (const auto *RC : {&RISCV::VRRegClass, &RISCV::VRM2RegClass,
                             &RISCV::VRM4RegClass, &RISCV::VRM8RegClass}) {
        if (TRI->isTypeLegalForClass(*RC, VT.SimpleTy))
          return std::make_pair(0U, RC);
      }
      break;
    default:
      break;
    }
  }

  // Clang will correctly decode the usage of register name aliases into their
  // official names. However, other frontends like `rustc` do not. This allows
  // users of these frontends to use the ABI names for registers in LLVM-style
  // register constraints.
  unsigned XRegFromAlias = StringSwitch<unsigned>(Constraint.lower())
                               .Case("{zero}", RISCV::X0)
                               .Case("{ra}", RISCV::X1)
                               .Case("{sp}", RISCV::X2)
                               .Case("{gp}", RISCV::X3)
                               .Case("{tp}", RISCV::X4)
                               .Case("{t0}", RISCV::X5)
                               .Case("{t1}", RISCV::X6)
                               .Case("{t2}", RISCV::X7)
                               .Cases("{s0}", "{fp}", RISCV::X8)
                               .Case("{s1}", RISCV::X9)
                               .Case("{a0}", RISCV::X10)
                               .Case("{a1}", RISCV::X11)
                               .Case("{a2}", RISCV::X12)
                               .Case("{a3}", RISCV::X13)
                               .Case("{a4}", RISCV::X14)
                               .Case("{a5}", RISCV::X15)
                               .Case("{a6}", RISCV::X16)
                               .Case("{a7}", RISCV::X17)
                               .Case("{s2}", RISCV::X18)
                               .Case("{s3}", RISCV::X19)
                               .Case("{s4}", RISCV::X20)
                               .Case("{s5}", RISCV::X21)
                               .Case("{s6}", RISCV::X22)
                               .Case("{s7}", RISCV::X23)
                               .Case("{s8}", RISCV::X24)
                               .Case("{s9}", RISCV::X25)
                               .Case("{s10}", RISCV::X26)
                               .Case("{s11}", RISCV::X27)
                               .Case("{t3}", RISCV::X28)
                               .Case("{t4}", RISCV::X29)
                               .Case("{t5}", RISCV::X30)
                               .Case("{t6}", RISCV::X31)
                               .Default(RISCV::NoRegister);
  if (XRegFromAlias != RISCV::NoRegister)
    return std::make_pair(XRegFromAlias, &RISCV::GPRRegClass);

  // Since TargetLowering::getRegForInlineAsmConstraint uses the name of the
  // TableGen record rather than the AsmName to choose registers for InlineAsm
  // constraints, plus we want to match those names to the widest floating point
  // register type available, manually select floating point registers here.
  //
  // The second case is the ABI name of the register, so that frontends can also
  // use the ABI names in register constraint lists.
  if (Subtarget.hasStdExtF() || Subtarget.hasStdExtD()) {
    unsigned FReg = StringSwitch<unsigned>(Constraint.lower())
                        .Cases("{f0}", "{ft0}", RISCV::F0_F)
                        .Cases("{f1}", "{ft1}", RISCV::F1_F)
                        .Cases("{f2}", "{ft2}", RISCV::F2_F)
                        .Cases("{f3}", "{ft3}", RISCV::F3_F)
                        .Cases("{f4}", "{ft4}", RISCV::F4_F)
                        .Cases("{f5}", "{ft5}", RISCV::F5_F)
                        .Cases("{f6}", "{ft6}", RISCV::F6_F)
                        .Cases("{f7}", "{ft7}", RISCV::F7_F)
                        .Cases("{f8}", "{fs0}", RISCV::F8_F)
                        .Cases("{f9}", "{fs1}", RISCV::F9_F)
                        .Cases("{f10}", "{fa0}", RISCV::F10_F)
                        .Cases("{f11}", "{fa1}", RISCV::F11_F)
                        .Cases("{f12}", "{fa2}", RISCV::F12_F)
                        .Cases("{f13}", "{fa3}", RISCV::F13_F)
                        .Cases("{f14}", "{fa4}", RISCV::F14_F)
                        .Cases("{f15}", "{fa5}", RISCV::F15_F)
                        .Cases("{f16}", "{fa6}", RISCV::F16_F)
                        .Cases("{f17}", "{fa7}", RISCV::F17_F)
                        .Cases("{f18}", "{fs2}", RISCV::F18_F)
                        .Cases("{f19}", "{fs3}", RISCV::F19_F)
                        .Cases("{f20}", "{fs4}", RISCV::F20_F)
                        .Cases("{f21}", "{fs5}", RISCV::F21_F)
                        .Cases("{f22}", "{fs6}", RISCV::F22_F)
                        .Cases("{f23}", "{fs7}", RISCV::F23_F)
                        .Cases("{f24}", "{fs8}", RISCV::F24_F)
                        .Cases("{f25}", "{fs9}", RISCV::F25_F)
                        .Cases("{f26}", "{fs10}", RISCV::F26_F)
                        .Cases("{f27}", "{fs11}", RISCV::F27_F)
                        .Cases("{f28}", "{ft8}", RISCV::F28_F)
                        .Cases("{f29}", "{ft9}", RISCV::F29_F)
                        .Cases("{f30}", "{ft10}", RISCV::F30_F)
                        .Cases("{f31}", "{ft11}", RISCV::F31_F)
                        .Default(RISCV::NoRegister);
    if (FReg != RISCV::NoRegister) {
      assert(RISCV::F0_F <= FReg && FReg <= RISCV::F31_F && "Unknown fp-reg");
      if (Subtarget.hasStdExtD()) {
        unsigned RegNo = FReg - RISCV::F0_F;
        unsigned DReg = RISCV::F0_D + RegNo;
        return std::make_pair(DReg, &RISCV::FPR64RegClass);
      }
      return std::make_pair(FReg, &RISCV::FPR32RegClass);
    }
  }

  return TargetLowering::getRegForInlineAsmConstraint(TRI, Constraint, VT);
}

unsigned
RISCVTargetLowering::getInlineAsmMemConstraint(StringRef ConstraintCode) const {
  // Currently only support length 1 constraints.
  if (ConstraintCode.size() == 1) {
    switch (ConstraintCode[0]) {
    case 'A':
      return InlineAsm::Constraint_A;
    default:
      break;
    }
  }

  return TargetLowering::getInlineAsmMemConstraint(ConstraintCode);
}

void RISCVTargetLowering::LowerAsmOperandForConstraint(
    SDValue Op, std::string &Constraint, std::vector<SDValue> &Ops,
    SelectionDAG &DAG) const {
  // Currently only support length 1 constraints.
  if (Constraint.length() == 1) {
    switch (Constraint[0]) {
    case 'I':
      // Validate & create a 12-bit signed immediate operand.
      if (auto *C = dyn_cast<ConstantSDNode>(Op)) {
        uint64_t CVal = C->getSExtValue();
        if (isInt<12>(CVal))
          Ops.push_back(
              DAG.getTargetConstant(CVal, SDLoc(Op), Subtarget.getXLenVT()));
      }
      return;
    case 'J':
      // Validate & create an integer zero operand.
      if (auto *C = dyn_cast<ConstantSDNode>(Op))
        if (C->getZExtValue() == 0)
          Ops.push_back(
              DAG.getTargetConstant(0, SDLoc(Op), Subtarget.getXLenVT()));
      return;
    case 'K':
      // Validate & create a 5-bit unsigned immediate operand.
      if (auto *C = dyn_cast<ConstantSDNode>(Op)) {
        uint64_t CVal = C->getZExtValue();
        if (isUInt<5>(CVal))
          Ops.push_back(
              DAG.getTargetConstant(CVal, SDLoc(Op), Subtarget.getXLenVT()));
      }
      return;
    default:
      break;
    }
  }
  TargetLowering::LowerAsmOperandForConstraint(Op, Constraint, Ops, DAG);
}

Instruction *RISCVTargetLowering::emitLeadingFence(IRBuilder<> &Builder,
                                                   Instruction *Inst,
                                                   AtomicOrdering Ord) const {
  if (isa<LoadInst>(Inst) && Ord == AtomicOrdering::SequentiallyConsistent)
    return Builder.CreateFence(Ord);
  if (isa<StoreInst>(Inst) && isReleaseOrStronger(Ord))
    return Builder.CreateFence(AtomicOrdering::Release);
  return nullptr;
}

Instruction *RISCVTargetLowering::emitTrailingFence(IRBuilder<> &Builder,
                                                    Instruction *Inst,
                                                    AtomicOrdering Ord) const {
  if (isa<LoadInst>(Inst) && isAcquireOrStronger(Ord))
    return Builder.CreateFence(AtomicOrdering::Acquire);
  return nullptr;
}

TargetLowering::AtomicExpansionKind
RISCVTargetLowering::shouldExpandAtomicRMWInIR(AtomicRMWInst *AI) const {
  // atomicrmw {fadd,fsub} must be expanded to use compare-exchange, as floating
  // point operations can't be used in an lr/sc sequence without breaking the
  // forward-progress guarantee.
  if (AI->isFloatingPointOperation())
    return AtomicExpansionKind::CmpXChg;
  // Always expand RMW if AMOs are not possible.
  if (Subtarget.doNotUseAMO())
    return AtomicExpansionKind::CmpXChg;

  unsigned Size = AI->getType()->getPrimitiveSizeInBits();
  if (Size == 8 || Size == 16)
    return AtomicExpansionKind::MaskedIntrinsic;
  return AtomicExpansionKind::None;
}

static Intrinsic::ID
getIntrinsicForMaskedAtomicRMWBinOp(unsigned XLen, AtomicRMWInst::BinOp BinOp) {
  if (XLen == 32) {
    switch (BinOp) {
    default:
      llvm_unreachable("Unexpected AtomicRMW BinOp");
    case AtomicRMWInst::Xchg:
      return Intrinsic::riscv_masked_atomicrmw_xchg_i32;
    case AtomicRMWInst::Add:
      return Intrinsic::riscv_masked_atomicrmw_add_i32;
    case AtomicRMWInst::Sub:
      return Intrinsic::riscv_masked_atomicrmw_sub_i32;
    case AtomicRMWInst::Nand:
      return Intrinsic::riscv_masked_atomicrmw_nand_i32;
    case AtomicRMWInst::Max:
      return Intrinsic::riscv_masked_atomicrmw_max_i32;
    case AtomicRMWInst::Min:
      return Intrinsic::riscv_masked_atomicrmw_min_i32;
    case AtomicRMWInst::UMax:
      return Intrinsic::riscv_masked_atomicrmw_umax_i32;
    case AtomicRMWInst::UMin:
      return Intrinsic::riscv_masked_atomicrmw_umin_i32;
    }
  }

  if (XLen == 64) {
    switch (BinOp) {
    default:
      llvm_unreachable("Unexpected AtomicRMW BinOp");
    case AtomicRMWInst::Xchg:
      return Intrinsic::riscv_masked_atomicrmw_xchg_i64;
    case AtomicRMWInst::Add:
      return Intrinsic::riscv_masked_atomicrmw_add_i64;
    case AtomicRMWInst::Sub:
      return Intrinsic::riscv_masked_atomicrmw_sub_i64;
    case AtomicRMWInst::Nand:
      return Intrinsic::riscv_masked_atomicrmw_nand_i64;
    case AtomicRMWInst::Max:
      return Intrinsic::riscv_masked_atomicrmw_max_i64;
    case AtomicRMWInst::Min:
      return Intrinsic::riscv_masked_atomicrmw_min_i64;
    case AtomicRMWInst::UMax:
      return Intrinsic::riscv_masked_atomicrmw_umax_i64;
    case AtomicRMWInst::UMin:
      return Intrinsic::riscv_masked_atomicrmw_umin_i64;
    }
  }

  llvm_unreachable("Unexpected XLen\n");
}

Value *RISCVTargetLowering::emitMaskedAtomicRMWIntrinsic(
    IRBuilder<> &Builder, AtomicRMWInst *AI, Value *AlignedAddr, Value *Incr,
    Value *Mask, Value *ShiftAmt, AtomicOrdering Ord) const {
  unsigned XLen = Subtarget.getXLen();
  Value *Ordering =
      Builder.getIntN(XLen, static_cast<uint64_t>(AI->getOrdering()));
  Type *Tys[] = {AlignedAddr->getType()};
  Function *LrwOpScwLoop = Intrinsic::getDeclaration(
      AI->getModule(),
      getIntrinsicForMaskedAtomicRMWBinOp(XLen, AI->getOperation()), Tys);

  if (XLen == 64) {
    Incr = Builder.CreateSExt(Incr, Builder.getInt64Ty());
    Mask = Builder.CreateSExt(Mask, Builder.getInt64Ty());
    ShiftAmt = Builder.CreateSExt(ShiftAmt, Builder.getInt64Ty());
  }

  Value *Result;

  // Must pass the shift amount needed to sign extend the loaded value prior
  // to performing a signed comparison for min/max. ShiftAmt is the number of
  // bits to shift the value into position. Pass XLen-ShiftAmt-ValWidth, which
  // is the number of bits to left+right shift the value in order to
  // sign-extend.
  if (AI->getOperation() == AtomicRMWInst::Min ||
      AI->getOperation() == AtomicRMWInst::Max) {
    const DataLayout &DL = AI->getModule()->getDataLayout();
    unsigned ValWidth =
        DL.getTypeStoreSizeInBits(AI->getValOperand()->getType());
    Value *SextShamt =
        Builder.CreateSub(Builder.getIntN(XLen, XLen - ValWidth), ShiftAmt);
    Result = Builder.CreateCall(LrwOpScwLoop,
                                {AlignedAddr, Incr, Mask, SextShamt, Ordering});
  } else {
    Result =
        Builder.CreateCall(LrwOpScwLoop, {AlignedAddr, Incr, Mask, Ordering});
  }

  if (XLen == 64)
    Result = Builder.CreateTrunc(Result, Builder.getInt32Ty());
  return Result;
}

TargetLowering::AtomicExpansionKind
RISCVTargetLowering::shouldExpandAtomicCmpXchgInIR(
    AtomicCmpXchgInst *CI) const {
  unsigned Size = CI->getCompareOperand()->getType()->getPrimitiveSizeInBits();
  if (Size == 8 || Size == 16)
    return AtomicExpansionKind::MaskedIntrinsic;
  return AtomicExpansionKind::None;
}

Value *RISCVTargetLowering::emitMaskedAtomicCmpXchgIntrinsic(
    IRBuilder<> &Builder, AtomicCmpXchgInst *CI, Value *AlignedAddr,
    Value *CmpVal, Value *NewVal, Value *Mask, AtomicOrdering Ord) const {
  unsigned XLen = Subtarget.getXLen();
  Value *Ordering = Builder.getIntN(XLen, static_cast<uint64_t>(Ord));
  Intrinsic::ID CmpXchgIntrID = Intrinsic::riscv_masked_cmpxchg_i32;
  if (XLen == 64) {
    CmpVal = Builder.CreateSExt(CmpVal, Builder.getInt64Ty());
    NewVal = Builder.CreateSExt(NewVal, Builder.getInt64Ty());
    Mask = Builder.CreateSExt(Mask, Builder.getInt64Ty());
    CmpXchgIntrID = Intrinsic::riscv_masked_cmpxchg_i64;
  }
  Type *Tys[] = {AlignedAddr->getType()};
  Function *MaskedCmpXchg =
      Intrinsic::getDeclaration(CI->getModule(), CmpXchgIntrID, Tys);
  Value *Result = Builder.CreateCall(
      MaskedCmpXchg, {AlignedAddr, CmpVal, NewVal, Mask, Ordering});
  if (XLen == 64)
    Result = Builder.CreateTrunc(Result, Builder.getInt32Ty());
  return Result;
}

bool RISCVTargetLowering::isFMAFasterThanFMulAndFAdd(const MachineFunction &MF,
                                                     EVT VT) const {
  VT = VT.getScalarType();

  if (!VT.isSimple())
    return false;

  switch (VT.getSimpleVT().SimpleTy) {
  case MVT::f32:
    return Subtarget.hasStdExtF();
  case MVT::f64:
    return Subtarget.hasStdExtD();
  default:
    break;
  }

  return false;
}

Register RISCVTargetLowering::getExceptionPointerRegister(
    const Constant *PersonalityFn) const {
  return RISCV::X10;
}

Register RISCVTargetLowering::getExceptionSelectorRegister(
    const Constant *PersonalityFn) const {
  return RISCV::X11;
}

bool RISCVTargetLowering::allowsMisalignedMemoryAccesses(
    EVT E, unsigned AddrSpace, unsigned Align, MachineMemOperand::Flags Flags,
    bool *Fast) const {
  if (!E.isScalableVector())
    return false;

  // Scalable vectors enforce only the alignment of the element type.
  // There is no reason to think these should be any slower.
  if (Fast)
    *Fast = true;

  EVT ElementType = E.getVectorElementType();
  return Align >= ElementType.getStoreSize();
}

bool RISCVTargetLowering::shouldExtendTypeInLibCall(EVT Type) const {
  // Return false to suppress the unnecessary extensions if the LibCall
  // arguments or return value is f32 type for LP64 ABI.
  RISCVABI::ABI ABI = Subtarget.getTargetABI();
  if (ABI == RISCVABI::ABI_LP64 && (Type == MVT::f32))
    return false;

  return true;
}

bool RISCVTargetLowering::decomposeMulByConstant(LLVMContext &Context, EVT VT,
                                                 SDValue C) const {
  // Check integral scalar types.
  if (VT.isScalarInteger()) {
    // Do not perform the transformation on riscv32 with the M extension.
    if (!Subtarget.is64Bit() && Subtarget.hasStdExtM())
      return false;
    if (auto *ConstNode = dyn_cast<ConstantSDNode>(C.getNode())) {
      if (ConstNode->getAPIntValue().getBitWidth() > 8 * sizeof(int64_t))
        return false;
      int64_t Imm = ConstNode->getSExtValue();
      if (isPowerOf2_64(Imm + 1) || isPowerOf2_64(Imm - 1) ||
          isPowerOf2_64(1 - Imm) || isPowerOf2_64(-1 - Imm))
        return true;
    }
  }

  return false;
}

#define GET_REGISTER_MATCHER
#include "RISCVGenAsmMatcher.inc"

Register
RISCVTargetLowering::getRegisterByName(const char *RegName, LLT VT,
                                       const MachineFunction &MF) const {
  Register Reg = MatchRegisterAltName(RegName);
  if (Reg == RISCV::NoRegister)
    Reg = MatchRegisterName(RegName);
  if (Reg == RISCV::NoRegister)
    report_fatal_error(
        Twine("Invalid register name \"" + StringRef(RegName) + "\"."));
  BitVector ReservedRegs = Subtarget.getRegisterInfo()->getReservedRegs(MF);
  if (!ReservedRegs.test(Reg) && !Subtarget.isRegisterReservedByUser(Reg))
    report_fatal_error(Twine("Trying to obtain non-reserved register \"" +
                             StringRef(RegName) + "\"."));
  return Reg;
}

bool RISCVTargetLowering::shouldSinkOperands(
    Instruction *I, SmallVectorImpl<Use *> &Ops) const {
  if (!isa<ScalableVectorType>(I->getType()))
    return false;

  auto *CurrentIntrinsic = dyn_cast<IntrinsicInst>(I);
  const RISCVEPIIntrinsicsTable::EPIIntrinsicInfo *EII =
      CurrentIntrinsic ? RISCVEPIIntrinsicsTable::getEPIIntrinsicInfo(
                             CurrentIntrinsic->getIntrinsicID())
                       : nullptr;
  if (EII && !EII->ExtendedOperand)
    EII = nullptr;

  // Sinking broadcasts is always beneficial because it avoids keeping
  // vector registers alive and often they can be folded into the operand.
  for (unsigned OpI = 0, E = I->getNumOperands(); OpI < E; OpI++) {
    Use &U = I->getOperandUse(OpI);
    if (auto *SI = dyn_cast<ShuffleVectorInst>(&U)) {
      if (SI->isZeroEltSplat()) {
        Ops.push_back(&SI->getOperandUse(0));
        Ops.push_back(&U);
      }
    } else if (auto *II = dyn_cast<IntrinsicInst>(&U)) {
      switch (II->getIntrinsicID()) {
      case Intrinsic::epi_vmv_v_x:
      case Intrinsic::epi_vfmv_v_f: {
        // Do only this for the operand that can be extended from scalar otherwise 
        // we may end introducing unnecesary broadcasts.
        if (EII && OpI == (EII->ExtendedOperand - 1))  {
          Ops.push_back(&U);
        }
        break;
      }
      default:
        break;
      }
    }
    if (!Ops.empty())
      return true;
  }

  return false;
}

TargetLoweringBase::LegalizeTypeAction
RISCVTargetLowering::getPreferredVectorAction(MVT VT) const {
  switch (VT.SimpleTy) {
  case MVT::nxv1i32:
  case MVT::nxv1i16:
  case MVT::nxv1i8:
  case MVT::nxv2i16:
  case MVT::nxv2i8:
  case MVT::nxv4i8:
  case MVT::nxv1f32:
    return TypeWidenVector;
  default:
    break;
  }

  return TargetLoweringBase::getPreferredVectorAction(VT);
}

const TargetRegisterClass *
RISCVTargetLowering::getRegClassFor(MVT VT, bool isDivergent) const {
  if (Subtarget.onlyLMUL1()) {
    switch (VT.SimpleTy) {
    default:
      break;
    case MVT::nxv2i64:
    case MVT::nxv4i32:
    case MVT::nxv8i16:
    case MVT::nxv16i8:
    case MVT::nxv2f64:
    case MVT::nxv4f32:
      return &RISCV::VRM2RegClass;
    }
  }
  return TargetLowering::getRegClassFor(VT, isDivergent);
}
