//===-- RISCVISelDAGToDAG.cpp - A dag to dag inst selector for RISCV ------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file defines an instruction selector for the RISCV target.
//
//===----------------------------------------------------------------------===//

#include "RISCVISelDAGToDAG.h"
#include "MCTargetDesc/RISCVMCTargetDesc.h"
#include "Utils/RISCVMatInt.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/SelectionDAGISel.h"
#include "llvm/IR/IntrinsicsEPI.h"
#include "llvm/Support/Alignment.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

#define DEBUG_TYPE "riscv-isel"

void RISCVDAGToDAGISel::PostprocessISelDAG() {
  doPeepholeLoadStoreADDI();
  if (Subtarget->onlyLMUL1()) {
    doPeepholeWideningNarrowingLMUL1();
  }
}

static SDNode *selectImm(SelectionDAG *CurDAG, const SDLoc &DL, int64_t Imm,
                         MVT XLenVT) {
  RISCVMatInt::InstSeq Seq;
  RISCVMatInt::generateInstSeq(Imm, XLenVT == MVT::i64, Seq);

  SDNode *Result = nullptr;
  SDValue SrcReg = CurDAG->getRegister(RISCV::X0, XLenVT);
  for (RISCVMatInt::Inst &Inst : Seq) {
    SDValue SDImm = CurDAG->getTargetConstant(Inst.Imm, DL, XLenVT);
    if (Inst.Opc == RISCV::LUI)
      Result = CurDAG->getMachineNode(RISCV::LUI, DL, XLenVT, SDImm);
    else
      Result = CurDAG->getMachineNode(Inst.Opc, DL, XLenVT, SrcReg, SDImm);

    // Only the first instruction has X0 as its source.
    SrcReg = SDValue(Result, 0);
  }

  return Result;
}

static SDNode *SelectSlideLeftFill(SDNode *Node, SelectionDAG *CurDAG,
                                   SDValue EVL1, SDValue EVL2, SDValue Offset,
                                   MVT XLenVT) {
  EVT VT = Node->getValueType(0);
  EVT VTMask = VT.changeVectorElementType(MVT::i1);
  SDLoc DL(Node);
  SDValue V1 = Node->getOperand(1);
  SDValue V2 = Node->getOperand(2);

  uint64_t SEW = VT.getVectorElementType().getSizeInBits().getFixedSize();

  ElementCount EC = VT.getVectorElementCount();
  assert(EC.isScalable() && "Unexpected VT");
  uint64_t LMul = EC.getKnownMinValue() * SEW / 64; // FIXME: ELEN=64 hardcoded.

  unsigned VIDInst, VSlideDownInst, VSlideUpInst, VMSetLessThanUnsignedInst,
      VMergeInst;
  switch (LMul) {
  default:
    llvm_unreachable("Unexpected LMUL");
  case 1:
    VIDInst = RISCV::PseudoVID_V_M1;
    VSlideDownInst = RISCV::PseudoVSLIDEDOWN_VX_M1;
    VSlideUpInst = RISCV::PseudoVSLIDEUP_VX_M1;
    VMSetLessThanUnsignedInst = RISCV::PseudoVMSLTU_VX_M1;
    VMergeInst = RISCV::PseudoVMERGE_VVM_M1;
    break;
  case 2:
    VIDInst = RISCV::PseudoVID_V_M2;
    VSlideDownInst = RISCV::PseudoVSLIDEDOWN_VX_M2;
    VSlideUpInst = RISCV::PseudoVSLIDEUP_VX_M2;
    VMSetLessThanUnsignedInst = RISCV::PseudoVMSLTU_VX_M2;
    VMergeInst = RISCV::PseudoVMERGE_VVM_M2;
    break;
  case 4:
    VIDInst = RISCV::PseudoVID_V_M4;
    VSlideDownInst = RISCV::PseudoVSLIDEDOWN_VX_M4;
    VSlideUpInst = RISCV::PseudoVSLIDEUP_VX_M4;
    VMSetLessThanUnsignedInst = RISCV::PseudoVMSLTU_VX_M4;
    VMergeInst = RISCV::PseudoVMERGE_VVM_M4;
    break;
  case 8:
    VIDInst = RISCV::PseudoVID_V_M8;
    VSlideDownInst = RISCV::PseudoVSLIDEDOWN_VX_M8;
    VSlideUpInst = RISCV::PseudoVSLIDEUP_VX_M8;
    VMSetLessThanUnsignedInst = RISCV::PseudoVMSLTU_VX_M8;
    VMergeInst = RISCV::PseudoVMERGE_VVM_M8;
    break;
  }

  // slideleftfill(v1, v2, offset, evl1, evl2) is implemented like this
  //
  // t1 <- vslidedown v1, evl1, offset
  // diff  <- evl1 - offset
  // t2 <- vslidedup v2, evl2, diff
  // vi <- vid, evl2
  // v0 <- vmsltu vi, diff, evl2
  // v <- vmerge t2, t1, v0, evl2

  SDValue Undef =
      SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, VT), 0);
  SDValue UndefMask =
      SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, VTMask), 0);

  SDValue SEWOperand = CurDAG->getTargetConstant(SEW, DL, MVT::i64);
  SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, VTMask);

  auto *VSLIDEDOWN = CurDAG->getMachineNode(
      VSlideDownInst, DL, VT, {Undef, V1, Offset, NoRegMask, EVL1, SEWOperand});

  auto *Difference =
      CurDAG->getMachineNode(RISCV::SUB, DL, XLenVT, EVL1, Offset);

  auto *VSLIDEUP = CurDAG->getMachineNode(
      VSlideUpInst, DL, VT,
      {Undef, V2, SDValue(Difference, 0), NoRegMask, EVL2, SEWOperand});

  auto *VID = CurDAG->getMachineNode(VIDInst, DL, VT,
                                     {Undef, NoRegMask, EVL2, SEWOperand});

  auto *VMSLTU = CurDAG->getMachineNode(VMSetLessThanUnsignedInst, DL, VTMask,
                                        {UndefMask, SDValue(VID, 0),
                                         SDValue(Difference, 0), NoRegMask,
                                         EVL2, SEWOperand});
  SDValue VMSLTU_V0 = CurDAG->getCopyToReg(
      CurDAG->getEntryNode(), DL, RISCV::V0, SDValue(VMSLTU, 0), SDValue());
  SDValue V0GlueCopy = SDValue(VMSLTU_V0.getNode(), 1);
  SDValue V0Reg = CurDAG->getRegister(RISCV::V0, VTMask);

  auto *VMERGE =
      CurDAG->getMachineNode(VMergeInst, DL, VT,
                             {SDValue(VSLIDEUP, 0), SDValue(VSLIDEDOWN, 0),
                              V0Reg, EVL2, SEWOperand, V0GlueCopy});
  return VMERGE;
}

// Returns true if the Node is an ISD::AND with a constant argument. If so,
// set Mask to that constant value.
static bool isConstantMask(SDNode *Node, uint64_t &Mask) {
  if (Node->getOpcode() == ISD::AND &&
      Node->getOperand(1).getOpcode() == ISD::Constant) {
    Mask = cast<ConstantSDNode>(Node->getOperand(1))->getZExtValue();
    return true;
  }
  return false;
}

void RISCVDAGToDAGISel::SelectEXTRACT_SUBVECTOR_Mask(SDNode *CurrentNode) {
  SDNode *Node = CurrentNode;
  uint64_t Index = Node->getConstantOperandVal(1);
  EVT VT = Node->getValueType(0);
  EVT OpVT = Node->getOperand(0).getValueType();

  SDLoc DL(Node);

  uint64_t NumElementsIn = OpVT.getVectorElementCount().getKnownMinValue();
  uint64_t SEWIn = 64 / NumElementsIn;
  uint64_t NumElementsOut = VT.getVectorElementCount().getKnownMinValue();
  uint64_t SEWOut = 64 / NumElementsOut;

  assert(Index % VT.getVectorMinNumElements() == 0 &&
         "Invalid Idx for EXTRACT_SUBVECTOR");
  // Scale down the index based on the result type.
  Index /= VT.getVectorMinNumElements();

  if (Index != 0) {
    SDValue Offset =
        SDValue(CurDAG->getMachineNode(RISCV::PseudoVSCALE, DL, MVT::i64), 0);
    if (Index > 1) {
      if (isPowerOf2_64(Index)) {
        Offset = SDValue(
            CurDAG->getMachineNode(RISCV::SLLI, DL, MVT::i64,
                                   {Offset, CurDAG->getTargetConstant(
                                                Log2_64(Index), DL, MVT::i64)}),
            0);
      } else {
        Offset = SDValue(
            CurDAG->getMachineNode(
                RISCV::MUL, DL, MVT::i64,
                {Offset, CurDAG->getTargetConstant(Index, DL, MVT::i64)}),
            0);
      }
    }

    // We need to move VLMAX elements first so they end being at the position
    // zero, ready for the widening.
    SDValue AVLMax = CurDAG->getRegister(RISCV::X0, MVT::i64);
    uint64_t LMul = 1;
    uint64_t VTypeI = (Log2_64(SEWIn / 8) << 2) | Log2_64(LMul);
    SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
    SDValue VLMAX = SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL,
                                                   MVT::i64, AVLMax, VTypeIOp),
                            0);

    SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, OpVT);
    SDValue Undef = SDValue(
        CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, OpVT), 0);
    Node = CurDAG->getMachineNode(
        RISCV::PseudoVSLIDEDOWN_VX_M1, DL, OpVT,
        {Undef, Node->getOperand(0), Offset, NoRegMask, VLMAX,
         CurDAG->getTargetConstant(SEWIn, DL, MVT::i64)});
  } else {
    Node = Node->getOperand(0).getNode();
  }

  // This is a sequence of widenings using the underlying vector element types.
  // Underlying integer vector types.
  // nxv8i8
  EVT SourceVT = OpVT.changeVectorElementType(MVT::getIntegerVT(SEWIn));
  // nxv1i64
  EVT TargetVT = VT.changeVectorElementType(MVT::getIntegerVT(SEWOut));

  SDValue SubReg = CurDAG->getTargetConstant(RISCV::sub_vrm2, DL, MVT::i32);
  SDValue Zero = CurDAG->getRegister(RISCV::X0, MVT::i64);

  SDValue AVLMax = CurDAG->getRegister(RISCV::X0, MVT::i64);
  uint64_t LMul = 1;
  uint64_t VTypeI = (Log2_64(SEWOut / 8) << 2) | Log2_64(LMul);
  SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
  SDValue VLMAX = SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL,
                                                 MVT::i64, AVLMax, VTypeIOp),
                          0);

  SDValue Source = SDValue(Node, 0);
  do {
    // SourceVT: nxv8i8, nxv16i4, nxv2i32
    // WidenVT: nxv8i16, nxv16i8, nxv2i64
    EVT NarrowVT = SourceVT; // Kept for later.
    EVT WidenVT = SourceVT.widenIntegerVectorElementType(*CurDAG->getContext());
    // nxv4i16, nxv8i8, nxv1i64
    SourceVT = WidenVT.getHalfNumVectorElementsVT(*CurDAG->getContext());

    SDValue Undef = SDValue(
        CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, WidenVT), 0);

    // Widen:
    // nxv8i8 -> nxv16i8
    // nxv4i16 -> nxv4i32
    // nxv2i32 -> nxv2i64
    // RVV always uses the smaller SEW in conversions.
    uint64_t CurrentSEW =
        NarrowVT.getVectorElementType().getSizeInBits().getFixedSize();
    // MaskVT: nxv8i1, nxv4i1, nxv2i1
    EVT MaskVT = WidenVT.changeVectorElementType(MVT::i1);
    SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, MaskVT);
    SDValue WidenOp =
        SDValue(CurDAG->getMachineNode(
                    RISCV::PseudoVWADDU_VX_M1, DL, WidenVT,
                    {Undef, Source, Zero, NoRegMask, VLMAX,
                     CurDAG->getTargetConstant(CurrentSEW, DL, MVT::i64)}),
                0);
    // Now extract the lower LMUL1.
    // nxv8i16 -> nxv4i16
    // nxv4i32 -> nxv2i32
    // nxv2i64 -> nxv1i64
    Source = SDValue(CurDAG->getMachineNode(TargetOpcode::EXTRACT_SUBREG, DL,
                                            SourceVT, WidenOp, SubReg),
                     0);
  } while (SourceVT != TargetVT);

  // Copy to the regclass of the mask.
  SDValue RC =
      CurDAG->getTargetConstant(RISCV::VRRegClass.getID(), DL, MVT::i64);
  SDNode *CopyToMask = CurDAG->getMachineNode(TargetOpcode::COPY_TO_REGCLASS,
                                              DL, VT, Source, RC);

  ReplaceNode(CurrentNode, CopyToMask);
}

void RISCVDAGToDAGISel::SelectINSERT_SUBVECTOR_Mask(SDNode *Node) {
  SDValue InVec = Node->getOperand(0);
  assert(Node->getConstantOperandVal(2) == 0 && InVec.isUndef() &&
         "Cannot insert here");
  (void)InVec;

  SDLoc DL(Node);
  EVT VT = Node->getValueType(0);
  EVT OpVT = Node->getOperand(1).getValueType();

  // This requires a sequence of narrowings from the underlying (integer)
  // vector type of OpVT to the underlying (integer) vector type of VT.
  auto HalfIntegerVectorElementType = [this](EVT PrevVT) {
    EVT EltVT = PrevVT.getVectorElementType();
    assert(EltVT.getSizeInBits() % 2 == 0 && "Invalid bit size");
    EltVT = EVT::getIntegerVT(*CurDAG->getContext(), EltVT.getSizeInBits() / 2);
    return EVT::getVectorVT(*CurDAG->getContext(), EltVT,
                            PrevVT.getVectorElementCount());
  };

  uint64_t SEWWide = 64 / OpVT.getVectorElementCount().getKnownMinValue();
  uint64_t SEWNarrow = 64 / VT.getVectorElementCount().getKnownMinValue();

  // Underlying integer vector types.
  // nxv1i64
  EVT SourceVT = OpVT.changeVectorElementType(MVT::getIntegerVT(SEWWide));
  // nxv8i8
  EVT TargetVT = VT.changeVectorElementType(MVT::getIntegerVT(SEWNarrow));

  SDValue SubReg = CurDAG->getTargetConstant(RISCV::sub_vrm2, DL, MVT::i32);
  SDValue Zero = CurDAG->getTargetConstant(0, DL, MVT::i64);

  uint64_t LMul = 1;
  SDValue VLOperand = CurDAG->getRegister(RISCV::X0, MVT::i64);
  uint64_t VTypeI = (Log2_64(SEWWide / 8) << 2) | Log2_64(LMul);
  SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
  SDValue VLMAX = SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL,
                                                 MVT::i64, VLOperand, VTypeIOp),
                          0);

  SDValue Source = Node->getOperand(1);
  do {
    // SourceVT: nxv1i64, nxv2i32, nxv4i16
    // DoubleVT: nxv2i64, nxv4i32, nxv8i16
    EVT DoubleVT = SourceVT.getDoubleNumVectorElementsVT(*CurDAG->getContext());
    // nxv2i32, nxv4i16, nxv8i8
    SourceVT = HalfIntegerVectorElementType(DoubleVT);

    SDValue UndefDouble = SDValue(
        CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, DoubleVT), 0);
    // Insert as a subreg of a LMUL=2
    // nxv1i64 -> nxv2i64
    // nxv2i32 -> nxv4i32
    // nxv4i16 -> nxv8i16
    SDValue DoubleOp = SDValue(
        CurDAG->getMachineNode(TargetOpcode::INSERT_SUBREG, DL, DoubleVT,
                               UndefDouble, Source, SubReg),
        0);

    // Now narrow:
    // nxv2i64 -> nxv2i32
    // nxv4i32 -> nxv4i16
    // nxv8i16 -> nxv8i8
    SDValue Undef = SDValue(
        CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, SourceVT), 0);
    // MaskVT: nxv2i1, nxv4i1, nxv8i1
    EVT MaskVT = SourceVT.changeVectorElementType(MVT::i1);
    SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, MaskVT);
    uint64_t CurrentSEW =
        SourceVT.getVectorElementType().getSizeInBits().getFixedSize();
    Source = SDValue(CurDAG->getMachineNode(
                         RISCV::PseudoVNSRL_WI_M1, DL, SourceVT,
                         {Undef, DoubleOp, Zero, NoRegMask, VLMAX,
                          CurDAG->getTargetConstant(CurrentSEW, DL, MVT::i64)}),
                     0);
  } while (SourceVT != TargetVT);

  // Copy to the regclass of the mask.
  SDValue RC =
      CurDAG->getTargetConstant(RISCV::VRRegClass.getID(), DL, MVT::i64);
  SDNode *CopyToMask = CurDAG->getMachineNode(TargetOpcode::COPY_TO_REGCLASS,
                                              DL, VT, Source, RC);
  ReplaceNode(Node, CopyToMask);
}

void RISCVDAGToDAGISel::SelectCONCAT_VECTORS(SDNode *Node) {
  SDLoc DL(Node);
  EVT VT = Node->getValueType(0);
  EVT OpVT = Node->getOperand(0).getValueType();

  uint64_t SEWWide = 64 / OpVT.getVectorElementCount().getKnownMinValue();
  uint64_t SEWNarrow = 64 / VT.getVectorElementCount().getKnownMinValue();

  uint64_t LMul = 1;
  uint64_t VTypeI = (Log2_64(SEWWide / 8) << 2) | Log2_64(LMul);
  SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
  SDValue VLOperand = CurDAG->getRegister(RISCV::X0, MVT::i64);
  SDValue VLMAX = SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL,
                                                 MVT::i64, VLOperand, VTypeIOp),
                          0);

  SDValue Zero = CurDAG->getTargetConstant(0, DL, MVT::i64);

  SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, VT);
  EVT SubRegVT = VT.changeVectorElementType(MVT::getIntegerVT(SEWWide));
  // EVT WidenVT = OpVT.changeVectorElementType(MVT::getIntegerVT(SEWWide));
  EVT NarrowVT = VT.changeVectorElementType(MVT::getIntegerVT(SEWNarrow));
  SDValue Undef = SDValue(
      CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, NarrowVT), 0);

  SDValue SEWNarrowValue = CurDAG->getTargetConstant(SEWNarrow, DL, MVT::i64);

  SDValue SubReg = CurDAG->getTargetConstant(RISCV::sub_vrm2, DL, MVT::i32);
  SDValue UndefWiden = SDValue(
      CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, MVT::i64), 0);
  SDValue InsertedOp1 = SDValue(
      CurDAG->getMachineNode(TargetOpcode::INSERT_SUBREG, DL, SubRegVT,
                             UndefWiden, Node->getOperand(0), SubReg),
      0);

  SDValue NarrowOp1 =
      SDValue(CurDAG->getMachineNode(
                  RISCV::PseudoVNSRL_WI_M1, DL, NarrowVT,
                  {Undef, InsertedOp1, Zero, NoRegMask, VLMAX, SEWNarrowValue}),
              0);

  SDValue InsertedOp2 = SDValue(
      CurDAG->getMachineNode(TargetOpcode::INSERT_SUBREG, DL, SubRegVT,
                             UndefWiden, Node->getOperand(1), SubReg),
      0);
  SDValue NarrowOp2 =
      SDValue(CurDAG->getMachineNode(
                  RISCV::PseudoVNSRL_WI_M1, DL, NarrowVT,
                  {Undef, InsertedOp2, Zero, NoRegMask, VLMAX, SEWNarrowValue}),
              0);

  VTypeI = (Log2_64(SEWNarrow / 8) << 2) | Log2_64(LMul);
  VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
  SDValue VLMAXNarrow =
      SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL, MVT::i64,
                                     VLOperand, VTypeIOp),
              0);

  SDValue VSLIDEUP =
      SDValue(CurDAG->getMachineNode(RISCV::PseudoVSLIDEUP_VX_M1, DL, NarrowVT,
                                     {NarrowOp1, NarrowOp2, VLMAX, NoRegMask,
                                      VLMAXNarrow, SEWNarrowValue}),
              0);
  SDValue RC =
      CurDAG->getTargetConstant(RISCV::VRRegClass.getID(), DL, MVT::i64);
  SDNode *CopyToMask = CurDAG->getMachineNode(TargetOpcode::COPY_TO_REGCLASS,
                                              DL, VT, VSLIDEUP, RC);
  ReplaceNode(Node, CopyToMask);
}

SDNode *RISCVDAGToDAGISel::NarrowMaskForLMUL1(SDValue MaskOp, SDValue VL,
                                              SDValue SEW) {
  EVT MaskTy = MaskOp.getValueType();
  ElementCount EC = MaskTy.getVectorElementCount();
  SDLoc DL(MaskOp);
  MaskOp = CurDAG->getTargetInsertSubreg(
      RISCV::sub_vrm2, DL, MVT::nxv2i64,
      SDValue(
          CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, MVT::nxv2i64),
          0),
      MaskOp);

  EVT NarrowedMaskTy = EVT::getVectorVT(*CurDAG->getContext(), MVT::i1, EC * 2);
  SDValue UndefMergeOp = SDValue(
      CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, NarrowedMaskTy),
      0);
  SDValue UndefMaskOp = CurDAG->getRegister(RISCV::NoRegister, NarrowedMaskTy);

  SDValue Zero = CurDAG->getTargetConstant(0, DL, MVT::i64);

  SmallVector<SDValue, 6> Ops = {
      UndefMergeOp, MaskOp, Zero,
      UndefMaskOp,  VL,     SEW};

  return CurDAG->getMachineNode(RISCV::PseudoVNSRL_WI_M1, DL, NarrowedMaskTy,
                                Ops);
}

SDNode *RISCVDAGToDAGISel::WidenMaskForLMUL1(SDValue MaskOp, SDValue VL,
                                             SDValue SEW) {
  EVT MaskTy = MaskOp.getValueType();
  ElementCount EC = MaskTy.getVectorElementCount();
  assert(EC.isKnownMultipleOf(2) && "Invalid element count");

  EVT WidenedMaskTy = EVT::getVectorVT(*CurDAG->getContext(), MVT::i1,
                                       EC.divideCoefficientBy(2));
  SDLoc DL(MaskOp);
  SDValue UndefMergeOp = SDValue(
      CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, WidenedMaskTy), 0);
  SDValue UndefMaskOp = CurDAG->getRegister(RISCV::NoRegister, WidenedMaskTy);

  SDValue Zero = CurDAG->getRegister(RISCV::X0, MVT::i64);

  SmallVector<SDValue, 6> Ops = {UndefMergeOp, MaskOp, Zero,
                                 UndefMaskOp,  VL,     SEW};

  SDNode *Result =
      CurDAG->getMachineNode(RISCV::PseudoVWADDU_VX_M1, DL, MVT::nxv2i64, Ops);
  Result = CurDAG
               ->getTargetExtractSubreg(RISCV::sub_vrm2, DL, WidenedMaskTy,
                                        SDValue(Result, 0))
               .getNode();

  return Result;
}

SDNode *RISCVDAGToDAGISel::SelectBinaryWideningForLMUL1(
    SDNode *Node, ArrayRef<LMUL1ValidTy> ValidTypes, int ResultOpcode,
    bool Masked) {
  SDLoc DL(Node);

  SDValue Op1 = Node->getOperand(!Masked ? 1 : 2);
  SDValue Op2 = Node->getOperand(!Masked ? 2 : 3);
  SDNode *Result = nullptr;

  for (const auto &ValidTy : ValidTypes) {
    if (ValidTy.ResultTy == Node->getValueType(0) &&
        ValidTy.OpTy == Op1.getValueType()) {

      SDValue UndefMergeOp =
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.TmpTy),
                  0);
      SDValue UndefMaskOp =
          CurDAG->getRegister(RISCV::NoRegister, ValidTy.MaskTy);

      SDValue VL = Node->getOperand(!Masked ? 3 : 5);
      SDValue SEW = CurDAG->getTargetConstant(ValidTy.SEW, DL, MVT::i64);
      SmallVector<SDValue, 6> Ops = {UndefMergeOp, Op1, Op2,
                                     UndefMaskOp,  VL,  SEW};

      Result = CurDAG->getMachineNode(ResultOpcode, DL, ValidTy.TmpTy, Ops);
      Result =
          CurDAG
              ->getTargetExtractSubreg(RISCV::sub_vrm2, DL, ValidTy.ResultTy,
                                       SDValue(Result, 0))
              .getNode();

      SDValue MergeOp = Node->getOperand(1);
      if (Masked && !MergeOp->isUndef()) {
         // We need to emit a VMERGE_VV because we don't support masked
         // widenings. But before we do that we need to convert the mask
         // accordingly.
        SDValue MaskOp = SDValue(
            WidenMaskForLMUL1(Node->getOperand(4), VL, SEW), 0);
        SDValue CopyToReg = CurDAG->getCopyToReg(CurDAG->getEntryNode(), DL,
                                                 RISCV::V0, MaskOp, SDValue());
        MaskOp = CurDAG->getRegister(RISCV::V0, MaskOp->getValueType(0));

        SDValue WidenedSEW =
            CurDAG->getTargetConstant(ValidTy.SEW * 2, DL, MVT::i64);

        SmallVector<SDValue, 6> MergeOps = {
            MergeOp, SDValue(Result, 0), MaskOp,
            VL,      WidenedSEW,         SDValue(CopyToReg.getNode(), 1)};
        Result = CurDAG->getMachineNode(RISCV::PseudoVMERGE_VVM_M1, DL,
                                        ValidTy.ResultTy, MergeOps);
      }
      break;
    }
  }

  return Result;
}

SDNode *RISCVDAGToDAGISel::SelectUnaryWideningForLMUL1(
    SDNode *Node, ArrayRef<LMUL1ValidTy> ValidTypes, int ResultOpcode,
    bool Masked) {
  SDLoc DL(Node);

  SDValue Op1 = Node->getOperand(!Masked ? 1 : 2);
  SDNode *Result = nullptr;

  for (const auto &ValidTy : ValidTypes) {
    if (ValidTy.ResultTy == Node->getValueType(0) &&
        ValidTy.OpTy == Op1.getValueType()) {

      SDValue UndefMergeOp =
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.TmpTy),
                  0);
      SDValue UndefMaskOp =
          CurDAG->getRegister(RISCV::NoRegister, ValidTy.MaskTy);

      SDValue VL = Node->getOperand(!Masked ? 2 : 4);
      SDValue SEW = CurDAG->getTargetConstant(ValidTy.SEW, DL, MVT::i64);
      SmallVector<SDValue, 6> Ops = {UndefMergeOp, Op1, UndefMaskOp, VL, SEW};

      Result = CurDAG->getMachineNode(ResultOpcode, DL, ValidTy.TmpTy, Ops);
      Result =
          CurDAG
              ->getTargetExtractSubreg(RISCV::sub_vrm2, DL, ValidTy.ResultTy,
                                       SDValue(Result, 0))
              .getNode();

      SDValue MergeOp = Node->getOperand(1);
      if (Masked && !MergeOp->isUndef()) {
         // We need to emit a VMERGE_VV because we don't support masked
         // widenings. But before we do that we need to convert the mask
         // accordingly.
         SDValue MaskOp =
             SDValue(WidenMaskForLMUL1(Node->getOperand(3), VL, SEW), 0);
         SDValue CopyToReg = CurDAG->getCopyToReg(CurDAG->getEntryNode(), DL,
                                                  RISCV::V0, MaskOp, SDValue());
         MaskOp = CurDAG->getRegister(RISCV::V0, MaskOp->getValueType(0));

         SDValue WidenedSEW =
             CurDAG->getTargetConstant(ValidTy.SEW * 2, DL, MVT::i64);

        SmallVector<SDValue, 6> MergeOps = {
            MergeOp, SDValue(Result, 0), MaskOp,
            VL,      WidenedSEW,         SDValue(CopyToReg.getNode(), 1)};
        Result = CurDAG->getMachineNode(RISCV::PseudoVMERGE_VVM_M1, DL,
                                        ValidTy.ResultTy, MergeOps);
      }
      break;
    }
  }

  return Result;
}

SDNode *RISCVDAGToDAGISel::SelectBinaryNarrowingForLMUL1(
    SDNode *Node, ArrayRef<LMUL1ValidTy> ValidTypes, int ResultOpcode,
    SDValue Op2, bool Masked) {
  SDLoc DL(Node);

  SDValue Op1 = Node->getOperand(!Masked ? 1 : 2);
  SDNode *Result = nullptr;

  for (const auto &ValidTy : ValidTypes) {
    if (ValidTy.ResultTy == Node->getValueType(0) &&
        ValidTy.OpTy == Op1.getValueType()) {

      SDValue UndefMergeOp =
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.ResultTy),
                  0);
      SDValue UndefMaskOp =
          CurDAG->getRegister(RISCV::NoRegister, ValidTy.MaskTy);

      Op1 = CurDAG->getTargetInsertSubreg(
          RISCV::sub_vrm2, DL, ValidTy.TmpTy,
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.TmpTy),
                  0),
          Op1);

      SDValue VL = Node->getOperand(!Masked ? 3 : 5);
      SDValue SEW = CurDAG->getTargetConstant(ValidTy.SEW, DL, MVT::i64);
      SmallVector<SDValue, 6> Ops = {UndefMergeOp, Op1, Op2,
                                     UndefMaskOp,  VL,  SEW};

      Result = CurDAG->getMachineNode(ResultOpcode, DL, ValidTy.ResultTy, Ops);

      SDValue MergeOp = Node->getOperand(1);
      if (Masked && !MergeOp->isUndef()) {
        // We need to emit a VMERGE_VV because we don't support masked
        // narrowings. But before we do that we need to convert the mask
        // accordingly.
        SDValue MaskOp =
            SDValue(NarrowMaskForLMUL1(Node->getOperand(4), VL, SEW), 0);
        SDValue CopyToReg = CurDAG->getCopyToReg(CurDAG->getEntryNode(), DL,
                                                 RISCV::V0, MaskOp, SDValue());
        MaskOp = CurDAG->getRegister(RISCV::V0, ValidTy.MaskTy);

        SmallVector<SDValue, 6> MergeOps = {
            MergeOp, SDValue(Result, 0),
            MaskOp,  VL,
            SEW,     SDValue(CopyToReg.getNode(), 1)};
        Result = CurDAG->getMachineNode(RISCV::PseudoVMERGE_VVM_M1, DL,
                                        ValidTy.ResultTy, MergeOps);
      }

      break;
    }
  }

  return Result;
}

SDNode *RISCVDAGToDAGISel::SelectUnaryNarrowingForLMUL1(
    SDNode *Node, ArrayRef<LMUL1ValidTy> ValidTypes, int ResultOpcode,
    bool Masked) {
  SDLoc DL(Node);

  SDValue Op1 = Node->getOperand(!Masked ? 1 : 2);
  SDNode *Result = nullptr;

  for (const auto &ValidTy : ValidTypes) {
    if (ValidTy.ResultTy == Node->getValueType(0) &&
        ValidTy.OpTy == Op1.getValueType()) {

      SDValue UndefMergeOp =
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.ResultTy),
                  0);
      SDValue UndefMaskOp =
          CurDAG->getRegister(RISCV::NoRegister, ValidTy.MaskTy);

      Op1 = CurDAG->getTargetInsertSubreg(
          RISCV::sub_vrm2, DL, ValidTy.TmpTy,
          SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL,
                                         ValidTy.TmpTy),
                  0),
          Op1);

      SDValue VL = Node->getOperand(!Masked ? 2 : 4);
      SDValue SEW = CurDAG->getTargetConstant(ValidTy.SEW, DL, MVT::i64);
      SmallVector<SDValue, 6> Ops = {UndefMergeOp, Op1, UndefMaskOp, VL, SEW};

      Result = CurDAG->getMachineNode(ResultOpcode, DL, ValidTy.ResultTy, Ops);

      SDValue MergeOp = Node->getOperand(1);
      if (Masked && MergeOp.getOpcode() != TargetOpcode::IMPLICIT_DEF) {
        // We need to emit a VMERGE_VV because we don't support masked
        // narrowings. But before we do that we need to convert the mask
        // accordingly.
        SDValue MaskOp =
            SDValue(NarrowMaskForLMUL1(Node->getOperand(3), VL, SEW), 0);
        SDValue CopyToReg = CurDAG->getCopyToReg(CurDAG->getEntryNode(), DL,
                                                 RISCV::V0, MaskOp, SDValue());
        MaskOp = CurDAG->getRegister(RISCV::V0, ValidTy.MaskTy);

        SmallVector<SDValue, 6> MergeOps = {
            MergeOp, SDValue(Result, 0),
            MaskOp,  VL,
            SEW,     SDValue(CopyToReg.getNode(), 1)};
        Result = CurDAG->getMachineNode(RISCV::PseudoVMERGE_VVM_M1, DL,
                                        ValidTy.ResultTy, MergeOps);
      }
      break;
    }
  }

  return Result;
}

static SDNode *SelectInsertVectorElement(SDNode *Node, SelectionDAG *CurDAG) {
  EVT VT = Node->getValueType(0);
  EVT VTMask = VT.changeVectorElementType(MVT::i1);
  SDLoc DL(Node);
  SDValue Vector = Node->getOperand(0);
  SDValue Value = Node->getOperand(1);
  SDValue Index = Node->getOperand(2);

  uint64_t SEW = VT.getVectorElementType().getSizeInBits().getFixedSize();

  ElementCount EC = VT.getVectorElementCount();
  assert(EC.isScalable() && "Unexpected VT");
  uint64_t LMul = EC.getKnownMinValue() * SEW / 64; // FIXME: ELEN=64 hardcoded.

  unsigned VIDInst, VMSetEqualInst, VMergeInst;
  switch (LMul) {
  default:
    llvm_unreachable("Unexpected LMUL");
  case 1:
    VIDInst = RISCV::PseudoVID_V_M1;
    VMSetEqualInst = RISCV::PseudoVMSEQ_VX_M1;
    VMergeInst = VT.isFloatingPoint() ? RISCV::PseudoVFMERGE_VFM_M1
                                      : RISCV::PseudoVMERGE_VXM_M1;
    break;
  case 2:
    VIDInst = RISCV::PseudoVID_V_M2;
    VMSetEqualInst = RISCV::PseudoVMSEQ_VX_M2;
    VMergeInst = VT.isFloatingPoint() ? RISCV::PseudoVFMERGE_VFM_M2
                                      : RISCV::PseudoVMERGE_VXM_M2;
    break;
  case 4:
    VIDInst = RISCV::PseudoVID_V_M4;
    VMSetEqualInst = RISCV::PseudoVMSEQ_VX_M4;
    VMergeInst = VT.isFloatingPoint() ? RISCV::PseudoVFMERGE_VFM_M4
                                      : RISCV::PseudoVMERGE_VXM_M4;
    break;
  case 8:
    VIDInst = RISCV::PseudoVID_V_M8;
    VMSetEqualInst = RISCV::PseudoVMSEQ_VX_M8;
    VMergeInst = VT.isFloatingPoint() ? RISCV::PseudoVFMERGE_VFM_M8
                                      : RISCV::PseudoVMERGE_VXM_M8;
    break;
  }
  SDValue Undef =
      SDValue(CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, VT), 0);
  SDValue UndefMask = SDValue(
      CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, VTMask), 0);

  SDValue SEWOperand = CurDAG->getTargetConstant(SEW, DL, MVT::i64);
  SDValue NoRegMask = CurDAG->getRegister(RISCV::NoRegister, VTMask);

  SDValue VLMax = CurDAG->getRegister(RISCV::X0, MVT::i64);

  auto *VID = CurDAG->getMachineNode(VIDInst, DL, VT,
                                     {Undef, NoRegMask, VLMax, SEWOperand});

  auto *VMSEQ = CurDAG->getMachineNode(
      VMSetEqualInst, DL, VTMask,
      {UndefMask, SDValue(VID, 0), Index, NoRegMask, VLMax, SEWOperand});
  SDValue VMSEQ_V0 = CurDAG->getCopyToReg(CurDAG->getEntryNode(), DL, RISCV::V0,
                                          SDValue(VMSEQ, 0), SDValue());
  SDValue V0GlueCopy = SDValue(VMSEQ_V0.getNode(), 1);

  if (VT.isFloatingPoint() && SEW == 32) {
    SDValue UndefFloat = SDValue(
        CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, MVT::f64), 0);
    Value = CurDAG->getTargetInsertSubreg(RISCV::sub_32, DL, MVT::f64,
                                          UndefFloat, Value);
  }

  SDValue V0Reg = CurDAG->getRegister(RISCV::V0, VTMask);
  auto *VMV = CurDAG->getMachineNode(
      VMergeInst, DL, VT,
      {Vector, Value, V0Reg, VLMax, SEWOperand, V0GlueCopy});

  return VMV;
}

void RISCVDAGToDAGISel::Select(SDNode *Node) {
  // If we have a custom node, we have already selected.
  if (Node->isMachineOpcode()) {
    LLVM_DEBUG(dbgs() << "== "; Node->dump(CurDAG); dbgs() << "\n");
    Node->setNodeId(-1);
    return;
  }

  // Instruction Selection not handled by the auto-generated tablegen selection
  // should be handled here.
  unsigned Opcode = Node->getOpcode();
  MVT XLenVT = Subtarget->getXLenVT();
  SDLoc DL(Node);
  EVT VT = Node->getValueType(0);

  switch (Opcode) {
  case ISD::ADD: {
    // Optimize (add r, imm) to (addi (addi r, imm0) imm1) if applicable. The
    // immediate must be in specific ranges and have a single use.
    if (auto *ConstOp = dyn_cast<ConstantSDNode>(Node->getOperand(1))) {
      if (!(ConstOp->hasOneUse()))
        break;
      // The imm must be in range [-4096,-2049] or [2048,4094].
      int64_t Imm = ConstOp->getSExtValue();
      if (!(-4096 <= Imm && Imm <= -2049) && !(2048 <= Imm && Imm <= 4094))
        break;
      // Break the imm to imm0+imm1.
      SDLoc DL(Node);
      EVT VT = Node->getValueType(0);
      const SDValue ImmOp0 = CurDAG->getTargetConstant(Imm - Imm / 2, DL, VT);
      const SDValue ImmOp1 = CurDAG->getTargetConstant(Imm / 2, DL, VT);
      auto *NodeAddi0 = CurDAG->getMachineNode(RISCV::ADDI, DL, VT,
                                               Node->getOperand(0), ImmOp0);
      auto *NodeAddi1 = CurDAG->getMachineNode(RISCV::ADDI, DL, VT,
                                               SDValue(NodeAddi0, 0), ImmOp1);
      ReplaceNode(Node, NodeAddi1);
      return;
    }
    break;
  }
  case ISD::Constant: {
    auto ConstNode = cast<ConstantSDNode>(Node);
    if (VT == XLenVT && ConstNode->isNullValue()) {
      SDValue New = CurDAG->getCopyFromReg(CurDAG->getEntryNode(), SDLoc(Node),
                                           RISCV::X0, XLenVT);
      ReplaceNode(Node, New.getNode());
      return;
    }
    int64_t Imm = ConstNode->getSExtValue();
    if (XLenVT == MVT::i64) {
      ReplaceNode(Node, selectImm(CurDAG, SDLoc(Node), Imm, XLenVT));
      return;
    }
    break;
  }
  case ISD::FrameIndex: {
    SDValue Imm = CurDAG->getTargetConstant(0, DL, XLenVT);
    int FI = cast<FrameIndexSDNode>(Node)->getIndex();
    SDValue TFI = CurDAG->getTargetFrameIndex(FI, VT);
    ReplaceNode(Node, CurDAG->getMachineNode(RISCV::ADDI, DL, VT, TFI, Imm));
    return;
  }
  case ISD::SRL: {
    if (!Subtarget->is64Bit())
      break;
    SDNode *Op0 = Node->getOperand(0).getNode();
    uint64_t Mask;
    // Match (srl (and val, mask), imm) where the result would be a
    // zero-extended 32-bit integer. i.e. the mask is 0xffffffff or the result
    // is equivalent to this (SimplifyDemandedBits may have removed lower bits
    // from the mask that aren't necessary due to the right-shifting).
    if (isa<ConstantSDNode>(Node->getOperand(1)) && isConstantMask(Op0, Mask)) {
      uint64_t ShAmt = Node->getConstantOperandVal(1);

      if ((Mask | maskTrailingOnes<uint64_t>(ShAmt)) == 0xffffffff) {
        SDValue ShAmtVal =
            CurDAG->getTargetConstant(ShAmt, SDLoc(Node), XLenVT);
        CurDAG->SelectNodeTo(Node, RISCV::SRLIW, XLenVT, Op0->getOperand(0),
                             ShAmtVal);
        return;
      }
    }
    break;
  }
  case RISCVISD::READ_CYCLE_WIDE:
    assert(!Subtarget->is64Bit() && "READ_CYCLE_WIDE is only used on riscv32");

    ReplaceNode(Node, CurDAG->getMachineNode(RISCV::ReadCycleWide, DL, MVT::i32,
                                             MVT::i32, MVT::Other,
                                             Node->getOperand(0)));
    return;
  case ISD::BITCAST: {
    SDValue Op0 = Node->getOperand(0);
    EVT VTOrig = Op0->getValueType(0);
    if (VT.isScalableVector() && VTOrig.isScalableVector()) {
      const TargetRegisterClass *RCDest = TLI->getRegClassFor(VT.getSimpleVT());
      const TargetRegisterClass *RCOrig =
          TLI->getRegClassFor(VTOrig.getSimpleVT());
      bool BitcastIsTrivial = RCDest == RCOrig;

      if (BitcastIsTrivial) {
        ReplaceUses(SDValue(Node, 0), Op0);
        CurDAG->RemoveDeadNode(Node);
        return;
      }
    }
    break;
  }
  case ISD::INSERT_VECTOR_ELT: {
    // SDValue Idx = Node->getOperand(2);
    // if (auto *C = dyn_cast<ConstantSDNode>(Idx)) {
    //   if (C->isNullValue()) {
    //     // We have a pattern for this case.
    //     break;
    //   }
    // }
    auto *Result = SelectInsertVectorElement(Node, CurDAG);
    ReplaceNode(Node, Result);
    return;
  }
  case ISD::INSERT_SUBVECTOR: {
    SDValue InVec = Node->getOperand(0);
    SDValue SubVec = Node->getOperand(1);
    if (InVec.getValueType().getVectorElementType() != MVT::i1) {
      if (Node->getConstantOperandVal(2) == 0 && InVec.isUndef()) {
        ReplaceUses(SDValue(Node, 0), SubVec);
        CurDAG->RemoveDeadNode(Node);
        return;
      } else {
        // Use INSERT_SUBREG to implement this.
      }
    } else {
      SelectINSERT_SUBVECTOR_Mask(Node);
      return;
    }
    break;
  }
  case ISD::EXTRACT_SUBVECTOR: {
    SDValue SubVec = Node->getOperand(0);
    if (SubVec.getValueType().getVectorElementType() != MVT::i1) {
      if (Node->getConstantOperandVal(1) == 0) {
        ReplaceUses(SDValue(Node, 0), SubVec);
        CurDAG->RemoveDeadNode(Node);
        return;
      } else {
        // Use EXTRACT_SUBREG to implement this.
      }
    } else {
      SelectEXTRACT_SUBVECTOR_Mask(Node);
      return;
    }
    break;
  }
  case ISD::INTRINSIC_WO_CHAIN: {
    unsigned IntNo = cast<ConstantSDNode>(Node->getOperand(0))->getZExtValue();
    SDLoc DL(Node);
    switch (IntNo) {
      // By default we do not lower any intrinsic.
    default:
      break;
    case Intrinsic::epi_vsetvl: {
      assert(Node->getNumOperands() == 4);
      selectVSETVL(Node, XLenVT, /* no flags */ 0);
      return;
    }
    case Intrinsic::epi_vsetvlmax: {
      assert(Node->getNumOperands() == 3);
      selectVSETVL(Node, XLenVT, /* no flags */ 0);
      return;
    }
    case Intrinsic::experimental_vector_vp_slideleftfill: {
      SDValue Offset = Node->getOperand(3);
      SDValue EVL1 = Node->getOperand(4);
      SDValue EVL2 = Node->getOperand(5);
      auto *Result =
          SelectSlideLeftFill(Node, CurDAG, EVL1, EVL2, Offset, XLenVT);
      ReplaceNode(Node, Result);
      return;
    }
    case Intrinsic::experimental_vector_slideleftfill: {
      SDValue Offset = Node->getOperand(3);

      // FIXME: Make all this easier.
      uint64_t SEW = VT.getVectorElementType().getSizeInBits().getFixedSize();
      ElementCount EC = VT.getVectorElementCount();
      assert(EC.isScalable() && "Unexpected VT");
      uint64_t LMul =
          EC.getKnownMinValue() * SEW / 64; // FIXME: ELEN=64 hardcoded.
      uint64_t VTypeI = (Log2_64(SEW / 8) << 2) | Log2_64(LMul);
      SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
      SDValue VLMax =
          SDValue(CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL, XLenVT,
                                         CurDAG->getRegister(RISCV::X0, XLenVT),
                                         VTypeIOp),
                  0);
      auto *Result =
          SelectSlideLeftFill(Node, CurDAG, VLMax, VLMax, Offset, XLenVT);
      ReplaceNode(Node, Result);
      return;
    }
    case Intrinsic::experimental_vector_reverse: {
      // (Whole) vector reverse can be computed in RISCV-V as:
      //  (VRGATHER_VV
      //    InputVector,
      //    (VRSUB_VX         # Computes permutation (VLMAX-1, VLMAX-2, ..., 0)
      //      VID_V,
      //      (ADDI (VLMAX), -1)))
      assert(Node->getNumOperands() == 2);
      SDValue Op = Node->getOperand(1);

      uint64_t SEW = VT.getVectorElementType().getSizeInBits().getFixedSize();
      bool IsMaskVector = false;
      if (SEW == 1) {
        IsMaskVector = true;
        switch(VT.getVectorElementCount().getKnownMinValue()) {
        default:
          llvm_unreachable("Wrong factor size");
        case 1:
          SEW = 64;
          VT = VT.changeVectorElementType(MVT::i64);
          break;
        case 2:
          SEW = 32;
          VT = VT.changeVectorElementType(MVT::i32);
          break;
        case 4:
          SEW = 16;
          VT = VT.changeVectorElementType(MVT::i16);
          break;
        case 8:
        case 16:
        case 32:
        case 64:
          SEW = 8;
          VT = VT.changeVectorElementType(MVT::i8);
          break;
        }
      }
      ElementCount EC = VT.getVectorElementCount();
      assert(EC.isScalable() && "Unexpected VT");
      uint64_t LMul =
          EC.getKnownMinValue() * SEW / 64; // FIXME: ELEN=64 hardcoded.

      unsigned VIDInst, VRSUBInst, VRGATHERInst;
      unsigned VMVInst, VMERGEInst, VMSNEInst;
      switch (LMul) {
      default:
        llvm_unreachable("Unexpected LMUL");
      case 1:
        VIDInst = RISCV::PseudoVID_V_M1;
        VRSUBInst = RISCV::PseudoVRSUB_VX_M1;
        VRGATHERInst = RISCV::PseudoVRGATHER_VV_M1;
        VMVInst = RISCV::PseudoVMV_V_I_M1;
        VMERGEInst = RISCV::PseudoVMERGE_VIM_M1;
        VMSNEInst = RISCV::PseudoVMSNE_VI_M1;
        break;
      case 2:
        VIDInst = RISCV::PseudoVID_V_M2;
        VRSUBInst = RISCV::PseudoVRSUB_VX_M2;
        VRGATHERInst = RISCV::PseudoVRGATHER_VV_M2;
        VMVInst = RISCV::PseudoVMV_V_I_M2;
        VMERGEInst = RISCV::PseudoVMERGE_VIM_M2;
        VMSNEInst = RISCV::PseudoVMSNE_VI_M2;
        break;
      case 4:
        VIDInst = RISCV::PseudoVID_V_M4;
        VRSUBInst = RISCV::PseudoVRSUB_VX_M4;
        VRGATHERInst = RISCV::PseudoVRGATHER_VV_M4;
        VMVInst = RISCV::PseudoVMV_V_I_M4;
        VMERGEInst = RISCV::PseudoVMERGE_VIM_M4;
        VMSNEInst = RISCV::PseudoVMSNE_VI_M4;
        break;
      case 8:
        VIDInst = RISCV::PseudoVID_V_M8;
        VRSUBInst = RISCV::PseudoVRSUB_VX_M8;
        VRGATHERInst = RISCV::PseudoVRGATHER_VV_M8;
        VMVInst = RISCV::PseudoVMV_V_I_M8;
        VMERGEInst = RISCV::PseudoVMERGE_VIM_M8;
        VMSNEInst = RISCV::PseudoVMSNE_VI_M8;
        break;
      }

      uint64_t VTypeI = (Log2_64(SEW / 8) << 2) | Log2_64(LMul);
      SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, MVT::i64);
      SDValue VLOperand = CurDAG->getRegister(RISCV::X0, MVT::i64);
      auto *VSETVLI = CurDAG->getMachineNode(RISCV::PseudoVSETVLI, DL, MVT::i64,
                                             VLOperand, VTypeIOp);

      auto *ADDI =
          CurDAG->getMachineNode(RISCV::ADDI, DL, MVT::i64, SDValue(VSETVLI, 0),
                                 CurDAG->getTargetConstant(-1, DL, MVT::i64));

      SDValue Undef = SDValue(
          CurDAG->getMachineNode(TargetOpcode::IMPLICIT_DEF, DL, VT), 0);
      SDValue NoRegMask = CurDAG->getRegister(
          RISCV::NoRegister, VT.changeVectorElementType(MVT::i1));
      SDValue SEWOperand = CurDAG->getTargetConstant(SEW, DL, MVT::i64);

      auto *VID = CurDAG->getMachineNode(
          VIDInst, DL, VT, {Undef, NoRegMask, VLOperand, SEWOperand});

      auto *VRSUB =
          CurDAG->getMachineNode(VRSUBInst, DL, VT,
                                 {Undef, SDValue(VID, 0), SDValue(ADDI, 0),
                                  NoRegMask, VLOperand, SEWOperand});

      if (IsMaskVector) {
        auto *VMV =
            CurDAG->getMachineNode(VMVInst, DL, VT,
                                   {CurDAG->getTargetConstant(0, DL, MVT::i64),
                                    VLOperand, SEWOperand});
        auto *VMERGE = CurDAG->getMachineNode(
            VMERGEInst, DL, VT,
            {SDValue(VMV, 0), CurDAG->getTargetConstant(1, DL, MVT::i64), Op,
             VLOperand, SEWOperand});

        Op = SDValue(VMERGE, 0);
      }

      auto *VRGATHER = CurDAG->getMachineNode(
          VRGATHERInst, DL, VT,
          {Undef, Op, SDValue(VRSUB, 0), NoRegMask, VLOperand, SEWOperand});

      if (IsMaskVector) {
        auto *VMSNE =
            CurDAG->getMachineNode(VMSNEInst, DL, Node->getValueType(0),
                                   {Undef, SDValue(VRGATHER, 0),
                                    CurDAG->getTargetConstant(0, DL, MVT::i64),
                                    NoRegMask, VLOperand, SEWOperand});

        ReplaceNode(Node, VMSNE);
        return;
      }

      ReplaceNode(Node, VRGATHER);
      return;
    }
    case Intrinsic::epi_vwadd:
    case Intrinsic::epi_vwadd_mask:
    case Intrinsic::epi_vwaddu:
    case Intrinsic::epi_vwaddu_mask: {
      // Special cases only for OnlyLMUL1.
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv1i64, MVT::nxv2i32, MVT::nxv2i64, MVT::nxv2i1, 32},
          {MVT::nxv2i32, MVT::nxv4i16, MVT::nxv4i32, MVT::nxv4i1, 16},
          {MVT::nxv4i16, MVT::nxv8i8, MVT::nxv8i16, MVT::nxv8i1, 8}};

      bool Masked = IntNo == Intrinsic::epi_vwadd_mask ||
                    IntNo == Intrinsic::epi_vwaddu_mask;
      SDValue Op2 = Node->getOperand(!Masked ? 2 : 3);
      int ResultOpcode =
          (IntNo == Intrinsic::epi_vwadd || IntNo == Intrinsic::epi_vwadd_mask)
              ? (Op2.getValueType().isVector() ? RISCV::PseudoVWADD_VV_M1
                                               : RISCV::PseudoVWADD_VX_M1)
              : (Op2.getValueType().isVector() ? RISCV::PseudoVWADDU_VV_M1
                                               : RISCV::PseudoVWADDU_VX_M1);

      if (SDNode *Result = SelectBinaryWideningForLMUL1(Node, ValidTypes,
                                                        ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vnsrl:
    case Intrinsic::epi_vnsrl_mask: {
      // Special cases only for OnlyLMUL1.
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv2i32, MVT::nxv1i64, MVT::nxv2i64, MVT::nxv2i1, 32},
          {MVT::nxv4i16, MVT::nxv2i32, MVT::nxv4i32, MVT::nxv4i1, 16},
          {MVT::nxv8i8, MVT::nxv4i16, MVT::nxv8i16, MVT::nxv8i1, 8}};
      bool Masked = IntNo == Intrinsic::epi_vnsrl_mask;
      SDValue Op2 = Node->getOperand(!Masked ? 2 : 3);

      int ResultOpcode = RISCV::PseudoVNSRL_WV_M1;
      if (!Op2.getValueType().isVector()) {
        ResultOpcode = RISCV::PseudoVNSRL_WX_M1;
        if (auto *C = dyn_cast<ConstantSDNode>(Op2)) {
          uint64_t CVal = C->getZExtValue();
          if (isUInt<5>(CVal)) {
            ResultOpcode = RISCV::PseudoVNSRL_WI_M1;
            Op2 = CurDAG->getTargetConstant(CVal, SDLoc(Node), MVT::i64);
          }
        }
      }

      if (SDNode *Result = SelectBinaryNarrowingForLMUL1(
              Node, ValidTypes, ResultOpcode, Op2, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfwcvt_f_f:
    case Intrinsic::epi_vfwcvt_f_f_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv1f64, MVT::nxv2f32, MVT::nxv2f64, MVT::nxv2i1, 32},
      };
      int ResultOpcode = RISCV::PseudoVFWCVT_F_F_V_M1;
      bool Masked = IntNo == Intrinsic::epi_vfwcvt_f_f_mask;

      if (SDNode *Result = SelectUnaryWideningForLMUL1(Node, ValidTypes,
                                                       ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfncvt_f_f:
    case Intrinsic::epi_vfncvt_f_f_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv2f32, MVT::nxv1f64, MVT::nxv2f64, MVT::nxv2i1, 32},
      };
      int ResultOpcode = RISCV::PseudoVFNCVT_F_F_W_M1;
      bool Masked = IntNo == Intrinsic::epi_vfncvt_f_f_mask;

      if (SDNode *Result = SelectUnaryNarrowingForLMUL1(Node, ValidTypes,
                                                        ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfwcvt_f_xu:
    case Intrinsic::epi_vfwcvt_f_xu_mask:
    case Intrinsic::epi_vfwcvt_f_x:
    case Intrinsic::epi_vfwcvt_f_x_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv1f64, MVT::nxv2i32, MVT::nxv2f64, MVT::nxv2i1, 32},
          {MVT::nxv2f32, MVT::nxv4i16, MVT::nxv4f32, MVT::nxv4i1, 16},
      };
      int ResultOpcode = (IntNo == Intrinsic::epi_vfwcvt_f_xu ||
                          IntNo == Intrinsic::epi_vfwcvt_f_xu_mask)
                             ? RISCV::PseudoVFWCVT_F_XU_V_M1
                             : RISCV::PseudoVFWCVT_F_X_V_M1;
      bool Masked = IntNo == Intrinsic::epi_vfwcvt_f_xu_mask ||
                    IntNo == Intrinsic::epi_vfwcvt_f_x_mask;

      if (SDNode *Result = SelectUnaryWideningForLMUL1(Node, ValidTypes,
                                                       ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfwcvt_xu_f:
    case Intrinsic::epi_vfwcvt_xu_f_mask:
    case Intrinsic::epi_vfwcvt_x_f:
    case Intrinsic::epi_vfwcvt_x_f_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv1i64, MVT::nxv2f32, MVT::nxv2i64, MVT::nxv2i1, 32},
      };
      int ResultOpcode = (IntNo == Intrinsic::epi_vfwcvt_xu_f ||
                          IntNo == Intrinsic::epi_vfwcvt_xu_f_mask)
                             ? RISCV::PseudoVFWCVT_XU_F_V_M1
                             : RISCV::PseudoVFWCVT_X_F_V_M1;
      bool Masked = IntNo == Intrinsic::epi_vfwcvt_xu_f_mask ||
                    IntNo == Intrinsic::epi_vfwcvt_x_f_mask;

      if (SDNode *Result = SelectUnaryWideningForLMUL1(Node, ValidTypes,
                                                       ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfncvt_f_xu:
    case Intrinsic::epi_vfncvt_f_xu_mask:
    case Intrinsic::epi_vfncvt_f_x:
    case Intrinsic::epi_vfncvt_f_x_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv2f32, MVT::nxv1i64, MVT::nxv2i64, MVT::nxv2i1, 32},
          {MVT::nxv4f16, MVT::nxv2i32, MVT::nxv4i32, MVT::nxv4i1, 16},
      };
      int ResultOpcode = (IntNo == Intrinsic::epi_vfncvt_f_xu ||
                          IntNo == Intrinsic::epi_vfncvt_f_xu_mask)
                             ? RISCV::PseudoVFNCVT_F_XU_W_M1
                             : RISCV::PseudoVFNCVT_F_X_W_M1;
      bool Masked = IntNo == Intrinsic::epi_vfncvt_f_xu_mask ||
                    IntNo == Intrinsic::epi_vfncvt_f_x_mask;

      if (SDNode *Result = SelectUnaryNarrowingForLMUL1(Node, ValidTypes,
                                                        ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    case Intrinsic::epi_vfncvt_xu_f:
    case Intrinsic::epi_vfncvt_xu_f_mask:
    case Intrinsic::epi_vfncvt_x_f:
    case Intrinsic::epi_vfncvt_x_f_mask: {
      LMUL1ValidTy ValidTypes[] = {
          {MVT::nxv2i32, MVT::nxv1f64, MVT::nxv2f64, MVT::nxv2i1, 32},
          {MVT::nxv4i16, MVT::nxv2f32, MVT::nxv4f32, MVT::nxv4i1, 16},
      };
      int ResultOpcode = (IntNo == Intrinsic::epi_vfncvt_xu_f ||
                          IntNo == Intrinsic::epi_vfncvt_xu_f_mask)
                             ? RISCV::PseudoVFNCVT_XU_F_W_M1
                             : RISCV::PseudoVFNCVT_X_F_W_M1;
      bool Masked = IntNo == Intrinsic::epi_vfncvt_xu_f_mask ||
                    IntNo == Intrinsic::epi_vfncvt_x_f_mask;

      if (SDNode *Result = SelectUnaryNarrowingForLMUL1(Node, ValidTypes,
                                                        ResultOpcode, Masked)) {
        ReplaceNode(Node, Result);
        return;
      }
      break;
    }
    }
    break;
  }
  case ISD::CONCAT_VECTORS: {
    SelectCONCAT_VECTORS(Node);
    return;
  }
  case RISCVISD::FP_TO_FP_REG: {
    // Stores a F16/BF16/F8 value into a f32, with nan-boxing.

    // First, the nan-boxing pattern is created, by setting all bits to 1,
    // except the lower 16 or 8, depending on the value type:

    SDValue M1 = CurDAG->getTargetConstant(-1, DL, MVT::i64);

    auto *NodeM1 = CurDAG->getMachineNode(RISCV::ADDI, DL, MVT::i64,
                                          CurDAG->getRegister(RISCV::X0, MVT::i64),
                                          M1);

    VTSDNode *VTNode = cast<VTSDNode>(Node->getOperand(1).getNode());
    EVT OperandVT = VTNode->getVT();
    assert((OperandVT == MVT::bf16 || OperandVT == MVT::f16 ||
            OperandVT == MVT::f8_143 || OperandVT == MVT::f8_152) &&
           "Invalid operand type for FP_TO_FP_REG");
    int MaskShiftQty =
        (OperandVT == MVT::bf16 || OperandVT == MVT::f16) ? 16 : 8;
    SDValue MaskShift = CurDAG->getTargetConstant(MaskShiftQty, DL, MVT::i64);

    auto *NodeMask = CurDAG->getMachineNode(RISCV::SLLI, DL, MVT::i64,
                                            SDValue(NodeM1, 0), MaskShift);

    // And then copying the f16 bit pattern into the lower part of the register:

    auto *NodeOR = CurDAG->getMachineNode(RISCV::OR, DL, MVT::i64,
                                          SDValue(NodeMask, 0),
                                          Node->getOperand(0));

    // Finally, move the value to a floating point register:

    auto *NodeFloat = CurDAG->getMachineNode(RISCV::FMV_W_X, DL, VT,
                                             SDValue(NodeOR, 0));

    ReplaceNode(Node, NodeFloat);
    return;
  }
  }

  // Select the default instruction.
  SelectCode(Node);
}

bool RISCVDAGToDAGISel::SelectInlineAsmMemoryOperand(
    const SDValue &Op, unsigned ConstraintID, std::vector<SDValue> &OutOps) {
  switch (ConstraintID) {
  case InlineAsm::Constraint_m:
    // We just support simple memory operands that have a single address
    // operand and need no special handling.
    OutOps.push_back(Op);
    return false;
  case InlineAsm::Constraint_A:
    OutOps.push_back(Op);
    return false;
  default:
    break;
  }

  return true;
}

bool RISCVDAGToDAGISel::SelectAddrFI(SDValue Addr, SDValue &Base) {
  if (auto FIN = dyn_cast<FrameIndexSDNode>(Addr)) {
    Base = CurDAG->getTargetFrameIndex(FIN->getIndex(), Subtarget->getXLenVT());
    return true;
  }
  return false;
}

// Check that it is a SLOI (Shift Left Ones Immediate). We first check that
// it is the right node tree:
//
//  (OR (SHL RS1, VC2), VC1)
//
// and then we check that VC1, the mask used to fill with ones, is compatible
// with VC2, the shamt:
//
//  VC1 == maskTrailingOnes<uint64_t>(VC2)

bool RISCVDAGToDAGISel::SelectSLOI(SDValue N, SDValue &RS1, SDValue &Shamt) {
  MVT XLenVT = Subtarget->getXLenVT();
  if (N.getOpcode() == ISD::OR) {
    SDValue Or = N;
    if (Or.getOperand(0).getOpcode() == ISD::SHL) {
      SDValue Shl = Or.getOperand(0);
      if (isa<ConstantSDNode>(Shl.getOperand(1)) &&
          isa<ConstantSDNode>(Or.getOperand(1))) {
        if (XLenVT == MVT::i64) {
          uint64_t VC1 = Or.getConstantOperandVal(1);
          uint64_t VC2 = Shl.getConstantOperandVal(1);
          if (VC1 == maskTrailingOnes<uint64_t>(VC2)) {
            RS1 = Shl.getOperand(0);
            Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                           Shl.getOperand(1).getValueType());
            return true;
          }
        }
        if (XLenVT == MVT::i32) {
          uint32_t VC1 = Or.getConstantOperandVal(1);
          uint32_t VC2 = Shl.getConstantOperandVal(1);
          if (VC1 == maskTrailingOnes<uint32_t>(VC2)) {
            RS1 = Shl.getOperand(0);
            Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                           Shl.getOperand(1).getValueType());
            return true;
          }
        }
      }
    }
  }
  return false;
}

// Check that it is a SROI (Shift Right Ones Immediate). We first check that
// it is the right node tree:
//
//  (OR (SRL RS1, VC2), VC1)
//
// and then we check that VC1, the mask used to fill with ones, is compatible
// with VC2, the shamt:
//
//  VC1 == maskLeadingOnes<uint64_t>(VC2)

bool RISCVDAGToDAGISel::SelectSROI(SDValue N, SDValue &RS1, SDValue &Shamt) {
  MVT XLenVT = Subtarget->getXLenVT();
  if (N.getOpcode() == ISD::OR) {
    SDValue Or = N;
    if (Or.getOperand(0).getOpcode() == ISD::SRL) {
      SDValue Srl = Or.getOperand(0);
      if (isa<ConstantSDNode>(Srl.getOperand(1)) &&
          isa<ConstantSDNode>(Or.getOperand(1))) {
        if (XLenVT == MVT::i64) {
          uint64_t VC1 = Or.getConstantOperandVal(1);
          uint64_t VC2 = Srl.getConstantOperandVal(1);
          if (VC1 == maskLeadingOnes<uint64_t>(VC2)) {
            RS1 = Srl.getOperand(0);
            Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                           Srl.getOperand(1).getValueType());
            return true;
          }
        }
        if (XLenVT == MVT::i32) {
          uint32_t VC1 = Or.getConstantOperandVal(1);
          uint32_t VC2 = Srl.getConstantOperandVal(1);
          if (VC1 == maskLeadingOnes<uint32_t>(VC2)) {
            RS1 = Srl.getOperand(0);
            Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                           Srl.getOperand(1).getValueType());
            return true;
          }
        }
      }
    }
  }
  return false;
}

// Check that it is a SLLIUW (Shift Logical Left Immediate Unsigned i32
// on RV64).
// SLLIUW is the same as SLLI except for the fact that it clears the bits
// XLEN-1:32 of the input RS1 before shifting.
// We first check that it is the right node tree:
//
//  (AND (SHL RS1, VC2), VC1)
//
// We check that VC2, the shamt is less than 32, otherwise the pattern is
// exactly the same as SLLI and we give priority to that.
// Eventually we check that that VC1, the mask used to clear the upper 32 bits
// of RS1, is correct:
//
//  VC1 == (0xFFFFFFFF << VC2)

bool RISCVDAGToDAGISel::SelectSLLIUW(SDValue N, SDValue &RS1, SDValue &Shamt) {
  if (N.getOpcode() == ISD::AND && Subtarget->getXLenVT() == MVT::i64) {
    SDValue And = N;
    if (And.getOperand(0).getOpcode() == ISD::SHL) {
      SDValue Shl = And.getOperand(0);
      if (isa<ConstantSDNode>(Shl.getOperand(1)) &&
          isa<ConstantSDNode>(And.getOperand(1))) {
        uint64_t VC1 = And.getConstantOperandVal(1);
        uint64_t VC2 = Shl.getConstantOperandVal(1);
        if (VC2 < 32 && VC1 == ((uint64_t)0xFFFFFFFF << VC2)) {
          RS1 = Shl.getOperand(0);
          Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                                            Shl.getOperand(1).getValueType());
          return true;
        }
      }
    }
  }
  return false;
}

// Check that it is a SLOIW (Shift Left Ones Immediate i32 on RV64).
// We first check that it is the right node tree:
//
//  (SIGN_EXTEND_INREG (OR (SHL RS1, VC2), VC1))
//
// and then we check that VC1, the mask used to fill with ones, is compatible
// with VC2, the shamt:
//
//  VC2 < 32
//  VC1 == maskTrailingOnes<uint64_t>(VC2)

bool RISCVDAGToDAGISel::SelectSLOIW(SDValue N, SDValue &RS1, SDValue &Shamt) {
  assert(Subtarget->is64Bit() && "SLOIW should only be matched on RV64");
  if (N.getOpcode() != ISD::SIGN_EXTEND_INREG ||
      cast<VTSDNode>(N.getOperand(1))->getVT() != MVT::i32)
    return false;

   SDValue Or = N.getOperand(0);

   if (Or.getOpcode() != ISD::OR || !isa<ConstantSDNode>(Or.getOperand(1)))
     return false;

   SDValue Shl = Or.getOperand(0);
   if (Shl.getOpcode() != ISD::SHL || !isa<ConstantSDNode>(Shl.getOperand(1)))
     return false;

   uint64_t VC1 = Or.getConstantOperandVal(1);
   uint64_t VC2 = Shl.getConstantOperandVal(1);

   if (VC2 >= 32 || VC1 != maskTrailingOnes<uint64_t>(VC2))
     return false;

  RS1 = Shl.getOperand(0);
  Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                                    Shl.getOperand(1).getValueType());
  return true;
}

// Check that it is a SROIW (Shift Right Ones Immediate i32 on RV64).
// We first check that it is the right node tree:
//
//  (OR (SRL RS1, VC2), VC1)
//
// and then we check that VC1, the mask used to fill with ones, is compatible
// with VC2, the shamt:
//
//  VC2 < 32
//  VC1 == maskTrailingZeros<uint64_t>(32 - VC2)
//
bool RISCVDAGToDAGISel::SelectSROIW(SDValue N, SDValue &RS1, SDValue &Shamt) {
  assert(Subtarget->is64Bit() && "SROIW should only be matched on RV64");
  if (N.getOpcode() != ISD::OR || !isa<ConstantSDNode>(N.getOperand(1)))
    return false;

  SDValue Srl = N.getOperand(0);
  if (Srl.getOpcode() != ISD::SRL || !isa<ConstantSDNode>(Srl.getOperand(1)))
    return false;

  uint64_t VC1 = N.getConstantOperandVal(1);
  uint64_t VC2 = Srl.getConstantOperandVal(1);

  if (VC2 >= 32 || VC1 != maskTrailingZeros<uint64_t>(32 - VC2))
    return false;

  RS1 = Srl.getOperand(0);
  Shamt = CurDAG->getTargetConstant(VC2, SDLoc(N),
                                    Srl.getOperand(1).getValueType());
  return true;
}

// Merge an ADDI into the offset of a load/store instruction where possible.
// (load (addi base, off1), off2) -> (load base, off1+off2)
// (store val, (addi base, off1), off2) -> (store val, base, off1+off2)
// This is possible when off1+off2 fits a 12-bit immediate.
void RISCVDAGToDAGISel::doPeepholeLoadStoreADDI() {
  SelectionDAG::allnodes_iterator Position(CurDAG->getRoot().getNode());
  ++Position;

  while (Position != CurDAG->allnodes_begin()) {
    SDNode *N = &*--Position;
    // Skip dead nodes and any non-machine opcodes.
    if (N->use_empty() || !N->isMachineOpcode())
      continue;

    int OffsetOpIdx;
    int BaseOpIdx;

    // Only attempt this optimisation for I-type loads and S-type stores.
    switch (N->getMachineOpcode()) {
    default:
      continue;
    case RISCV::LB:
    case RISCV::LH:
    case RISCV::LW:
    case RISCV::LBU:
    case RISCV::LHU:
    case RISCV::LWU:
    case RISCV::LD:
    case RISCV::FLW:
    case RISCV::FLD:
      BaseOpIdx = 0;
      OffsetOpIdx = 1;
      break;
    case RISCV::SB:
    case RISCV::SH:
    case RISCV::SW:
    case RISCV::SD:
    case RISCV::FSW:
    case RISCV::FSD:
      BaseOpIdx = 1;
      OffsetOpIdx = 2;
      break;
    }

    if (!isa<ConstantSDNode>(N->getOperand(OffsetOpIdx)))
      continue;

    SDValue Base = N->getOperand(BaseOpIdx);

    // If the base is an ADDI, we can merge it in to the load/store.
    if (!Base.isMachineOpcode() || Base.getMachineOpcode() != RISCV::ADDI)
      continue;

    SDValue ImmOperand = Base.getOperand(1);
    uint64_t Offset2 = N->getConstantOperandVal(OffsetOpIdx);

    if (auto Const = dyn_cast<ConstantSDNode>(ImmOperand)) {
      int64_t Offset1 = Const->getSExtValue();
      int64_t CombinedOffset = Offset1 + Offset2;
      if (!isInt<12>(CombinedOffset))
        continue;
      ImmOperand = CurDAG->getTargetConstant(CombinedOffset, SDLoc(ImmOperand),
                                             ImmOperand.getValueType());
    } else if (auto GA = dyn_cast<GlobalAddressSDNode>(ImmOperand)) {
      // If the off1 in (addi base, off1) is a global variable's address (its
      // low part, really), then we can rely on the alignment of that variable
      // to provide a margin of safety before off1 can overflow the 12 bits.
      // Check if off2 falls within that margin; if so off1+off2 can't overflow.
      const DataLayout &DL = CurDAG->getDataLayout();
      Align Alignment = GA->getGlobal()->getPointerAlignment(DL);
      if (Offset2 != 0 && Alignment <= Offset2)
        continue;
      int64_t Offset1 = GA->getOffset();
      int64_t CombinedOffset = Offset1 + Offset2;
      ImmOperand = CurDAG->getTargetGlobalAddress(
          GA->getGlobal(), SDLoc(ImmOperand), ImmOperand.getValueType(),
          CombinedOffset, GA->getTargetFlags());
    } else if (auto CP = dyn_cast<ConstantPoolSDNode>(ImmOperand)) {
      // Ditto.
      Align Alignment = CP->getAlign();
      if (Offset2 != 0 && Alignment <= Offset2)
        continue;
      int64_t Offset1 = CP->getOffset();
      int64_t CombinedOffset = Offset1 + Offset2;
      ImmOperand = CurDAG->getTargetConstantPool(
          CP->getConstVal(), ImmOperand.getValueType(), CP->getAlign(),
          CombinedOffset, CP->getTargetFlags());
    } else {
      continue;
    }

    LLVM_DEBUG(dbgs() << "Folding add-immediate into mem-op:\nBase:    ");
    LLVM_DEBUG(Base->dump(CurDAG));
    LLVM_DEBUG(dbgs() << "\nN: ");
    LLVM_DEBUG(N->dump(CurDAG));
    LLVM_DEBUG(dbgs() << "\n");

    // Modify the offset operand of the load/store.
    if (BaseOpIdx == 0) // Load
      CurDAG->UpdateNodeOperands(N, Base.getOperand(0), ImmOperand,
                                 N->getOperand(2));
    else // Store
      CurDAG->UpdateNodeOperands(N, N->getOperand(0), Base.getOperand(0),
                                 ImmOperand, N->getOperand(3));

    // The add-immediate may now be dead, in which case remove it.
    if (Base.getNode()->use_empty())
      CurDAG->RemoveDeadNode(Base.getNode());
  }
}

void RISCVDAGToDAGISel::doPeepholeWideningNarrowingLMUL1() {
  SelectionDAG::allnodes_iterator Position = CurDAG->allnodes_end();

  bool Changed = false;

  while (Position != CurDAG->allnodes_begin()) {
    SDNode *N = &*--Position;
    // Skip dead nodes and any non-machine opcodes.
    if (N->use_empty() || !N->isMachineOpcode())
      continue;

    switch (N->getMachineOpcode()) {
    default:
      continue;
    case RISCV::PseudoVNSRL_WI_M1:
      {
        // t97: nxv2i32 = PseudoVNSRL_WI_M1 IMPLICIT_DEF:nxv2i32, t96, TargetConstant:i64<0>, Register:nxv2i1 $noreg, t90, TargetConstant:i64<32>
        //  t96: nxv2i64 = INSERT_SUBREG IMPLICIT_DEF:nxv2i64, t109, TargetConstant:i32<2>
        //    t109: nxv1i1 = COPY_TO_REGCLASS t108, TargetConstant:i64<15>
        //      t108: nxv1i64 = EXTRACT_SUBREG t107, TargetConstant:i32<2>
        //        t107: nxv2i64 = PseudoVWADDU_VX_M1 IMPLICIT_DEF:nxv2i64, t106, Register:i64 $x0, Register:nxv2i1 $noreg, t90, TargetConstant:i64<32>
        if (!N->getOperand(0).isMachineOpcode() ||
            N->getOperand(0).getMachineOpcode() != TargetOpcode::IMPLICIT_DEF)
          continue;

        if (!isa<ConstantSDNode>(N->getOperand(2))
            || cast<ConstantSDNode>(N->getOperand(2).getNode())
                ->getZExtValue() != 0)
          continue;

        if (!isa<RegisterSDNode>(N->getOperand(3).getNode()) ||
            cast<RegisterSDNode>(N->getOperand(3).getNode())->getReg() !=
                RISCV::NoRegister)
          continue;

        if (!N->getOperand(1).isMachineOpcode() ||
            N->getOperand(1).getMachineOpcode() != TargetOpcode::INSERT_SUBREG)
          continue;

        if (!N->getOperand(1).getOperand(0).isMachineOpcode() ||
            N->getOperand(1).getOperand(0).getMachineOpcode() !=
                TargetOpcode::IMPLICIT_DEF)
          continue;

        if (!N->getOperand(1).getOperand(1).isMachineOpcode() ||
            N->getOperand(1).getOperand(1).getMachineOpcode() !=
                TargetOpcode::COPY_TO_REGCLASS)
          continue;

        if (!N->getOperand(1).getOperand(1).getOperand(0).isMachineOpcode() ||
            N->getOperand(1).getOperand(1).getOperand(0).getMachineOpcode() !=
                TargetOpcode::EXTRACT_SUBREG)
          continue;

        if (!N->getOperand(1)
                 .getOperand(1)
                 .getOperand(0)
                 .getOperand(0)
                 .isMachineOpcode() ||
            N->getOperand(1)
                    .getOperand(1)
                    .getOperand(0)
                    .getOperand(0)
                    .getMachineOpcode() != RISCV::PseudoVWADDU_VX_M1)
          continue;

        SDValue Widening =
            N->getOperand(1).getOperand(1).getOperand(0).getOperand(0);

        if (!Widening.getOperand(0).isMachineOpcode() ||
            Widening.getOperand(0).getMachineOpcode() !=
                TargetOpcode::IMPLICIT_DEF)
          continue;

        if (!isa<RegisterSDNode>(Widening.getOperand(2).getNode()) ||
            cast<RegisterSDNode>(Widening.getOperand(2).getNode())->getReg() !=
                RISCV::X0)
          continue;

        if (!isa<RegisterSDNode>(Widening.getOperand(3).getNode()) ||
            cast<RegisterSDNode>(Widening.getOperand(3).getNode())->getReg() !=
                RISCV::NoRegister)
          continue;

        if (N->getOperand(4) != Widening.getOperand(4))
          continue;

        if (!isa<ConstantSDNode>(N->getOperand(5).getNode()) ||
            !isa<ConstantSDNode>(Widening->getOperand(5).getNode()) ||
            cast<ConstantSDNode>(N->getOperand(5).getNode())->getZExtValue() !=
                cast<ConstantSDNode>(Widening->getOperand(5).getNode())
                    ->getZExtValue())
          continue;

        SDValue WidenedValue = Widening.getOperand(1);
        ReplaceUses(SDValue(N, 0), WidenedValue);
        Changed = true;
        break;
      }
    }
  }

  if (Changed)
    CurDAG->RemoveDeadNodes();
}

void RISCVDAGToDAGISel::selectVSETVL(SDNode *Node, MVT XLenVT,
                                     uint64_t FlagsIndex) {
  SDLoc DL(Node);
  ConstantSDNode *SEW;
  ConstantSDNode *VMul;
  unsigned IntNo = cast<ConstantSDNode>(Node->getOperand(0))->getZExtValue();

  SDValue VLOperand;
  if (IntNo == Intrinsic::epi_vsetvl) {
    SEW = cast<ConstantSDNode>(Node->getOperand(2));
    VMul = cast<ConstantSDNode>(Node->getOperand(3));
    VLOperand = Node->getOperand(1);
    if (auto *C = dyn_cast<ConstantSDNode>(VLOperand)) {
      if (C->isNullValue()) {
        VLOperand = SDValue(
            CurDAG->getMachineNode(RISCV::ADDI, DL, XLenVT,
                                   CurDAG->getRegister(RISCV::X0, XLenVT),
                                   CurDAG->getTargetConstant(0, DL, XLenVT)),
            0);
      }
    }
  } else {
    assert(IntNo == Intrinsic::epi_vsetvlmax &&
           "Wrong Intrinsic: cannot select VSETVL");
    SEW = cast<ConstantSDNode>(Node->getOperand(1));
    VMul = cast<ConstantSDNode>(Node->getOperand(2));
    // FIXME DstReg for VLMAX must be != X0
    VLOperand = CurDAG->getRegister(RISCV::X0, XLenVT);
  }

  uint64_t SEWBits = SEW->getZExtValue() & 0x7;
  uint64_t VMulBits = VMul->getZExtValue() & 0x3;

  if (Subtarget->onlyLMUL1() && VMulBits != 0) {
    // We cannot use anything but ,m1 so downscale the type equivalently.
    // i.e. vsetvl e16,m2 can be computed doing e8,m1
    //
    // Note that things like e8,m2 cannot work in this scenario because e4 does
    // not exist.
    uint64_t SEW = (1 << SEWBits) * 8;
    uint64_t LMUL = 1 << VMulBits;

    SEW = SEW / LMUL;

    if (SEW < 8) {
      llvm::report_fatal_error(
          "unsupported set vector length operation in LMUL=1 only mode");
    }

    SEWBits = Log2_64(SEW / 8);
    // ,m1
    VMulBits = 0;
  }

  uint64_t VTypeI = (SEWBits << 2) | VMulBits;

  bool NonTemporal = false;
  bool HasDynamicFlags = false;
  if (FlagsIndex) {
    HasDynamicFlags = true;
    if (isa<ConstantSDNode>(Node->getOperand(FlagsIndex))) {
      // If flags is 0 or just the non-temporal bit
      // we can use our existing vsetvli
      unsigned Flags = Node->getConstantOperandVal(FlagsIndex);
      NonTemporal = Flags == 0x200L;
      HasDynamicFlags = !NonTemporal && Flags != 0;
    }
  }

  if (NonTemporal)
    VTypeI |= (0x1 << 9);

  SDValue VTypeIOp = CurDAG->getTargetConstant(VTypeI, DL, XLenVT);

  SmallVector<EVT, 2> VTs;
  VTs.push_back(XLenVT);
  SmallVector<SDValue, 4> Ops;
  Ops.push_back(VLOperand);
  Ops.push_back(VTypeIOp);
  unsigned OpCode;
  if (!HasDynamicFlags) {
    OpCode = RISCV::PseudoVSETVLI;
  } else {
    llvm::report_fatal_error("Dynamic flags in vsetvl not supported yet");
  }
  ReplaceNode(Node, CurDAG->getMachineNode(OpCode, DL, VTs, Ops));
  return;
}

// This pass converts a legalized DAG into a RISCV-specific DAG, ready
// for instruction scheduling.
FunctionPass *llvm::createRISCVISelDag(RISCVTargetMachine &TM) {
  return new RISCVDAGToDAGISel(TM);
}
