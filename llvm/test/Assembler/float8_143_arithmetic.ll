; RUN: opt < %s -O3 -S | FileCheck %s --check-prefix=OPT
; RUN: verify-uselistorder %s
; Basic arithmetic tests for float8_143 type.

define float8_143 @test1() {
  %tmp = fadd float8_143 0xS0, 0xS14
; OPT: 0xS14
  ret float8_143 %tmp
}

define float8_143 @test2() {
  %tmp = fadd float8_143 0xS0, 0xS3F
; OPT: 0xS3F
  ret float8_143 %tmp
}

define float8_143 @test3() {
  %tmp = fadd float8_143 0xS0, 0xS28
; OPT: 0xS28
  ret float8_143 %tmp
}

define float8_143 @test4() {
  %tmp = fadd float8_143 0xS0, 0xS24
; OPT: 0xS24
  ret float8_143 %tmp
}

define float8_143 @test5() {
  %tmp = fadd float8_143 0xS0, 0xS75
; OPT: 0xS75
  ret float8_143 %tmp
}

define float8_143 @test6() {
  %tmp = fadd float8_143 0xS0, 0xS7B
; OPT: 0xS7B
  ret float8_143 %tmp
}

define float8_143 @test7() {
  %tmp = fadd float8_143 0xS1, 0xS1
; OPT: 0xS02
  ret float8_143 %tmp
}

define float8_143 @test8() {
  %tmp = fadd float8_143 0xS4, 0xS1
; OPT: 0xS05
  ret float8_143 %tmp
}

define float8_143 @test9() {
  %tmp = fadd float8_143 0xS7, 0xS7
; OPT: 0xS0E
  ret float8_143 %tmp
}

define float8_143 @test10() {
  %tmp = fadd float8_143 0xS8, 0xSB
; OPT: 0xS12
  ret float8_143 %tmp
}

define float8_143 @test11() {
  %tmp = fadd float8_143 0xS38, 0xS3E
; OPT: 0xS43
  ret float8_143 %tmp
}

define float8_143 @test12() {
  %tmp = fadd float8_143 0xS26, 0xS26
; OPT: 0xS2E
  ret float8_143 %tmp
}

define float8_143 @test13() {
  %tmp = fadd float8_143 0xS10, 0xS18
; OPT: 0xS1C
  ret float8_143 %tmp
}

define float8_143 @test14() {
  %tmp = fadd float8_143 0xS70, 0xS58
; OPT: 0xS71
  ret float8_143 %tmp
}

define float8_143 @test15() {
  %tmp = fadd float8_143 0xS58, 0xS4C
; OPT: 0xS5B
  ret float8_143 %tmp
}

define float8_143 @test16() {
  %tmp = fadd float8_143 0xS78, 0xS60
; OPT: 0xS79
  ret float8_143 %tmp
}

define float8_143 @test17() {
  %tmp = fadd float8_143 0xS7E, 0xS7E
; OPT: 0xS7E
  ret float8_143 %tmp
}

define float8_143 @test18() {
  %tmp = fadd float8_143 0xS7F, 0xS4F
; OPT: 0xS7F
  ret float8_143 %tmp
}

define float8_143 @test19() {
  %tmp = fadd float8_143 0xS22, 0xS7F
; OPT: 0xS7F
  ret float8_143 %tmp
}

define float8_143 @test20() {
  %tmp = fadd float8_143 0xSFE, 0xSFE
; OPT: 0xSFE
  ret float8_143 %tmp
}

define float8_143 @test21() {
  %tmp = fadd float8_143 0xSFF, 0xS3C
; OPT: 0xSFF
  ret float8_143 %tmp
}

define float8_143 @test22() {
  %tmp = fadd float8_143 0xS80, 0xS0
; OPT: 0xS00
  ret float8_143 %tmp
}

define float8_143 @test23() {
  %tmp = fadd float8_143 0xS38, 0xSB0
; OPT: 0xS30
  ret float8_143 %tmp
}

define float8_143 @test24() {
  %tmp = fadd float8_143 0xSD0, 0xSCC
; OPT: 0xSD6
  ret float8_143 %tmp
}

define float8_143 @test25() {
  %tmp = fadd float8_143 0xSFE, 0xS60
; OPT: 0xSFD
  ret float8_143 %tmp
}

define float8_143 @test26() {
  %tmp = fadd float8_143 0xS8, 0xS87
; OPT: 0xS01
  ret float8_143 %tmp
}

define float8_143 @test27() {
  %tmp = fmul float8_143 0xS7F, 0xSCD
; OPT: 0xS7F
  ret float8_143 %tmp
}

define float8_143 @test28() {
  %tmp = fmul float8_143 0xSD0, 0xSB0
; OPT: 0xS48
  ret float8_143 %tmp
}

define float8_143 @test29() {
  %tmp = fmul float8_143 0xSFE, 0xS0
; OPT: 0xS80
  ret float8_143 %tmp
}

define float8_143 @test30() {
  %tmp = fmul float8_143 0xS80, 0xSA3
; OPT: 0xS00
  ret float8_143 %tmp
}
