; RUN: llvm-as < %s | llvm-dis | FileCheck %s --check-prefix=ASSEM-DISASS
; RUN: opt < %s -O3 -S | FileCheck %s --check-prefix=OPT
; RUN: verify-uselistorder %s
; Basic smoke tests for float8_143 type.

define float8_143 @check_float8(float8_143 %A) {
; ASSEM-DISASS: ret float8_143 %A
    ret float8_143 %A
}

define float8_143 @check_float8_143_literal() {
; ASSEM-DISASS: ret float8_143 0xS00
    ret float8_143 0xS00
}

define <4 x float8_143> @check_fixed_vector() {
; ASSEM-DISASS: ret <4 x float8_143> %tmp
  %tmp = fadd <4 x float8_143> undef, undef
  ret <4 x float8_143> %tmp
}

define <vscale x 4 x float8_143> @check_vector() {
; ASSEM-DISASS: ret <vscale x 4 x float8_143> %tmp
  %tmp = fadd <vscale x 4 x float8_143> undef, undef
  ret <vscale x 4 x float8_143> %tmp
}

define float8_143 @check_float8_143_constprop() {
  %tmp = fadd float8_143 0xS40, 0xS2E ; 2 (0x40) + 0.4375 (0x2E) = 2,5 (0x42)
; OPT: 0xS42
  ret float8_143 %tmp
}

define float8_143 @check_float8_143_constprop_infinity() {
  %tmp = fmul float8_143 0xS7E, 0xS7E
; OPT: 0xS7E
  ret float8_143 %tmp
}

define float @check_float8_143_convert() {
  %tmp = fpext float8_143 0xS35 to float
; OPT: 8.125000e-01
  ret float %tmp
}

; ASSEM-DISASS-LABEL @qnan_float8_143
define float8_143 @qnan_float8_143() {
; ASSEM-DISASS: ret float8_143 0xSFF
    ret float8_143 0xSFF
}
