; RUN: llvm-as < %s | llvm-dis | FileCheck %s --check-prefix=ASSEM-DISASS
; RUN: opt < %s -O3 -S | FileCheck %s --check-prefix=OPT
; RUN: verify-uselistorder %s
; Basic smoke tests for float8_152 type.

define float8_152 @check_float8(float8_152 %A) {
; ASSEM-DISASS: ret float8_152 %A
    ret float8_152 %A
}

define float8_152 @check_float8_152_literal() {
; ASSEM-DISASS: ret float8_152 0xT00
    ret float8_152 0xT00
}

define <4 x float8_152> @check_fixed_vector() {
; ASSEM-DISASS: ret <4 x float8_152> %tmp
  %tmp = fadd <4 x float8_152> undef, undef
  ret <4 x float8_152> %tmp
}

define <vscale x 4 x float8_152> @check_vector() {
; ASSEM-DISASS: ret <vscale x 4 x float8_152> %tmp
  %tmp = fadd <vscale x 4 x float8_152> undef, undef
  ret <vscale x 4 x float8_152> %tmp
}

define float8_152 @check_float8_152_constprop() {
  %tmp = fadd float8_152 0xT24, 0xT2E ; 0.015625 (0x24) + 0.09375 (0x2E) = 0.109375 (0x2F)
; OPT: 0xT2F
  ret float8_152 %tmp
}

define float @check_float8_152_convert() {
  %tmp = fpext float8_152 0xT4D to float
; OPT: 2.000000e+01
  ret float %tmp
}

; ASSEM-DISASS-LABEL @snan_float8_152
define float8_152 @snan_float8_152() {
; ASSEM-DISASS: ret float8_152 0xT7D
    ret float8_152 0xT7D
}

; ASSEM-DISASS-LABEL @qnan_float8_152
define float8_152 @qnan_float8_152() {
; ASSEM-DISASS: ret float8_152 0xT7F
    ret float8_152 0xT7F
}

; ASSEM-DISASS-LABEL @pos_inf_float8_152
define float8_152 @pos_inf_float8_152() {
; ASSEM-DISASS: ret float8_152 0xT7C
    ret float8_152 0xT7C
}

; ASSEM-DISASS-LABEL @neg_inf_float8_152
define float8_152 @neg_inf_float8_152() {
; ASSEM-DISASS: ret float8_152 0xTFC
    ret float8_152 0xTFC
}
