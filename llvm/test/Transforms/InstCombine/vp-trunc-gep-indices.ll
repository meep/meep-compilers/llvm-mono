; NOTE: Assertions have been autogenerated by utils/update_test_checks.py
; RUN: opt -passes=instcombine %s -S | FileCheck %s
target datalayout = "e-m:e-p:32:32-i64:64-n32-S128"
target triple = "riscv32"

define <vscale x 2 x i32> @trunc_gep_indices(i32* %a, <vscale x 2 x i64> %indices, <vscale x 2 x i1> %mask, i32 %evl) {
; CHECK-LABEL: @trunc_gep_indices(
; CHECK-NEXT:    [[TMP1:%.*]] = call <vscale x 2 x i32> @llvm.vp.trunc.nxv2i32.nxv2i64(<vscale x 2 x i64> [[INDICES:%.*]], <vscale x 2 x i1> shufflevector (<vscale x 2 x i1> insertelement (<vscale x 2 x i1> undef, i1 true, i32 0), <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer), i32 [[EVL:%.*]])
; CHECK-NEXT:    [[TMP2:%.*]] = call <vscale x 2 x i32> @llvm.vp.shl.nxv2i32(<vscale x 2 x i32> [[TMP1]], <vscale x 2 x i32> shufflevector (<vscale x 2 x i32> insertelement (<vscale x 2 x i32> undef, i32 2, i32 0), <vscale x 2 x i32> undef, <vscale x 2 x i32> zeroinitializer), <vscale x 2 x i1> shufflevector (<vscale x 2 x i1> insertelement (<vscale x 2 x i1> undef, i1 true, i32 0), <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer), i32 [[EVL]])
; CHECK-NEXT:    [[DOTSPLATINSERT:%.*]] = insertelement <vscale x 2 x i32*> undef, i32* [[A:%.*]], i32 0
; CHECK-NEXT:    [[DOTSPLAT:%.*]] = shufflevector <vscale x 2 x i32*> [[DOTSPLATINSERT]], <vscale x 2 x i32*> undef, <vscale x 2 x i32> zeroinitializer
; CHECK-NEXT:    [[TMP3:%.*]] = call <vscale x 2 x i32> @llvm.vp.ptrtoint.nxv2i32.nxv2p0i32(<vscale x 2 x i32*> [[DOTSPLAT]], <vscale x 2 x i1> shufflevector (<vscale x 2 x i1> insertelement (<vscale x 2 x i1> undef, i1 true, i32 0), <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer), i32 [[EVL]])
; CHECK-NEXT:    [[TMP4:%.*]] = call <vscale x 2 x i32> @llvm.vp.add.nxv2i32(<vscale x 2 x i32> [[TMP3]], <vscale x 2 x i32> [[TMP2]], <vscale x 2 x i1> shufflevector (<vscale x 2 x i1> insertelement (<vscale x 2 x i1> undef, i1 true, i32 0), <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer), i32 [[EVL]])
; CHECK-NEXT:    [[TMP5:%.*]] = call <vscale x 2 x i32*> @llvm.vp.inttoptr.nxv2p0i32.nxv2i32(<vscale x 2 x i32> [[TMP4]], <vscale x 2 x i1> shufflevector (<vscale x 2 x i1> insertelement (<vscale x 2 x i1> undef, i1 true, i32 0), <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer), i32 [[EVL]])
; CHECK-NEXT:    [[RET:%.*]] = call <vscale x 2 x i32> @llvm.vp.gather.nxv2i32.nxv2p0i32(<vscale x 2 x i32*> [[TMP5]], i32 4, <vscale x 2 x i1> [[MASK:%.*]], i32 [[EVL]])
; CHECK-NEXT:    ret <vscale x 2 x i32> [[RET]]
;
  %gep = getelementptr i32, i32* %a, <vscale x 2 x i64> %indices
  %ret = call <vscale x 2 x i32> @llvm.vp.gather.nxv2i32(<vscale x 2 x i32*> %gep, i32 4, <vscale x 2 x i1> %mask, i32 %evl)
  ret <vscale x 2 x i32> %ret
}

declare <vscale x 2 x i32> @llvm.vp.gather.nxv2i32(<vscale x 2 x i32*>, i32, <vscale x 2 x i1>, i32)
