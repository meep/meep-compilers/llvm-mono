; NOTE: Assertions have been autogenerated by utils/update_test_checks.py
; RUN: opt -mtriple riscv64 -mattr +m,+a,+f,+d,+experimental-v -S \
; RUN:    -loop-vectorize -vector-register-width-factor=8 < %s -o - | FileCheck %s
; RUN: opt -mtriple riscv64 -mattr +m,+a,+f,+d,+experimental-v -S \
; RUN:    -loop-vectorize < %s -o - | FileCheck %s --check-prefix=CHECK1
; ModuleID = 't3.i'
source_filename = "t3.i"
target datalayout = "e-m:e-p:64:64-i64:64-i128:128-n64-S128"
target triple = "riscv64-unknown-linux-gnu"

; Function Attrs: nounwind readnone
define dso_local signext i32 @main(i32 signext %argc, i8** nocapture readnone %argv) local_unnamed_addr #0 {
; CHECK-LABEL: @main(
; CHECK-NEXT:  entry:
; CHECK-NEXT:    [[A:%.*]] = alloca [1024 x i64], align 8
; CHECK-NEXT:    [[TMP0:%.*]] = bitcast [1024 x i64]* [[A]] to i8*
; CHECK-NEXT:    call void @llvm.lifetime.start.p0i8(i64 8192, i8* nonnull [[TMP0]]) [[ATTR4:#.*]]
; CHECK-NEXT:    [[TMP1:%.*]] = call i64 @llvm.vscale.i64()
; CHECK-NEXT:    [[STEP_VSCALE:%.*]] = mul i64 [[TMP1]], 8
; CHECK-NEXT:    [[MIN_ITERS_CHECK:%.*]] = icmp ult i64 1024, [[STEP_VSCALE]]
; CHECK-NEXT:    br i1 [[MIN_ITERS_CHECK]], label [[SCALAR_PH:%.*]], label [[VECTOR_PH:%.*]]
; CHECK:       vector.ph:
; CHECK-NEXT:    [[TMP2:%.*]] = call i64 @llvm.vscale.i64()
; CHECK-NEXT:    [[STEP_VSCALE1:%.*]] = mul i64 8, [[TMP2]]
; CHECK-NEXT:    [[N_MOD_VF:%.*]] = urem i64 1024, [[STEP_VSCALE1]]
; CHECK-NEXT:    [[N_VEC:%.*]] = sub i64 1024, [[N_MOD_VF]]
; CHECK-NEXT:    [[DOTSPLATINSERT:%.*]] = insertelement <vscale x 8 x i64> undef, i64 0, i32 0
; CHECK-NEXT:    [[DOTSPLAT:%.*]] = shufflevector <vscale x 8 x i64> [[DOTSPLATINSERT]], <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
; CHECK-NEXT:    [[TMP3:%.*]] = call <vscale x 8 x i64> @llvm.experimental.vector.stepvector.nxv8i64()
; CHECK-NEXT:    [[DOTSPLATINSERT2:%.*]] = insertelement <vscale x 8 x i64> undef, i64 0, i32 0
; CHECK-NEXT:    [[DOTSPLAT3:%.*]] = shufflevector <vscale x 8 x i64> [[DOTSPLATINSERT2]], <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
; CHECK-NEXT:    [[TMP4:%.*]] = add <vscale x 8 x i64> [[TMP3]], [[DOTSPLAT3]]
; CHECK-NEXT:    [[DOTSPLATINSERT4:%.*]] = insertelement <vscale x 8 x i64> undef, i64 1, i32 0
; CHECK-NEXT:    [[DOTSPLAT5:%.*]] = shufflevector <vscale x 8 x i64> [[DOTSPLATINSERT4]], <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
; CHECK-NEXT:    [[TMP5:%.*]] = mul <vscale x 8 x i64> [[TMP4]], [[DOTSPLAT5]]
; CHECK-NEXT:    [[INDUCTION:%.*]] = add <vscale x 8 x i64> [[DOTSPLAT]], [[TMP5]]
; CHECK-NEXT:    [[TMP6:%.*]] = call i64 @llvm.vscale.i64()
; CHECK-NEXT:    [[TMP7:%.*]] = mul i64 8, [[TMP6]]
; CHECK-NEXT:    [[DOTSPLATINSERT6:%.*]] = insertelement <vscale x 8 x i64> undef, i64 [[TMP7]], i32 0
; CHECK-NEXT:    [[DOTSPLAT7:%.*]] = shufflevector <vscale x 8 x i64> [[DOTSPLATINSERT6]], <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
; CHECK-NEXT:    [[BROADCAST_SPLATINSERT:%.*]] = insertelement <vscale x 8 x i64> undef, i64 42, i32 0
; CHECK-NEXT:    [[BROADCAST_SPLAT:%.*]] = shufflevector <vscale x 8 x i64> [[BROADCAST_SPLATINSERT]], <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
; CHECK-NEXT:    br label [[VECTOR_BODY:%.*]]
; CHECK:       vector.body:
; CHECK-NEXT:    [[INDEX:%.*]] = phi i64 [ 0, [[VECTOR_PH]] ], [ [[INDEX_NEXT:%.*]], [[VECTOR_BODY]] ]
; CHECK-NEXT:    [[VEC_IND:%.*]] = phi <vscale x 8 x i64> [ [[INDUCTION]], [[VECTOR_PH]] ], [ [[VEC_IND_NEXT:%.*]], [[VECTOR_BODY]] ]
; CHECK-NEXT:    [[TMP8:%.*]] = add nuw nsw <vscale x 8 x i64> [[VEC_IND]], [[BROADCAST_SPLAT]]
; CHECK-NEXT:    [[TMP9:%.*]] = extractelement <vscale x 8 x i64> [[VEC_IND]], i32 0
; CHECK-NEXT:    [[TMP10:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[TMP9]]
; CHECK-NEXT:    [[TMP11:%.*]] = getelementptr inbounds i64, i64* [[TMP10]], i32 0
; CHECK-NEXT:    [[TMP12:%.*]] = bitcast i64* [[TMP11]] to <vscale x 8 x i64>*
; CHECK-NEXT:    store <vscale x 8 x i64> [[TMP8]], <vscale x 8 x i64>* [[TMP12]], align 8, [[TBAA2:!tbaa !.*]]
; CHECK-NEXT:    [[TMP13:%.*]] = call i64 @llvm.vscale.i64()
; CHECK-NEXT:    [[INDEX_VSCALE:%.*]] = mul i64 [[TMP13]], 8
; CHECK-NEXT:    [[INDEX_NEXT]] = add i64 [[INDEX]], [[INDEX_VSCALE]]
; CHECK-NEXT:    [[VEC_IND_NEXT]] = add <vscale x 8 x i64> [[VEC_IND]], [[DOTSPLAT7]]
; CHECK-NEXT:    [[TMP14:%.*]] = icmp eq i64 [[INDEX_NEXT]], [[N_VEC]]
; CHECK-NEXT:    br i1 [[TMP14]], label [[MIDDLE_BLOCK:%.*]], label [[VECTOR_BODY]], [[LOOP6:!llvm.loop !.*]]
; CHECK:       middle.block:
; CHECK-NEXT:    [[CMP_N:%.*]] = icmp eq i64 1024, [[N_VEC]]
; CHECK-NEXT:    br i1 [[CMP_N]], label [[FOR_COND_CLEANUP:%.*]], label [[SCALAR_PH]]
; CHECK:       scalar.ph:
; CHECK-NEXT:    [[BC_RESUME_VAL:%.*]] = phi i64 [ [[N_VEC]], [[MIDDLE_BLOCK]] ], [ 0, [[ENTRY:%.*]] ]
; CHECK-NEXT:    br label [[FOR_BODY:%.*]]
; CHECK:       for.cond.cleanup:
; CHECK-NEXT:    [[IDXPROM:%.*]] = sext i32 [[ARGC:%.*]] to i64
; CHECK-NEXT:    [[ARRAYIDX1:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[IDXPROM]]
; CHECK-NEXT:    [[TMP15:%.*]] = load i64, i64* [[ARRAYIDX1]], align 8, [[TBAA2]]
; CHECK-NEXT:    [[CONV:%.*]] = trunc i64 [[TMP15]] to i32
; CHECK-NEXT:    call void @llvm.lifetime.end.p0i8(i64 8192, i8* nonnull [[TMP0]]) [[ATTR4]]
; CHECK-NEXT:    ret i32 [[CONV]]
; CHECK:       for.body:
; CHECK-NEXT:    [[I_07:%.*]] = phi i64 [ [[BC_RESUME_VAL]], [[SCALAR_PH]] ], [ [[INC:%.*]], [[FOR_BODY]] ]
; CHECK-NEXT:    [[ADD:%.*]] = add nuw nsw i64 [[I_07]], 42
; CHECK-NEXT:    [[ARRAYIDX:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[I_07]]
; CHECK-NEXT:    store i64 [[ADD]], i64* [[ARRAYIDX]], align 8, [[TBAA2]]
; CHECK-NEXT:    [[INC]] = add nuw nsw i64 [[I_07]], 1
; CHECK-NEXT:    [[EXITCOND:%.*]] = icmp eq i64 [[INC]], 1024
; CHECK-NEXT:    br i1 [[EXITCOND]], label [[FOR_COND_CLEANUP]], label [[FOR_BODY]], [[LOOP8:!llvm.loop !.*]]
;
; CHECK1-LABEL: @main(
; CHECK1-NEXT:  entry:
; CHECK1-NEXT:    [[A:%.*]] = alloca [1024 x i64], align 8
; CHECK1-NEXT:    [[TMP0:%.*]] = bitcast [1024 x i64]* [[A]] to i8*
; CHECK1-NEXT:    call void @llvm.lifetime.start.p0i8(i64 8192, i8* nonnull [[TMP0]]) [[ATTR4:#.*]]
; CHECK1-NEXT:    [[TMP1:%.*]] = call i64 @llvm.vscale.i64()
; CHECK1-NEXT:    [[STEP_VSCALE:%.*]] = mul i64 [[TMP1]], 1
; CHECK1-NEXT:    [[MIN_ITERS_CHECK:%.*]] = icmp ult i64 1024, [[STEP_VSCALE]]
; CHECK1-NEXT:    br i1 [[MIN_ITERS_CHECK]], label [[SCALAR_PH:%.*]], label [[VECTOR_PH:%.*]]
; CHECK1:       vector.ph:
; CHECK1-NEXT:    [[TMP2:%.*]] = call i64 @llvm.vscale.i64()
; CHECK1-NEXT:    [[STEP_VSCALE1:%.*]] = mul i64 1, [[TMP2]]
; CHECK1-NEXT:    [[N_MOD_VF:%.*]] = urem i64 1024, [[STEP_VSCALE1]]
; CHECK1-NEXT:    [[N_VEC:%.*]] = sub i64 1024, [[N_MOD_VF]]
; CHECK1-NEXT:    [[DOTSPLATINSERT:%.*]] = insertelement <vscale x 1 x i64> undef, i64 0, i32 0
; CHECK1-NEXT:    [[DOTSPLAT:%.*]] = shufflevector <vscale x 1 x i64> [[DOTSPLATINSERT]], <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
; CHECK1-NEXT:    [[TMP3:%.*]] = call <vscale x 1 x i64> @llvm.experimental.vector.stepvector.nxv1i64()
; CHECK1-NEXT:    [[DOTSPLATINSERT2:%.*]] = insertelement <vscale x 1 x i64> undef, i64 0, i32 0
; CHECK1-NEXT:    [[DOTSPLAT3:%.*]] = shufflevector <vscale x 1 x i64> [[DOTSPLATINSERT2]], <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
; CHECK1-NEXT:    [[TMP4:%.*]] = add <vscale x 1 x i64> [[TMP3]], [[DOTSPLAT3]]
; CHECK1-NEXT:    [[DOTSPLATINSERT4:%.*]] = insertelement <vscale x 1 x i64> undef, i64 1, i32 0
; CHECK1-NEXT:    [[DOTSPLAT5:%.*]] = shufflevector <vscale x 1 x i64> [[DOTSPLATINSERT4]], <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
; CHECK1-NEXT:    [[TMP5:%.*]] = mul <vscale x 1 x i64> [[TMP4]], [[DOTSPLAT5]]
; CHECK1-NEXT:    [[INDUCTION:%.*]] = add <vscale x 1 x i64> [[DOTSPLAT]], [[TMP5]]
; CHECK1-NEXT:    [[TMP6:%.*]] = call i64 @llvm.vscale.i64()
; CHECK1-NEXT:    [[TMP7:%.*]] = mul i64 1, [[TMP6]]
; CHECK1-NEXT:    [[DOTSPLATINSERT6:%.*]] = insertelement <vscale x 1 x i64> undef, i64 [[TMP7]], i32 0
; CHECK1-NEXT:    [[DOTSPLAT7:%.*]] = shufflevector <vscale x 1 x i64> [[DOTSPLATINSERT6]], <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
; CHECK1-NEXT:    [[BROADCAST_SPLATINSERT:%.*]] = insertelement <vscale x 1 x i64> undef, i64 42, i32 0
; CHECK1-NEXT:    [[BROADCAST_SPLAT:%.*]] = shufflevector <vscale x 1 x i64> [[BROADCAST_SPLATINSERT]], <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
; CHECK1-NEXT:    br label [[VECTOR_BODY:%.*]]
; CHECK1:       vector.body:
; CHECK1-NEXT:    [[INDEX:%.*]] = phi i64 [ 0, [[VECTOR_PH]] ], [ [[INDEX_NEXT:%.*]], [[VECTOR_BODY]] ]
; CHECK1-NEXT:    [[VEC_IND:%.*]] = phi <vscale x 1 x i64> [ [[INDUCTION]], [[VECTOR_PH]] ], [ [[VEC_IND_NEXT:%.*]], [[VECTOR_BODY]] ]
; CHECK1-NEXT:    [[TMP8:%.*]] = add nuw nsw <vscale x 1 x i64> [[VEC_IND]], [[BROADCAST_SPLAT]]
; CHECK1-NEXT:    [[TMP9:%.*]] = extractelement <vscale x 1 x i64> [[VEC_IND]], i32 0
; CHECK1-NEXT:    [[TMP10:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[TMP9]]
; CHECK1-NEXT:    [[TMP11:%.*]] = getelementptr inbounds i64, i64* [[TMP10]], i32 0
; CHECK1-NEXT:    [[TMP12:%.*]] = bitcast i64* [[TMP11]] to <vscale x 1 x i64>*
; CHECK1-NEXT:    store <vscale x 1 x i64> [[TMP8]], <vscale x 1 x i64>* [[TMP12]], align 8, [[TBAA2:!tbaa !.*]]
; CHECK1-NEXT:    [[TMP13:%.*]] = call i64 @llvm.vscale.i64()
; CHECK1-NEXT:    [[INDEX_VSCALE:%.*]] = mul i64 [[TMP13]], 1
; CHECK1-NEXT:    [[INDEX_NEXT]] = add i64 [[INDEX]], [[INDEX_VSCALE]]
; CHECK1-NEXT:    [[VEC_IND_NEXT]] = add <vscale x 1 x i64> [[VEC_IND]], [[DOTSPLAT7]]
; CHECK1-NEXT:    [[TMP14:%.*]] = icmp eq i64 [[INDEX_NEXT]], [[N_VEC]]
; CHECK1-NEXT:    br i1 [[TMP14]], label [[MIDDLE_BLOCK:%.*]], label [[VECTOR_BODY]], [[LOOP6:!llvm.loop !.*]]
; CHECK1:       middle.block:
; CHECK1-NEXT:    [[CMP_N:%.*]] = icmp eq i64 1024, [[N_VEC]]
; CHECK1-NEXT:    br i1 [[CMP_N]], label [[FOR_COND_CLEANUP:%.*]], label [[SCALAR_PH]]
; CHECK1:       scalar.ph:
; CHECK1-NEXT:    [[BC_RESUME_VAL:%.*]] = phi i64 [ [[N_VEC]], [[MIDDLE_BLOCK]] ], [ 0, [[ENTRY:%.*]] ]
; CHECK1-NEXT:    br label [[FOR_BODY:%.*]]
; CHECK1:       for.cond.cleanup:
; CHECK1-NEXT:    [[IDXPROM:%.*]] = sext i32 [[ARGC:%.*]] to i64
; CHECK1-NEXT:    [[ARRAYIDX1:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[IDXPROM]]
; CHECK1-NEXT:    [[TMP15:%.*]] = load i64, i64* [[ARRAYIDX1]], align 8, [[TBAA2]]
; CHECK1-NEXT:    [[CONV:%.*]] = trunc i64 [[TMP15]] to i32
; CHECK1-NEXT:    call void @llvm.lifetime.end.p0i8(i64 8192, i8* nonnull [[TMP0]]) [[ATTR4]]
; CHECK1-NEXT:    ret i32 [[CONV]]
; CHECK1:       for.body:
; CHECK1-NEXT:    [[I_07:%.*]] = phi i64 [ [[BC_RESUME_VAL]], [[SCALAR_PH]] ], [ [[INC:%.*]], [[FOR_BODY]] ]
; CHECK1-NEXT:    [[ADD:%.*]] = add nuw nsw i64 [[I_07]], 42
; CHECK1-NEXT:    [[ARRAYIDX:%.*]] = getelementptr inbounds [1024 x i64], [1024 x i64]* [[A]], i64 0, i64 [[I_07]]
; CHECK1-NEXT:    store i64 [[ADD]], i64* [[ARRAYIDX]], align 8, [[TBAA2]]
; CHECK1-NEXT:    [[INC]] = add nuw nsw i64 [[I_07]], 1
; CHECK1-NEXT:    [[EXITCOND:%.*]] = icmp eq i64 [[INC]], 1024
; CHECK1-NEXT:    br i1 [[EXITCOND]], label [[FOR_COND_CLEANUP]], label [[FOR_BODY]], [[LOOP8:!llvm.loop !.*]]
;
entry:
  %a = alloca [1024 x i64], align 8
  %0 = bitcast [1024 x i64]* %a to i8*
  call void @llvm.lifetime.start.p0i8(i64 8192, i8* nonnull %0) #2
  br label %for.body

for.cond.cleanup:                                 ; preds = %for.body
  %idxprom = sext i32 %argc to i64
  %arrayidx1 = getelementptr inbounds [1024 x i64], [1024 x i64]* %a, i64 0, i64 %idxprom
  %1 = load i64, i64* %arrayidx1, align 8, !tbaa !2
  %conv = trunc i64 %1 to i32
  call void @llvm.lifetime.end.p0i8(i64 8192, i8* nonnull %0) #2
  ret i32 %conv

for.body:                                         ; preds = %for.body, %entry
  %i.07 = phi i64 [ 0, %entry ], [ %inc, %for.body ]
  %add = add nuw nsw i64 %i.07, 42
  %arrayidx = getelementptr inbounds [1024 x i64], [1024 x i64]* %a, i64 0, i64 %i.07
  store i64 %add, i64* %arrayidx, align 8, !tbaa !2
  %inc = add nuw nsw i64 %i.07, 1
  %exitcond = icmp eq i64 %inc, 1024
  br i1 %exitcond, label %for.cond.cleanup, label %for.body
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

attributes #0 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-features"="+a,+c,+d,+experimental-v,+f,+m,-relax" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 10.0.0 (git@repo.hca.bsc.es:EPI/System-Software/llvm-mono.git 823a818e0f44dc9b594f14a328ec52b247f3611a)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"long", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C/C++ TBAA"}
