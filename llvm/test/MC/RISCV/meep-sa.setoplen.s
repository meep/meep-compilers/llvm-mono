# RUN: llvm-mc %s -triple=riscv64 -mattr=+meep-sa -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s
# RUN: llvm-mc -filetype=obj -triple=riscv64 -mattr=+meep-sa < %s \
# RUN:     | llvm-objdump -M no-aliases --mattr=+meep-sa -d -r - \
# RUN:     | FileCheck --check-prefix=CHECK-ASM-AND-OBJ %s

# CHECK-ASM-AND-OBJ: sa.setoplen.0.0 a0, a1
# CHECK-ASM: [0x2b,0xf5,0x15,0x00]
sa.setoplen.0.0 x10, x11

# CHECK-ASM-AND-OBJ: sa.setoplen.0.1 a0, a1
# CHECK-ASM: [0x2b,0xf5,0x25,0x00]
sa.setoplen.0.1 x10, x11

# CHECK-ASM-AND-OBJ: sa.setoplen.1.0 a0, a1
# CHECK-ASM: [0x2b,0xf5,0x15,0x04]
sa.setoplen.1.0 x10, x11

# CHECK-ASM-AND-OBJ: sa.setoplen.1.1 a0, a1
# CHECK-ASM: [0x2b,0xf5,0x25,0x04]
sa.setoplen.1.1 x10, x11

# CHECK-ASM-AND-OBJ: sa.setoplen.0.0 a2, a3
# CHECK-ASM: [0x2b,0xf6,0x16,0x00]
sa.setoplen.0.0 x12, x13
