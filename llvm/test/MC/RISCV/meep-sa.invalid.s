# RUN: not llvm-mc %s -triple=riscv64 -mattr=+meep-sa -filetype=asm 2>&1 | FileCheck %s


# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setopleni.0.0 x1, 32

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setopleni.0.1 x7, 2138972

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setopleni.1.0 x10, 2138972

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setopleni.1.1 x4, 2138972

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setopleni.0.1 x0, 2138972

# CHECK: error: immediate must be an integer in the range [0, 15]
sa.setssr.0 16, x0

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setssr.0 11, 32

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setssr.0 7, 1452389

# CHECK: error: immediate must be an integer in the range [0, 31]
sa.setssr.0 0, 236712

# CHECK: error: invalid operand for instruction
sa.op11.1.1 sa.12, sa.4, sa.5

# CHECK: error: invalid operand for instruction
sa.op13.0.2 sa.31, sa.10, sa.1, sa.6

# CHECK: error: unrecognized instruction mnemonic
sa.op13.1.4 sa.1, sa.18, sa.20, sa.12

# CHECK: error: unrecognized instruction mnemonic
sa.op22.2.2 sa.12, sa.12, sa.0, sa.3

# CHECK: error: unrecognized instruction mnemonic
sa.load.0.3 sa.5, (x1)

# CHECK: error: unrecognized instruction mnemonic
sa.store.1.2 sa.23, (x3)

# CHECK: error: invalid operand for instruction
sa.load1d0.0.3 sa.5, x1

# CHECK: error: unrecognized instruction mnemonic
sa.store2d0x1 sa.20, (x10)

# CHECK: error: unrecognized instruction mnemonic
sa.load1d1.2.0 sa.13, (x7)

# CHECK: error: unrecognized instruction mnemonic
sa.store1d0.1.4 sa.27, (x31)

# CHECK: error: invalid operand for instruction
sa.load1d1.0.3 sa.31, (x2)

# CHECK: error: invalid operand for instruction
sa.store1d0.0.1 sa.17, (x32)
