# RUN: llvm-mc %s -triple=riscv64 -mattr=+meep-sa -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s
# RUN: llvm-mc -filetype=obj -triple=riscv64 -mattr=+meep-sa < %s \
# RUN:     | llvm-objdump -M no-aliases --mattr=+meep-sa -d -r - \
# RUN:     | FileCheck --check-prefix=CHECK-ASM-AND-OBJ %s

# CHECK-ASM-AND-OBJ: sa.op11.0.0 sa.0, sa.12
# CHECK-ASM: [0x2b,0x00,0xf6,0xf9]
sa.op11.0.0 sa.0, sa.12

# CHECK-ASM-AND-OBJ: sa.op12.1.1 sa.2, sa.7, sa.5 
# CHECK-ASM: [0x2b,0xa1,0x53,0xfc]
sa.op12.1.1 sa.2, sa.7, sa.5 

# CHECK-ASM-AND-OBJ: sa.op13.0.2 sa.1, sa.27, sa.16, sa.17
# CHECK-ASM: [0xab,0xc0,0x0d,0x89]
sa.op13.0.2 sa.1, sa.27, sa.16, sa.17

# CHECK-ASM-AND-OBJ: sa.op22.1.3 sa.22, sa.30, sa.15, sa.3
# CHECK-ASM: [0x2b,0xeb,0x37,0xf6]
sa.op22.1.3 sa.22, sa.30, sa.15, sa.3

# CHECK-ASM-AND-OBJ: sa.op12.1.2 sa.12, sa.0, sa.3
# CHECK-ASM: [0x2b,0x46,0x30,0xfc]
sa.op12.1.2 sa.12, sa.0, sa.3


