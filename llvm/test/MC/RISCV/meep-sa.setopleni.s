# RUN: llvm-mc %s -triple=riscv64 -mattr=+meep-sa -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s
# RUN: llvm-mc -filetype=obj -triple=riscv64 -mattr=+meep-sa < %s \
# RUN:     | llvm-objdump -M no-aliases --mattr=+meep-sa -d -r - \
# RUN:     | FileCheck --check-prefix=CHECK-ASM-AND-OBJ %s

# CHECK-ASM-AND-OBJ: sa.setopleni.0.0 ra, 0
# CHECK-ASM: [0xab,0x70,0x10,0x02]
sa.setopleni.0.0 x1, 0

# CHECK-ASM-AND-OBJ: sa.setopleni.0.1 t2, 31
# CHECK-ASM: [0xab,0xf3,0x2f,0x02]
sa.setopleni.0.1 x7, 31

# CHECK-ASM-AND-OBJ: sa.setopleni.1.0 a0, 21
# CHECK-ASM: [0x2b,0xf5,0x1a,0x06]
sa.setopleni.1.0 x10, 21

# CHECK-ASM-AND-OBJ: sa.setopleni.1.1 a6, 17
# CHECK-ASM: [0x2b,0xf8,0x28,0x06]
sa.setopleni.1.1 x16, 17

# CHECK-ASM-AND-OBJ: sa.setopleni.0.0 a2, 6
# CHECK-ASM: [0x2b,0x76,0x13,0x02]
sa.setopleni.0.0 x12, 6
