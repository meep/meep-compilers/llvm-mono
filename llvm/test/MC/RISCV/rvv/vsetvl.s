# RUN: llvm-mc -triple=riscv64 -show-encoding --mattr=+experimental-v %s \
# RUN:        | FileCheck %s --check-prefixes=CHECK-ENCODING,CHECK-INST
# RUN: not llvm-mc -triple=riscv64 -show-encoding %s 2>&1 \
# RUN:        | FileCheck %s --check-prefix=CHECK-ERROR
# RUN: llvm-mc -triple=riscv64 -filetype=obj --mattr=+experimental-v %s \
# RUN:        | llvm-objdump -d --mattr=+experimental-v - \
# RUN:        | FileCheck %s --check-prefix=CHECK-INST
# RUN: llvm-mc -triple=riscv64 -filetype=obj --mattr=+experimental-v %s \
# RUN:        | llvm-objdump -d - | FileCheck %s --check-prefix=CHECK-UNKNOWN

vsetvli a2, a0, e32,m4
# CHECK-INST: vsetvli a2, a0, e32,m4
# CHECK-ENCODING: [0x57,0x76,0xa5,0x00]
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 76 a5 00 <unknown>

vsetvli a2, a0, e32,m4,nt
# CHECK-INST: vsetvli a2, a0, e32,m4,nt
# CHECK-ENCODING: [0x57,0x76,0xa5,0x20]
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 76 a5 20 <unknown>

vsetvl a2, a0, a1
# CHECK-INST: vsetvl a2, a0, a1
# CHECK-ENCODING: [0x57,0x76,0xb5,0x80]
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 76 b5 80 <unknown>

vsetvli a2, a0, e32,m4,nt
# CHECK-INST: vsetvli a2, a0, e32,m4,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 76 a5 20 <unknown>

vsetvli s2, gp, b16,m1,a
# CHECK-INST: vsetvli s2, gp, b16,m1,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f9 41 50 <unknown>

vsetvli a6, s7, b16,m4,nt,a
# CHECK-INST: vsetvli a6, s7, b16,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f8 6b 70 <unknown>

vsetvli t1, s8, b16,m2
# CHECK-INST: vsetvli t1, s8, b16,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 73 5c 40 <unknown>

vsetvli ra, a2, b16,m2,a
# CHECK-INST: vsetvli ra, a2, b16,m2,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 70 56 50 <unknown>

vsetvli gp, s10, e16,m1
# CHECK-INST: vsetvli gp, s10, e16,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 71 4d 00 <unknown>

vsetvli t5, a3, e8,m2
# CHECK-INST: vsetvli t5, a3, e8,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 ff 16 00 <unknown>

vsetvli a5, sp, b16,m8
# CHECK-INST: vsetvli a5, sp, b16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 77 71 40 <unknown>

vsetvli t6, t2, e16,m1,a
# CHECK-INST: vsetvli t6, t2, e16,m1,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 ff 43 10 <unknown>

vsetvli a6, s10, b16,m2,nt
# CHECK-INST: vsetvli a6, s10, b16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 78 5d 60 <unknown>

vsetvli s7, s4, b16,m4
# CHECK-INST: vsetvli s7, s4, b16,m4
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7b 6a 40 <unknown>

vsetvli a4, s6, b16,m4,nt
# CHECK-INST: vsetvli a4, s6, b16,m4,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 77 6b 60 <unknown>

vsetvli s4, t0, e64,m4,a
# CHECK-INST: vsetvli s4, t0, e64,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fa e2 10 <unknown>

vsetvli s7, ra, e8,m4,nt,a
# CHECK-INST: vsetvli s7, ra, e8,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fb 20 30 <unknown>

vsetvli a5, t5, e8,m4,a
# CHECK-INST: vsetvli a5, t5, e8,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 77 2f 10 <unknown>

vsetvli tp, tp, b16,m1,a
# CHECK-INST: vsetvli tp, tp, b16,m1,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 72 42 50 <unknown>

vsetvli a7, s6, e8,m2
# CHECK-INST: vsetvli a7, s6, e8,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 78 1b 00 <unknown>

vsetvli s6, s1, b16,m8,a
# CHECK-INST: vsetvli s6, s1, b16,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fb 74 50 <unknown>

vsetvli s5, t0, e16,m2,nt
# CHECK-INST: vsetvli s5, t0, e16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fa 52 20 <unknown>

vsetvli a1, s3, b16,m2
# CHECK-INST: vsetvli a1, s3, b16,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f5 59 40 <unknown>

vsetvli s8, s1, b16,m8,nt
# CHECK-INST: vsetvli s8, s1, b16,m8,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fc 74 60 <unknown>

vsetvli t3, s3, e8,m1,nt
# CHECK-INST: vsetvli t3, s3, e8,m1,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fe 09 20 <unknown>

vsetvli s2, s9, e128,m1
# CHECK-INST: vsetvli s2, s9, e128,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f9 0c 01 <unknown>

vsetvli t6, a1, b16,m1,a
# CHECK-INST: vsetvli t6, a1, b16,m1,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 ff 45 50 <unknown>

vsetvli t6, s2, e16,m2,nt
# CHECK-INST: vsetvli t6, s2, e16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7f 59 20 <unknown>

vsetvli s4, t4, e16,m2,a
# CHECK-INST: vsetvli s4, t4, e16,m2,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fa 5e 10 <unknown>

vsetvli s6, s8, e8,m2,nt
# CHECK-INST: vsetvli s6, s8, e8,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7b 1c 20 <unknown>

vsetvli s7, t1, b16,m1,nt
# CHECK-INST: vsetvli s7, t1, b16,m1,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7b 43 60 <unknown>

vsetvli t5, t0, b16,m8,nt,a
# CHECK-INST: vsetvli t5, t0, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 ff 72 70 <unknown>

vsetvli a7, t5, b16,m8
# CHECK-INST: vsetvli a7, t5, b16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 78 7f 40 <unknown>

vsetvli t4, s11, b16,m8,a
# CHECK-INST: vsetvli t4, s11, b16,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fe 7d 50 <unknown>

vsetvli s2, s2, b16,m8,a
# CHECK-INST: vsetvli s2, s2, b16,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 79 79 50 <unknown>

vsetvli gp, s7, b16,m8,nt
# CHECK-INST: vsetvli gp, s7, b16,m8,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f1 7b 60 <unknown>

vsetvli t6, a5, b16,m4
# CHECK-INST: vsetvli t6, a5, b16,m4
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 ff 67 40 <unknown>

vsetvli s8, s1, e32,m2,nt
# CHECK-INST: vsetvli s8, s1, e32,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fc 94 20 <unknown>

vsetvli t5, t4, b16,m2,nt,a
# CHECK-INST: vsetvli t5, t4, b16,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 ff 5e 70 <unknown>

vsetvli t4, a0, e128,m4,a
# CHECK-INST: vsetvli t4, a0, e128,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7e 25 11 <unknown>

vsetvli a5, s8, e8,m4,nt,a
# CHECK-INST: vsetvli a5, s8, e8,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 77 2c 30 <unknown>

vsetvli t3, t3, b16,m8,nt,a
# CHECK-INST: vsetvli t3, t3, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7e 7e 70 <unknown>

vsetvli s10, s10, b16,m8,nt,a
# CHECK-INST: vsetvli s10, s10, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7d 7d 70 <unknown>

vsetvli s7, s7, b16,m2,a
# CHECK-INST: vsetvli s7, s7, b16,m2,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fb 5b 50 <unknown>

vsetvli s3, tp, b16,m8,nt,a
# CHECK-INST: vsetvli s3, tp, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 79 72 70 <unknown>

vsetvli a6, s10, e64,m1
# CHECK-INST: vsetvli a6, s10, e64,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 78 cd 00 <unknown>

vsetvli s9, a3, e16,m1
# CHECK-INST: vsetvli s9, a3, e16,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fc 46 00 <unknown>

vsetvli s5, t2, e32,m1,nt,a
# CHECK-INST: vsetvli s5, t2, e32,m1,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fa 83 30 <unknown>

vsetvli a1, t0, e32,m4,nt,a
# CHECK-INST: vsetvli a1, t0, e32,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f5 a2 30 <unknown>

vsetvli s5, s11, b16,m1
# CHECK-INST: vsetvli s5, s11, b16,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fa 4d 40 <unknown>

vsetvli s7, s0, b16,m4,nt,a
# CHECK-INST: vsetvli s7, s0, b16,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7b 64 70 <unknown>

vsetvli a6, t6, b16,m4
# CHECK-INST: vsetvli a6, t6, b16,m4
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f8 6f 40 <unknown>

vsetvli a2, a7, e64,m8,a
# CHECK-INST: vsetvli a2, a7, e64,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f6 f8 10 <unknown>

vsetvli t4, s5, e8,m1,nt
# CHECK-INST: vsetvli t4, s5, e8,m1,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fe 0a 20 <unknown>

vsetvli t4, t0, e8,m1,nt
# CHECK-INST: vsetvli t4, t0, e8,m1,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fe 02 20 <unknown>

vsetvli s10, t5, e8,m8,nt
# CHECK-INST: vsetvli s10, t5, e8,m8,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7d 3f 20 <unknown>

vsetvli t3, s3, e32,m2
# CHECK-INST: vsetvli t3, s3, e32,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fe 99 00 <unknown>

vsetvli s4, s6, e32,m1,nt,a
# CHECK-INST: vsetvli s4, s6, e32,m1,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7a 8b 30 <unknown>

vsetvli s5, t2, b16,m8,nt,a
# CHECK-INST: vsetvli s5, t2, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fa 73 70 <unknown>

vsetvli a4, s5, b16,m8,nt,a
# CHECK-INST: vsetvli a4, s5, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f7 7a 70 <unknown>

vsetvli s2, s0, b16,m2,nt
# CHECK-INST: vsetvli s2, s0, b16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 79 54 60 <unknown>

vsetvli a2, s2, b16,m2,nt,a
# CHECK-INST: vsetvli a2, s2, b16,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 76 59 70 <unknown>

vsetvli a0, s5, b16,m2,a
# CHECK-INST: vsetvli a0, s5, b16,m2,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f5 5a 50 <unknown>

vsetvli a3, a2, b16,m2,nt,a
# CHECK-INST: vsetvli a3, a2, b16,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 76 56 70 <unknown>

vsetvli a7, ra, b16,m2,nt
# CHECK-INST: vsetvli a7, ra, b16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f8 50 60 <unknown>

vsetvli ra, gp, b16,m2,nt
# CHECK-INST: vsetvli ra, gp, b16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f0 51 60 <unknown>

vsetvli s11, a1, b16,m4,nt
# CHECK-INST: vsetvli s11, a1, b16,m4,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fd 65 60 <unknown>

vsetvli ra, gp, b16,m2
# CHECK-INST: vsetvli ra, gp, b16,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f0 51 40 <unknown>

vsetvli a1, sp, b16,m1,nt,a
# CHECK-INST: vsetvli a1, sp, b16,m1,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 75 41 70 <unknown>

vsetvli t4, t3, b16,m8,nt,a
# CHECK-INST: vsetvli t4, t3, b16,m8,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7e 7e 70 <unknown>

vsetvli s9, t5, e64,m4,a
# CHECK-INST: vsetvli s9, t5, e64,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7c ef 10 <unknown>

vsetvli t6, t0, e32,m1
# CHECK-INST: vsetvli t6, t0, e32,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 ff 82 00 <unknown>

vsetvli s0, a3, e8,m8,a
# CHECK-INST: vsetvli s0, a3, e8,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f4 36 10 <unknown>

vsetvli a2, t4, e128,m1,nt,a
# CHECK-INST: vsetvli a2, t4, e128,m1,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f6 0e 31 <unknown>

vsetvli s11, s8, e128,m1,a
# CHECK-INST: vsetvli s11, s8, e128,m1,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7d 0c 11 <unknown>

vsetvli s2, tp, b16,m8,a
# CHECK-INST: vsetvli s2, tp, b16,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 79 72 50 <unknown>

vsetvli gp, s0, e128,m8,nt
# CHECK-INST: vsetvli gp, s0, e128,m8,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 71 34 21 <unknown>

vsetvli zero, s9, b16,m4
# CHECK-INST: vsetvli zero, s9, b16,m4
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f0 6c 40 <unknown>

vsetvli a5, s10, b16,m2,nt,a
# CHECK-INST: vsetvli a5, s10, b16,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 77 5d 70 <unknown>

vsetvli s4, t1, e8,m4,a
# CHECK-INST: vsetvli s4, t1, e8,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7a 23 10 <unknown>

vsetvli s6, a1, e128,m4,nt
# CHECK-INST: vsetvli s6, a1, e128,m4,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fb 25 21 <unknown>

vsetvli a3, t5, e16,m8,a
# CHECK-INST: vsetvli a3, t5, e16,m8,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 76 7f 10 <unknown>

vsetvli s11, a3, b16,m2,a
# CHECK-INST: vsetvli s11, a3, b16,m2,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fd 56 50 <unknown>

vsetvli ra, t3, b16,m4,nt,a
# CHECK-INST: vsetvli ra, t3, b16,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 70 6e 70 <unknown>

vsetvli gp, a2, e16,m8
# CHECK-INST: vsetvli gp, a2, e16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 71 76 00 <unknown>

vsetvli s0, t4, b16,m4,a
# CHECK-INST: vsetvli s0, t4, b16,m4,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f4 6e 50 <unknown>

vsetvli s7, zero, e64,m1
# CHECK-INST: vsetvli s7, zero, e64,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7b c0 00 <unknown>

vsetvli s5, a0, b16,m8
# CHECK-INST: vsetvli s5, a0, b16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7a 75 40 <unknown>

vsetvli sp, a6, b16,m1,nt,a
# CHECK-INST: vsetvli sp, a6, b16,m1,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 71 48 70 <unknown>

vsetvli s7, t2, b16,m4,nt
# CHECK-INST: vsetvli s7, t2, b16,m4,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 fb 63 60 <unknown>

vsetvli tp, a2, b16,m8
# CHECK-INST: vsetvli tp, a2, b16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 72 76 40 <unknown>

vsetvli s6, t2, b16,m4
# CHECK-INST: vsetvli s6, t2, b16,m4
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 fb 63 40 <unknown>

vsetvli a0, a6, e16,m4,nt,a
# CHECK-INST: vsetvli a0, a6, e16,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 75 68 30 <unknown>

vsetvli a4, s10, b16,m8
# CHECK-INST: vsetvli a4, s10, b16,m8
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 77 7d 40 <unknown>

vsetvli s2, a4, e64,m2
# CHECK-INST: vsetvli s2, a4, e64,m2
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 79 d7 00 <unknown>

vsetvli tp, sp, b16,m2,nt
# CHECK-INST: vsetvli tp, sp, b16,m2,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 72 51 60 <unknown>

vsetvli s4, sp, e128,m1
# CHECK-INST: vsetvli s4, sp, e128,m1
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 7a 01 01 <unknown>

vsetvli a1, s6, e128,m2,nt,a
# CHECK-INST: vsetvli a1, s6, e128,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 75 1b 31 <unknown>

vsetvli sp, s1, b16,m4,nt,a
# CHECK-INST: vsetvli sp, s1, b16,m4,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: 57 f1 64 70 <unknown>

vsetvli t6, s0, e16,m2,nt,a
# CHECK-INST: vsetvli t6, s0, e16,m2,nt,a
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 7f 54 30 <unknown>

vsetvli s3, s9, e16,m8,nt
# CHECK-INST: vsetvli s3, s9, e16,m8,nt
# CHECK-ERROR: instruction requires the following: 'V' (Vector Instructions)
# CHECK-UNKNOWN: d7 f9 7c 20 <unknown>
