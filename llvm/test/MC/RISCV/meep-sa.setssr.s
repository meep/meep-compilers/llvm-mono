# RUN: llvm-mc %s -triple=riscv64 -mattr=+meep-sa -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s
# RUN: llvm-mc -filetype=obj -triple=riscv64 -mattr=+meep-sa < %s \
# RUN:     | llvm-objdump -M no-aliases --mattr=+meep-sa -d -r - \
# RUN:     | FileCheck --check-prefix=CHECK-ASM-AND-OBJ %s

# CHECK-ASM-AND-OBJ: sa.setssr.0 15, zero
# CHECK-ASM: [0x2b,0x70,0xf0,0x01]
sa.setssr.0 15, x0

# CHECK-ASM-AND-OBJ:  sa.setssr.0 7, zero
# CHECK-ASM: [0x2b,0x70,0x70,0x01]
sa.setssr.0 7, x0

# CHECK-ASM-AND-OBJ: sa.setssr.0 8, zero 
# CHECK-ASM: [0x2b,0x70,0x80,0x01]
sa.setssr.0 8, x0

# CHECK-ASM-AND-OBJ: sa.setssr.0 10, zero
# CHECK-ASM: [0x2b,0x70,0xa0,0x01]
sa.setssr.0 10, x0

# CHECK-ASM-AND-OBJ: sa.setssr.0 14, zero
# CHECK-ASM: [0x2b,0x70,0xe0,0x01]
sa.setssr.0 14, x0
	
# CHECK-ASM-AND-OBJ: sa.setssr.0 10, 4
# CHECK-ASM: [0x2b,0x70,0xa2,0x03]
sa.setssr.0 10, 4

# CHECK-ASM-AND-OBJ: sa.setssr.1 5, s5
# CHECK-ASM: [0x2b,0xf0,0x5a,0x05]
sa.setssr.1 5, x21

# CHECK-ASM-AND-OBJ: sa.setssr.1 6, 17
# CHECK-ASM: [0x2b,0xf0,0x68,0x07]
sa.setssr.1 6, 17

# CHECK-ASM-AND-OBJ: sa.setssr.0 8, t1
# CHECK-ASM: [0x2b,0x70,0x83,0x01]
sa.setssr.0 8, x6
