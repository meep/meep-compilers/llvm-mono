# RUN: llvm-mc %s -triple=riscv64 -mattr=+meep-sa -riscv-no-aliases -show-encoding \
# RUN:     | FileCheck -check-prefixes=CHECK-ASM,CHECK-ASM-AND-OBJ %s
# RUN: llvm-mc -filetype=obj -triple=riscv64 -mattr=+meep-sa < %s \
# RUN:     | llvm-objdump -M no-aliases --mattr=+meep-sa -d -r - \
# RUN:     | FileCheck --check-prefix=CHECK-ASM-AND-OBJ %s


# CHECK-ASM-AND-OBJ: sa.load1d0.0.3  sa.5, (ra)
# CHECK-ASM: encoding: [0xab,0x92,0x30,0x00]
sa.load1d0.0.3 sa.5, (x1)

# CHECK-ASM-AND-OBJ: sa.load1d0.1.0  sa.1, (t0)
# CHECK-ASM: encoding: [0xab,0x90,0x02,0x04]
sa.load1d0.1.0 sa.1, (x5)
	
# CHECK-ASM-AND-OBJ: sa.load1d1.1.2  sa.0, (zero)
# CHECK-ASM: encoding: [0x2b,0x10,0x20,0x0c]
sa.load1d1.1.2 sa.0, (x0)
	
# CHECK-ASM-AND-OBJ: sa.load1d1.0.1  sa.20, (ra)
# CHECK-ASM: encoding: [0x2b,0x9a,0x10,0x08]
sa.load1d1.0.1 sa.20, (x1)
	
# CHECK-ASM-AND-OBJ: sa.load2d0x1.1.2        sa.15, (s11)
# CHECK-ASM: encoding: [0xab,0x97,0x2d,0x14]
sa.load2d0x1.1.2 sa.15, (x27)
		
# CHECK-ASM-AND-OBJ: sa.load2d0x1.0.0        sa.7, (tp)
# CHECK-ASM: encoding: [0xab,0x13,0x02,0x10]
sa.load2d0x1.0.0 sa.7, (x4)
		
# CHECK-ASM-AND-OBJ: sa.store1d0.1.2 sa.8, (t0)
# CHECK-ASM: encoding: [0x2b,0xb4,0x22,0x04]
sa.store1d0.1.2 sa.8, (x5)
		
# CHECK-ASM-AND-OBJ: sa.store1d0.0.2 sa.16, (s3)
# CHECK-ASM: encoding: [0x2b,0xb8,0x29,0x00]
sa.store1d0.0.2 sa.16, (x19)
		
# CHECK-ASM-AND-OBJ: sa.store1d1.1.3 sa.30, (t6)
# CHECK-ASM: encoding: [0x2b,0xbf,0x3f,0x0c]
sa.store1d1.1.3 sa.30, (x31)
		
# CHECK-ASM-AND-OBJ: sa.store1d1.0.1 sa.9, (gp)
# CHECK-ASM: encoding: [0xab,0xb4,0x11,0x08]
sa.store1d1.0.1 sa.9, (x3)
		
# CHECK-ASM-AND-OBJ: sa.store2d0x1.0.0       sa.20, (a7)
# CHECK-ASM: encoding: [0x2b,0xba,0x08,0x10]
sa.store2d0x1.0.0 sa.20, (x17)
		
# CHECK-ASM-AND-OBJ: sa.store2d0x1.1.0       sa.12, (s6)
# CHECK-ASM: encoding: [0x2b,0x36,0x0b,0x14]
sa.store2d0x1.1.0 sa.12, (x22)
	
