; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -target-abi lp64d -mattr=+f,+d,+experimental-v \
; RUN:     -O0 < %s -verify-machineinstrs | FileCheck --check-prefix=CHECK-O0 %s
; RUN: llc -mtriple=riscv64 -target-abi lp64d -mattr=+f,+d,+experimental-v \
; RUN:     -O2 < %s -verify-machineinstrs | FileCheck --check-prefix=CHECK-O2 %s

@scratch = global i8 0, align 16

define void @test_vp_fold_greater_splats(<vscale x 1 x double> %a, double %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_greater_splats:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a1
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    mv a1, a0
; CHECK-O0-NEXT:    ld a3, -56(s0)
; CHECK-O0-NEXT:    rdvtype a2
; CHECK-O0-NEXT:    rdvl a0
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a3)
; CHECK-O0-NEXT:    vsetvl zero, a0, a2
; CHECK-O0-NEXT:    # kill: def $x10 killed $x11
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfgt.vf v1, v16, fa0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmflt.vf v1, v16, fa0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfge.vf v1, v16, fa0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfle.vf v1, v16, fa0, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_greater_splats:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a1, %hi(scratch)
; CHECK-O2-NEXT:    addi a1, a1, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfgt.vf v1, v16, fa0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmflt.vf v1, v16, fa0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfge.vf v1, v16, fa0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfle.vf v1, v16, fa0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x double> undef, double %b, i32 0
  %splat = shufflevector <vscale x 1 x double> %head, <vscale x 1 x double> undef, <vscale x 1 x i32> zeroinitializer

  ; ; x > splat(y) → vmfgt.vf x, y
  %ogt.0 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %splat, i8 2, <vscale x 1 x i1> %m, i32 %n)
  %ogt.1 = zext <vscale x 1 x i1> %ogt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ogt.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) > x → x < splat(y) → vmflt.vf x, y
  %ogt.2 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %splat, <vscale x 1 x double> %a, i8 2, <vscale x 1 x i1> %m, i32 %n)
  %ogt.3 = zext <vscale x 1 x i1> %ogt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ogt.3, <vscale x 1 x i64>* %store_addr

  ; x >= splat(y) → vmfge.vf x, y
  %oge.0 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %splat, i8 3, <vscale x 1 x i1> %m, i32 %n)
  %oge.1 = zext <vscale x 1 x i1> %oge.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %oge.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) >= x → x <= splat(y) → vmfle.vf x, y
  %oge.2 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %splat, <vscale x 1 x double> %a, i8 3, <vscale x 1 x i1> %m, i32 %n)
  %oge.3 = zext <vscale x 1 x i1> %oge.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %oge.3, <vscale x 1 x i64>* %store_addr

  ret void
}

define void @test_vp_fold_lower_splats(<vscale x 1 x double> %a, <vscale x 1 x double> %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_lower_splats:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a1
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    mv a1, a0
; CHECK-O0-NEXT:    ld a3, -56(s0)
; CHECK-O0-NEXT:    rdvtype a2
; CHECK-O0-NEXT:    rdvl a0
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a3)
; CHECK-O0-NEXT:    vsetvl zero, a0, a2
; CHECK-O0-NEXT:    # kill: def $x10 killed $x11
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    lui a2, %hi(.LCPI1_0)
; CHECK-O0-NEXT:    fld ft0, %lo(.LCPI1_0)(a2)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmflt.vf v1, v16, ft0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfgt.vf v1, v16, ft0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfle.vf v1, v16, ft0, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmfge.vf v1, v16, ft0, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_lower_splats:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a1, %hi(.LCPI1_0)
; CHECK-O2-NEXT:    fld ft0, %lo(.LCPI1_0)(a1)
; CHECK-O2-NEXT:    lui a1, %hi(scratch)
; CHECK-O2-NEXT:    addi a1, a1, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmflt.vf v1, v16, ft0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfgt.vf v1, v16, ft0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfle.vf v1, v16, ft0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-O2-NEXT:    vmfge.vf v1, v16, ft0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a1)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x double> undef, double 2.0, i32 0
  %splat = shufflevector <vscale x 1 x double> %head, <vscale x 1 x double> undef, <vscale x 1 x i32> zeroinitializer

  ; x < splat(y) → vmflt.vf x, y
  %olt.0 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %splat, i8 4, <vscale x 1 x i1> %m, i32 %n)
  %olt.1 = zext <vscale x 1 x i1> %olt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %olt.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) < x → x > splat(y) → vmfgt.vf x, y
  %olt.2 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %splat, <vscale x 1 x double> %a, i8 4, <vscale x 1 x i1> %m, i32 %n)
  %olt.3 = zext <vscale x 1 x i1> %olt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %olt.3, <vscale x 1 x i64>* %store_addr

  ; x <= splat(y) → vmfle.vf x, y
  %ole.0 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %splat, i8 5, <vscale x 1 x i1> %m, i32 %n)
  %ole.1 = zext <vscale x 1 x i1> %ole.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ole.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) <= x → x >= splat(y) → vmfge.vf x, y
  %ole.2 = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %splat, <vscale x 1 x double> %a, i8 5, <vscale x 1 x i1> %m, i32 %n)
  %ole.3 = zext <vscale x 1 x i1> %ole.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ole.3, <vscale x 1 x i64>* %store_addr

  ret void
}

declare <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i8 immarg, <vscale x 1 x i1>, i32)

