; RUN: llc -mtriple=riscv64 -mattr=+m,+f,+d,+a,+c,+experimental-v \
; RUN:    -verify-machineinstrs --riscv-no-aliases < %s | FileCheck %s


define void @test1(i16* %spa, i16* %spb, i16* %spc) {
; CHECK: Begin function test1
  %pa = bitcast i16*  %spa to bfloat*
  %pb = bitcast i16*  %spb to bfloat*
  %pc = bitcast i16*  %spc to bfloat*

; CHECK: c.slli  {{.*}}, 16
; CHECK: slli    {{.*}}, {{.*}}, 16
  %a = load bfloat, bfloat* %pa
  %b = load bfloat, bfloat* %pb

  %d = fadd bfloat %a, %b

; CHECK: call    __truncsfbf2
  store bfloat %d, bfloat* %pc

  ret void
}
