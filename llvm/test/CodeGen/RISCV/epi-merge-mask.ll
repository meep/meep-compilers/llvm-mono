; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O0 \
; RUN:    < %s | FileCheck --check-prefix=CHECK-O0 %s
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O2 \
; RUN:    < %s | FileCheck --check-prefix=CHECK-O2 %s

declare i64 @llvm.epi.vsetvl(i64, i64, i64)

declare <vscale x 1 x double> @llvm.epi.vload.v1f64(
  <vscale x 1 x double>*,
  i64)

declare <vscale x 1 x i1> @llvm.epi.vmflt.v1i1.v1f64.v1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>,
  i64)

declare <vscale x 1 x double> @llvm.epi.vfsub.mask.v1f64.v1f64.v1i1(
  <vscale x 1 x double>,
  <vscale x 1 x double>,
  <vscale x 1 x double>,
  <vscale x 1 x i1>,
  i64)

declare void @llvm.epi.vstore.v1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64)

define void @merge_mask(i64 %vl, double* %c, double* %a, double* %b) nounwind {
; CHECK-O0-LABEL: merge_mask:
; CHECK-O0:       # %bb.0: # %entry
; CHECK-O0-NEXT:    vsetvli a0, a0, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v2
; CHECK-O0-NEXT:    vle.v v2, (a2)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vle.v v1, (a3)
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vmflt.vv v0, v2, v1
; CHECK-O0-NEXT:    vfsub.vv v1, v1, v2, v0.t
; CHECK-O0-NEXT:    vse.v v1, (a1)
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: merge_mask:
; CHECK-O2:       # %bb.0: # %entry
; CHECK-O2-NEXT:    vsetvli a0, a0, e64,m1
; CHECK-O2-NEXT:    vle.v v1, (a2)
; CHECK-O2-NEXT:    vle.v v2, (a3)
; CHECK-O2-NEXT:    vmflt.vv v0, v1, v2
; CHECK-O2-NEXT:    vfsub.vv v2, v2, v1, v0.t
; CHECK-O2-NEXT:    vse.v v2, (a1)
; CHECK-O2-NEXT:    ret
entry:

  %gvl = call i64 @llvm.epi.vsetvl(i64 %vl, i64 3, i64 0)

  %addr_a = bitcast double* %a to <vscale x 1 x double>*
  %vec_a = call <vscale x 1 x double> @llvm.epi.vload.v1f64(
    <vscale x 1 x double>* %addr_a,
    i64 %gvl)

  %addr_b = bitcast double* %b to <vscale x 1 x double>*
  %vec_b = call <vscale x 1 x double> @llvm.epi.vload.v1f64(
    <vscale x 1 x double>* %addr_b,
    i64 %gvl)

  %cmp = call <vscale x 1 x i1> @llvm.epi.vmflt.v1i1.v1f64.v1f64(
    <vscale x 1 x double> %vec_a,
    <vscale x 1 x double> %vec_b,
    i64 %gvl)

  %sub = call <vscale x 1 x double> @llvm.epi.vfsub.mask.v1f64.v1f64.v1i1(
    <vscale x 1 x double> %vec_b,
    <vscale x 1 x double> %vec_b,
    <vscale x 1 x double> %vec_a,
    <vscale x 1 x i1> %cmp,
    i64 %gvl)

  %addr_c = bitcast double* %c to <vscale x 1 x double>*
  call void @llvm.epi.vstore.v1f64(
    <vscale x 1 x double> %sub,
    <vscale x 1 x double>* %addr_c,
    i64 %gvl)

  ret void
}
