; RUN: llc -O0 -mtriple riscv64 -mattr=+m,+a,+f,+d,+c,+experimental-v \
; RUN:     -target-abi lp64d -o - < %s | FileCheck %s

; This used to fail but now it doesn't anymore. Kept here for regressions.
XFAIL: *
; CHECK:      bge     a0, a1, .LBB0_2
; CHECK-NEXT: j       .LBB0_4

source_filename = "n1fv_11.c"
target datalayout = "e-m:e-p:64:64-i64:64-i128:128-n64-S128-v128:128:128-v256:128:128-v512:128:128-v1024:128:128"
target triple = "riscv64-unknown-linux-gnu"

%struct.kdft_desc_s = type { i64, i8*, %struct.opcnt, %struct.kdft_genus*, i64, i64, i64, i64 }
%struct.opcnt = type { double, double, double, double }
%struct.kdft_genus = type { i32 (%struct.kdft_desc_s*, double*, double*, double*, double*, i64, i64, i64, i64, i64, %struct.planner_s*)*, i64 }
%struct.planner_s = type <{ %struct.planner_adt*, void (%struct.planner_s*, %struct.plan_s*, %struct.problem_s*, i32)*, double (%struct.problem_s*, double, i32)*, i32 (%struct.problem_s*, i64)*, void (%struct.problem_s*)*, i32 (i32, %struct.problem_s*)*, %struct.slvdesc_s*, i32, i32, i8*, i32, [8 x i32], i32, %struct.hashtab, %struct.hashtab, i32, %struct.flags_t, [4 x i8], %struct.timeval, double, i32, i32, i32, [4 x i8], double, double, i32, [4 x i8] }>
%struct.planner_adt = type { void (%struct.planner_s*, %struct.solver_s*)*, %struct.plan_s* (%struct.planner_s*, %struct.problem_s*)*, void (%struct.planner_s*, i32)*, void (%struct.planner_s*, %struct.printer_s*)*, i32 (%struct.planner_s*, %struct.scanner_s*)* }
%struct.solver_s = type { %struct.solver_adt*, i32 }
%struct.solver_adt = type { i32, %struct.plan_s* (%struct.solver_s*, %struct.problem_s*, %struct.planner_s*)*, void (%struct.solver_s*)* }
%struct.plan_s = type { %struct.plan_adt*, %struct.opcnt, double, i32, i32 }
%struct.plan_adt = type { void (%struct.plan_s*, %struct.problem_s*)*, void (%struct.plan_s*, i32)*, void (%struct.plan_s*, %struct.printer_s*)*, void (%struct.plan_s*)* }
%struct.printer_s = type { void (%struct.printer_s*, i8*, ...)*, void (%struct.printer_s*, i8*, i8*)*, void (%struct.printer_s*, i8)*, void (%struct.printer_s*)*, i32, i32 }
%struct.problem_s = type { %struct.problem_adt* }
%struct.problem_adt = type { i32, void (%struct.problem_s*, %struct.md5*)*, void (%struct.problem_s*)*, void (%struct.problem_s*, %struct.printer_s*)*, void (%struct.problem_s*)* }
%struct.md5 = type { [4 x i32], [64 x i8], i32 }
%struct.scanner_s = type { i32 (%struct.scanner_s*, i8*, ...)*, i32 (%struct.scanner_s*, i8*, i8*)*, i32 (%struct.scanner_s*)*, i32 }
%struct.slvdesc_s = type { %struct.solver_s*, i8*, i32, i32, i32 }
%struct.hashtab = type { %struct.solution_s*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.solution_s = type opaque
%struct.flags_t = type { i64 }
%struct.timeval = type { i64, i64 }

@desc = internal constant %struct.kdft_desc_s { i64 11, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0), %struct.opcnt { double 3.000000e+01, double 1.000000e+01, double 4.000000e+01, double 0.000000e+00 }, %struct.kdft_genus* @fftw_dft_n1fsimd_genus_r5v128, i64 0, i64 0, i64 0, i64 0 }, align 8
@fftw_an_INT_guaranteed_to_be_zero = external dso_local constant i64, align 8
@.str = private unnamed_addr constant [15 x i8] c"n1fv_11_r5v128\00", align 1
@fftw_dft_n1fsimd_genus_r5v128 = external dso_local constant %struct.kdft_genus, align 8

; Function Attrs: noinline nounwind optnone
define void @n1fv_11(double* %ri, double* %ii, double* %ro, double* %io, i64 %is, i64 %os, i64 %v, i64 %ivs, i64 %ovs) #2 {
entry:
  %ri.addr = alloca double*, align 8
  %ii.addr = alloca double*, align 8
  %ro.addr = alloca double*, align 8
  %io.addr = alloca double*, align 8
  %is.addr = alloca i64, align 8
  %os.addr = alloca i64, align 8
  %v.addr = alloca i64, align 8
  %ivs.addr = alloca i64, align 8
  %ovs.addr = alloca i64, align 8
  %KP654860733 = alloca <vscale x 1 x double>, align 8
  %KP142314838 = alloca <vscale x 1 x double>, align 8
  %KP959492973 = alloca <vscale x 1 x double>, align 8
  %KP415415013 = alloca <vscale x 1 x double>, align 8
  %KP841253532 = alloca <vscale x 1 x double>, align 8
  %KP989821441 = alloca <vscale x 1 x double>, align 8
  %KP909631995 = alloca <vscale x 1 x double>, align 8
  %KP281732556 = alloca <vscale x 1 x double>, align 8
  %KP540640817 = alloca <vscale x 1 x double>, align 8
  %KP755749574 = alloca <vscale x 1 x double>, align 8
  %i = alloca i64, align 8
  %xi = alloca double*, align 8
  %xo = alloca double*, align 8
  %T1 = alloca <vscale x 1 x double>, align 8
  %T4 = alloca <vscale x 1 x double>, align 8
  %Ti = alloca <vscale x 1 x double>, align 8
  %Tg = alloca <vscale x 1 x double>, align 8
  %Tl = alloca <vscale x 1 x double>, align 8
  %Td = alloca <vscale x 1 x double>, align 8
  %Tk = alloca <vscale x 1 x double>, align 8
  %Ta = alloca <vscale x 1 x double>, align 8
  %Tj = alloca <vscale x 1 x double>, align 8
  %T7 = alloca <vscale x 1 x double>, align 8
  %Tm = alloca <vscale x 1 x double>, align 8
  %Tb = alloca <vscale x 1 x double>, align 8
  %Tc = alloca <vscale x 1 x double>, align 8
  %Tt = alloca <vscale x 1 x double>, align 8
  %Ts = alloca <vscale x 1 x double>, align 8
  %T2 = alloca <vscale x 1 x double>, align 8
  %T3 = alloca <vscale x 1 x double>, align 8
  %Te = alloca <vscale x 1 x double>, align 8
  %Tf = alloca <vscale x 1 x double>, align 8
  %T8 = alloca <vscale x 1 x double>, align 8
  %T9 = alloca <vscale x 1 x double>, align 8
  %T5 = alloca <vscale x 1 x double>, align 8
  %T6 = alloca <vscale x 1 x double>, align 8
  %Tn = alloca <vscale x 1 x double>, align 8
  %Th = alloca <vscale x 1 x double>, align 8
  %Tv = alloca <vscale x 1 x double>, align 8
  %Tu = alloca <vscale x 1 x double>, align 8
  %Tr = alloca <vscale x 1 x double>, align 8
  %Tq = alloca <vscale x 1 x double>, align 8
  %Tp = alloca <vscale x 1 x double>, align 8
  %To = alloca <vscale x 1 x double>, align 8
  store double* %ri, double** %ri.addr, align 8
  store double* %ii, double** %ii.addr, align 8
  store double* %ro, double** %ro.addr, align 8
  store double* %io, double** %io.addr, align 8
  store i64 %is, i64* %is.addr, align 8
  store i64 %os, i64* %os.addr, align 8
  store i64 %v, i64* %v.addr, align 8
  store i64 %ivs, i64* %ivs.addr, align 8
  store i64 %ovs, i64* %ovs.addr, align 8
  %0 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FE4F49E7F775887, i64 2)
  store <vscale x 1 x double> %0, <vscale x 1 x double>* %KP654860733, align 8
  %1 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FC2375F640F44DB, i64 2)
  store <vscale x 1 x double> %1, <vscale x 1 x double>* %KP142314838, align 8
  %2 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FEEB42A9BCD5057, i64 2)
  store <vscale x 1 x double> %2, <vscale x 1 x double>* %KP959492973, align 8
  %3 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FDA9628D9C712B6, i64 2)
  store <vscale x 1 x double> %3, <vscale x 1 x double>* %KP415415013, align 8
  %4 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FEAEB8C8764F0BA, i64 2)
  store <vscale x 1 x double> %4, <vscale x 1 x double>* %KP841253532, align 8
  %5 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FEFAC9E043842EF, i64 2)
  store <vscale x 1 x double> %5, <vscale x 1 x double>* %KP989821441, align 8
  %6 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FED1BB48EEE2C13, i64 2)
  store <vscale x 1 x double> %6, <vscale x 1 x double>* %KP909631995, align 8
  %7 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FD207E7FD768DBF, i64 2)
  store <vscale x 1 x double> %7, <vscale x 1 x double>* %KP281732556, align 8
  %8 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FE14CEDF8BB580B, i64 2)
  store <vscale x 1 x double> %8, <vscale x 1 x double>* %KP540640817, align 8
  %9 = call <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double 0x3FE82F19BB3A28A1, i64 2)
  store <vscale x 1 x double> %9, <vscale x 1 x double>* %KP755749574, align 8
  %10 = load double*, double** %ri.addr, align 8
  store double* %10, double** %xi, align 8
  %11 = load double*, double** %ro.addr, align 8
  store double* %11, double** %xo, align 8
  %12 = load i64, i64* %v.addr, align 8
  store i64 %12, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i64, i64* %i, align 8
  %cmp = icmp sgt i64 %13, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load double*, double** %xi, align 8
  %arrayidx = getelementptr inbounds double, double* %14, i64 0
  %15 = load i64, i64* %ivs.addr, align 8
  %16 = load double*, double** %xi, align 8
  %arrayidx1 = getelementptr inbounds double, double* %16, i64 0
  %call = call <vscale x 1 x double> @LDu(double* %arrayidx, i64 %15, double* %arrayidx1)
  store <vscale x 1 x double> %call, <vscale x 1 x double>* %T1, align 8
  %17 = load double*, double** %xi, align 8
  %18 = load i64, i64* %is.addr, align 8
  %mul = mul nsw i64 %18, 1
  %arrayidx2 = getelementptr inbounds double, double* %17, i64 %mul
  %19 = load i64, i64* %ivs.addr, align 8
  %20 = load double*, double** %xi, align 8
  %21 = load i64, i64* %is.addr, align 8
  %mul3 = mul nsw i64 %21, 1
  %arrayidx4 = getelementptr inbounds double, double* %20, i64 %mul3
  %call5 = call <vscale x 1 x double> @LDu(double* %arrayidx2, i64 %19, double* %arrayidx4)
  store <vscale x 1 x double> %call5, <vscale x 1 x double>* %T2, align 8
  %22 = load double*, double** %xi, align 8
  %23 = load i64, i64* %is.addr, align 8
  %mul6 = mul nsw i64 %23, 10
  %arrayidx7 = getelementptr inbounds double, double* %22, i64 %mul6
  %24 = load i64, i64* %ivs.addr, align 8
  %25 = load double*, double** %xi, align 8
  %arrayidx8 = getelementptr inbounds double, double* %25, i64 0
  %call9 = call <vscale x 1 x double> @LDu(double* %arrayidx7, i64 %24, double* %arrayidx8)
  store <vscale x 1 x double> %call9, <vscale x 1 x double>* %T3, align 8
  %26 = load <vscale x 1 x double>, <vscale x 1 x double>* %T2, align 8
  %27 = load <vscale x 1 x double>, <vscale x 1 x double>* %T3, align 8
  %28 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %26, <vscale x 1 x double> %27, i64 2)
  store <vscale x 1 x double> %28, <vscale x 1 x double>* %T4, align 8
  %29 = load <vscale x 1 x double>, <vscale x 1 x double>* %T3, align 8
  %30 = load <vscale x 1 x double>, <vscale x 1 x double>* %T2, align 8
  %31 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %29, <vscale x 1 x double> %30, i64 2)
  store <vscale x 1 x double> %31, <vscale x 1 x double>* %Ti, align 8
  %32 = load double*, double** %xi, align 8
  %33 = load i64, i64* %is.addr, align 8
  %mul10 = mul nsw i64 %33, 5
  %arrayidx11 = getelementptr inbounds double, double* %32, i64 %mul10
  %34 = load i64, i64* %ivs.addr, align 8
  %35 = load double*, double** %xi, align 8
  %36 = load i64, i64* %is.addr, align 8
  %mul12 = mul nsw i64 %36, 1
  %arrayidx13 = getelementptr inbounds double, double* %35, i64 %mul12
  %call14 = call <vscale x 1 x double> @LDu(double* %arrayidx11, i64 %34, double* %arrayidx13)
  store <vscale x 1 x double> %call14, <vscale x 1 x double>* %Te, align 8
  %37 = load double*, double** %xi, align 8
  %38 = load i64, i64* %is.addr, align 8
  %mul15 = mul nsw i64 %38, 6
  %arrayidx16 = getelementptr inbounds double, double* %37, i64 %mul15
  %39 = load i64, i64* %ivs.addr, align 8
  %40 = load double*, double** %xi, align 8
  %arrayidx17 = getelementptr inbounds double, double* %40, i64 0
  %call18 = call <vscale x 1 x double> @LDu(double* %arrayidx16, i64 %39, double* %arrayidx17)
  store <vscale x 1 x double> %call18, <vscale x 1 x double>* %Tf, align 8
  %41 = load <vscale x 1 x double>, <vscale x 1 x double>* %Te, align 8
  %42 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tf, align 8
  %43 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %41, <vscale x 1 x double> %42, i64 2)
  store <vscale x 1 x double> %43, <vscale x 1 x double>* %Tg, align 8
  %44 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tf, align 8
  %45 = load <vscale x 1 x double>, <vscale x 1 x double>* %Te, align 8
  %46 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %44, <vscale x 1 x double> %45, i64 2)
  store <vscale x 1 x double> %46, <vscale x 1 x double>* %Tl, align 8
  %47 = load double*, double** %xi, align 8
  %48 = load i64, i64* %is.addr, align 8
  %mul19 = mul nsw i64 %48, 4
  %arrayidx20 = getelementptr inbounds double, double* %47, i64 %mul19
  %49 = load i64, i64* %ivs.addr, align 8
  %50 = load double*, double** %xi, align 8
  %arrayidx21 = getelementptr inbounds double, double* %50, i64 0
  %call22 = call <vscale x 1 x double> @LDu(double* %arrayidx20, i64 %49, double* %arrayidx21)
  store <vscale x 1 x double> %call22, <vscale x 1 x double>* %Tb, align 8
  %51 = load double*, double** %xi, align 8
  %52 = load i64, i64* %is.addr, align 8
  %mul23 = mul nsw i64 %52, 7
  %arrayidx24 = getelementptr inbounds double, double* %51, i64 %mul23
  %53 = load i64, i64* %ivs.addr, align 8
  %54 = load double*, double** %xi, align 8
  %55 = load i64, i64* %is.addr, align 8
  %mul25 = mul nsw i64 %55, 1
  %arrayidx26 = getelementptr inbounds double, double* %54, i64 %mul25
  %call27 = call <vscale x 1 x double> @LDu(double* %arrayidx24, i64 %53, double* %arrayidx26)
  store <vscale x 1 x double> %call27, <vscale x 1 x double>* %Tc, align 8
  %56 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tb, align 8
  %57 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tc, align 8
  %58 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %56, <vscale x 1 x double> %57, i64 2)
  store <vscale x 1 x double> %58, <vscale x 1 x double>* %Td, align 8
  %59 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tc, align 8
  %60 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tb, align 8
  %61 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %59, <vscale x 1 x double> %60, i64 2)
  store <vscale x 1 x double> %61, <vscale x 1 x double>* %Tk, align 8
  %62 = load double*, double** %xi, align 8
  %63 = load i64, i64* %is.addr, align 8
  %mul28 = mul nsw i64 %63, 3
  %arrayidx29 = getelementptr inbounds double, double* %62, i64 %mul28
  %64 = load i64, i64* %ivs.addr, align 8
  %65 = load double*, double** %xi, align 8
  %66 = load i64, i64* %is.addr, align 8
  %mul30 = mul nsw i64 %66, 1
  %arrayidx31 = getelementptr inbounds double, double* %65, i64 %mul30
  %call32 = call <vscale x 1 x double> @LDu(double* %arrayidx29, i64 %64, double* %arrayidx31)
  store <vscale x 1 x double> %call32, <vscale x 1 x double>* %T8, align 8
  %67 = load double*, double** %xi, align 8
  %68 = load i64, i64* %is.addr, align 8
  %mul33 = mul nsw i64 %68, 8
  %arrayidx34 = getelementptr inbounds double, double* %67, i64 %mul33
  %69 = load i64, i64* %ivs.addr, align 8
  %70 = load double*, double** %xi, align 8
  %arrayidx35 = getelementptr inbounds double, double* %70, i64 0
  %call36 = call <vscale x 1 x double> @LDu(double* %arrayidx34, i64 %69, double* %arrayidx35)
  store <vscale x 1 x double> %call36, <vscale x 1 x double>* %T9, align 8
  %71 = load <vscale x 1 x double>, <vscale x 1 x double>* %T8, align 8
  %72 = load <vscale x 1 x double>, <vscale x 1 x double>* %T9, align 8
  %73 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %71, <vscale x 1 x double> %72, i64 2)
  store <vscale x 1 x double> %73, <vscale x 1 x double>* %Ta, align 8
  %74 = load <vscale x 1 x double>, <vscale x 1 x double>* %T9, align 8
  %75 = load <vscale x 1 x double>, <vscale x 1 x double>* %T8, align 8
  %76 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %74, <vscale x 1 x double> %75, i64 2)
  store <vscale x 1 x double> %76, <vscale x 1 x double>* %Tj, align 8
  %77 = load double*, double** %xi, align 8
  %78 = load i64, i64* %is.addr, align 8
  %mul37 = mul nsw i64 %78, 2
  %arrayidx38 = getelementptr inbounds double, double* %77, i64 %mul37
  %79 = load i64, i64* %ivs.addr, align 8
  %80 = load double*, double** %xi, align 8
  %arrayidx39 = getelementptr inbounds double, double* %80, i64 0
  %call40 = call <vscale x 1 x double> @LDu(double* %arrayidx38, i64 %79, double* %arrayidx39)
  store <vscale x 1 x double> %call40, <vscale x 1 x double>* %T5, align 8
  %81 = load double*, double** %xi, align 8
  %82 = load i64, i64* %is.addr, align 8
  %mul41 = mul nsw i64 %82, 9
  %arrayidx42 = getelementptr inbounds double, double* %81, i64 %mul41
  %83 = load i64, i64* %ivs.addr, align 8
  %84 = load double*, double** %xi, align 8
  %85 = load i64, i64* %is.addr, align 8
  %mul43 = mul nsw i64 %85, 1
  %arrayidx44 = getelementptr inbounds double, double* %84, i64 %mul43
  %call45 = call <vscale x 1 x double> @LDu(double* %arrayidx42, i64 %83, double* %arrayidx44)
  store <vscale x 1 x double> %call45, <vscale x 1 x double>* %T6, align 8
  %86 = load <vscale x 1 x double>, <vscale x 1 x double>* %T5, align 8
  %87 = load <vscale x 1 x double>, <vscale x 1 x double>* %T6, align 8
  %88 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %86, <vscale x 1 x double> %87, i64 2)
  store <vscale x 1 x double> %88, <vscale x 1 x double>* %T7, align 8
  %89 = load <vscale x 1 x double>, <vscale x 1 x double>* %T6, align 8
  %90 = load <vscale x 1 x double>, <vscale x 1 x double>* %T5, align 8
  %91 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %89, <vscale x 1 x double> %90, i64 2)
  store <vscale x 1 x double> %91, <vscale x 1 x double>* %Tm, align 8
  %92 = load double*, double** %xo, align 8
  %arrayidx46 = getelementptr inbounds double, double* %92, i64 0
  %93 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %94 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %95 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %96 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %97 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %98 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %99 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %97, <vscale x 1 x double> %98, i64 2)
  %100 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %96, <vscale x 1 x double> %99, i64 2)
  %101 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %95, <vscale x 1 x double> %100, i64 2)
  %102 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %94, <vscale x 1 x double> %101, i64 2)
  %103 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %93, <vscale x 1 x double> %102, i64 2)
  %104 = load i64, i64* %ovs.addr, align 8
  %105 = load double*, double** %xo, align 8
  %arrayidx47 = getelementptr inbounds double, double* %105, i64 0
  call void @STu(double* %arrayidx46, <vscale x 1 x double> %103, i64 %104, double* %arrayidx47)
  %106 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP281732556, align 8
  %107 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tk, align 8
  %108 = call <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double> %106, <vscale x 1 x double> %107, i64 2)
  %109 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP989821441, align 8
  %110 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tm, align 8
  %111 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %108, <vscale x 1 x double> %109, <vscale x 1 x double> %110, i64 2)
  %112 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP909631995, align 8
  %113 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tl, align 8
  %114 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %111, <vscale x 1 x double> %112, <vscale x 1 x double> %113, i64 2)
  %115 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP540640817, align 8
  %116 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tj, align 8
  %117 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %114, <vscale x 1 x double> %115, <vscale x 1 x double> %116, i64 2)
  %118 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP755749574, align 8
  %119 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ti, align 8
  %120 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %117, <vscale x 1 x double> %118, <vscale x 1 x double> %119, i64 2)
  %call48 = call <vscale x 1 x double> @VBYI(<vscale x 1 x double> %120)
  store <vscale x 1 x double> %call48, <vscale x 1 x double>* %Tn, align 8
  %121 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %122 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP654860733, align 8
  %123 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %124 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %121, <vscale x 1 x double> %122, <vscale x 1 x double> %123, i64 2)
  %125 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP142314838, align 8
  %126 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %127 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %124, <vscale x 1 x double> %125, <vscale x 1 x double> %126, i64 2)
  %128 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP959492973, align 8
  %129 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %130 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %127, <vscale x 1 x double> %128, <vscale x 1 x double> %129, i64 2)
  %131 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP415415013, align 8
  %132 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %133 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %130, <vscale x 1 x double> %131, <vscale x 1 x double> %132, i64 2)
  %134 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP841253532, align 8
  %135 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %136 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %133, <vscale x 1 x double> %134, <vscale x 1 x double> %135, i64 2)
  store <vscale x 1 x double> %136, <vscale x 1 x double>* %Th, align 8
  %137 = load double*, double** %xo, align 8
  %138 = load i64, i64* %os.addr, align 8
  %mul49 = mul nsw i64 %138, 7
  %arrayidx50 = getelementptr inbounds double, double* %137, i64 %mul49
  %139 = load <vscale x 1 x double>, <vscale x 1 x double>* %Th, align 8
  %140 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tn, align 8
  %141 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %139, <vscale x 1 x double> %140, i64 2)
  %142 = load i64, i64* %ovs.addr, align 8
  %143 = load double*, double** %xo, align 8
  %144 = load i64, i64* %os.addr, align 8
  %mul51 = mul nsw i64 %144, 1
  %arrayidx52 = getelementptr inbounds double, double* %143, i64 %mul51
  call void @STu(double* %arrayidx50, <vscale x 1 x double> %141, i64 %142, double* %arrayidx52)
  %145 = load double*, double** %xo, align 8
  %146 = load i64, i64* %os.addr, align 8
  %mul53 = mul nsw i64 %146, 4
  %arrayidx54 = getelementptr inbounds double, double* %145, i64 %mul53
  %147 = load <vscale x 1 x double>, <vscale x 1 x double>* %Th, align 8
  %148 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tn, align 8
  %149 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %147, <vscale x 1 x double> %148, i64 2)
  %150 = load i64, i64* %ovs.addr, align 8
  %151 = load double*, double** %xo, align 8
  %arrayidx55 = getelementptr inbounds double, double* %151, i64 0
  call void @STu(double* %arrayidx54, <vscale x 1 x double> %149, i64 %150, double* %arrayidx55)
  %152 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP989821441, align 8
  %153 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tl, align 8
  %154 = call <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double> %152, <vscale x 1 x double> %153, i64 2)
  %155 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP540640817, align 8
  %156 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tm, align 8
  %157 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %154, <vscale x 1 x double> %155, <vscale x 1 x double> %156, i64 2)
  %158 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP909631995, align 8
  %159 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tk, align 8
  %160 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %157, <vscale x 1 x double> %158, <vscale x 1 x double> %159, i64 2)
  %161 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP755749574, align 8
  %162 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tj, align 8
  %163 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %160, <vscale x 1 x double> %161, <vscale x 1 x double> %162, i64 2)
  %164 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP281732556, align 8
  %165 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ti, align 8
  %166 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %163, <vscale x 1 x double> %164, <vscale x 1 x double> %165, i64 2)
  %call56 = call <vscale x 1 x double> @VBYI(<vscale x 1 x double> %166)
  store <vscale x 1 x double> %call56, <vscale x 1 x double>* %Tv, align 8
  %167 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %168 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP959492973, align 8
  %169 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %170 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %167, <vscale x 1 x double> %168, <vscale x 1 x double> %169, i64 2)
  %171 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP654860733, align 8
  %172 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %173 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %170, <vscale x 1 x double> %171, <vscale x 1 x double> %172, i64 2)
  %174 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP142314838, align 8
  %175 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %176 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %173, <vscale x 1 x double> %174, <vscale x 1 x double> %175, i64 2)
  %177 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP415415013, align 8
  %178 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %179 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %176, <vscale x 1 x double> %177, <vscale x 1 x double> %178, i64 2)
  %180 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP841253532, align 8
  %181 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %182 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %179, <vscale x 1 x double> %180, <vscale x 1 x double> %181, i64 2)
  store <vscale x 1 x double> %182, <vscale x 1 x double>* %Tu, align 8
  %183 = load double*, double** %xo, align 8
  %184 = load i64, i64* %os.addr, align 8
  %mul57 = mul nsw i64 %184, 6
  %arrayidx58 = getelementptr inbounds double, double* %183, i64 %mul57
  %185 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tu, align 8
  %186 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tv, align 8
  %187 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %185, <vscale x 1 x double> %186, i64 2)
  %188 = load i64, i64* %ovs.addr, align 8
  %189 = load double*, double** %xo, align 8
  %arrayidx59 = getelementptr inbounds double, double* %189, i64 0
  call void @STu(double* %arrayidx58, <vscale x 1 x double> %187, i64 %188, double* %arrayidx59)
  %190 = load double*, double** %xo, align 8
  %191 = load i64, i64* %os.addr, align 8
  %mul60 = mul nsw i64 %191, 5
  %arrayidx61 = getelementptr inbounds double, double* %190, i64 %mul60
  %192 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tu, align 8
  %193 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tv, align 8
  %194 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %192, <vscale x 1 x double> %193, i64 2)
  %195 = load i64, i64* %ovs.addr, align 8
  %196 = load double*, double** %xo, align 8
  %197 = load i64, i64* %os.addr, align 8
  %mul62 = mul nsw i64 %197, 1
  %arrayidx63 = getelementptr inbounds double, double* %196, i64 %mul62
  call void @STu(double* %arrayidx61, <vscale x 1 x double> %194, i64 %195, double* %arrayidx63)
  %198 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP755749574, align 8
  %199 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tl, align 8
  %200 = call <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double> %198, <vscale x 1 x double> %199, i64 2)
  %201 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP281732556, align 8
  %202 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tm, align 8
  %203 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %200, <vscale x 1 x double> %201, <vscale x 1 x double> %202, i64 2)
  %204 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP909631995, align 8
  %205 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tj, align 8
  %206 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %203, <vscale x 1 x double> %204, <vscale x 1 x double> %205, i64 2)
  %207 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP540640817, align 8
  %208 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tk, align 8
  %209 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %206, <vscale x 1 x double> %207, <vscale x 1 x double> %208, i64 2)
  %210 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP989821441, align 8
  %211 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ti, align 8
  %212 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %209, <vscale x 1 x double> %210, <vscale x 1 x double> %211, i64 2)
  %call64 = call <vscale x 1 x double> @VBYI(<vscale x 1 x double> %212)
  store <vscale x 1 x double> %call64, <vscale x 1 x double>* %Tt, align 8
  %213 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %214 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP142314838, align 8
  %215 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %216 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %213, <vscale x 1 x double> %214, <vscale x 1 x double> %215, i64 2)
  %217 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP959492973, align 8
  %218 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %219 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %216, <vscale x 1 x double> %217, <vscale x 1 x double> %218, i64 2)
  %220 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP654860733, align 8
  %221 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %222 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %219, <vscale x 1 x double> %220, <vscale x 1 x double> %221, i64 2)
  %223 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP841253532, align 8
  %224 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %225 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %222, <vscale x 1 x double> %223, <vscale x 1 x double> %224, i64 2)
  %226 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP415415013, align 8
  %227 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %228 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %225, <vscale x 1 x double> %226, <vscale x 1 x double> %227, i64 2)
  store <vscale x 1 x double> %228, <vscale x 1 x double>* %Ts, align 8
  %229 = load double*, double** %xo, align 8
  %230 = load i64, i64* %os.addr, align 8
  %mul65 = mul nsw i64 %230, 8
  %arrayidx66 = getelementptr inbounds double, double* %229, i64 %mul65
  %231 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ts, align 8
  %232 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tt, align 8
  %233 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %231, <vscale x 1 x double> %232, i64 2)
  %234 = load i64, i64* %ovs.addr, align 8
  %235 = load double*, double** %xo, align 8
  %arrayidx67 = getelementptr inbounds double, double* %235, i64 0
  call void @STu(double* %arrayidx66, <vscale x 1 x double> %233, i64 %234, double* %arrayidx67)
  %236 = load double*, double** %xo, align 8
  %237 = load i64, i64* %os.addr, align 8
  %mul68 = mul nsw i64 %237, 3
  %arrayidx69 = getelementptr inbounds double, double* %236, i64 %mul68
  %238 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ts, align 8
  %239 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tt, align 8
  %240 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %238, <vscale x 1 x double> %239, i64 2)
  %241 = load i64, i64* %ovs.addr, align 8
  %242 = load double*, double** %xo, align 8
  %243 = load i64, i64* %os.addr, align 8
  %mul70 = mul nsw i64 %243, 1
  %arrayidx71 = getelementptr inbounds double, double* %242, i64 %mul70
  call void @STu(double* %arrayidx69, <vscale x 1 x double> %240, i64 %241, double* %arrayidx71)
  %244 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP281732556, align 8
  %245 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tl, align 8
  %246 = call <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double> %244, <vscale x 1 x double> %245, i64 2)
  %247 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP755749574, align 8
  %248 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tk, align 8
  %249 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %246, <vscale x 1 x double> %247, <vscale x 1 x double> %248, i64 2)
  %250 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP989821441, align 8
  %251 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tj, align 8
  %252 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %249, <vscale x 1 x double> %250, <vscale x 1 x double> %251, i64 2)
  %253 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP909631995, align 8
  %254 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tm, align 8
  %255 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %252, <vscale x 1 x double> %253, <vscale x 1 x double> %254, i64 2)
  %256 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP540640817, align 8
  %257 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ti, align 8
  %258 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %255, <vscale x 1 x double> %256, <vscale x 1 x double> %257, i64 2)
  %call72 = call <vscale x 1 x double> @VBYI(<vscale x 1 x double> %258)
  store <vscale x 1 x double> %call72, <vscale x 1 x double>* %Tr, align 8
  %259 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %260 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP142314838, align 8
  %261 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %262 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %259, <vscale x 1 x double> %260, <vscale x 1 x double> %261, i64 2)
  %263 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP654860733, align 8
  %264 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %265 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %262, <vscale x 1 x double> %263, <vscale x 1 x double> %264, i64 2)
  %266 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP959492973, align 8
  %267 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %268 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %265, <vscale x 1 x double> %266, <vscale x 1 x double> %267, i64 2)
  %269 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP415415013, align 8
  %270 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %271 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %268, <vscale x 1 x double> %269, <vscale x 1 x double> %270, i64 2)
  %272 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP841253532, align 8
  %273 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %274 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %271, <vscale x 1 x double> %272, <vscale x 1 x double> %273, i64 2)
  store <vscale x 1 x double> %274, <vscale x 1 x double>* %Tq, align 8
  %275 = load double*, double** %xo, align 8
  %276 = load i64, i64* %os.addr, align 8
  %mul73 = mul nsw i64 %276, 10
  %arrayidx74 = getelementptr inbounds double, double* %275, i64 %mul73
  %277 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tq, align 8
  %278 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tr, align 8
  %279 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %277, <vscale x 1 x double> %278, i64 2)
  %280 = load i64, i64* %ovs.addr, align 8
  %281 = load double*, double** %xo, align 8
  %arrayidx75 = getelementptr inbounds double, double* %281, i64 0
  call void @STu(double* %arrayidx74, <vscale x 1 x double> %279, i64 %280, double* %arrayidx75)
  %282 = load double*, double** %xo, align 8
  %283 = load i64, i64* %os.addr, align 8
  %mul76 = mul nsw i64 %283, 1
  %arrayidx77 = getelementptr inbounds double, double* %282, i64 %mul76
  %284 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tq, align 8
  %285 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tr, align 8
  %286 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %284, <vscale x 1 x double> %285, i64 2)
  %287 = load i64, i64* %ovs.addr, align 8
  %288 = load double*, double** %xo, align 8
  %289 = load i64, i64* %os.addr, align 8
  %mul78 = mul nsw i64 %289, 1
  %arrayidx79 = getelementptr inbounds double, double* %288, i64 %mul78
  call void @STu(double* %arrayidx77, <vscale x 1 x double> %286, i64 %287, double* %arrayidx79)
  %290 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP755749574, align 8
  %291 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tm, align 8
  %292 = call <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double> %290, <vscale x 1 x double> %291, i64 2)
  %293 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP281732556, align 8
  %294 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tj, align 8
  %295 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %292, <vscale x 1 x double> %293, <vscale x 1 x double> %294, i64 2)
  %296 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP989821441, align 8
  %297 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tk, align 8
  %298 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %295, <vscale x 1 x double> %296, <vscale x 1 x double> %297, i64 2)
  %299 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP540640817, align 8
  %300 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tl, align 8
  %301 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %298, <vscale x 1 x double> %299, <vscale x 1 x double> %300, i64 2)
  %302 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP909631995, align 8
  %303 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ti, align 8
  %304 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %301, <vscale x 1 x double> %302, <vscale x 1 x double> %303, i64 2)
  %call80 = call <vscale x 1 x double> @VBYI(<vscale x 1 x double> %304)
  store <vscale x 1 x double> %call80, <vscale x 1 x double>* %Tp, align 8
  %305 = load <vscale x 1 x double>, <vscale x 1 x double>* %T1, align 8
  %306 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP654860733, align 8
  %307 = load <vscale x 1 x double>, <vscale x 1 x double>* %T7, align 8
  %308 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %305, <vscale x 1 x double> %306, <vscale x 1 x double> %307, i64 2)
  %309 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP959492973, align 8
  %310 = load <vscale x 1 x double>, <vscale x 1 x double>* %Ta, align 8
  %311 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %308, <vscale x 1 x double> %309, <vscale x 1 x double> %310, i64 2)
  %312 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP142314838, align 8
  %313 = load <vscale x 1 x double>, <vscale x 1 x double>* %Td, align 8
  %314 = call <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double> %311, <vscale x 1 x double> %312, <vscale x 1 x double> %313, i64 2)
  %315 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP841253532, align 8
  %316 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tg, align 8
  %317 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %314, <vscale x 1 x double> %315, <vscale x 1 x double> %316, i64 2)
  %318 = load <vscale x 1 x double>, <vscale x 1 x double>* %KP415415013, align 8
  %319 = load <vscale x 1 x double>, <vscale x 1 x double>* %T4, align 8
  %320 = call <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double> %317, <vscale x 1 x double> %318, <vscale x 1 x double> %319, i64 2)
  store <vscale x 1 x double> %320, <vscale x 1 x double>* %To, align 8
  %321 = load double*, double** %xo, align 8
  %322 = load i64, i64* %os.addr, align 8
  %mul81 = mul nsw i64 %322, 9
  %arrayidx82 = getelementptr inbounds double, double* %321, i64 %mul81
  %323 = load <vscale x 1 x double>, <vscale x 1 x double>* %To, align 8
  %324 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tp, align 8
  %325 = call <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double> %323, <vscale x 1 x double> %324, i64 2)
  %326 = load i64, i64* %ovs.addr, align 8
  %327 = load double*, double** %xo, align 8
  %328 = load i64, i64* %os.addr, align 8
  %mul83 = mul nsw i64 %328, 1
  %arrayidx84 = getelementptr inbounds double, double* %327, i64 %mul83
  call void @STu(double* %arrayidx82, <vscale x 1 x double> %325, i64 %326, double* %arrayidx84)
  %329 = load double*, double** %xo, align 8
  %330 = load i64, i64* %os.addr, align 8
  %mul85 = mul nsw i64 %330, 2
  %arrayidx86 = getelementptr inbounds double, double* %329, i64 %mul85
  %331 = load <vscale x 1 x double>, <vscale x 1 x double>* %To, align 8
  %332 = load <vscale x 1 x double>, <vscale x 1 x double>* %Tp, align 8
  %333 = call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %331, <vscale x 1 x double> %332, i64 2)
  %334 = load i64, i64* %ovs.addr, align 8
  %335 = load double*, double** %xo, align 8
  %arrayidx87 = getelementptr inbounds double, double* %335, i64 0
  call void @STu(double* %arrayidx86, <vscale x 1 x double> %333, i64 %334, double* %arrayidx87)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %336 = load i64, i64* %i, align 8
  %sub = sub nsw i64 %336, 1
  store i64 %sub, i64* %i, align 8
  %337 = load double*, double** %xi, align 8
  %338 = load i64, i64* %ivs.addr, align 8
  %mul88 = mul nsw i64 1, %338
  %add.ptr = getelementptr inbounds double, double* %337, i64 %mul88
  store double* %add.ptr, double** %xi, align 8
  %339 = load double*, double** %xo, align 8
  %340 = load i64, i64* %ovs.addr, align 8
  %mul89 = mul nsw i64 1, %340
  %add.ptr90 = getelementptr inbounds double, double* %339, i64 %mul89
  store double* %add.ptr90, double** %xo, align 8
  %341 = load i64, i64* %is.addr, align 8
  %342 = load i64, i64* @fftw_an_INT_guaranteed_to_be_zero, align 8
  %xor = xor i64 %341, %342
  store i64 %xor, i64* %is.addr, align 8
  %343 = load i64, i64* %os.addr, align 8
  %344 = load i64, i64* @fftw_an_INT_guaranteed_to_be_zero, align 8
  %xor91 = xor i64 %343, %344
  store i64 %xor91, i64* %os.addr, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfmv.v.f.nxv1f64.f64(double, i64) #3

; ; Function Attrs: noinline nounwind optnone
declare <vscale x 1 x double> @LDu(double* %x, i64 %ivs, double* %aligned_like) #2

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfsub.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i64) #3

; Function Attrs: noinline nounwind optnone
declare void @STu(double* %x, <vscale x 1 x double> %v, i64 %ovs, double* %aligned_like) #2 

; Function Attrs: noinline nounwind optnone
declare <vscale x 1 x double> @VBYI(<vscale x 1 x double> %x) #2

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfmul.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfnmsac.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, <vscale x 1 x double>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfmacc.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, <vscale x 1 x double>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vid.nxv1i64(i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vmv.v.x.nxv1i64.i64(i64, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vsrl.nxv1i64.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vmul.nxv1i64.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vand.nxv1i64.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vadd.nxv1i64.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, i64) #3

; Function Attrs: nounwind readonly
declare <vscale x 1 x double> @llvm.epi.vload.indexed.nxv1f64.nxv1i64(<vscale x 1 x double>* nocapture, <vscale x 1 x i64>, i64) #4

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vrgather.nxv1f64.nxv1i64(<vscale x 1 x double>, <vscale x 1 x i64>, i64) #3

; Function Attrs: nounwind writeonly
declare void @llvm.epi.vstore.indexed.nxv1f64.nxv1i64(<vscale x 1 x double>, <vscale x 1 x double>* nocapture, <vscale x 1 x i64>, i64) #5

; Function Attrs: nounwind readnone
declare <vscale x 1 x double> @llvm.epi.vfsgnjn.mask.nxv1f64.nxv1f64.nxv1i1(<vscale x 1 x double>, <vscale x 1 x double>, <vscale x 1 x double>, <vscale x 1 x i1>, i64) #3

; Function Attrs: nounwind readnone
declare <vscale x 1 x i64> @llvm.epi.vxor.nxv1i64.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, i64) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+a,+c,+d,+experimental-v,+experimental-zvlsseg,+f,+m,-relax,-save-restore" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+a,+c,+d,+experimental-v,+experimental-zvlsseg,+f,+m,-relax,-save-restore" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="64" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+a,+c,+d,+experimental-v,+experimental-zvlsseg,+f,+m,-relax,-save-restore" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone }
attributes #4 = { nounwind readonly }
attributes #5 = { nounwind writeonly }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 1, !"target-abi", !"lp64d"}
!2 = !{i32 1, !"SmallDataLimit", i32 8}
!3 = !{!"clang version 12.0.0"}
