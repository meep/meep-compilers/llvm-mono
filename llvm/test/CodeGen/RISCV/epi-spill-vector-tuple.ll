; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O0 \
; RUN:    < %s | FileCheck --check-prefix=SPILL-O0 %s
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O2 \
; RUN:    < %s | FileCheck --check-prefix=SPILL-O2 %s

define dso_local void @bar(double* nocapture %pa, double* nocapture readonly %pb, i64 %gvl) nounwind {
; SPILL-O0-LABEL: bar:
; SPILL-O0:       # %bb.0: # %entry
; SPILL-O0-NEXT:    addi sp, sp, -80
; SPILL-O0-NEXT:    sd ra, 72(sp) # 8-byte Folded Spill
; SPILL-O0-NEXT:    sd s0, 64(sp) # 8-byte Folded Spill
; SPILL-O0-NEXT:    addi s0, sp, 80
; SPILL-O0-NEXT:    rdvtype ra
; SPILL-O0-NEXT:    rdvl t0
; SPILL-O0-NEXT:    vsetvli a3, zero, e8,m1
; SPILL-O0-NEXT:    vsetvl zero, t0, ra
; SPILL-O0-NEXT:    sub sp, sp, a3
; SPILL-O0-NEXT:    sd sp, -72(s0)
; SPILL-O0-NEXT:    sub sp, sp, a3
; SPILL-O0-NEXT:    sd sp, -80(s0)
; SPILL-O0-NEXT:    sd a2, -64(s0) # 8-byte Folded Spill
; SPILL-O0-NEXT:    sd a0, -56(s0) # 8-byte Folded Spill
; SPILL-O0-NEXT:    # implicit-def: $v3
; SPILL-O0-NEXT:    vsetvli zero, a2, e64,m1
; SPILL-O0-NEXT:    vle.v v3, (a0)
; SPILL-O0-NEXT:    # implicit-def: $v4
; SPILL-O0-NEXT:    vle.v v4, (a1)
; SPILL-O0-NEXT:    # implicit-def: $v1_v2
; SPILL-O0-NEXT:    vzip2.vv v1, v3, v4
; SPILL-O0-NEXT:    rdvtype ra
; SPILL-O0-NEXT:    rdvl t0
; SPILL-O0-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O0-NEXT:    vmv.v.v v3, v2
; SPILL-O0-NEXT:    vsetvl zero, t0, ra
; SPILL-O0-NEXT:    ld a2, -72(s0)
; SPILL-O0-NEXT:    rdvtype a1
; SPILL-O0-NEXT:    rdvl a0
; SPILL-O0-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O0-NEXT:    vse.v v3, (a2)
; SPILL-O0-NEXT:    vsetvl zero, a0, a1
; SPILL-O0-NEXT:    # kill: def $v1 killed $v1 killed $v1_v2
; SPILL-O0-NEXT:    ld a2, -80(s0)
; SPILL-O0-NEXT:    rdvtype a1
; SPILL-O0-NEXT:    rdvl a0
; SPILL-O0-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O0-NEXT:    vse.v v1, (a2)
; SPILL-O0-NEXT:    vsetvl zero, a0, a1
; SPILL-O0-NEXT:    call foo
; SPILL-O0-NEXT:    ld a2, -64(s0) # 8-byte Folded Reload
; SPILL-O0-NEXT:    ld a3, -80(s0)
; SPILL-O0-NEXT:    rdvtype a1
; SPILL-O0-NEXT:    rdvl a0
; SPILL-O0-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O0-NEXT:    # implicit-def: $v2
; SPILL-O0-NEXT:    vle.v v2, (a3)
; SPILL-O0-NEXT:    vsetvl zero, a0, a1
; SPILL-O0-NEXT:    ld a0, -56(s0) # 8-byte Folded Reload
; SPILL-O0-NEXT:    ld a4, -72(s0)
; SPILL-O0-NEXT:    rdvtype a3
; SPILL-O0-NEXT:    rdvl a1
; SPILL-O0-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O0-NEXT:    # implicit-def: $v1
; SPILL-O0-NEXT:    vle.v v1, (a4)
; SPILL-O0-NEXT:    vsetvl zero, a1, a3
; SPILL-O0-NEXT:    vsetvli zero, a2, e64,m1
; SPILL-O0-NEXT:    vse.v v2, (a0)
; SPILL-O0-NEXT:    vse.v v1, (a0)
; SPILL-O0-NEXT:    addi sp, s0, -80
; SPILL-O0-NEXT:    ld s0, 64(sp) # 8-byte Folded Reload
; SPILL-O0-NEXT:    ld ra, 72(sp) # 8-byte Folded Reload
; SPILL-O0-NEXT:    addi sp, sp, 80
; SPILL-O0-NEXT:    ret
;
; SPILL-O2-LABEL: bar:
; SPILL-O2:       # %bb.0: # %entry
; SPILL-O2-NEXT:    addi sp, sp, -80
; SPILL-O2-NEXT:    sd ra, 72(sp) # 8-byte Folded Spill
; SPILL-O2-NEXT:    sd s0, 64(sp) # 8-byte Folded Spill
; SPILL-O2-NEXT:    sd s1, 56(sp) # 8-byte Folded Spill
; SPILL-O2-NEXT:    sd s2, 48(sp) # 8-byte Folded Spill
; SPILL-O2-NEXT:    addi s0, sp, 80
; SPILL-O2-NEXT:    rdvtype ra
; SPILL-O2-NEXT:    rdvl t0
; SPILL-O2-NEXT:    vsetvli a3, zero, e8,m1
; SPILL-O2-NEXT:    vsetvl zero, t0, ra
; SPILL-O2-NEXT:    slli a3, a3, 1
; SPILL-O2-NEXT:    sub sp, sp, a3
; SPILL-O2-NEXT:    sd sp, -72(s0)
; SPILL-O2-NEXT:    mv s2, a2
; SPILL-O2-NEXT:    mv s1, a0
; SPILL-O2-NEXT:    vsetvli zero, a2, e64,m1
; SPILL-O2-NEXT:    vle.v v1, (a0)
; SPILL-O2-NEXT:    vle.v v2, (a1)
; SPILL-O2-NEXT:    vzip2.vv v1, v1, v2
; SPILL-O2-NEXT:    ld a3, -72(s0)
; SPILL-O2-NEXT:    rdvtype ra
; SPILL-O2-NEXT:    rdvl t0
; SPILL-O2-NEXT:    vsetvli a0, zero, e8,m1
; SPILL-O2-NEXT:    vsetvl zero, t0, ra
; SPILL-O2-NEXT:    add a2, a3, a0
; SPILL-O2-NEXT:    rdvtype a1
; SPILL-O2-NEXT:    rdvl a0
; SPILL-O2-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O2-NEXT:    vse.v v1, (a3)
; SPILL-O2-NEXT:    vsetvl zero, a0, a1
; SPILL-O2-NEXT:    rdvtype a1
; SPILL-O2-NEXT:    rdvl a0
; SPILL-O2-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O2-NEXT:    vse.v v2, (a2)
; SPILL-O2-NEXT:    vsetvl zero, a0, a1
; SPILL-O2-NEXT:    call foo
; SPILL-O2-NEXT:    vsetvli zero, s2, e64,m1
; SPILL-O2-NEXT:    ld a3, -72(s0)
; SPILL-O2-NEXT:    rdvtype ra
; SPILL-O2-NEXT:    rdvl t0
; SPILL-O2-NEXT:    vsetvli a0, zero, e8,m1
; SPILL-O2-NEXT:    vsetvl zero, t0, ra
; SPILL-O2-NEXT:    add a2, a3, a0
; SPILL-O2-NEXT:    rdvtype a1
; SPILL-O2-NEXT:    rdvl a0
; SPILL-O2-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O2-NEXT:    # implicit-def: $v1
; SPILL-O2-NEXT:    vle.v v1, (a3)
; SPILL-O2-NEXT:    vsetvl zero, a0, a1
; SPILL-O2-NEXT:    rdvtype a1
; SPILL-O2-NEXT:    rdvl a0
; SPILL-O2-NEXT:    vsetvli zero, zero, e64,m1
; SPILL-O2-NEXT:    # implicit-def: $v2
; SPILL-O2-NEXT:    vle.v v2, (a2)
; SPILL-O2-NEXT:    vsetvl zero, a0, a1
; SPILL-O2-NEXT:    vse.v v1, (s1)
; SPILL-O2-NEXT:    vse.v v2, (s1)
; SPILL-O2-NEXT:    addi sp, s0, -80
; SPILL-O2-NEXT:    ld s2, 48(sp) # 8-byte Folded Reload
; SPILL-O2-NEXT:    ld s1, 56(sp) # 8-byte Folded Reload
; SPILL-O2-NEXT:    ld s0, 64(sp) # 8-byte Folded Reload
; SPILL-O2-NEXT:    ld ra, 72(sp) # 8-byte Folded Reload
; SPILL-O2-NEXT:    addi sp, sp, 80
; SPILL-O2-NEXT:    ret
entry:
  %0 = bitcast double* %pa to <vscale x 1 x double>*
  %1 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %0, i64 %gvl)
  %2 = bitcast double* %pb to <vscale x 1 x double>*
  %3 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %2, i64 %gvl)
  %4 = tail call { <vscale x 1 x double>, <vscale x 1 x double> } @llvm.epi.vzip2.nxv1f64(<vscale x 1 x double> %1, <vscale x 1 x double> %3, i64 %gvl)
  %5 = extractvalue { <vscale x 1 x double>, <vscale x 1 x double> } %4, 0
  %6 = extractvalue { <vscale x 1 x double>, <vscale x 1 x double> } %4, 1
  tail call void @foo() #5
  tail call void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double> %5, <vscale x 1 x double>* %0, i64 %gvl)
  tail call void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double> %6, <vscale x 1 x double>* %0, i64 %gvl)
  ret void
}

declare <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* nocapture, i64)

declare { <vscale x 1 x double>, <vscale x 1 x double> } @llvm.epi.vzip2.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i64)

declare dso_local void @foo()

declare void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>* nocapture, i64)

