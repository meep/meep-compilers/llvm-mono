; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+d,+f,+experimental-v -verify-machineinstrs < %s \
; RUN:   | FileCheck %s

define <vscale x 1 x double> @test_vp_sitofp_masks_nxv1f64(<vscale x 1 x i1> %src, <vscale x 1 x i1> %mask, i32 zeroext %evl) {
; CHECK-LABEL: test_vp_sitofp_masks_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmv.v.i v1, 0
; CHECK-NEXT:    vmerge.vim v1, v1, -1, v0
; CHECK-NEXT:    vfcvt.f.x.v v16, v1
; CHECK-NEXT:    ret
  %dst = call <vscale x 1 x double> @llvm.vp.sitofp.nxv1f64.nxv1i1(<vscale x 1 x i1> %src,
                                                                    metadata !"round.tonearest", metadata !"fpexcept.ignore",
                                                                    <vscale x 1 x i1> %mask, i32 %evl)
  ret <vscale x 1 x double> %dst
}

declare <vscale x 1 x double> @llvm.vp.sitofp.nxv1f64.nxv1i1(<vscale x 1 x i1>, metadata, metadata, <vscale x 1 x i1>, i32)

define <vscale x 2 x float> @test_vp_sitofp_masks_nxv2f32(<vscale x 2 x i1> %src, <vscale x 2 x i1> %mask, i32 zeroext %evl) {
; CHECK-LABEL: test_vp_sitofp_masks_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmv.v.i v1, 0
; CHECK-NEXT:    vmerge.vim v1, v1, -1, v0
; CHECK-NEXT:    vfcvt.f.x.v v16, v1
; CHECK-NEXT:    ret
  %dst = call <vscale x 2 x float> @llvm.vp.sitofp.nxv2f32.nxv2i1(<vscale x 2 x i1> %src,
                                                                  metadata !"round.tonearest", metadata !"fpexcept.ignore",
                                                                  <vscale x 2 x i1> %mask, i32 %evl)
  ret <vscale x 2 x float> %dst
}

declare <vscale x 2 x float> @llvm.vp.sitofp.nxv2f32.nxv2i1(<vscale x 2 x i1>, metadata, metadata, <vscale x 2 x i1>, i32)

define <vscale x 1 x double> @test_vp_uitofp_masks_nxv1f64(<vscale x 1 x i1> %src, <vscale x 1 x i1> %mask, i32 zeroext %evl) {
; CHECK-LABEL: test_vp_uitofp_masks_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmv.v.i v1, 0
; CHECK-NEXT:    vmerge.vim v1, v1, 1, v0
; CHECK-NEXT:    vfcvt.f.xu.v v16, v1
; CHECK-NEXT:    ret
  %dst = call <vscale x 1 x double> @llvm.vp.uitofp.nxv1f64.nxv1i1(<vscale x 1 x i1> %src,
                                                                    metadata !"round.tonearest", metadata !"fpexcept.ignore",
                                                                    <vscale x 1 x i1> %mask, i32 %evl)
  ret <vscale x 1 x double> %dst
}

declare <vscale x 1 x double> @llvm.vp.uitofp.nxv1f64.nxv1i1(<vscale x 1 x i1>, metadata, metadata, <vscale x 1 x i1>, i32)

define <vscale x 2 x float> @test_vp_uitofp_masks_nxv2f32(<vscale x 2 x i1> %src, <vscale x 2 x i1> %mask, i32 zeroext %evl) {
; CHECK-LABEL: test_vp_uitofp_masks_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmv.v.i v1, 0
; CHECK-NEXT:    vmerge.vim v1, v1, 1, v0
; CHECK-NEXT:    vfcvt.f.xu.v v16, v1
; CHECK-NEXT:    ret
  %dst = call <vscale x 2 x float> @llvm.vp.uitofp.nxv2f32.nxv2i1(<vscale x 2 x i1> %src,
                                                                  metadata !"round.tonearest", metadata !"fpexcept.ignore",
                                                                  <vscale x 2 x i1> %mask, i32 %evl)
  ret <vscale x 2 x float> %dst
}

declare <vscale x 2 x float> @llvm.vp.uitofp.nxv2f32.nxv2i1(<vscale x 2 x i1>, metadata, metadata, <vscale x 2 x i1>, i32)

define <vscale x 1 x i1> @test_vp_fptosi_masks_nxv1f64(<vscale x 1 x double> %src, <vscale x 1 x i1> %mask, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptosi_masks_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.d.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0, v0.t
; CHECK-NEXT:    ret
  %dst = call <vscale x 1 x i1> @llvm.vp.fptosi.nxv1i1.nxv1f64(<vscale x 1 x double> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 1 x i1> %mask, i32 %evl)
  ret <vscale x 1 x i1> %dst
}

define <vscale x 1 x i1> @test_vp_fptosi_masks_nxv1f64_unmasked(<vscale x 1 x double> %src, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptosi_masks_nxv1f64_unmasked:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.d.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i1> undef, i1 1, i32 0
  %allones = shufflevector <vscale x 1 x i1> %head, <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer

  %dst = call <vscale x 1 x i1> @llvm.vp.fptosi.nxv1i1.nxv1f64(<vscale x 1 x double> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 1 x i1> %allones, i32 %evl)
  ret <vscale x 1 x i1> %dst
}

declare <vscale x 1 x i1> @llvm.vp.fptosi.nxv1i1.nxv1f64(<vscale x 1 x double>, metadata, <vscale x 1 x i1>, i32)

define <vscale x 2 x i1> @test_vp_fptosi_masks_nxv2f32(<vscale x 2 x float> %src, <vscale x 2 x i1> %mask, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptosi_masks_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.w.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0, v0.t
; CHECK-NEXT:    ret
  %dst = call <vscale x 2 x i1> @llvm.vp.fptosi.nxv2i1.nxv2f32(<vscale x 2 x float> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 2 x i1> %mask, i32 %evl)
  ret <vscale x 2 x i1> %dst
}

define <vscale x 2 x i1> @test_vp_fptosi_masks_nxv2f32_unmasked(<vscale x 2 x float> %src, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptosi_masks_nxv2f32_unmasked:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.w.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i1> undef, i1 1, i32 0
  %allones = shufflevector <vscale x 2 x i1> %head, <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer

  %dst = call <vscale x 2 x i1> @llvm.vp.fptosi.nxv2i1.nxv2f32(<vscale x 2 x float> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 2 x i1> %allones, i32 %evl)
  ret <vscale x 2 x i1> %dst
}

declare <vscale x 2 x i1> @llvm.vp.fptosi.nxv2i1.nxv2f32(<vscale x 2 x float>, metadata, <vscale x 2 x i1>, i32)

define <vscale x 1 x i1> @test_vp_fptoui_masks_nxv1f64(<vscale x 1 x double> %src, <vscale x 1 x i1> %mask, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptoui_masks_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.d.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0, v0.t
; CHECK-NEXT:    ret
  %dst = call <vscale x 1 x i1> @llvm.vp.fptoui.nxv1i1.nxv1f64(<vscale x 1 x double> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 1 x i1> %mask, i32 %evl)
  ret <vscale x 1 x i1> %dst
}

define <vscale x 1 x i1> @test_vp_fptoui_masks_nxv1f64_unmasked(<vscale x 1 x double> %src, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptoui_masks_nxv1f64_unmasked:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.d.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e64,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i1> undef, i1 1, i32 0
  %allones = shufflevector <vscale x 1 x i1> %head, <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer

  %dst = call <vscale x 1 x i1> @llvm.vp.fptoui.nxv1i1.nxv1f64(<vscale x 1 x double> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 1 x i1> %allones, i32 %evl)
  ret <vscale x 1 x i1> %dst
}

declare <vscale x 1 x i1> @llvm.vp.fptoui.nxv1i1.nxv1f64(<vscale x 1 x double>, metadata, <vscale x 1 x i1>, i32)

define <vscale x 2 x i1> @test_vp_fptoui_masks_nxv2f32(<vscale x 2 x float> %src, <vscale x 2 x i1> %mask, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptoui_masks_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.w.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0, v0.t
; CHECK-NEXT:    ret
  %dst = call <vscale x 2 x i1> @llvm.vp.fptoui.nxv2i1.nxv2f32(<vscale x 2 x float> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 2 x i1> %mask, i32 %evl)
  ret <vscale x 2 x i1> %dst
}

define <vscale x 2 x i1> @test_vp_fptoui_masks_nxv2f32_unmasked(<vscale x 2 x float> %src, i32 zeroext %evl){
; CHECK-LABEL: test_vp_fptoui_masks_nxv2f32_unmasked:
; CHECK:       # %bb.0:
; CHECK-NEXT:    fmv.w.x ft0, zero
; CHECK-NEXT:    vsetvli zero, a0, e32,m1
; CHECK-NEXT:    vmfne.vf v0, v16, ft0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i1> undef, i1 1, i32 0
  %allones = shufflevector <vscale x 2 x i1> %head, <vscale x 2 x i1> undef, <vscale x 2 x i32> zeroinitializer

  %dst = call <vscale x 2 x i1> @llvm.vp.fptoui.nxv2i1.nxv2f32(<vscale x 2 x float> %src,
                                                                metadata !"fpexcept.ignore",
                                                                <vscale x 2 x i1> %allones, i32 %evl)
  ret <vscale x 2 x i1> %dst
}

declare <vscale x 2 x i1> @llvm.vp.fptoui.nxv2i1.nxv2f32(<vscale x 2 x float>, metadata, <vscale x 2 x i1>, i32)
