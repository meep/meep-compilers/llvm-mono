; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+m,+f,+d,+a,+c,+experimental-v \
; RUN:    -verify-machineinstrs < %s | FileCheck %s

define i1 @mask_reduce_or_1(<vscale x 1 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv1i1(<vscale x 1 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv1i1(<vscale x 1 x i1> %m)

define i1 @mask_reduce_and_1(<vscale x 1 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv1i1(<vscale x 1 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv1i1(<vscale x 1 x i1> %m)

define i1 @mask_reduce_xor_1(<vscale x 1 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv1i1(<vscale x 1 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv1i1(<vscale x 1 x i1> %m)

define i1 @mask_reduce_or_2(<vscale x 2 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv2i1(<vscale x 2 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv2i1(<vscale x 2 x i1> %m)

define i1 @mask_reduce_and_2(<vscale x 2 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv2i1(<vscale x 2 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv2i1(<vscale x 2 x i1> %m)

define i1 @mask_reduce_xor_2(<vscale x 2 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv2i1(<vscale x 2 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv2i1(<vscale x 2 x i1> %m)

define i1 @mask_reduce_or_4(<vscale x 4 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv4i1(<vscale x 4 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv4i1(<vscale x 4 x i1> %m)

define i1 @mask_reduce_and_4(<vscale x 4 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv4i1(<vscale x 4 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv4i1(<vscale x 4 x i1> %m)

define i1 @mask_reduce_xor_4(<vscale x 4 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv4i1(<vscale x 4 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv4i1(<vscale x 4 x i1> %m)

define i1 @mask_reduce_or_8(<vscale x 8 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv8i1(<vscale x 8 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv8i1(<vscale x 8 x i1> %m)

define i1 @mask_reduce_and_8(<vscale x 8 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv8i1(<vscale x 8 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv8i1(<vscale x 8 x i1> %m)

define i1 @mask_reduce_xor_8(<vscale x 8 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv8i1(<vscale x 8 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv8i1(<vscale x 8 x i1> %m)

define i1 @mask_reduce_or_16(<vscale x 16 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv16i1(<vscale x 16 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv16i1(<vscale x 16 x i1> %m)

define i1 @mask_reduce_and_16(<vscale x 16 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv16i1(<vscale x 16 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv16i1(<vscale x 16 x i1> %m)

define i1 @mask_reduce_xor_16(<vscale x 16 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv16i1(<vscale x 16 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv16i1(<vscale x 16 x i1> %m)

define i1 @mask_reduce_or_32(<vscale x 32 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv32i1(<vscale x 32 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv32i1(<vscale x 32 x i1> %m)

define i1 @mask_reduce_and_32(<vscale x 32 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv32i1(<vscale x 32 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv32i1(<vscale x 32 x i1> %m)

define i1 @mask_reduce_xor_32(<vscale x 32 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv32i1(<vscale x 32 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv32i1(<vscale x 32 x i1> %m)

define i1 @mask_reduce_or_64(<vscale x 64 x i1> %m) {
; CHECK-LABEL: mask_reduce_or_64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m8
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    snez a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.or.nxv64i1(<vscale x 64 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.or.nxv64i1(<vscale x 64 x i1> %m)

define i1 @mask_reduce_and_64(<vscale x 64 x i1> %m) {
; CHECK-LABEL: mask_reduce_and_64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m8
; CHECK-NEXT:    vmnot.m v1, v0
; CHECK-NEXT:    vmpopc.m a0, v1
; CHECK-NEXT:    seqz a0, a0
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.and.nxv64i1(<vscale x 64 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.and.nxv64i1(<vscale x 64 x i1> %m)

define i1 @mask_reduce_xor_64(<vscale x 64 x i1> %m) {
; CHECK-LABEL: mask_reduce_xor_64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m8
; CHECK-NEXT:    vmpopc.m a0, v0
; CHECK-NEXT:    andi a0, a0, 1
; CHECK-NEXT:    ret
  %r = call i1 @llvm.vector.reduce.xor.nxv64i1(<vscale x 64 x i1> %m)
  ret i1 %r
}

declare i1 @llvm.vector.reduce.xor.nxv64i1(<vscale x 64 x i1> %m)
