; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+d,+experimental-v -verify-machineinstrs \
; RUN:    < %s | FileCheck %s

define void @epi_propagate_AVL(i64 %N, double* %c, double* %a, double* %b) {
; CHECK-LABEL: epi_propagate_AVL:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    addi a4, zero, 64
; CHECK-NEXT:    addi a5, zero, 1
; CHECK-NEXT:    vsetvli a6, a4, e64,m1
; CHECK-NEXT:    blt a0, a5, .LBB0_3
; CHECK-NEXT:  # %bb.1: # %for.body.preheader
; CHECK-NEXT:    mv a5, zero
; CHECK-NEXT:    mv t0, zero
; CHECK-NEXT:    slli a7, a6, 3
; CHECK-NEXT:  .LBB0_2: # %for.body
; CHECK-NEXT:    # =>This Inner Loop Header: Depth=1
; CHECK-NEXT:    add a4, a2, a5
; CHECK-NEXT:    vle.v v1, (a4)
; CHECK-NEXT:    add a4, a3, a5
; CHECK-NEXT:    vle.v v2, (a4)
; CHECK-NEXT:    vfadd.vv v1, v1, v2
; CHECK-NEXT:    add a4, a1, a5
; CHECK-NEXT:    vse.v v1, (a4)
; CHECK-NEXT:    add t0, t0, a6
; CHECK-NEXT:    add a5, a5, a7
; CHECK-NEXT:    blt t0, a0, .LBB0_2
; CHECK-NEXT:  .LBB0_3: # %for.end
; CHECK-NEXT:    ret
entry:
  %0 = tail call i64 @llvm.epi.vsetvl(i64 64, i64 3, i64 0)
  %cmp13 = icmp sgt i64 %N, 0
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %entry, %for.body
  %i.014 = phi i64 [ %add, %for.body ], [ 0, %entry ]
  %arrayidx = getelementptr inbounds double, double* %a, i64 %i.014
  %1 = bitcast double* %arrayidx to <vscale x 1 x double>*
  %2 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %1, i64 %0)
  %arrayidx1 = getelementptr inbounds double, double* %b, i64 %i.014
  %3 = bitcast double* %arrayidx1 to <vscale x 1 x double>*
  %4 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %3, i64 %0)
  %5 = tail call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %2, <vscale x 1 x double> %4, i64 %0)
  %arrayidx2 = getelementptr inbounds double, double* %c, i64 %i.014
  %6 = bitcast double* %arrayidx2 to <vscale x 1 x double>*
  tail call void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double> %5, <vscale x 1 x double>* %6, i64 %0)
  %add = add nsw i64 %i.014, %0
  %cmp = icmp slt i64 %add, %N
  br i1 %cmp, label %for.body, label %for.end

for.end:                                          ; preds = %for.body, %entry
  ret void
}

define void @epi_propagate_AVL_vlmax(i64 %N, double* %c, double* %a, double* %b) {
; CHECK-LABEL: epi_propagate_AVL_vlmax:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    addi a4, zero, 1
; CHECK-NEXT:    vsetvli a6, zero, e64,m1
; CHECK-NEXT:    blt a0, a4, .LBB1_3
; CHECK-NEXT:  # %bb.1: # %for.body.preheader
; CHECK-NEXT:    mv a5, zero
; CHECK-NEXT:    mv t0, zero
; CHECK-NEXT:    slli a7, a6, 3
; CHECK-NEXT:  .LBB1_2: # %for.body
; CHECK-NEXT:    # =>This Inner Loop Header: Depth=1
; CHECK-NEXT:    add a4, a2, a5
; CHECK-NEXT:    vle.v v1, (a4)
; CHECK-NEXT:    add a4, a3, a5
; CHECK-NEXT:    vle.v v2, (a4)
; CHECK-NEXT:    vfadd.vv v1, v1, v2
; CHECK-NEXT:    add a4, a1, a5
; CHECK-NEXT:    vse.v v1, (a4)
; CHECK-NEXT:    add t0, t0, a6
; CHECK-NEXT:    add a5, a5, a7
; CHECK-NEXT:    blt t0, a0, .LBB1_2
; CHECK-NEXT:  .LBB1_3: # %for.end
; CHECK-NEXT:    ret
entry:
  %0 = tail call i64 @llvm.epi.vsetvlmax(i64 3, i64 0)
  %cmp13 = icmp sgt i64 %N, 0
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %entry, %for.body
  %i.014 = phi i64 [ %add, %for.body ], [ 0, %entry ]
  %arrayidx = getelementptr inbounds double, double* %a, i64 %i.014
  %1 = bitcast double* %arrayidx to <vscale x 1 x double>*
  %2 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %1, i64 %0)
  %arrayidx1 = getelementptr inbounds double, double* %b, i64 %i.014
  %3 = bitcast double* %arrayidx1 to <vscale x 1 x double>*
  %4 = tail call <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* %3, i64 %0)
  %5 = tail call <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double> %2, <vscale x 1 x double> %4, i64 %0)
  %arrayidx2 = getelementptr inbounds double, double* %c, i64 %i.014
  %6 = bitcast double* %arrayidx2 to <vscale x 1 x double>*
  tail call void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double> %5, <vscale x 1 x double>* %6, i64 %0)
  %add = add nsw i64 %i.014, %0
  %cmp = icmp slt i64 %add, %N
  br i1 %cmp, label %for.body, label %for.end

for.end:                                          ; preds = %for.body, %entry
  ret void
}

declare i64 @llvm.epi.vsetvl(i64, i64, i64)
declare i64 @llvm.epi.vsetvlmax(i64, i64)

declare <vscale x 1 x double> @llvm.epi.vload.nxv1f64(<vscale x 1 x double>* nocapture, i64)
declare <vscale x 1 x double> @llvm.epi.vfadd.nxv1f64.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i64)
declare void @llvm.epi.vstore.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>* nocapture, i64)
