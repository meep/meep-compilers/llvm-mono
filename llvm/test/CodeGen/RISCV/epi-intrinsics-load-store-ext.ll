; RUN: llc -mtriple=riscv64 -mattr=+m,+f,+d,+a,+c,+experimental-v \
; RUN:    -verify-machineinstrs --riscv-no-aliases < %s | FileCheck %s

@scratch = global i8 0, align 16


declare <vscale x 8 x i8> @llvm.epi.vload.ext.nxv8i8(
  <vscale x 8 x i8>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.nxv8i8(
    <vscale x 8 x i8>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}

declare <vscale x 8 x i8> @llvm.epi.vload.ext.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}


declare <vscale x 16 x i8> @llvm.epi.vload.ext.nxv16i8(
  <vscale x 16 x i8>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.nxv16i8(
    <vscale x 16 x i8>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}

declare <vscale x 16 x i8> @llvm.epi.vload.ext.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}


declare <vscale x 32 x i8> @llvm.epi.vload.ext.nxv32i8(
  <vscale x 32 x i8>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.nxv32i8(
    <vscale x 32 x i8>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}

declare <vscale x 32 x i8> @llvm.epi.vload.ext.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}


declare <vscale x 4 x i16> @llvm.epi.vload.ext.nxv4i16(
  <vscale x 4 x i16>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.nxv4i16(
    <vscale x 4 x i16>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}

declare <vscale x 4 x i16> @llvm.epi.vload.ext.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}


declare <vscale x 8 x i16> @llvm.epi.vload.ext.nxv8i16(
  <vscale x 8 x i16>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.nxv8i16(
    <vscale x 8 x i16>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}

declare <vscale x 8 x i16> @llvm.epi.vload.ext.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}


declare <vscale x 16 x i16> @llvm.epi.vload.ext.nxv16i16(
  <vscale x 16 x i16>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.nxv16i16(
    <vscale x 16 x i16>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}

declare <vscale x 16 x i16> @llvm.epi.vload.ext.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}


declare <vscale x 32 x i16> @llvm.epi.vload.ext.nxv32i16(
  <vscale x 32 x i16>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.nxv32i16(
    <vscale x 32 x i16>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}

declare <vscale x 32 x i16> @llvm.epi.vload.ext.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}


declare <vscale x 2 x i32> @llvm.epi.vload.ext.nxv2i32(
  <vscale x 2 x i32>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.nxv2i32(
    <vscale x 2 x i32>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}

declare <vscale x 2 x i32> @llvm.epi.vload.ext.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}


declare <vscale x 4 x i32> @llvm.epi.vload.ext.nxv4i32(
  <vscale x 4 x i32>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.nxv4i32(
    <vscale x 4 x i32>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}

declare <vscale x 4 x i32> @llvm.epi.vload.ext.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}


declare <vscale x 8 x i32> @llvm.epi.vload.ext.nxv8i32(
  <vscale x 8 x i32>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.nxv8i32(
    <vscale x 8 x i32>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}

declare <vscale x 8 x i32> @llvm.epi.vload.ext.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}


declare <vscale x 16 x i32> @llvm.epi.vload.ext.nxv16i32(
  <vscale x 16 x i32>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.nxv16i32(
    <vscale x 16 x i32>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}

declare <vscale x 16 x i32> @llvm.epi.vload.ext.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}


declare <vscale x 1 x i64> @llvm.epi.vload.ext.nxv1i64(
  <vscale x 1 x i64>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.nxv1i64(
    <vscale x 1 x i64>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}

declare <vscale x 1 x i64> @llvm.epi.vload.ext.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}


declare <vscale x 2 x i64> @llvm.epi.vload.ext.nxv2i64(
  <vscale x 2 x i64>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.nxv2i64(
    <vscale x 2 x i64>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}

declare <vscale x 2 x i64> @llvm.epi.vload.ext.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}


declare <vscale x 4 x i64> @llvm.epi.vload.ext.nxv4i64(
  <vscale x 4 x i64>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.nxv4i64(
    <vscale x 4 x i64>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}

declare <vscale x 4 x i64> @llvm.epi.vload.ext.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}


declare <vscale x 8 x i64> @llvm.epi.vload.ext.nxv8i64(
  <vscale x 8 x i64>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.nxv8i64(
    <vscale x 8 x i64>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}

declare <vscale x 8 x i64> @llvm.epi.vload.ext.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}


declare <vscale x 4 x half> @llvm.epi.vload.ext.nxv4f16(
  <vscale x 4 x half>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.nxv4f16(
    <vscale x 4 x half>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}

declare <vscale x 4 x half> @llvm.epi.vload.ext.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}


declare <vscale x 8 x half> @llvm.epi.vload.ext.nxv8f16(
  <vscale x 8 x half>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.nxv8f16(
    <vscale x 8 x half>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}

declare <vscale x 8 x half> @llvm.epi.vload.ext.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}


declare <vscale x 16 x half> @llvm.epi.vload.ext.nxv16f16(
  <vscale x 16 x half>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.nxv16f16(
    <vscale x 16 x half>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}

declare <vscale x 16 x half> @llvm.epi.vload.ext.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}


declare <vscale x 32 x half> @llvm.epi.vload.ext.nxv32f16(
  <vscale x 32 x half>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.nxv32f16(
    <vscale x 32 x half>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}

declare <vscale x 32 x half> @llvm.epi.vload.ext.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}


declare <vscale x 2 x float> @llvm.epi.vload.ext.nxv2f32(
  <vscale x 2 x float>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.nxv2f32(
    <vscale x 2 x float>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}

declare <vscale x 2 x float> @llvm.epi.vload.ext.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}


declare <vscale x 4 x float> @llvm.epi.vload.ext.nxv4f32(
  <vscale x 4 x float>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.nxv4f32(
    <vscale x 4 x float>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}

declare <vscale x 4 x float> @llvm.epi.vload.ext.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}


declare <vscale x 8 x float> @llvm.epi.vload.ext.nxv8f32(
  <vscale x 8 x float>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.nxv8f32(
    <vscale x 8 x float>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}

declare <vscale x 8 x float> @llvm.epi.vload.ext.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}


declare <vscale x 16 x float> @llvm.epi.vload.ext.nxv16f32(
  <vscale x 16 x float>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.nxv16f32(
    <vscale x 16 x float>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}

declare <vscale x 16 x float> @llvm.epi.vload.ext.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}


declare <vscale x 1 x double> @llvm.epi.vload.ext.nxv1f64(
  <vscale x 1 x double>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.nxv1f64(
    <vscale x 1 x double>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}

declare <vscale x 1 x double> @llvm.epi.vload.ext.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}


declare <vscale x 2 x double> @llvm.epi.vload.ext.nxv2f64(
  <vscale x 2 x double>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.nxv2f64(
    <vscale x 2 x double>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}

declare <vscale x 2 x double> @llvm.epi.vload.ext.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}


declare <vscale x 4 x double> @llvm.epi.vload.ext.nxv4f64(
  <vscale x 4 x double>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.nxv4f64(
    <vscale x 4 x double>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}

declare <vscale x 4 x double> @llvm.epi.vload.ext.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}


declare <vscale x 8 x double> @llvm.epi.vload.ext.nxv8f64(
  <vscale x 8 x double>*,
  i64, i64);

define void @intrinsic_vload.ext_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0)
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.nxv8f64(
    <vscale x 8 x double>* undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}

declare <vscale x 8 x double> @llvm.epi.vload.ext.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vle.v {{v[0-9]+}}, (a0), v0.t
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64, i64);

define void @intrinsic_vstore.ext_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0)
  call void @llvm.epi.vstore.ext.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vse.v {{v[0-9]+}}, (a0), v0.t
  call void @llvm.epi.vstore.ext.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare <vscale x 8 x i8> @llvm.epi.vload.ext.strided.nxv8i8(
  <vscale x 8 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.strided.nxv8i8(
    <vscale x 8 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}

declare <vscale x 8 x i8> @llvm.epi.vload.ext.strided.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.strided.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}


declare <vscale x 16 x i8> @llvm.epi.vload.ext.strided.nxv16i8(
  <vscale x 16 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.strided.nxv16i8(
    <vscale x 16 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}

declare <vscale x 16 x i8> @llvm.epi.vload.ext.strided.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.strided.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}


declare <vscale x 32 x i8> @llvm.epi.vload.ext.strided.nxv32i8(
  <vscale x 32 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.strided.nxv32i8(
    <vscale x 32 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}

declare <vscale x 32 x i8> @llvm.epi.vload.ext.strided.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.strided.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}


declare <vscale x 4 x i16> @llvm.epi.vload.ext.strided.nxv4i16(
  <vscale x 4 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.strided.nxv4i16(
    <vscale x 4 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}

declare <vscale x 4 x i16> @llvm.epi.vload.ext.strided.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.strided.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}


declare <vscale x 8 x i16> @llvm.epi.vload.ext.strided.nxv8i16(
  <vscale x 8 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.strided.nxv8i16(
    <vscale x 8 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}

declare <vscale x 8 x i16> @llvm.epi.vload.ext.strided.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.strided.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}


declare <vscale x 16 x i16> @llvm.epi.vload.ext.strided.nxv16i16(
  <vscale x 16 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.strided.nxv16i16(
    <vscale x 16 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}

declare <vscale x 16 x i16> @llvm.epi.vload.ext.strided.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.strided.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}


declare <vscale x 32 x i16> @llvm.epi.vload.ext.strided.nxv32i16(
  <vscale x 32 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.strided.nxv32i16(
    <vscale x 32 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}

declare <vscale x 32 x i16> @llvm.epi.vload.ext.strided.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.strided.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}


declare <vscale x 2 x i32> @llvm.epi.vload.ext.strided.nxv2i32(
  <vscale x 2 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.strided.nxv2i32(
    <vscale x 2 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}

declare <vscale x 2 x i32> @llvm.epi.vload.ext.strided.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.strided.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}


declare <vscale x 4 x i32> @llvm.epi.vload.ext.strided.nxv4i32(
  <vscale x 4 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.strided.nxv4i32(
    <vscale x 4 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}

declare <vscale x 4 x i32> @llvm.epi.vload.ext.strided.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.strided.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}


declare <vscale x 8 x i32> @llvm.epi.vload.ext.strided.nxv8i32(
  <vscale x 8 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.strided.nxv8i32(
    <vscale x 8 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}

declare <vscale x 8 x i32> @llvm.epi.vload.ext.strided.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.strided.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}


declare <vscale x 16 x i32> @llvm.epi.vload.ext.strided.nxv16i32(
  <vscale x 16 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.strided.nxv16i32(
    <vscale x 16 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}

declare <vscale x 16 x i32> @llvm.epi.vload.ext.strided.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.strided.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}


declare <vscale x 1 x i64> @llvm.epi.vload.ext.strided.nxv1i64(
  <vscale x 1 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.strided.nxv1i64(
    <vscale x 1 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}

declare <vscale x 1 x i64> @llvm.epi.vload.ext.strided.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.strided.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}


declare <vscale x 2 x i64> @llvm.epi.vload.ext.strided.nxv2i64(
  <vscale x 2 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.strided.nxv2i64(
    <vscale x 2 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}

declare <vscale x 2 x i64> @llvm.epi.vload.ext.strided.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.strided.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}


declare <vscale x 4 x i64> @llvm.epi.vload.ext.strided.nxv4i64(
  <vscale x 4 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.strided.nxv4i64(
    <vscale x 4 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}

declare <vscale x 4 x i64> @llvm.epi.vload.ext.strided.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.strided.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}


declare <vscale x 8 x i64> @llvm.epi.vload.ext.strided.nxv8i64(
  <vscale x 8 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.strided.nxv8i64(
    <vscale x 8 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}

declare <vscale x 8 x i64> @llvm.epi.vload.ext.strided.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.strided.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}


declare <vscale x 4 x half> @llvm.epi.vload.ext.strided.nxv4f16(
  <vscale x 4 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.strided.nxv4f16(
    <vscale x 4 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}

declare <vscale x 4 x half> @llvm.epi.vload.ext.strided.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.strided.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}


declare <vscale x 8 x half> @llvm.epi.vload.ext.strided.nxv8f16(
  <vscale x 8 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.strided.nxv8f16(
    <vscale x 8 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}

declare <vscale x 8 x half> @llvm.epi.vload.ext.strided.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.strided.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}


declare <vscale x 16 x half> @llvm.epi.vload.ext.strided.nxv16f16(
  <vscale x 16 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.strided.nxv16f16(
    <vscale x 16 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}

declare <vscale x 16 x half> @llvm.epi.vload.ext.strided.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.strided.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}


declare <vscale x 32 x half> @llvm.epi.vload.ext.strided.nxv32f16(
  <vscale x 32 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.strided.nxv32f16(
    <vscale x 32 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}

declare <vscale x 32 x half> @llvm.epi.vload.ext.strided.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.strided.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}


declare <vscale x 2 x float> @llvm.epi.vload.ext.strided.nxv2f32(
  <vscale x 2 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.strided.nxv2f32(
    <vscale x 2 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}

declare <vscale x 2 x float> @llvm.epi.vload.ext.strided.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.strided.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}


declare <vscale x 4 x float> @llvm.epi.vload.ext.strided.nxv4f32(
  <vscale x 4 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.strided.nxv4f32(
    <vscale x 4 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}

declare <vscale x 4 x float> @llvm.epi.vload.ext.strided.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.strided.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}


declare <vscale x 8 x float> @llvm.epi.vload.ext.strided.nxv8f32(
  <vscale x 8 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.strided.nxv8f32(
    <vscale x 8 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}

declare <vscale x 8 x float> @llvm.epi.vload.ext.strided.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.strided.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}


declare <vscale x 16 x float> @llvm.epi.vload.ext.strided.nxv16f32(
  <vscale x 16 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.strided.nxv16f32(
    <vscale x 16 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}

declare <vscale x 16 x float> @llvm.epi.vload.ext.strided.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.strided.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}


declare <vscale x 1 x double> @llvm.epi.vload.ext.strided.nxv1f64(
  <vscale x 1 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.strided.nxv1f64(
    <vscale x 1 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}

declare <vscale x 1 x double> @llvm.epi.vload.ext.strided.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.strided.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}


declare <vscale x 2 x double> @llvm.epi.vload.ext.strided.nxv2f64(
  <vscale x 2 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.strided.nxv2f64(
    <vscale x 2 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}

declare <vscale x 2 x double> @llvm.epi.vload.ext.strided.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.strided.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}


declare <vscale x 4 x double> @llvm.epi.vload.ext.strided.nxv4f64(
  <vscale x 4 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.strided.nxv4f64(
    <vscale x 4 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}

declare <vscale x 4 x double> @llvm.epi.vload.ext.strided.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.strided.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}


declare <vscale x 8 x double> @llvm.epi.vload.ext.strided.nxv8f64(
  <vscale x 8 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vload.ext.strided_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.strided.nxv8f64(
    <vscale x 8 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}

declare <vscale x 8 x double> @llvm.epi.vload.ext.strided.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.strided_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.strided_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlse.v {{v[0-9]+}}, (a0), a0, v0.t
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.strided.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  i64,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  i64,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    i64 undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  i64,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    i64 undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  i64,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  i64,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  i64,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.strided.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64,
  i64, i64);

define void @intrinsic_vstore.ext.strided_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0
  call void @llvm.epi.vstore.ext.strided.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.strided.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  i64,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.strided_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.strided_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsse.v {{v[0-9]+}}, (a0), a0, v0.t
  call void @llvm.epi.vstore.ext.strided.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    i64 undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare <vscale x 8 x i8> @llvm.epi.vload.ext.indexed.nxv8i8(
  <vscale x 8 x i8>*,
  <vscale x 8 x i8>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.indexed.nxv8i8(
    <vscale x 8 x i8>* undef,
    <vscale x 8 x i8> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}

declare <vscale x 8 x i8> @llvm.epi.vload.ext.indexed.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  <vscale x 8 x i8>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x i8> @llvm.epi.vload.ext.indexed.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    <vscale x 8 x i8> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i8>*
  store <vscale x 8 x i8> %a, <vscale x 8 x i8>* %p

  ret void
}


declare <vscale x 16 x i8> @llvm.epi.vload.ext.indexed.nxv16i8(
  <vscale x 16 x i8>*,
  <vscale x 16 x i8>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.indexed.nxv16i8(
    <vscale x 16 x i8>* undef,
    <vscale x 16 x i8> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}

declare <vscale x 16 x i8> @llvm.epi.vload.ext.indexed.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  <vscale x 16 x i8>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 16 x i8> @llvm.epi.vload.ext.indexed.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    <vscale x 16 x i8> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i8>*
  store <vscale x 16 x i8> %a, <vscale x 16 x i8>* %p

  ret void
}


declare <vscale x 32 x i8> @llvm.epi.vload.ext.indexed.nxv32i8(
  <vscale x 32 x i8>*,
  <vscale x 32 x i8>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.indexed.nxv32i8(
    <vscale x 32 x i8>* undef,
    <vscale x 32 x i8> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}

declare <vscale x 32 x i8> @llvm.epi.vload.ext.indexed.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  <vscale x 32 x i8>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 32 x i8> @llvm.epi.vload.ext.indexed.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    <vscale x 32 x i8> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i8>*
  store <vscale x 32 x i8> %a, <vscale x 32 x i8>* %p

  ret void
}


declare <vscale x 4 x i16> @llvm.epi.vload.ext.indexed.nxv4i16(
  <vscale x 4 x i16>*,
  <vscale x 4 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.indexed.nxv4i16(
    <vscale x 4 x i16>* undef,
    <vscale x 4 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}

declare <vscale x 4 x i16> @llvm.epi.vload.ext.indexed.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  <vscale x 4 x i16>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x i16> @llvm.epi.vload.ext.indexed.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    <vscale x 4 x i16> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i16>*
  store <vscale x 4 x i16> %a, <vscale x 4 x i16>* %p

  ret void
}


declare <vscale x 8 x i16> @llvm.epi.vload.ext.indexed.nxv8i16(
  <vscale x 8 x i16>*,
  <vscale x 8 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.indexed.nxv8i16(
    <vscale x 8 x i16>* undef,
    <vscale x 8 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}

declare <vscale x 8 x i16> @llvm.epi.vload.ext.indexed.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  <vscale x 8 x i16>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x i16> @llvm.epi.vload.ext.indexed.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    <vscale x 8 x i16> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i16>*
  store <vscale x 8 x i16> %a, <vscale x 8 x i16>* %p

  ret void
}


declare <vscale x 16 x i16> @llvm.epi.vload.ext.indexed.nxv16i16(
  <vscale x 16 x i16>*,
  <vscale x 16 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.indexed.nxv16i16(
    <vscale x 16 x i16>* undef,
    <vscale x 16 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}

declare <vscale x 16 x i16> @llvm.epi.vload.ext.indexed.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  <vscale x 16 x i16>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 16 x i16> @llvm.epi.vload.ext.indexed.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    <vscale x 16 x i16> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i16>*
  store <vscale x 16 x i16> %a, <vscale x 16 x i16>* %p

  ret void
}


declare <vscale x 32 x i16> @llvm.epi.vload.ext.indexed.nxv32i16(
  <vscale x 32 x i16>*,
  <vscale x 32 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.indexed.nxv32i16(
    <vscale x 32 x i16>* undef,
    <vscale x 32 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}

declare <vscale x 32 x i16> @llvm.epi.vload.ext.indexed.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  <vscale x 32 x i16>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 32 x i16> @llvm.epi.vload.ext.indexed.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    <vscale x 32 x i16> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x i16>*
  store <vscale x 32 x i16> %a, <vscale x 32 x i16>* %p

  ret void
}


declare <vscale x 2 x i32> @llvm.epi.vload.ext.indexed.nxv2i32(
  <vscale x 2 x i32>*,
  <vscale x 2 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.indexed.nxv2i32(
    <vscale x 2 x i32>* undef,
    <vscale x 2 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}

declare <vscale x 2 x i32> @llvm.epi.vload.ext.indexed.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  <vscale x 2 x i32>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 2 x i32> @llvm.epi.vload.ext.indexed.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    <vscale x 2 x i32> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i32>*
  store <vscale x 2 x i32> %a, <vscale x 2 x i32>* %p

  ret void
}


declare <vscale x 4 x i32> @llvm.epi.vload.ext.indexed.nxv4i32(
  <vscale x 4 x i32>*,
  <vscale x 4 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.indexed.nxv4i32(
    <vscale x 4 x i32>* undef,
    <vscale x 4 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}

declare <vscale x 4 x i32> @llvm.epi.vload.ext.indexed.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  <vscale x 4 x i32>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x i32> @llvm.epi.vload.ext.indexed.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    <vscale x 4 x i32> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i32>*
  store <vscale x 4 x i32> %a, <vscale x 4 x i32>* %p

  ret void
}


declare <vscale x 8 x i32> @llvm.epi.vload.ext.indexed.nxv8i32(
  <vscale x 8 x i32>*,
  <vscale x 8 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.indexed.nxv8i32(
    <vscale x 8 x i32>* undef,
    <vscale x 8 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}

declare <vscale x 8 x i32> @llvm.epi.vload.ext.indexed.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  <vscale x 8 x i32>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x i32> @llvm.epi.vload.ext.indexed.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    <vscale x 8 x i32> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i32>*
  store <vscale x 8 x i32> %a, <vscale x 8 x i32>* %p

  ret void
}


declare <vscale x 16 x i32> @llvm.epi.vload.ext.indexed.nxv16i32(
  <vscale x 16 x i32>*,
  <vscale x 16 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.indexed.nxv16i32(
    <vscale x 16 x i32>* undef,
    <vscale x 16 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}

declare <vscale x 16 x i32> @llvm.epi.vload.ext.indexed.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  <vscale x 16 x i32>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 16 x i32> @llvm.epi.vload.ext.indexed.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    <vscale x 16 x i32> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x i32>*
  store <vscale x 16 x i32> %a, <vscale x 16 x i32>* %p

  ret void
}


declare <vscale x 1 x i64> @llvm.epi.vload.ext.indexed.nxv1i64(
  <vscale x 1 x i64>*,
  <vscale x 1 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.indexed.nxv1i64(
    <vscale x 1 x i64>* undef,
    <vscale x 1 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}

declare <vscale x 1 x i64> @llvm.epi.vload.ext.indexed.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  <vscale x 1 x i64>,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 1 x i64> @llvm.epi.vload.ext.indexed.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    <vscale x 1 x i64> undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x i64>*
  store <vscale x 1 x i64> %a, <vscale x 1 x i64>* %p

  ret void
}


declare <vscale x 2 x i64> @llvm.epi.vload.ext.indexed.nxv2i64(
  <vscale x 2 x i64>*,
  <vscale x 2 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.indexed.nxv2i64(
    <vscale x 2 x i64>* undef,
    <vscale x 2 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}

declare <vscale x 2 x i64> @llvm.epi.vload.ext.indexed.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  <vscale x 2 x i64>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 2 x i64> @llvm.epi.vload.ext.indexed.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    <vscale x 2 x i64> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x i64>*
  store <vscale x 2 x i64> %a, <vscale x 2 x i64>* %p

  ret void
}


declare <vscale x 4 x i64> @llvm.epi.vload.ext.indexed.nxv4i64(
  <vscale x 4 x i64>*,
  <vscale x 4 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.indexed.nxv4i64(
    <vscale x 4 x i64>* undef,
    <vscale x 4 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}

declare <vscale x 4 x i64> @llvm.epi.vload.ext.indexed.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  <vscale x 4 x i64>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x i64> @llvm.epi.vload.ext.indexed.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    <vscale x 4 x i64> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x i64>*
  store <vscale x 4 x i64> %a, <vscale x 4 x i64>* %p

  ret void
}


declare <vscale x 8 x i64> @llvm.epi.vload.ext.indexed.nxv8i64(
  <vscale x 8 x i64>*,
  <vscale x 8 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.indexed.nxv8i64(
    <vscale x 8 x i64>* undef,
    <vscale x 8 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}

declare <vscale x 8 x i64> @llvm.epi.vload.ext.indexed.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  <vscale x 8 x i64>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x i64> @llvm.epi.vload.ext.indexed.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    <vscale x 8 x i64> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x i64>*
  store <vscale x 8 x i64> %a, <vscale x 8 x i64>* %p

  ret void
}


declare <vscale x 4 x half> @llvm.epi.vload.ext.indexed.nxv4f16(
  <vscale x 4 x half>*,
  <vscale x 4 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.indexed.nxv4f16(
    <vscale x 4 x half>* undef,
    <vscale x 4 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}

declare <vscale x 4 x half> @llvm.epi.vload.ext.indexed.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  <vscale x 4 x i16>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x half> @llvm.epi.vload.ext.indexed.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    <vscale x 4 x i16> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x half>*
  store <vscale x 4 x half> %a, <vscale x 4 x half>* %p

  ret void
}


declare <vscale x 8 x half> @llvm.epi.vload.ext.indexed.nxv8f16(
  <vscale x 8 x half>*,
  <vscale x 8 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.indexed.nxv8f16(
    <vscale x 8 x half>* undef,
    <vscale x 8 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}

declare <vscale x 8 x half> @llvm.epi.vload.ext.indexed.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  <vscale x 8 x i16>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x half> @llvm.epi.vload.ext.indexed.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    <vscale x 8 x i16> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x half>*
  store <vscale x 8 x half> %a, <vscale x 8 x half>* %p

  ret void
}


declare <vscale x 16 x half> @llvm.epi.vload.ext.indexed.nxv16f16(
  <vscale x 16 x half>*,
  <vscale x 16 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.indexed.nxv16f16(
    <vscale x 16 x half>* undef,
    <vscale x 16 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}

declare <vscale x 16 x half> @llvm.epi.vload.ext.indexed.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  <vscale x 16 x i16>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 16 x half> @llvm.epi.vload.ext.indexed.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    <vscale x 16 x i16> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x half>*
  store <vscale x 16 x half> %a, <vscale x 16 x half>* %p

  ret void
}


declare <vscale x 32 x half> @llvm.epi.vload.ext.indexed.nxv32f16(
  <vscale x 32 x half>*,
  <vscale x 32 x i16>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.indexed.nxv32f16(
    <vscale x 32 x half>* undef,
    <vscale x 32 x i16> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}

declare <vscale x 32 x half> @llvm.epi.vload.ext.indexed.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  <vscale x 32 x i16>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 32 x half> @llvm.epi.vload.ext.indexed.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    <vscale x 32 x i16> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 32 x half>*
  store <vscale x 32 x half> %a, <vscale x 32 x half>* %p

  ret void
}


declare <vscale x 2 x float> @llvm.epi.vload.ext.indexed.nxv2f32(
  <vscale x 2 x float>*,
  <vscale x 2 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.indexed.nxv2f32(
    <vscale x 2 x float>* undef,
    <vscale x 2 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}

declare <vscale x 2 x float> @llvm.epi.vload.ext.indexed.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  <vscale x 2 x i32>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 2 x float> @llvm.epi.vload.ext.indexed.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    <vscale x 2 x i32> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x float>*
  store <vscale x 2 x float> %a, <vscale x 2 x float>* %p

  ret void
}


declare <vscale x 4 x float> @llvm.epi.vload.ext.indexed.nxv4f32(
  <vscale x 4 x float>*,
  <vscale x 4 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.indexed.nxv4f32(
    <vscale x 4 x float>* undef,
    <vscale x 4 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}

declare <vscale x 4 x float> @llvm.epi.vload.ext.indexed.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  <vscale x 4 x i32>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x float> @llvm.epi.vload.ext.indexed.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    <vscale x 4 x i32> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x float>*
  store <vscale x 4 x float> %a, <vscale x 4 x float>* %p

  ret void
}


declare <vscale x 8 x float> @llvm.epi.vload.ext.indexed.nxv8f32(
  <vscale x 8 x float>*,
  <vscale x 8 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.indexed.nxv8f32(
    <vscale x 8 x float>* undef,
    <vscale x 8 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}

declare <vscale x 8 x float> @llvm.epi.vload.ext.indexed.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  <vscale x 8 x i32>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x float> @llvm.epi.vload.ext.indexed.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    <vscale x 8 x i32> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x float>*
  store <vscale x 8 x float> %a, <vscale x 8 x float>* %p

  ret void
}


declare <vscale x 16 x float> @llvm.epi.vload.ext.indexed.nxv16f32(
  <vscale x 16 x float>*,
  <vscale x 16 x i32>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.indexed.nxv16f32(
    <vscale x 16 x float>* undef,
    <vscale x 16 x i32> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}

declare <vscale x 16 x float> @llvm.epi.vload.ext.indexed.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  <vscale x 16 x i32>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 16 x float> @llvm.epi.vload.ext.indexed.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    <vscale x 16 x i32> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 16 x float>*
  store <vscale x 16 x float> %a, <vscale x 16 x float>* %p

  ret void
}


declare <vscale x 1 x double> @llvm.epi.vload.ext.indexed.nxv1f64(
  <vscale x 1 x double>*,
  <vscale x 1 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.indexed.nxv1f64(
    <vscale x 1 x double>* undef,
    <vscale x 1 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}

declare <vscale x 1 x double> @llvm.epi.vload.ext.indexed.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  <vscale x 1 x i64>,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 1 x double> @llvm.epi.vload.ext.indexed.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    <vscale x 1 x i64> undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 1 x double>*
  store <vscale x 1 x double> %a, <vscale x 1 x double>* %p

  ret void
}


declare <vscale x 2 x double> @llvm.epi.vload.ext.indexed.nxv2f64(
  <vscale x 2 x double>*,
  <vscale x 2 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.indexed.nxv2f64(
    <vscale x 2 x double>* undef,
    <vscale x 2 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}

declare <vscale x 2 x double> @llvm.epi.vload.ext.indexed.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  <vscale x 2 x i64>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 2 x double> @llvm.epi.vload.ext.indexed.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    <vscale x 2 x i64> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 2 x double>*
  store <vscale x 2 x double> %a, <vscale x 2 x double>* %p

  ret void
}


declare <vscale x 4 x double> @llvm.epi.vload.ext.indexed.nxv4f64(
  <vscale x 4 x double>*,
  <vscale x 4 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.indexed.nxv4f64(
    <vscale x 4 x double>* undef,
    <vscale x 4 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}

declare <vscale x 4 x double> @llvm.epi.vload.ext.indexed.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  <vscale x 4 x i64>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 4 x double> @llvm.epi.vload.ext.indexed.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    <vscale x 4 x i64> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 4 x double>*
  store <vscale x 4 x double> %a, <vscale x 4 x double>* %p

  ret void
}


declare <vscale x 8 x double> @llvm.epi.vload.ext.indexed.nxv8f64(
  <vscale x 8 x double>*,
  <vscale x 8 x i64>,
  i64, i64);

define void @intrinsic_vload.ext.indexed_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.indexed.nxv8f64(
    <vscale x 8 x double>* undef,
    <vscale x 8 x i64> undef,
    i64 undef, i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}

declare <vscale x 8 x double> @llvm.epi.vload.ext.indexed.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  <vscale x 8 x i64>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vload.ext.indexed_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vload.ext.indexed_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vlxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  %a = call <vscale x 8 x double> @llvm.epi.vload.ext.indexed.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    <vscale x 8 x i64> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  %p = bitcast i8* @scratch to <vscale x 8 x double>*
  store <vscale x 8 x double> %a, <vscale x 8 x double>* %p

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  <vscale x 8 x i8>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    <vscale x 8 x i8> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8i8(
  <vscale x 8 x i8>,
  <vscale x 8 x i8>*,
  <vscale x 8 x i8>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8i8_nxv8i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8i8_nxv8i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8i8(
    <vscale x 8 x i8> undef,
    <vscale x 8 x i8>* undef,
    <vscale x 8 x i8> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  <vscale x 16 x i8>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    <vscale x 16 x i8> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv16i8(
  <vscale x 16 x i8>,
  <vscale x 16 x i8>*,
  <vscale x 16 x i8>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv16i8_nxv16i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv16i8_nxv16i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv16i8(
    <vscale x 16 x i8> undef,
    <vscale x 16 x i8>* undef,
    <vscale x 16 x i8> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  <vscale x 32 x i8>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    <vscale x 32 x i8> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv32i8(
  <vscale x 32 x i8>,
  <vscale x 32 x i8>*,
  <vscale x 32 x i8>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv32i8_nxv32i8() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv32i8_nxv32i8
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv32i8(
    <vscale x 32 x i8> undef,
    <vscale x 32 x i8>* undef,
    <vscale x 32 x i8> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  <vscale x 4 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    <vscale x 4 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4i16(
  <vscale x 4 x i16>,
  <vscale x 4 x i16>*,
  <vscale x 4 x i16>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4i16_nxv4i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4i16_nxv4i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4i16(
    <vscale x 4 x i16> undef,
    <vscale x 4 x i16>* undef,
    <vscale x 4 x i16> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  <vscale x 8 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    <vscale x 8 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8i16(
  <vscale x 8 x i16>,
  <vscale x 8 x i16>*,
  <vscale x 8 x i16>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8i16_nxv8i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8i16_nxv8i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8i16(
    <vscale x 8 x i16> undef,
    <vscale x 8 x i16>* undef,
    <vscale x 8 x i16> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  <vscale x 16 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    <vscale x 16 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv16i16(
  <vscale x 16 x i16>,
  <vscale x 16 x i16>*,
  <vscale x 16 x i16>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv16i16_nxv16i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv16i16_nxv16i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv16i16(
    <vscale x 16 x i16> undef,
    <vscale x 16 x i16>* undef,
    <vscale x 16 x i16> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  <vscale x 32 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    <vscale x 32 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv32i16(
  <vscale x 32 x i16>,
  <vscale x 32 x i16>*,
  <vscale x 32 x i16>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv32i16_nxv32i16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv32i16_nxv32i16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv32i16(
    <vscale x 32 x i16> undef,
    <vscale x 32 x i16>* undef,
    <vscale x 32 x i16> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  <vscale x 2 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    <vscale x 2 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv2i32(
  <vscale x 2 x i32>,
  <vscale x 2 x i32>*,
  <vscale x 2 x i32>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv2i32_nxv2i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv2i32_nxv2i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv2i32(
    <vscale x 2 x i32> undef,
    <vscale x 2 x i32>* undef,
    <vscale x 2 x i32> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  <vscale x 4 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    <vscale x 4 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4i32(
  <vscale x 4 x i32>,
  <vscale x 4 x i32>*,
  <vscale x 4 x i32>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4i32_nxv4i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4i32_nxv4i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4i32(
    <vscale x 4 x i32> undef,
    <vscale x 4 x i32>* undef,
    <vscale x 4 x i32> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  <vscale x 8 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    <vscale x 8 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8i32(
  <vscale x 8 x i32>,
  <vscale x 8 x i32>*,
  <vscale x 8 x i32>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8i32_nxv8i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8i32_nxv8i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8i32(
    <vscale x 8 x i32> undef,
    <vscale x 8 x i32>* undef,
    <vscale x 8 x i32> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  <vscale x 16 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    <vscale x 16 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv16i32(
  <vscale x 16 x i32>,
  <vscale x 16 x i32>*,
  <vscale x 16 x i32>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv16i32_nxv16i32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv16i32_nxv16i32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv16i32(
    <vscale x 16 x i32> undef,
    <vscale x 16 x i32>* undef,
    <vscale x 16 x i32> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  <vscale x 1 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    <vscale x 1 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv1i64(
  <vscale x 1 x i64>,
  <vscale x 1 x i64>*,
  <vscale x 1 x i64>,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv1i64_nxv1i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv1i64_nxv1i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv1i64(
    <vscale x 1 x i64> undef,
    <vscale x 1 x i64>* undef,
    <vscale x 1 x i64> undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  <vscale x 2 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    <vscale x 2 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv2i64(
  <vscale x 2 x i64>,
  <vscale x 2 x i64>*,
  <vscale x 2 x i64>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv2i64_nxv2i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv2i64_nxv2i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv2i64(
    <vscale x 2 x i64> undef,
    <vscale x 2 x i64>* undef,
    <vscale x 2 x i64> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  <vscale x 4 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    <vscale x 4 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4i64(
  <vscale x 4 x i64>,
  <vscale x 4 x i64>*,
  <vscale x 4 x i64>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4i64_nxv4i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4i64_nxv4i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4i64(
    <vscale x 4 x i64> undef,
    <vscale x 4 x i64>* undef,
    <vscale x 4 x i64> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  <vscale x 8 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    <vscale x 8 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8i64(
  <vscale x 8 x i64>,
  <vscale x 8 x i64>*,
  <vscale x 8 x i64>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8i64_nxv8i64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8i64_nxv8i64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8i64(
    <vscale x 8 x i64> undef,
    <vscale x 8 x i64>* undef,
    <vscale x 8 x i64> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  <vscale x 4 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    <vscale x 4 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4f16(
  <vscale x 4 x half>,
  <vscale x 4 x half>*,
  <vscale x 4 x i16>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4f16_nxv4f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4f16_nxv4f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4f16(
    <vscale x 4 x half> undef,
    <vscale x 4 x half>* undef,
    <vscale x 4 x i16> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  <vscale x 8 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    <vscale x 8 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8f16(
  <vscale x 8 x half>,
  <vscale x 8 x half>*,
  <vscale x 8 x i16>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8f16_nxv8f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8f16_nxv8f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8f16(
    <vscale x 8 x half> undef,
    <vscale x 8 x half>* undef,
    <vscale x 8 x i16> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  <vscale x 16 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    <vscale x 16 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv16f16(
  <vscale x 16 x half>,
  <vscale x 16 x half>*,
  <vscale x 16 x i16>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv16f16_nxv16f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv16f16_nxv16f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv16f16(
    <vscale x 16 x half> undef,
    <vscale x 16 x half>* undef,
    <vscale x 16 x i16> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  <vscale x 32 x i16>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    <vscale x 32 x i16> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv32f16(
  <vscale x 32 x half>,
  <vscale x 32 x half>*,
  <vscale x 32 x i16>,
  i64,
  <vscale x 32 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv32f16_nxv32f16() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv32f16_nxv32f16
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv32f16(
    <vscale x 32 x half> undef,
    <vscale x 32 x half>* undef,
    <vscale x 32 x i16> undef,
    i64 undef,
    <vscale x 32 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  <vscale x 2 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    <vscale x 2 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv2f32(
  <vscale x 2 x float>,
  <vscale x 2 x float>*,
  <vscale x 2 x i32>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv2f32_nxv2f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv2f32_nxv2f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv2f32(
    <vscale x 2 x float> undef,
    <vscale x 2 x float>* undef,
    <vscale x 2 x i32> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  <vscale x 4 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    <vscale x 4 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4f32(
  <vscale x 4 x float>,
  <vscale x 4 x float>*,
  <vscale x 4 x i32>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4f32_nxv4f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4f32_nxv4f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4f32(
    <vscale x 4 x float> undef,
    <vscale x 4 x float>* undef,
    <vscale x 4 x i32> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  <vscale x 8 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    <vscale x 8 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8f32(
  <vscale x 8 x float>,
  <vscale x 8 x float>*,
  <vscale x 8 x i32>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8f32_nxv8f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8f32_nxv8f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8f32(
    <vscale x 8 x float> undef,
    <vscale x 8 x float>* undef,
    <vscale x 8 x i32> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  <vscale x 16 x i32>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    <vscale x 16 x i32> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv16f32(
  <vscale x 16 x float>,
  <vscale x 16 x float>*,
  <vscale x 16 x i32>,
  i64,
  <vscale x 16 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv16f32_nxv16f32() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv16f32_nxv16f32
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv16f32(
    <vscale x 16 x float> undef,
    <vscale x 16 x float>* undef,
    <vscale x 16 x i32> undef,
    i64 undef,
    <vscale x 16 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  <vscale x 1 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    <vscale x 1 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv1f64(
  <vscale x 1 x double>,
  <vscale x 1 x double>*,
  <vscale x 1 x i64>,
  i64,
  <vscale x 1 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv1f64_nxv1f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv1f64_nxv1f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv1f64(
    <vscale x 1 x double> undef,
    <vscale x 1 x double>* undef,
    <vscale x 1 x i64> undef,
    i64 undef,
    <vscale x 1 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  <vscale x 2 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    <vscale x 2 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv2f64(
  <vscale x 2 x double>,
  <vscale x 2 x double>*,
  <vscale x 2 x i64>,
  i64,
  <vscale x 2 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv2f64_nxv2f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv2f64_nxv2f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv2f64(
    <vscale x 2 x double> undef,
    <vscale x 2 x double>* undef,
    <vscale x 2 x i64> undef,
    i64 undef,
    <vscale x 2 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  <vscale x 4 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    <vscale x 4 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv4f64(
  <vscale x 4 x double>,
  <vscale x 4 x double>*,
  <vscale x 4 x i64>,
  i64,
  <vscale x 4 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv4f64_nxv4f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv4f64_nxv4f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv4f64(
    <vscale x 4 x double> undef,
    <vscale x 4 x double>* undef,
    <vscale x 4 x i64> undef,
    i64 undef,
    <vscale x 4 x i1> undef,
    i64 undef)

  ret void
}


declare void @llvm.epi.vstore.ext.indexed.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  <vscale x 8 x i64>,
  i64, i64);

define void @intrinsic_vstore.ext.indexed_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}
  call void @llvm.epi.vstore.ext.indexed.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    <vscale x 8 x i64> undef,
    i64 undef, i64 undef)

  ret void
}

declare void @llvm.epi.vstore.ext.indexed.mask.nxv8f64(
  <vscale x 8 x double>,
  <vscale x 8 x double>*,
  <vscale x 8 x i64>,
  i64,
  <vscale x 8 x i1>,
  i64);

define void @intrinsic_vstore.ext.indexed_mask_v_nxv8f64_nxv8f64() nounwind {
entry:
; CHECK-LABEL: intrinsic_vstore.ext.indexed_mask_v_nxv8f64_nxv8f64
; CHECK:       vsetvl {{.*}}, {{.*}}, {{.*}}
; CHECK:       vsxe.v {{v[0-9]+}}, (a0), {{v[0-9]+}}, v0.t
  call void @llvm.epi.vstore.ext.indexed.mask.nxv8f64(
    <vscale x 8 x double> undef,
    <vscale x 8 x double>* undef,
    <vscale x 8 x i64> undef,
    i64 undef,
    <vscale x 8 x i1> undef,
    i64 undef)

  ret void
}
