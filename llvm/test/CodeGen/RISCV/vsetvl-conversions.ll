; RUN: llc -mtriple=riscv64 -mattr=+m,+f,+d,+a,+c,+experimental-v \
; RUN:    -verify-machineinstrs --riscv-no-aliases < %s | FileCheck %s


; These tests checks that consecutive VSETVLI operations are fused
; together when they have the same SEW and alternative bits

declare <vscale x 2 x float>  @llvm.epi.vfncvt.f.f.nxv2f32.nxv2f64 (<vscale x 2 x double>, i64)
declare <vscale x 2 x i32> @llvm.epi.vfncvt.x.f.nxv2i32.nxv2f64(<vscale x 2 x double>, i64)

declare void @llvm.epi.vstore.nxv2f32 (<vscale x 2 x float>,  <vscale x 2 x float>*,i64)
declare void @llvm.epi.vstore.nxv2i32 (<vscale x 2 x i32>, <vscale x 2 x i32>*,i64)

define void @testFusion1(
  <vscale x 2 x double> %a,
  <vscale x 2 x double> %b,
  <vscale x 2 x float>* %pa,
  <vscale x 2 x i32>* %pb,
  i64 %gvl) nounwind {
entry:
; CHECK: vsetvli {{.*}}, {{.*}}, e32,m1
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vfncvt.x.f.v {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 2 x float> @llvm.epi.vfncvt.f.f.nxv2f32.nxv2f64(<vscale x 2 x double> %a, i64 %gvl)
  %vb = call <vscale x 2 x i32>   @llvm.epi.vfncvt.x.f.nxv2i32.nxv2f64(<vscale x 2 x double> %b, i64 %gvl)

; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv2f32 (<vscale x 2 x float> %va, <vscale x 2 x float>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv2i32 (<vscale x 2 x i32>   %vb, <vscale x 2 x i32>*   %pb, i64 %gvl)

  ret void
}



declare <vscale x 2 x double>  @llvm.epi.vfwcvt.f.f.nxv2f64.nxv2f32 (<vscale x 2 x float>, i64)
declare <vscale x 2 x float>  @llvm.epi.vfncvt.f.x.nxv2f32.nxv2i64 (<vscale x 2 x i64>, i64)

declare void @llvm.epi.vstore.nxv2f64 (<vscale x 2 x double>, <vscale x 2 x double>*,i64)

define void @testFusion2(
  <vscale x 2 x float> %a,
  <vscale x 2 x i64> %b,
  <vscale x 2 x double>* %pa,
  <vscale x 2 x float>* %pb,
  i64 %gvl) nounwind {
entry:
; CHECK: vsetvli {{.*}}, {{.*}}, e32,m1
; CHECK-NEXT: vfwcvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vfncvt.f.x.v {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 2 x double> @llvm.epi.vfwcvt.f.f.nxv2f64.nxv2f32(<vscale x 2 x float> %a, i64 %gvl)
  %vb = call <vscale x 2 x float>  @llvm.epi.vfncvt.f.x.nxv2f32.nxv2i64(<vscale x 2 x i64>  %b, i64 %gvl)

; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e64,m2
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv2f64 (<vscale x 2 x double> %va, <vscale x 2 x double>* %pa, i64 %gvl)
; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e32,m1
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv2f32 (<vscale x 2 x float>  %vb, <vscale x 2 x float>*  %pb, i64 %gvl)

  ret void
}



declare <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.f.nxv4bf16.nxv4f32(<vscale x 4 x float>, i64)
declare <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.x.nxv4bf16.nxv4i32(<vscale x 4 x i32>, i64)

declare void @llvm.epi.vstore.nxv4bf16 (<vscale x 4 x bfloat> %va, <vscale x 4 x bfloat>*, i64)

define void @testFusion3(
  <vscale x 4 x float> %a,
  <vscale x 4 x i32> %b,
  <vscale x 4 x bfloat>* %pa,
  <vscale x 4 x bfloat>* %pb,
  i64 %gvl) nounwind {
entry:
; CHECK: vsetvli {{.*}}, {{.*}}, e16,m1,a
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vfncvt.f.x.v {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.f.nxv4bf16.nxv4f32(<vscale x 4 x float> %a, i64 %gvl)
  %vb = call <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.x.nxv4bf16.nxv4i32(<vscale x 4 x i32> %b, i64 %gvl)

; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv4bf16 (<vscale x 4 x bfloat> %va, <vscale x 4 x bfloat>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv4bf16 (<vscale x 4 x bfloat> %vb, <vscale x 4 x bfloat>* %pb, i64 %gvl)

  ret void
}



declare <vscale x 4 x float> @llvm.epi.vfwcvt.f.f.nxv4f32.nxv4bf16(<vscale x 4 x bfloat>, i64)
declare <vscale x 4 x i32> @llvm.epi.vfwcvt.x.f.nxv4i32.nxv4bf16(<vscale x 4 x bfloat>, i64)

declare void @llvm.epi.vstore.nxv4f32 (<vscale x 4 x float>, <vscale x 4 x float>*, i64)
declare void @llvm.epi.vstore.nxv4i32 (<vscale x 4 x i32>, <vscale x 4 x i32>*, i64)

define void @testFusion4(
  <vscale x 4 x bfloat> %a,
  <vscale x 4 x bfloat> %b,
  <vscale x 4 x float>* %pa,
  <vscale x 4 x i32>* %pb,
  i64 %gvl) nounwind {
entry:
; CHECK: vsetvli {{.*}}, {{.*}}, b16,m1
; CHECK-NEXT: vfwcvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vfwcvt.x.f.v {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 4 x float> @llvm.epi.vfwcvt.f.f.nxv4f32.nxv4bf16(<vscale x 4 x bfloat> %a, i64 %gvl)
  %vb = call <vscale x 4 x i32> @llvm.epi.vfwcvt.x.f.nxv4i32.nxv4bf16(<vscale x 4 x bfloat> %b, i64 %gvl)

; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e32,m2
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv4f32 (<vscale x 4 x float> %va, <vscale x 4 x float>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv4i32 (<vscale x 4 x i32> %vb, <vscale x 4 x i32>* %pb, i64 %gvl)

  ret void
}



; These tests checks that the alternative bits and SEWs are managed
; appropriately, and that no extra VSETVL instructions are inserted
; when there's no need for them

declare <vscale x 4 x half> @llvm.epi.vfncvt.f.f.nxv2f16.nxv2f32(<vscale x 4 x float>, i64)

declare  void @llvm.epi.vstore.nxv4f16 (<vscale x 4 x half> %vb, <vscale x 4 x half>*, i64)

define void @testAltBits1(
  <vscale x 2 x double> %a,
  <vscale x 4 x float> %b,
  <vscale x 2 x float>* %pa,
  <vscale x 4 x half>* %pb,
  i64 %gvl) nounwind {

; CHECK: vsetvli {{.*}}, {{.*}}, e32,m1
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e16,m1
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}  
  %va = call <vscale x 2 x float> @llvm.epi.vfncvt.f.f.nxv2f32.nxv2f64(<vscale x 2 x double> %a, i64 %gvl)
  %vb = call <vscale x 4 x half> @llvm.epi.vfncvt.f.f.nxv2f16.nxv2f32(<vscale x 4 x float> %b, i64 %gvl)

; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e32,m1
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e16,m1
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv2f32 (<vscale x 2 x float> %va, <vscale x 2 x float>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv4f16 (<vscale x 4 x half> %vb, <vscale x 4 x half>* %pb, i64 %gvl)

  ret void
}


declare <vscale x 4 x bfloat> @llvm.epi.vfadd.vv.nxv4bf16.nxv4bf16.nxv4bf16(<vscale x 4 x bfloat>, <vscale x 4 x bfloat>, i64)

define void @testAltBits2(
  <vscale x 4 x float> %a,
  <vscale x 4 x bfloat> %b,
  <vscale x 4 x bfloat>* %pb,
  i64 %gvl) nounwind {

; CHECK: vsetvli {{.*}}, {{.*}}, e16,m1,a
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, b16,m1
; CHECK-NEXT: vfadd.vv {{v[0-9]+}}, {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.f.nxv4bf16.nxv4f32(<vscale x 4 x float> %a, i64 %gvl)
  %vb = call <vscale x 4 x bfloat> @llvm.epi.vfadd.vv.nxv4bf16.nxv4bf16.nxv4bf16(<vscale x 4 x bfloat> %va, <vscale x 4 x bfloat> %b, i64 %gvl)

; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv4bf16 (<vscale x 4 x bfloat> %vb, <vscale x 4 x bfloat>* %pb, i64 %gvl)

  ret void
}


declare  <vscale x 4 x half> @llvm.epi.vfadd.vv.nxv4f16.nxv4f16.nxv4f16(<vscale x 4 x half>, <vscale x 4 x half>, i64)

define void @testAltBits3(
  <vscale x 4 x float> %a,
  <vscale x 4 x half> %b1,
  <vscale x 4 x half> %b2,
  <vscale x 4 x bfloat>* %pa,
  <vscale x 4 x half>* %pb,
  i64 %gvl) nounwind {

; CHECK: vsetvli {{.*}}, {{.*}}, e16,m1,a
; CHECK-NEXT: vfncvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vfadd.vv {{v[0-9]+}}, {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 4 x bfloat> @llvm.epi.vfncvt.f.f.nxv4bf16.nxv4f32(<vscale x 4 x float> %a, i64 %gvl)
  %vb = call <vscale x 4 x half> @llvm.epi.vfadd.vv.nxv4f16.nxv4f16.nxv4f16(<vscale x 4 x half> %b1, <vscale x 4 x half> %b2, i64 %gvl)

; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv4bf16 (<vscale x 4 x bfloat> %va, <vscale x 4 x bfloat>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv4f16 (<vscale x 4 x half> %vb, <vscale x 4 x half>* %pb, i64 %gvl)

  ret void
}



declare <vscale x 4 x i16> @llvm.epi.vadd.vv.nxv4i16.nxv4i16.nxv4i16(<vscale x 4 x i16>, <vscale x 4 x i16>, i64)
declare void @llvm.epi.vstore.nxv4i16 (<vscale x 4 x i16>, <vscale x 4 x i16>*, i64)

define void @testAltBits4(
  <vscale x 4 x bfloat> %a,
  <vscale x 4 x i16> %b1,
  <vscale x 4 x i16> %b2,
  <vscale x 4 x float>* %pa,
  <vscale x 4 x i16>* %pb,
  i64 %gvl) nounwind {

; CHECK: vsetvli {{.*}}, {{.*}}, b16,m1
; CHECK-NEXT: vfwcvt.f.f.v {{v[0-9]+}}, {{v[0-9]+}}
; CHECK-NEXT: vadd.vv {{v[0-9]+}}, {{v[0-9]+}}, {{v[0-9]+}}
  %va = call <vscale x 4 x float> @llvm.epi.vfwcvt.f.f.nxv4f32.nxv4bf16(<vscale x 4 x bfloat> %a, i64 %gvl)
  %vb = call <vscale x 4 x i16> @llvm.epi.vadd.vv.nxv4i16.nxv4i16.nxv4i16(<vscale x 4 x i16> %b1, <vscale x 4 x i16> %b2, i64 %gvl)

; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e32,m2
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
; CHECK-NEXT: vsetvli {{.*}}, {{.*}}, e16,m1
; CHECK-NEXT: vse.v {{v[0-9]+}}, ({{.*}})
  call void @llvm.epi.vstore.nxv4f32 (<vscale x 4 x float> %va, <vscale x 4 x float>* %pa, i64 %gvl)
  call void @llvm.epi.vstore.nxv4i16 (<vscale x 4 x i16> %vb, <vscale x 4 x i16>* %pb, i64 %gvl)

  ret void
}
