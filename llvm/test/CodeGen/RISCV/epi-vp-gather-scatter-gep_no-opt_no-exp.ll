; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple riscv64 -mattr=+d,+experimental-v -verify-machineinstrs < %s | FileCheck %s

; ModuleID = '/home/lalbano/tmp/base_offset.c'
source_filename = "/home/lalbano/tmp/base_offset.c"
target datalayout = "e-m:e-p:64:64-i64:64-i128:128-n64-S128-v128:128:128-v256:128:128-v512:128:128-v1024:128:128"
target triple = "riscv64-unknown-linux-gnu"

; Function Attrs: nofree norecurse nounwind
define dso_local void @autovec_vsxe(i32 signext %N, double* nocapture %OUT, i64* nocapture readonly %offsets) local_unnamed_addr #0 {
; CHECK-LABEL: autovec_vsxe:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    addi a3, zero, 1
; CHECK-NEXT:    blt a0, a3, .LBB0_3
; CHECK-NEXT:  # %bb.1: # %for.body.preheader
; CHECK-NEXT:    lui a3, %hi(.LCPI0_0)
; CHECK-NEXT:    fld ft0, %lo(.LCPI0_0)(a3)
; CHECK-NEXT:    lui a3, %hi(.LCPI0_1)
; CHECK-NEXT:    fld ft1, %lo(.LCPI0_1)(a3)
; CHECK-NEXT:    mv a3, zero
; CHECK-NEXT:    slli a0, a0, 32
; CHECK-NEXT:    srli a6, a0, 32
; CHECK-NEXT:  .LBB0_2: # %vector.body
; CHECK-NEXT:    # =>This Inner Loop Header: Depth=1
; CHECK-NEXT:    slli a4, a3, 3
; CHECK-NEXT:    add a4, a4, a2
; CHECK-NEXT:    sub a5, a6, a3
; CHECK-NEXT:    vsetvli a0, a5, e64,m1
; CHECK-NEXT:    vle.v v1, (a4)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vsll.vi v2, v1, 3
; CHECK-NEXT:    vfmv.v.f v3, ft0
; CHECK-NEXT:    vsetvli zero, a5, e64,m1
; CHECK-NEXT:    vsxe.v v3, (a1), v2
; CHECK-NEXT:    vadd.vi v1, v1, 1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vsll.vi v1, v1, 3
; CHECK-NEXT:    vfmv.v.f v2, ft1
; CHECK-NEXT:    vsetvli zero, a5, e64,m1
; CHECK-NEXT:    slli a0, a0, 32
; CHECK-NEXT:    srli a0, a0, 32
; CHECK-NEXT:    add a3, a3, a0
; CHECK-NEXT:    vsxe.v v2, (a1), v1
; CHECK-NEXT:    bne a3, a6, .LBB0_2
; CHECK-NEXT:  .LBB0_3: # %for.cond.cleanup
; CHECK-NEXT:    ret
entry:
  %cmp9 = icmp sgt i32 %N, 0
  br i1 %cmp9, label %for.body.preheader, label %for.cond.cleanup

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %N to i64
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %for.body.preheader
  %index = phi i64 [ 0, %for.body.preheader ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds i64, i64* %offsets, i64 %index
  %1 = sub i64 %wide.trip.count, %index
  %2 = call i64 @llvm.epi.vsetvl(i64 %1, i64 3, i64 0)
  %3 = trunc i64 %2 to i32
  %4 = bitcast i64* %0 to <vscale x 1 x i64>*
  %vp.op.load = call <vscale x 1 x i64> @llvm.vp.load.nxv1i64.p0nxv1i64(<vscale x 1 x i64>* %4, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %3), !tbaa !4, !llvm.access.group !8
  %5 = getelementptr inbounds double, double* %OUT, <vscale x 1 x i64> %vp.op.load
  call void @llvm.vp.scatter.nxv1f64.nxv1p0f64(<vscale x 1 x double> shufflevector (<vscale x 1 x double> insertelement (<vscale x 1 x double> undef, double 1.000000e+00, i32 0), <vscale x 1 x double> undef, <vscale x 1 x i32> zeroinitializer), <vscale x 1 x double*> %5, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %3), !tbaa !9, !llvm.access.group !8
  %vp.op = call <vscale x 1 x i64> @llvm.vp.add.nxv1i64(<vscale x 1 x i64> %vp.op.load, <vscale x 1 x i64> shufflevector (<vscale x 1 x i64> insertelement (<vscale x 1 x i64> undef, i64 1, i32 0), <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer), <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %3)
  %6 = getelementptr inbounds double, double* %OUT, <vscale x 1 x i64> %vp.op
  call void @llvm.vp.scatter.nxv1f64.nxv1p0f64(<vscale x 1 x double> shufflevector (<vscale x 1 x double> insertelement (<vscale x 1 x double> undef, double 2.000000e+00, i32 0), <vscale x 1 x double> undef, <vscale x 1 x i32> zeroinitializer), <vscale x 1 x double*> %6, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %3), !tbaa !9, !llvm.access.group !8
  %7 = and i64 %2, 4294967295
  %index.next = add i64 %index, %7
  %8 = icmp eq i64 %index.next, %wide.trip.count
  br i1 %8, label %for.cond.cleanup, label %vector.body, !llvm.loop !11

for.cond.cleanup:                                 ; preds = %vector.body, %entry
  ret void
}

; Function Attrs: nounwind readnone
declare i64 @llvm.epi.vsetvl(i64, i64, i64) #1

; Function Attrs: argmemonly nosync nounwind readonly willreturn
declare <vscale x 1 x i64> @llvm.vp.load.nxv1i64.p0nxv1i64(<vscale x 1 x i64>* nocapture, i32 immarg, <vscale x 1 x i1>, i32) #2

; Function Attrs: argmemonly nosync nounwind willreturn writeonly
declare void @llvm.vp.scatter.nxv1f64.nxv1p0f64(<vscale x 1 x double>, <vscale x 1 x double*>, i32 immarg, <vscale x 1 x i1>, i32) #3

; Function Attrs: nofree nosync nounwind readnone speculatable willreturn
declare <vscale x 1 x i64> @llvm.vp.add.nxv1i64(<vscale x 1 x i64>, <vscale x 1 x i64>, <vscale x 1 x i1>, i32) #4

attributes #0 = { nofree norecurse nounwind "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-features"="+a,+c,+d,+experimental-v,+experimental-zvlsseg,+f,+m,-relax,-save-restore" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { argmemonly nosync nounwind readonly willreturn }
attributes #3 = { argmemonly nosync nounwind willreturn writeonly }
attributes #4 = { nofree nosync nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 1, !"target-abi", !"lp64d"}
!2 = !{i32 1, !"SmallDataLimit", i32 8}
!3 = !{!"clang version 12.0.0"}
!4 = !{!5, !5, i64 0}
!5 = !{!"long", !6, i64 0}
!6 = !{!"omnipotent char", !7, i64 0}
!7 = !{!"Simple C/C++ TBAA"}
!8 = distinct !{}
!9 = !{!10, !10, i64 0}
!10 = !{!"double", !6, i64 0}
!11 = distinct !{!11, !12, !13, !14}
!12 = !{!"llvm.loop.mustprogress"}
!13 = !{!"llvm.loop.parallel_accesses", !8}
!14 = !{!"llvm.loop.isvectorized", i32 1}
