; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple riscv64 -mattr=+m,+f,+a,+d,+c,+experimental-v < %s \
; RUN:    | FileCheck %s

define void @vec_add(i32 signext %N, double* noalias nocapture %c, double* noalias nocapture readonly %a, double* noalias nocapture readonly %b, double* noalias nocapture %d) nounwind {
; CHECK-LABEL: vec_add:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    addi a5, zero, 1
; CHECK-NEXT:    blt a0, a5, .LBB0_3
; CHECK-NEXT:  # %bb.1: # %for.body.preheader
; CHECK-NEXT:    lui a5, %hi(.LCPI0_0)
; CHECK-NEXT:    fld ft0, %lo(.LCPI0_0)(a5)
; CHECK-NEXT:    mv a5, zero
; CHECK-NEXT:    slli a0, a0, 32
; CHECK-NEXT:    srli a6, a0, 32
; CHECK-NEXT:  .LBB0_2: # %vector.body
; CHECK-NEXT:    # =>This Inner Loop Header: Depth=1
; CHECK-NEXT:    slli t0, a5, 3
; CHECK-NEXT:    add a7, a1, t0
; CHECK-NEXT:    sub t3, a6, a5
; CHECK-NEXT:    vsetvli t1, t3, e64,m1
; CHECK-NEXT:    vle.v v1, (a7)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmset.m v2
; CHECK-NEXT:    vsetvli zero, t3, e64,m1
; CHECK-NEXT:    vmflt.vf v1, v1, ft0
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmand.mm v1, v1, v2
; CHECK-NEXT:    add t2, a2, t0
; CHECK-NEXT:    vsetvli zero, t3, e64,m1
; CHECK-NEXT:    vle.v v3, (t2)
; CHECK-NEXT:    add a0, a3, t0
; CHECK-NEXT:    vle.v v4, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmxor.mm v0, v1, v2
; CHECK-NEXT:    vsetvli zero, t3, e64,m1
; CHECK-NEXT:    vfmul.vv v2, v3, v4, v0.t
; CHECK-NEXT:    add a0, a4, t0
; CHECK-NEXT:    vse.v v2, (a0), v0.t
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t2
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v0, v1
; CHECK-NEXT:    vsetvl zero, t2, t0
; CHECK-NEXT:    vfadd.vv v2, v3, v4, v0.t
; CHECK-NEXT:    add a5, a5, t1
; CHECK-NEXT:    vse.v v2, (a7), v0.t
; CHECK-NEXT:    bne a5, a6, .LBB0_2
; CHECK-NEXT:  .LBB0_3: # %for.cond.cleanup
; CHECK-NEXT:    ret
entry:
  %cmp22 = icmp sgt i32 %N, 0
  br i1 %cmp22, label %for.body.preheader, label %for.cond.cleanup

for.body.preheader:                               ; preds = %entry
  %wide.trip.count = zext i32 %N to i64
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %for.body.preheader
  %index = phi i64 [ 0, %for.body.preheader ], [ %index.next, %vector.body ]
  %0 = getelementptr inbounds double, double* %c, i64 %index
  %1 = sub i64 %wide.trip.count, %index
  %2 = call i64 @llvm.epi.vsetvl(i64 %1, i64 3, i64 0)
  %3 = bitcast double* %0 to <vscale x 1 x double>*
  %4 = trunc i64 %2 to i32
  %vp.op.load = call <vscale x 1 x double> @llvm.vp.load.nxv1f64.p0nxv1f64(<vscale x 1 x double>* %3, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %4)
  %vp.op.fcmp = call <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double> %vp.op.load, <vscale x 1 x double> shufflevector (<vscale x 1 x double> insertelement (<vscale x 1 x double> undef, double 3.000000e+00, i32 0), <vscale x 1 x double> undef, <vscale x 1 x i32> zeroinitializer), i8 4, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %4)
  %5 = and <vscale x 1 x i1> %vp.op.fcmp, shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer)
  %6 = getelementptr inbounds double, double* %a, i64 %index
  %7 = bitcast double* %6 to <vscale x 1 x double>*
  %vp.op.load30 = call <vscale x 1 x double> @llvm.vp.load.nxv1f64.p0nxv1f64(<vscale x 1 x double>* %7, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %4)
  %8 = getelementptr inbounds double, double* %b, i64 %index
  %9 = bitcast double* %8 to <vscale x 1 x double>*
  %vp.op.load33 = call <vscale x 1 x double> @llvm.vp.load.nxv1f64.p0nxv1f64(<vscale x 1 x double>* %9, i32 8, <vscale x 1 x i1> shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer), i32 %4)
  %10 = xor <vscale x 1 x i1> %5, shufflevector (<vscale x 1 x i1> insertelement (<vscale x 1 x i1> undef, i1 true, i32 0), <vscale x 1 x i1> undef, <vscale x 1 x i32> zeroinitializer)
  %vp.op = call <vscale x 1 x double> @llvm.vp.fmul.nxv1f64(<vscale x 1 x double> %vp.op.load30, <vscale x 1 x double> %vp.op.load33, metadata !"round.tonearest", metadata !"fpexcept.ignore", <vscale x 1 x i1> %10, i32 %4)
  %11 = getelementptr inbounds double, double* %d, i64 %index
  %12 = bitcast double* %11 to <vscale x 1 x double>*
  call void @llvm.vp.store.nxv1f64.p0nxv1f64(<vscale x 1 x double> %vp.op, <vscale x 1 x double>* %12, i32 8, <vscale x 1 x i1> %10, i32 %4)
  %vp.op36 = call <vscale x 1 x double> @llvm.vp.fadd.nxv1f64(<vscale x 1 x double> %vp.op.load30, <vscale x 1 x double> %vp.op.load33, metadata !"round.tonearest", metadata !"fpexcept.ignore", <vscale x 1 x i1> %5, i32 %4)
  call void @llvm.vp.store.nxv1f64.p0nxv1f64(<vscale x 1 x double> %vp.op36, <vscale x 1 x double>* %3, i32 8, <vscale x 1 x i1> %5, i32 %4)
  %index.next = add i64 %index, %2
  %13 = icmp eq i64 %index.next, %wide.trip.count
  br i1 %13, label %for.cond.cleanup, label %vector.body

for.cond.cleanup:                                 ; preds = %vector.body, %entry
  ret void
}

; Function Attrs: nounwind readnone
declare i64 @llvm.epi.vsetvl(i64, i64, i64) #1

; Function Attrs: argmemonly nosync nounwind readonly willreturn
declare <vscale x 1 x double> @llvm.vp.load.nxv1f64.p0nxv1f64(<vscale x 1 x double>* nocapture, i32 immarg, <vscale x 1 x i1>, i32) #2

; Function Attrs: nosync nounwind readnone willreturn
declare <vscale x 1 x i1> @llvm.vp.fcmp.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, i8 immarg, <vscale x 1 x i1>, i32) #3

; Function Attrs: nounwind readnone willreturn
declare <vscale x 1 x double> @llvm.vp.fmul.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, metadata, metadata, <vscale x 1 x i1>, i32) #4

; Function Attrs: argmemonly nosync nounwind willreturn writeonly
declare void @llvm.vp.store.nxv1f64.p0nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>* nocapture, i32 immarg, <vscale x 1 x i1>, i32) #5

; Function Attrs: nounwind readnone willreturn
declare <vscale x 1 x double> @llvm.vp.fadd.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>, metadata, metadata, <vscale x 1 x i1>, i32) #4
