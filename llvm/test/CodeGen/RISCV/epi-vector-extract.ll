; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v,+f,+d -target-abi lp64d \
; RUN:    -verify-machineinstrs < %s | FileCheck %s

define i64 @extract_nxv1i64(<vscale x 1 x i64> %v, i64 %idx)
; CHECK-LABEL: extract_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 1 x i64> %v, i64 %idx
  %first = extractelement <vscale x 1 x i64> %v, i64 0
  %res = add i64 %elem, %first
  ret i64 %res
}

define i32 @extract_nxv2i32(<vscale x 2 x i32> %v, i64 %idx)
; CHECK-LABEL: extract_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    addw a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 2 x i32> %v, i64 %idx
  %first = extractelement <vscale x 2 x i32> %v, i64 0
  %res = add i32 %elem, %first
  ret i32 %res
}

define i16 @extract_nxv4i16(<vscale x 4 x i16> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x i16> %v, i64 %idx
  %first = extractelement <vscale x 4 x i16> %v, i64 0
  %res = add i16 %elem, %first
  ret i16 %res
}

define i8 @extract_nxv8i8(<vscale x 8 x i8> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x i8> %v, i64 %idx
  %first = extractelement <vscale x 8 x i8> %v, i64 0
  %res = add i8 %elem, %first
  ret i8 %res
}

define i64 @extract_nxv2i64(<vscale x 2 x i64> %v, i64 %idx)
; CHECK-LABEL: extract_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 2 x i64> %v, i64 %idx
  %first = extractelement <vscale x 2 x i64> %v, i64 0
  %res = add i64 %elem, %first
  ret i64 %res
}

define i32 @extract_nxv4i32(<vscale x 4 x i32> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m2
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    addw a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x i32> %v, i64 %idx
  %first = extractelement <vscale x 4 x i32> %v, i64 0
  %res = add i32 %elem, %first
  ret i32 %res
}

define i16 @extract_nxv8i16(<vscale x 8 x i16> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m2
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x i16> %v, i64 %idx
  %first = extractelement <vscale x 8 x i16> %v, i64 0
  %res = add i16 %elem, %first
  ret i16 %res
}

define i8 @extract_nxv16i8(<vscale x 16 x i8> %v, i64 %idx)
; CHECK-LABEL: extract_nxv16i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 16 x i8> %v, i64 %idx
  %first = extractelement <vscale x 16 x i8> %v, i64 0
  %res = add i8 %elem, %first
  ret i8 %res
}

define i64 @extract_nxv4i64(<vscale x 4 x i64> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x i64> %v, i64 %idx
  %first = extractelement <vscale x 4 x i64> %v, i64 0
  %res = add i64 %elem, %first
  ret i64 %res
}

define i32 @extract_nxv8i32(<vscale x 8 x i32> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m4
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    addw a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x i32> %v, i64 %idx
  %first = extractelement <vscale x 8 x i32> %v, i64 0
  %res = add i32 %elem, %first
  ret i32 %res
}

define i16 @extract_nxv16i16(<vscale x 16 x i16> %v, i64 %idx)
; CHECK-LABEL: extract_nxv16i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m4
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 16 x i16> %v, i64 %idx
  %first = extractelement <vscale x 16 x i16> %v, i64 0
  %res = add i16 %elem, %first
  ret i16 %res
}

define i8 @extract_nxv32i8(<vscale x 32 x i8> %v, i64 %idx)
; CHECK-LABEL: extract_nxv32i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 32 x i8> %v, i64 %idx
  %first = extractelement <vscale x 32 x i8> %v, i64 0
  %res = add i8 %elem, %first
  ret i8 %res
}

define i64 @extract_nxv8i64(<vscale x 8 x i64> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x i64> %v, i64 %idx
  %first = extractelement <vscale x 8 x i64> %v, i64 0
  %res = add i64 %elem, %first
  ret i64 %res
}

define i32 @extract_nxv16i32(<vscale x 16 x i32> %v, i64 %idx)
; CHECK-LABEL: extract_nxv16i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m8
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    addw a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 16 x i32> %v, i64 %idx
  %first = extractelement <vscale x 16 x i32> %v, i64 0
  %res = add i32 %elem, %first
  ret i32 %res
}

define i16 @extract_nxv32i16(<vscale x 32 x i16> %v, i64 %idx)
; CHECK-LABEL: extract_nxv32i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m8
; CHECK-NEXT:    vext.x.v a0, v16, a0
; CHECK-NEXT:    vmv.x.s a1, v16
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 32 x i16> %v, i64 %idx
  %first = extractelement <vscale x 32 x i16> %v, i64 0
  %res = add i16 %elem, %first
  ret i16 %res
}

; FIXME nxv64i8 unsupported
;define i8 @extract_nxv64i8(<vscale x 64 x i8> %v, i64 %idx)
;{
;  %elem = extractelement <vscale x 64 x i8> %v, i64 %idx
;  %first = extractelement <vscale x 64 x i8> %v, i64 0
;  %res = add i8 %elem, %first
;  ret i8 %res
;}

define double @extract_nxv1f64(<vscale x 1 x double> %v, i64 %idx)
; CHECK-LABEL: extract_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vslidedown.vx v1, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v1
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.d fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 1 x double> %v, i64 %idx
  %first = extractelement <vscale x 1 x double> %v, i64 0
  %res = fadd double %elem, %first
  ret double %res
}

define float @extract_nxv2f32(<vscale x 2 x float> %v, i64 %idx)
; CHECK-LABEL: extract_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vslidedown.vx v1, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v1
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.s fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 2 x float> %v, i64 %idx
  %first = extractelement <vscale x 2 x float> %v, i64 0
  %res = fadd float %elem, %first
  ret float %res
}

define double @extract_nxv2f64(<vscale x 2 x double> %v, i64 %idx)
; CHECK-LABEL: extract_nxv2f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vslidedown.vx v2, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v2
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.d fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 2 x double> %v, i64 %idx
  %first = extractelement <vscale x 2 x double> %v, i64 0
  %res = fadd double %elem, %first
  ret double %res
}

define float @extract_nxv4f32(<vscale x 4 x float> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m2
; CHECK-NEXT:    vslidedown.vx v2, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v2
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.s fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x float> %v, i64 %idx
  %first = extractelement <vscale x 4 x float> %v, i64 0
  %res = fadd float %elem, %first
  ret float %res
}

define double @extract_nxv4f64(<vscale x 4 x double> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vslidedown.vx v4, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v4
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.d fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x double> %v, i64 %idx
  %first = extractelement <vscale x 4 x double> %v, i64 0
  %res = fadd double %elem, %first
  ret double %res
}

define float @extract_nxv8f32(<vscale x 8 x float> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m4
; CHECK-NEXT:    vslidedown.vx v4, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v4
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.s fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x float> %v, i64 %idx
  %first = extractelement <vscale x 8 x float> %v, i64 0
  %res = fadd float %elem, %first
  ret float %res
}

define double @extract_nxv8f64(<vscale x 8 x double> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vslidedown.vx v8, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v8
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.d fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x double> %v, i64 %idx
  %first = extractelement <vscale x 8 x double> %v, i64 0
  %res = fadd double %elem, %first
  ret double %res
}

define float @extract_nxv16f32(<vscale x 16 x float> %v, i64 %idx)
; CHECK-LABEL: extract_nxv16f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m8
; CHECK-NEXT:    vslidedown.vx v8, v16, a0
; CHECK-NEXT:    vfmv.f.s ft0, v8
; CHECK-NEXT:    vfmv.f.s ft1, v16
; CHECK-NEXT:    fadd.s fa0, ft0, ft1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 16 x float> %v, i64 %idx
  %first = extractelement <vscale x 16 x float> %v, i64 0
  %res = fadd float %elem, %first
  ret float %res
}

define i1 @extract_nxv1i1(<vscale x 1 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv1i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vext.x.v a0, v0, a0
; CHECK-NEXT:    vmv.x.s a1, v0
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 1 x i1> %v, i64 %idx
  %first = extractelement <vscale x 1 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

define i1 @extract_nxv2i1(<vscale x 2 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv2i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vext.x.v a0, v0, a0
; CHECK-NEXT:    vmv.x.s a1, v0
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 2 x i1> %v, i64 %idx
  %first = extractelement <vscale x 2 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

define i1 @extract_nxv4i1(<vscale x 4 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv4i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vext.x.v a0, v0, a0
; CHECK-NEXT:    vmv.x.s a1, v0
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 4 x i1> %v, i64 %idx
  %first = extractelement <vscale x 4 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

define i1 @extract_nxv8i1(<vscale x 8 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv8i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vext.x.v a0, v0, a0
; CHECK-NEXT:    vmv.x.s a1, v0
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 8 x i1> %v, i64 %idx
  %first = extractelement <vscale x 8 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

define i1 @extract_nxv16i1(<vscale x 16 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv16i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vmv.v.i v2, 0
; CHECK-NEXT:    vmerge.vim v2, v2, 1, v0
; CHECK-NEXT:    vext.x.v a0, v2, a0
; CHECK-NEXT:    vmv.x.s a1, v2
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 16 x i1> %v, i64 %idx
  %first = extractelement <vscale x 16 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

define i1 @extract_nxv32i1(<vscale x 32 x i1> %v, i64 %idx)
; CHECK-LABEL: extract_nxv32i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vmv.v.i v4, 0
; CHECK-NEXT:    vmerge.vim v4, v4, 1, v0
; CHECK-NEXT:    vext.x.v a0, v4, a0
; CHECK-NEXT:    vmv.x.s a1, v4
; CHECK-NEXT:    add a0, a0, a1
; CHECK-NEXT:    ret
{
  %elem = extractelement <vscale x 32 x i1> %v, i64 %idx
  %first = extractelement <vscale x 32 x i1> %v, i64 0
  %res = add i1 %elem, %first
  ret i1 %res
}

; FIXME: Enable when nxv1i8 is supported.
;define i1 @extract_nxv64i1(<vscale x 64 x i1> %v, i64 %idx)
;{
;  %elem = extractelement <vscale x 64 x i1> %v, i64 %idx
;  %first = extractelement <vscale x 64 x i1> %v, i64 0
;  %res = add i1 %elem, %first
;  ret i1 %res
;}
