; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple riscv64 -mattr=+f,+d,+experimental-v < %s | FileCheck %s

define <vscale x 1 x double> @pow_nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b) nounwind {
; CHECK-LABEL: pow_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv1f64
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 1 x double> @llvm.pow.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b)
  ret <vscale x 1 x double> %1
}

define <vscale x 2 x double> @pow_nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b) nounwind {
; CHECK-LABEL: pow_nxv2f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv2f64
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 2 x double> @llvm.pow.nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b)
  ret <vscale x 2 x double> %1
}

define <vscale x 4 x double> @pow_nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b) nounwind {
; CHECK-LABEL: pow_nxv4f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv4f64
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 4 x double> @llvm.pow.nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b)
  ret <vscale x 4 x double> %1
}

define <vscale x 8 x double> @pow_nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b) nounwind {
; CHECK-LABEL: pow_nxv8f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a1, 3
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    ld a0, -56(s0)
; CHECK-NEXT:    vse.v v8, (a0)
; CHECK-NEXT:    call __epi_pow_nxv8f64
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %1 = call <vscale x 8 x double> @llvm.pow.nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b)
  ret <vscale x 8 x double> %1
}

define <vscale x 1 x float> @pow_nxv1f32(<vscale x 1 x float> %a, <vscale x 1 x float> %b) nounwind {
; CHECK-LABEL: pow_nxv1f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv2f32
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 1 x float> @llvm.pow.nxv1f32(<vscale x 1 x float> %a, <vscale x 1 x float> %b)
  ret <vscale x 1 x float> %1
}

define <vscale x 2 x float> @pow_nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b) nounwind {
; CHECK-LABEL: pow_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv2f32
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 2 x float> @llvm.pow.nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b)
  ret <vscale x 2 x float> %1
}

define <vscale x 4 x float> @pow_nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b) nounwind {
; CHECK-LABEL: pow_nxv4f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv4f32
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 4 x float> @llvm.pow.nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b)
  ret <vscale x 4 x float> %1
}

define <vscale x 8 x float> @pow_nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b) nounwind {
; CHECK-LABEL: pow_nxv8f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    sd ra, 8(sp) # 8-byte Folded Spill
; CHECK-NEXT:    call __epi_pow_nxv8f32
; CHECK-NEXT:    ld ra, 8(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    ret
  %1 = call <vscale x 8 x float> @llvm.pow.nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b)
  ret <vscale x 8 x float> %1
}

define <vscale x 16 x float> @pow_nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b) nounwind {
; CHECK-LABEL: pow_nxv16f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a1, 3
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    vsetvli zero, zero, e32,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    ld a0, -56(s0)
; CHECK-NEXT:    vse.v v8, (a0)
; CHECK-NEXT:    call __epi_pow_nxv16f32
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %1 = call <vscale x 16 x float> @llvm.pow.nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b)
  ret <vscale x 16 x float> %1
}

declare <vscale x 1 x double> @llvm.pow.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>)
declare <vscale x 2 x double> @llvm.pow.nxv2f64(<vscale x 2 x double>, <vscale x 2 x double>)
declare <vscale x 4 x double> @llvm.pow.nxv4f64(<vscale x 4 x double>, <vscale x 4 x double>)
declare <vscale x 8 x double> @llvm.pow.nxv8f64(<vscale x 8 x double>, <vscale x 8 x double>)
declare <vscale x 1 x float> @llvm.pow.nxv1f32(<vscale x 1 x float>, <vscale x 1 x float>)
declare <vscale x 2 x float> @llvm.pow.nxv2f32(<vscale x 2 x float>, <vscale x 2 x float>)
declare <vscale x 4 x float> @llvm.pow.nxv4f32(<vscale x 4 x float>, <vscale x 4 x float>)
declare <vscale x 8 x float> @llvm.pow.nxv8f32(<vscale x 8 x float>, <vscale x 8 x float>)
declare <vscale x 16 x float> @llvm.pow.nxv16f32(<vscale x 16 x float>, <vscale x 16 x float>)
