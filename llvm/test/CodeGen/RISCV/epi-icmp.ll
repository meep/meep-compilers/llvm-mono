; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs < %s | FileCheck %s

define <vscale x 8 x i1> @test_eq_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_eq_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmseq.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_eq_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_eq_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_eq_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_eq_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ne_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_ne_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsne.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp ne <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ne_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ne_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsne.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ne <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ne_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ne_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsne.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ne <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ne_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ne_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsne.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ne <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ne_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ne_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsne.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ne <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ugt_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_ugt_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsltu.vv v0, v17, v16
; CHECK-NEXT:    ret
  %vc = icmp ugt <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ugt_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ugt_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsltu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ugt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ugt_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ugt_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsltu.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ugt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ugt_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ugt_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsltu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ugt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ugt_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ugt_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsltu.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ugt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_uge_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_uge_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsleu.vv v0, v17, v16
; CHECK-NEXT:    ret
  %vc = icmp uge <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_uge_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_uge_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsleu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp uge <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_uge_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_uge_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsleu.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp uge <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_uge_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_uge_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsleu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp uge <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_uge_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_uge_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsleu.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp uge <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ult_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_ult_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsltu.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp ult <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ult_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ult_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsltu.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ult <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ult_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ult_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsltu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ult <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ult_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ult_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi a0, zero, 5
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsltu.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ult <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ult_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ult_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsltu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ult <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ule_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_ule_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsleu.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp ule <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ule_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ule_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsleu.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ule <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ule_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_ule_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsleu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ule <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ule_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ule_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsleu.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ule <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_ule_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_ule_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsleu.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp ule <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sgt_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_sgt_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmslt.vv v0, v17, v16
; CHECK-NEXT:    ret
  %vc = icmp sgt <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sgt_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sgt_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmslt.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sgt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sgt_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sgt_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmslt.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sgt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sgt_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sgt_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmslt.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sgt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sgt_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sgt_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmslt.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sgt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sge_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_sge_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsle.vv v0, v17, v16
; CHECK-NEXT:    ret
  %vc = icmp sge <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sge_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sge_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsle.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sge <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sge_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sge_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsle.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sge <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sge_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sge_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsle.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sge <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sge_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sge_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsle.vv v0, v16, v1
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sge <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_slt_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_slt_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmslt.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp slt <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_slt_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_slt_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmslt.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp slt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_slt_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_slt_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmslt.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp slt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_slt_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_slt_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi a0, zero, 5
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmslt.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp slt <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_slt_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_slt_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmslt.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp slt <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sle_vv_nxv8i8(<vscale x 8 x i8> %va, <vscale x 8 x i8> %vb) {
; CHECK-LABEL: test_sle_vv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsle.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp sle <vscale x 8 x i8> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sle_vx_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sle_vx_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsle.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sle <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sle_xv_nxv8i8(<vscale x 8 x i8> %va, i8 %b) {
; CHECK-LABEL: test_sle_xv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmsle.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 %b, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sle <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sle_vi_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sle_vi_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmsle.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sle <vscale x 8 x i8> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_sle_iv_nxv8i8(<vscale x 8 x i8> %va) {
; CHECK-LABEL: test_sle_iv_nxv8i8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmsle.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i8> undef, i8 5, i32 0
  %splat = shufflevector <vscale x 8 x i8> %head, <vscale x 8 x i8> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp sle <vscale x 8 x i8> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vv_nxv4i16(<vscale x 4 x i16> %va, <vscale x 4 x i16> %vb) {
; CHECK-LABEL: test_eq_vv_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmseq.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 4 x i16> %va, %vb
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vx_nxv4i16(<vscale x 4 x i16> %va, i16 %b) {
; CHECK-LABEL: test_eq_vx_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i16> undef, i16 %b, i32 0
  %splat = shufflevector <vscale x 4 x i16> %head, <vscale x 4 x i16> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i16> %va, %splat
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_xv_nxv4i16(<vscale x 4 x i16> %va, i16 %b) {
; CHECK-LABEL: test_eq_xv_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i16> undef, i16 %b, i32 0
  %splat = shufflevector <vscale x 4 x i16> %head, <vscale x 4 x i16> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i16> %splat, %va
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vi_nxv4i16(<vscale x 4 x i16> %va) {
; CHECK-LABEL: test_eq_vi_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i16> undef, i16 5, i32 0
  %splat = shufflevector <vscale x 4 x i16> %head, <vscale x 4 x i16> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i16> %va, %splat
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_iv_nxv4i16(<vscale x 4 x i16> %va) {
; CHECK-LABEL: test_eq_iv_nxv4i16:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i16> undef, i16 5, i32 0
  %splat = shufflevector <vscale x 4 x i16> %head, <vscale x 4 x i16> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i16> %splat, %va
  ret <vscale x 4 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vv_nxv2i32(<vscale x 2 x i32> %va, <vscale x 2 x i32> %vb) {
; CHECK-LABEL: test_eq_vv_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmseq.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 2 x i32> %va, %vb
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vx_nxv2i32(<vscale x 2 x i32> %va, i32 %b) {
; CHECK-LABEL: test_eq_vx_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i32> undef, i32 %b, i32 0
  %splat = shufflevector <vscale x 2 x i32> %head, <vscale x 2 x i32> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i32> %va, %splat
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_xv_nxv2i32(<vscale x 2 x i32> %va, i32 %b) {
; CHECK-LABEL: test_eq_xv_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i32> undef, i32 %b, i32 0
  %splat = shufflevector <vscale x 2 x i32> %head, <vscale x 2 x i32> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i32> %splat, %va
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vi_nxv2i32(<vscale x 2 x i32> %va) {
; CHECK-LABEL: test_eq_vi_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i32> undef, i32 5, i32 0
  %splat = shufflevector <vscale x 2 x i32> %head, <vscale x 2 x i32> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i32> %va, %splat
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_iv_nxv2i32(<vscale x 2 x i32> %va) {
; CHECK-LABEL: test_eq_iv_nxv2i32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i32> undef, i32 5, i32 0
  %splat = shufflevector <vscale x 2 x i32> %head, <vscale x 2 x i32> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i32> %splat, %va
  ret <vscale x 2 x i1> %vc
}

define <vscale x 1 x i1> @test_eq_vv_nxv1i64(<vscale x 1 x i64> %va, <vscale x 1 x i64> %vb) {
; CHECK-LABEL: test_eq_vv_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmseq.vv v0, v16, v17
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 1 x i64> %va, %vb
  ret <vscale x 1 x i1> %vc
}

define <vscale x 1 x i1> @test_eq_vx_nxv1i64(<vscale x 1 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_vx_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
  %vc = icmp eq <vscale x 1 x i64> %va, %splat
  ret <vscale x 1 x i1> %vc
}

define <vscale x 1 x i1> @test_eq_xv_nxv1i64(<vscale x 1 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_xv_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.x v1, a0
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
  %vc = icmp eq <vscale x 1 x i64> %splat, %va
  ret <vscale x 1 x i1> %vc
}

define <vscale x 1 x i1> @test_eq_vi_nxv1i64(<vscale x 1 x i64> %va) {
; CHECK-LABEL: test_eq_vi_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
  %vc = icmp eq <vscale x 1 x i64> %va, %splat
  ret <vscale x 1 x i1> %vc
}

define <vscale x 1 x i1> @test_eq_iv_nxv1i64(<vscale x 1 x i64> %va) {
; CHECK-LABEL: test_eq_iv_nxv1i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.i v1, 5
; CHECK-NEXT:    vmseq.vv v0, v1, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 1 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer
  %vc = icmp eq <vscale x 1 x i64> %splat, %va
  ret <vscale x 1 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vv_nxv2i64(<vscale x 2 x i64> %va, <vscale x 2 x i64> %vb) {
; CHECK-LABEL: test_eq_vv_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vmseq.vv v0, v16, v18
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 2 x i64> %va, %vb
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vx_nxv2i64(<vscale x 2 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_vx_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 2 x i64> %head, <vscale x 2 x i64> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i64> %va, %splat
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_xv_nxv2i64(<vscale x 2 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_xv_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vmv.v.x v2, a0
; CHECK-NEXT:    vmseq.vv v0, v2, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 2 x i64> %head, <vscale x 2 x i64> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i64> %splat, %va
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_vi_nxv2i64(<vscale x 2 x i64> %va) {
; CHECK-LABEL: test_eq_vi_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 2 x i64> %head, <vscale x 2 x i64> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i64> %va, %splat
  ret <vscale x 2 x i1> %vc
}

define <vscale x 2 x i1> @test_eq_iv_nxv2i64(<vscale x 2 x i64> %va) {
; CHECK-LABEL: test_eq_iv_nxv2i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vmv.v.i v2, 5
; CHECK-NEXT:    vmseq.vv v0, v2, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 2 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 2 x i64> %head, <vscale x 2 x i64> undef, <vscale x 2 x i32> zeroinitializer
  %vc = icmp eq <vscale x 2 x i64> %splat, %va
  ret <vscale x 2 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vv_nxv4i64(<vscale x 4 x i64> %va, <vscale x 4 x i64> %vb) {
; CHECK-LABEL: test_eq_vv_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmseq.vv v0, v16, v20
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 4 x i64> %va, %vb
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vx_nxv4i64(<vscale x 4 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_vx_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 4 x i64> %head, <vscale x 4 x i64> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i64> %va, %splat
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_xv_nxv4i64(<vscale x 4 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_xv_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmv.v.x v4, a0
; CHECK-NEXT:    vmseq.vv v0, v4, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 4 x i64> %head, <vscale x 4 x i64> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i64> %splat, %va
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_vi_nxv4i64(<vscale x 4 x i64> %va) {
; CHECK-LABEL: test_eq_vi_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 4 x i64> %head, <vscale x 4 x i64> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i64> %va, %splat
  ret <vscale x 4 x i1> %vc
}

define <vscale x 4 x i1> @test_eq_iv_nxv4i64(<vscale x 4 x i64> %va) {
; CHECK-LABEL: test_eq_iv_nxv4i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmv.v.i v4, 5
; CHECK-NEXT:    vmseq.vv v0, v4, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 4 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 4 x i64> %head, <vscale x 4 x i64> undef, <vscale x 4 x i32> zeroinitializer
  %vc = icmp eq <vscale x 4 x i64> %splat, %va
  ret <vscale x 4 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_vv_nxv8i64(<vscale x 8 x i64> %va, <vscale x 8 x i64> %vb) {
; CHECK-LABEL: test_eq_vv_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    vmseq.vv v0, v16, v8
; CHECK-NEXT:    ret
  %vc = icmp eq <vscale x 8 x i64> %va, %vb
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_vx_nxv8i64(<vscale x 8 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_vx_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vmseq.vx v0, v16, a0
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 8 x i64> %head, <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i64> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_xv_nxv8i64(<vscale x 8 x i64> %va, i64 %b) {
; CHECK-LABEL: test_eq_xv_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vmv.v.x v8, a0
; CHECK-NEXT:    vmseq.vv v0, v8, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 8 x i64> %head, <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i64> %splat, %va
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_vi_nxv8i64(<vscale x 8 x i64> %va) {
; CHECK-LABEL: test_eq_vi_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vmseq.vi v0, v16, 5
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 8 x i64> %head, <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i64> %va, %splat
  ret <vscale x 8 x i1> %vc
}

define <vscale x 8 x i1> @test_eq_iv_nxv8i64(<vscale x 8 x i64> %va) {
; CHECK-LABEL: test_eq_iv_nxv8i64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vmv.v.i v8, 5
; CHECK-NEXT:    vmseq.vv v0, v8, v16
; CHECK-NEXT:    ret
  %head = insertelement <vscale x 8 x i64> undef, i64 5, i32 0
  %splat = shufflevector <vscale x 8 x i64> %head, <vscale x 8 x i64> undef, <vscale x 8 x i32> zeroinitializer
  %vc = icmp eq <vscale x 8 x i64> %splat, %va
  ret <vscale x 8 x i1> %vc
}
