; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple riscv64 -mattr=+experimental-v < %s | FileCheck %s

@scratch = global i8 0, align 16

define void @nxv1i1(<vscale x 1 x i1> %a, <vscale x 1 x i1> %b) nounwind {
; CHECK-LABEL: nxv1i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmand.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmxor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %log_1 = and <vscale x 1 x i1> %a, %b
  %val_1 = zext <vscale x 1 x i1> %log_1 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %val_1, <vscale x 1 x i64>* %store_addr

  %log_2 = or <vscale x 1 x i1> %a, %b
  %val_2 = zext <vscale x 1 x i1> %log_2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %val_2, <vscale x 1 x i64>* %store_addr

  %log_3 = xor <vscale x 1 x i1> %a, %b
  %val_3 = zext <vscale x 1 x i1> %log_3 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %val_3, <vscale x 1 x i64>* %store_addr

  ret void
}

define void @nxv2i1(<vscale x 2 x i1> %a, <vscale x 2 x i1> %b) nounwind {
; CHECK-LABEL: nxv2i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmand.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vmxor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 2 x i32>*

  %log_1 = and <vscale x 2 x i1> %a, %b
  %val_1 = zext <vscale x 2 x i1> %log_1 to <vscale x 2 x i32>
  store <vscale x 2 x i32> %val_1, <vscale x 2 x i32>* %store_addr

  %log_2 = or <vscale x 2 x i1> %a, %b
  %val_2 = zext <vscale x 2 x i1> %log_2 to <vscale x 2 x i32>
  store <vscale x 2 x i32> %val_2, <vscale x 2 x i32>* %store_addr

  %log_3 = xor <vscale x 2 x i1> %a, %b
  %val_3 = zext <vscale x 2 x i1> %log_3 to <vscale x 2 x i32>
  store <vscale x 2 x i32> %val_3, <vscale x 2 x i32>* %store_addr

  ret void
}

define void @nxv4i1(<vscale x 4 x i1> %a, <vscale x 4 x i1> %b) nounwind {
; CHECK-LABEL: nxv4i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmand.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vsetvli zero, zero, e16,m1
; CHECK-NEXT:    vmxor.mm v1, v0, v16
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 4 x i16>*

  %log_1 = and <vscale x 4 x i1> %a, %b
  %val_1 = zext <vscale x 4 x i1> %log_1 to <vscale x 4 x i16>
  store <vscale x 4 x i16> %val_1, <vscale x 4 x i16>* %store_addr

  %log_2 = or <vscale x 4 x i1> %a, %b
  %val_2 = zext <vscale x 4 x i1> %log_2 to <vscale x 4 x i16>
  store <vscale x 4 x i16> %val_2, <vscale x 4 x i16>* %store_addr

  %log_3 = xor <vscale x 4 x i1> %a, %b
  %val_3 = zext <vscale x 4 x i1> %log_3 to <vscale x 4 x i16>
  store <vscale x 4 x i16> %val_3, <vscale x 4 x i16>* %store_addr

  ret void
}

define void @nxv8i1(<vscale x 8 x i1> %a, <vscale x 8 x i1> %b) nounwind {
; CHECK-LABEL: nxv8i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vmand.mm v1, v0, v16
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vmor.mm v1, v0, v16
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    vmxor.mm v1, v0, v16
; CHECK-NEXT:    vse.v v1, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 8 x i8>*

  %log_1 = and <vscale x 8 x i1> %a, %b
  %val_1 = zext <vscale x 8 x i1> %log_1 to <vscale x 8 x i8>
  store <vscale x 8 x i8> %val_1, <vscale x 8 x i8>* %store_addr

  %log_2 = or <vscale x 8 x i1> %a, %b
  %val_2 = zext <vscale x 8 x i1> %log_2 to <vscale x 8 x i8>
  store <vscale x 8 x i8> %val_2, <vscale x 8 x i8>* %store_addr

  %log_3 = xor <vscale x 8 x i1> %a, %b
  %val_3 = zext <vscale x 8 x i1> %log_3 to <vscale x 8 x i8>
  store <vscale x 8 x i8> %val_3, <vscale x 8 x i8>* %store_addr

  ret void
}

define void @nxv16i1(<vscale x 16 x i1> %a, <vscale x 16 x i1> %b) nounwind {
; CHECK-LABEL: nxv16i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v1, v0
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e8,m2
; CHECK-NEXT:    vmand.mm v0, v0, v16
; CHECK-NEXT:    vmv.v.i v2, 0
; CHECK-NEXT:    vmerge.vim v4, v2, 1, v0
; CHECK-NEXT:    vmor.mm v0, v1, v16
; CHECK-NEXT:    vse.v v4, (a0)
; CHECK-NEXT:    vmerge.vim v4, v2, 1, v0
; CHECK-NEXT:    vmxor.mm v0, v1, v16
; CHECK-NEXT:    vse.v v4, (a0)
; CHECK-NEXT:    vmerge.vim v2, v2, 1, v0
; CHECK-NEXT:    vse.v v2, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 16 x i8>*

  %log_1 = and <vscale x 16 x i1> %a, %b
  %val_1 = zext <vscale x 16 x i1> %log_1 to <vscale x 16 x i8>
  store <vscale x 16 x i8> %val_1, <vscale x 16 x i8>* %store_addr

  %log_2 = or <vscale x 16 x i1> %a, %b
  %val_2 = zext <vscale x 16 x i1> %log_2 to <vscale x 16 x i8>
  store <vscale x 16 x i8> %val_2, <vscale x 16 x i8>* %store_addr

  %log_3 = xor <vscale x 16 x i1> %a, %b
  %val_3 = zext <vscale x 16 x i1> %log_3 to <vscale x 16 x i8>
  store <vscale x 16 x i8> %val_3, <vscale x 16 x i8>* %store_addr

  ret void
}

define void @nxv32i1(<vscale x 32 x i1> %a, <vscale x 32 x i1> %b) nounwind {
; CHECK-LABEL: nxv32i1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v1, v0
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    lui a0, %hi(scratch)
; CHECK-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-NEXT:    vsetvli zero, zero, e8,m4
; CHECK-NEXT:    vmand.mm v0, v0, v16
; CHECK-NEXT:    vmv.v.i v4, 0
; CHECK-NEXT:    vmerge.vim v20, v4, 1, v0
; CHECK-NEXT:    vmor.mm v0, v1, v16
; CHECK-NEXT:    vse.v v20, (a0)
; CHECK-NEXT:    vmerge.vim v20, v4, 1, v0
; CHECK-NEXT:    vmxor.mm v0, v1, v16
; CHECK-NEXT:    vse.v v20, (a0)
; CHECK-NEXT:    vmerge.vim v4, v4, 1, v0
; CHECK-NEXT:    vse.v v4, (a0)
; CHECK-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 32 x i8>*

  %log_1 = and <vscale x 32 x i1> %a, %b
  %val_1 = zext <vscale x 32 x i1> %log_1 to <vscale x 32 x i8>
  store <vscale x 32 x i8> %val_1, <vscale x 32 x i8>* %store_addr

  %log_2 = or <vscale x 32 x i1> %a, %b
  %val_2 = zext <vscale x 32 x i1> %log_2 to <vscale x 32 x i8>
  store <vscale x 32 x i8> %val_2, <vscale x 32 x i8>* %store_addr

  %log_3 = xor <vscale x 32 x i1> %a, %b
  %val_3 = zext <vscale x 32 x i1> %log_3 to <vscale x 32 x i8>
  store <vscale x 32 x i8> %val_3, <vscale x 32 x i8>* %store_addr

  ret void
}

; FIXME enable when nxv64i8 is supported

;define void @nxv64i1(<vscale x 64 x i1> %a, <vscale x 64 x i1> %b) nounwind {
;  %store_addr = bitcast i8* @scratch to <vscale x 64 x i8>*
;
;  %log_1 = and <vscale x 64 x i1> %a, %b
;  %val_1 = zext <vscale x 64 x i1> %log_1 to <vscale x 64 x i8>
;  store <vscale x 64 x i8> %val_1, <vscale x 64 x i8>* %store_addr
;
;  %log_2 = or <vscale x 64 x i1> %a, %b
;  %val_2 = zext <vscale x 64 x i1> %log_2 to <vscale x 64 x i8>
;  store <vscale x 64 x i8> %val_2, <vscale x 64 x i8>* %store_addr
;
;  %log_3 = xor <vscale x 64 x i1> %a, %b
;  %val_3 = zext <vscale x 64 x i1> %log_2 to <vscale x 64 x i8>
;  store <vscale x 64 x i8> %val_3, <vscale x 64 x i8>* %store_addr
;
;  ret void
;}
