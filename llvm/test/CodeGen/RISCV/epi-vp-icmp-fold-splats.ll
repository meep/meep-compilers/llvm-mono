; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O0 \
; RUN:    < %s | FileCheck --check-prefix=CHECK-O0 %s
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs -O2 \
; RUN:    < %s | FileCheck --check-prefix=CHECK-O2 %s

@scratch = global i8 0, align 16

define void @test_vp_fold_unsigned_greater(<vscale x 1 x i64> %a, i64 %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_unsigned_greater:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a2, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a2
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    mv a2, a1
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a1
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a1, a3
; CHECK-O0-NEXT:    mv a1, a0
; CHECK-O0-NEXT:    # kill: def $x10 killed $x12
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vmv.v.x v2, a1
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmsgtu.vx v1, v16, a1, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmsltu.vx v1, v16, a1, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmsleu.vv v1, v2, v16, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmsleu.vx v1, v16, a1, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_unsigned_greater:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a2, %hi(scratch)
; CHECK-O2-NEXT:    addi a2, a2, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O2-NEXT:    vmv.v.x v1, a0
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsgtu.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsltu.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsleu.vv v1, v1, v16, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsleu.vx v1, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer

  ; x > splat(y) → vmsgtu.vx x, y
  %ugt.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 34, <vscale x 1 x i1> %m, i32 %n)
  %ugt.1 = zext <vscale x 1 x i1> %ugt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ugt.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) > x → x < splat(y) → vmsltu.vx x, y
  %ugt.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 34, <vscale x 1 x i1> %m, i32 %n)
  %ugt.3 = zext <vscale x 1 x i1> %ugt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ugt.3, <vscale x 1 x i64>* %store_addr

  ; x >= splat(y) → cannot be folded (vmsleu.vv)
  %uge.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 35, <vscale x 1 x i1> %m, i32 %n)
  %uge.1 = zext <vscale x 1 x i1> %uge.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %uge.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) >= x → x <= splat(y) → vmsleu.vx x, y
  %uge.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 35, <vscale x 1 x i1> %m, i32 %n)
  %uge.3 = zext <vscale x 1 x i1> %uge.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %uge.3, <vscale x 1 x i64>* %store_addr

  ret void
}

define void @test_vp_fold_unsigned_lower(<vscale x 1 x i64> %a, i64 %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_unsigned_lower:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a2, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a2
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    mv a2, a0
; CHECK-O0-NEXT:    # kill: def $x10 killed $x11
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vmv.v.x v2, a2
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsltu.vx v1, v16, a2, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsgtu.vx v1, v16, a2, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsleu.vx v1, v16, a2, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsleu.vv v1, v2, v16, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_unsigned_lower:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a2, %hi(scratch)
; CHECK-O2-NEXT:    addi a2, a2, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O2-NEXT:    vmv.v.x v1, a0
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsltu.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsgtu.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsleu.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsleu.vv v1, v1, v16, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer

  ; x < splat(y) → vmsltu.vx x, y
  %ult.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 36, <vscale x 1 x i1> %m, i32 %n)
  %ult.1 = zext <vscale x 1 x i1> %ult.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ult.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) < x → x > splat(y) → vmsgtu.vx x, y
  %ult.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 36, <vscale x 1 x i1> %m, i32 %n)
  %ult.3 = zext <vscale x 1 x i1> %ult.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ult.3, <vscale x 1 x i64>* %store_addr

  ; x <= splat(y) → vmsleu.vx x, y
  %ule.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 37, <vscale x 1 x i1> %m, i32 %n)
  %ule.1 = zext <vscale x 1 x i1> %ule.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ule.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) <= y → y >= splat(y) → cannot be folded (vmsleu.vv)
  %ule.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 37, <vscale x 1 x i1> %m, i32 %n)
  %ule.3 = zext <vscale x 1 x i1> %ule.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %ule.3, <vscale x 1 x i64>* %store_addr

  ret void
}

define void @test_vp_fold_signed_greater(<vscale x 1 x i64> %a, i64 %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_signed_greater:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a2, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a2
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    mv a2, a1
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a1
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a1, a3
; CHECK-O0-NEXT:    mv a1, a0
; CHECK-O0-NEXT:    # kill: def $x10 killed $x12
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    # implicit-def: $v2
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmsgt.vx v2, v16, a1, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v2, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a2, e64,m1
; CHECK-O0-NEXT:    vmslt.vx v1, v16, a1, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    vse.v v2, (a0)
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_signed_greater:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a2, %hi(scratch)
; CHECK-O2-NEXT:    addi a2, a2, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsgt.vx v1, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmslt.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer

  ; x > splat(y) → vmsgt.vx x, y
  %sgt.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 38, <vscale x 1 x i1> %m, i32 %n)
  %sgt.1 = zext <vscale x 1 x i1> %sgt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sgt.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) > x → x < splat(y) → vmslt.vx x, y
  %sgt.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 38, <vscale x 1 x i1> %m, i32 %n)
  %sgt.3 = zext <vscale x 1 x i1> %sgt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sgt.3, <vscale x 1 x i64>* %store_addr

  ; x >= splat(y) → cannot be folded (vmsle.vv)
  %sge.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 39, <vscale x 1 x i1> %m, i32 %n)
  %sge.1 = zext <vscale x 1 x i1> %sgt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sge.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) >= x → x <= splat(y) → vmsle.vx x, y
  %sge.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 39, <vscale x 1 x i1> %m, i32 %n)
  %sge.3 = zext <vscale x 1 x i1> %sgt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sge.3, <vscale x 1 x i64>* %store_addr

  ret void
}

define void @test_vp_fold_signed_lower(<vscale x 1 x i64> %a, i64 %b, <vscale x 1 x i1> %m, i32 %n) nounwind {
; CHECK-O0-LABEL: test_vp_fold_signed_lower:
; CHECK-O0:       # %bb.0:
; CHECK-O0-NEXT:    addi sp, sp, -64
; CHECK-O0-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-O0-NEXT:    addi s0, sp, 64
; CHECK-O0-NEXT:    rdvtype ra
; CHECK-O0-NEXT:    rdvl t0
; CHECK-O0-NEXT:    vsetvli a2, zero, e8,m1
; CHECK-O0-NEXT:    vsetvl zero, t0, ra
; CHECK-O0-NEXT:    sub sp, sp, a2
; CHECK-O0-NEXT:    sd sp, -56(s0)
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vse.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    mv a2, a0
; CHECK-O0-NEXT:    # kill: def $x10 killed $x11
; CHECK-O0-NEXT:    lui a0, %hi(scratch)
; CHECK-O0-NEXT:    addi a0, a0, %lo(scratch)
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    vmv.v.x v2, a2
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmslt.vx v1, v16, a2, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmslt.vv v1, v2, v16, v0.t
; CHECK-O0-NEXT:    ld a5, -56(s0)
; CHECK-O0-NEXT:    rdvtype a4
; CHECK-O0-NEXT:    rdvl a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a5)
; CHECK-O0-NEXT:    vsetvl zero, a3, a4
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsle.vx v1, v16, a2, v0.t
; CHECK-O0-NEXT:    ld a4, -56(s0)
; CHECK-O0-NEXT:    rdvtype a3
; CHECK-O0-NEXT:    rdvl a2
; CHECK-O0-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O0-NEXT:    # implicit-def: $v0
; CHECK-O0-NEXT:    vle.v v0, (a4)
; CHECK-O0-NEXT:    vsetvl zero, a2, a3
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    # implicit-def: $v1
; CHECK-O0-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O0-NEXT:    vmsle.vv v1, v2, v16, v0.t
; CHECK-O0-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O0-NEXT:    vse.v v1, (a0)
; CHECK-O0-NEXT:    addi sp, s0, -64
; CHECK-O0-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-O0-NEXT:    addi sp, sp, 64
; CHECK-O0-NEXT:    ret
;
; CHECK-O2-LABEL: test_vp_fold_signed_lower:
; CHECK-O2:       # %bb.0:
; CHECK-O2-NEXT:    lui a2, %hi(scratch)
; CHECK-O2-NEXT:    addi a2, a2, %lo(scratch)
; CHECK-O2-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-O2-NEXT:    vmv.v.x v1, a0
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmslt.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmslt.vv v2, v1, v16, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsle.vx v2, v16, a0, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v2, (a2)
; CHECK-O2-NEXT:    vsetvli zero, a1, e64,m1
; CHECK-O2-NEXT:    vmsle.vv v1, v1, v16, v0.t
; CHECK-O2-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-O2-NEXT:    vse.v v1, (a2)
; CHECK-O2-NEXT:    ret
  %store_addr = bitcast i8* @scratch to <vscale x 1 x i64>*

  %head = insertelement <vscale x 1 x i64> undef, i64 %b, i32 0
  %splat = shufflevector <vscale x 1 x i64> %head, <vscale x 1 x i64> undef, <vscale x 1 x i32> zeroinitializer

  ; y < splat(x) → vmslt.vx x, y
  %slt.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 40, <vscale x 1 x i1> %m, i32 %n)
  %slt.1 = zext <vscale x 1 x i1> %slt.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %slt.1, <vscale x 1 x i64>* %store_addr

  ; splat(x) < y → y > splat(x) → cannot be folded (vmslt.vv)
  %slt.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 40, <vscale x 1 x i1> %m, i32 %n)
  %slt.3 = zext <vscale x 1 x i1> %slt.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %slt.3, <vscale x 1 x i64>* %store_addr

  ; x <= splat(y) → vmsle.vx x, y
  %sle.0 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %splat, i8 41, <vscale x 1 x i1> %m, i32 %n)
  %sle.1 = zext <vscale x 1 x i1> %sle.0 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sle.1, <vscale x 1 x i64>* %store_addr

  ; splat(y) <= x → x >= splat(y) → cannot be folded (vmsle.vv)
  %sle.2 = call <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %splat, <vscale x 1 x i64> %a, i8 41, <vscale x 1 x i1> %m, i32 %n)
  %sle.3 = zext <vscale x 1 x i1> %sle.2 to <vscale x 1 x i64>
  store <vscale x 1 x i64> %sle.3, <vscale x 1 x i64>* %store_addr

  ret void
}

declare <vscale x 1 x i1> @llvm.vp.icmp.nxv1i64(<vscale x 1 x i64> %a, <vscale x 1 x i64> %b, i8 %kind, <vscale x 1 x i1> %m, i32 %n)
