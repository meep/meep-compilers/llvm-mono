; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+m,+a,+f,+d,+c,+experimental-v -o - < %s \
; RUN:     --verify-machineinstrs | FileCheck %s

define <vscale x 8 x i1> @indirect_register_param(<vscale x 8 x i32> %a, <vscale x 8 x i32> %b, <vscale x 8 x i1> %merge, <vscale x 8 x i1> %m, i64 %gvl) nounwind {
; CHECK-LABEL: indirect_register_param:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vle.v v1, (a0)
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v2, v0
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    vsetvli zero, a1, e32,m4
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v0, v1
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    vmseq.vv v2, v16, v20, v0.t
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v0, v2
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    ret
entry:
    %0 = tail call <vscale x 8 x i1> @llvm.epi.vmseq.mask.nxv8i1.nxv8i32.nxv8i32(<vscale x 8 x i1> %merge, <vscale x 8 x i32> %a, <vscale x 8 x i32> %b, <vscale x 8 x i1> %m, i64 %gvl)
    ret <vscale x 8 x i1> %0
}

define void @indirect_register_argument(<vscale x 8 x i32> %a, <vscale x 8 x i1> %m, i64 %gvl) nounwind {
; CHECK-LABEL: indirect_register_argument:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    add a1, zero, a0
; CHECK-NEXT:    ld a0, -56(s0)
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vse.v v0, (a0)
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmv.v.v v20, v16
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    call indirect_register_param
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
   %x = call <vscale x 8 x i1> @indirect_register_param(<vscale x 8 x i32> %a, <vscale x 8 x i32> %a, <vscale x 8 x i1> %m, <vscale x 8 x i1> %m, i64 %gvl)
   ret void
}

define <vscale x 8 x i1> @indirect_stack_param(
; CHECK-LABEL: indirect_stack_param:
; CHECK:       # %bb.0: # %entry
; CHECK-NEXT:    ld a1, 0(sp)
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    vle.v v1, (a1)
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v2, v0
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    vsetvli zero, a0, e32,m4
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v0, v1
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    vmseq.vv v2, v16, v20, v0.t
; CHECK-NEXT:    rdvtype t0
; CHECK-NEXT:    rdvl t1
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vmv.v.v v0, v2
; CHECK-NEXT:    vsetvl zero, t1, t0
; CHECK-NEXT:    ret
    i64 %gvl, i64 %_2, i64 %_3, i64 %_4, i64 %_5, i64 %_6, i64 %_7, i64 %_8,
    <vscale x 8 x i32> %a, <vscale x 8 x i32> %b, <vscale x 8 x i1> %merge, <vscale x 8 x i1> %m) nounwind {
entry:
    %0 = tail call <vscale x 8 x i1> @llvm.epi.vmseq.mask.nxv8i1.nxv8i32.nxv8i32(<vscale x 8 x i1> %merge, <vscale x 8 x i32> %a, <vscale x 8 x i32> %b, <vscale x 8 x i1> %m, i64 %gvl)
    ret <vscale x 8 x i1> %0
}

define void @indirect_stack_argument(<vscale x 8 x i32> %a, <vscale x 8 x i1> %m, i64 %gvl) nounwind {
; CHECK-LABEL: indirect_stack_argument:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp) # 8-byte Folded Spill
; CHECK-NEXT:    sd s0, 48(sp) # 8-byte Folded Spill
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a1, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, sp, -16
; CHECK-NEXT:    ld t0, -56(s0)
; CHECK-NEXT:    sd t0, 0(sp)
; CHECK-NEXT:    vsetvli zero, zero, e8,m1
; CHECK-NEXT:    addi a1, zero, 2
; CHECK-NEXT:    addi a2, zero, 3
; CHECK-NEXT:    addi a3, zero, 4
; CHECK-NEXT:    addi a4, zero, 5
; CHECK-NEXT:    addi a5, zero, 6
; CHECK-NEXT:    addi a6, zero, 7
; CHECK-NEXT:    addi a7, zero, 8
; CHECK-NEXT:    vse.v v0, (t0)
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vmv.v.v v20, v16
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    call indirect_stack_param
; CHECK-NEXT:    addi sp, sp, 16
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp) # 8-byte Folded Reload
; CHECK-NEXT:    ld ra, 56(sp) # 8-byte Folded Reload
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
   %x = call <vscale x 8 x i1> @indirect_stack_param(i64 %gvl, i64 2, i64 3, i64 4, i64 5, i64 6, i64 7, i64 8, <vscale x 8 x i32> %a, <vscale x 8 x i32> %a, <vscale x 8 x i1> %m, <vscale x 8 x i1> %m)
   ret void
}

declare <vscale x 8 x i1> @llvm.epi.vmseq.mask.nxv8i1.nxv8i32.nxv8i32(<vscale x 8 x i1>, <vscale x 8 x i32>, <vscale x 8 x i32>, <vscale x 8 x i1>, i64)
