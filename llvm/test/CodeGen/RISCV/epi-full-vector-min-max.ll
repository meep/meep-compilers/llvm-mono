; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple riscv64 -mattr=+experimental-v < %s | FileCheck %s

@scratch = global i8 0, align 16

define <vscale x 1 x double> @min_nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b) nounwind {
; CHECK-LABEL: min_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vfmin.vv v16, v16, v17
; CHECK-NEXT:    ret
  %res = call <vscale x 1 x double> @llvm.minnum.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b)
  ret <vscale x 1 x double> %res
}

define <vscale x 2 x float> @min_nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b) nounwind {
; CHECK-LABEL: min_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vfmin.vv v16, v16, v17
; CHECK-NEXT:    ret
  %res = call <vscale x 2 x float> @llvm.minnum.nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b)
  ret <vscale x 2 x float> %res
}

define <vscale x 2 x double> @min_nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b) nounwind {
; CHECK-LABEL: min_nxv2f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vfmin.vv v16, v16, v18
; CHECK-NEXT:    ret
  %res = call <vscale x 2 x double> @llvm.minnum.nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b)
  ret <vscale x 2 x double> %res
}

define <vscale x 4 x float> @min_nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b) nounwind {
; CHECK-LABEL: min_nxv4f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m2
; CHECK-NEXT:    vfmin.vv v16, v16, v18
; CHECK-NEXT:    ret
  %res = call <vscale x 4 x float> @llvm.minnum.nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b)
  ret <vscale x 4 x float> %res
}

define <vscale x 4 x double> @min_nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b) nounwind {
; CHECK-LABEL: min_nxv4f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vfmin.vv v16, v16, v20
; CHECK-NEXT:    ret
  %res = call <vscale x 4 x double> @llvm.minnum.nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b)
  ret <vscale x 4 x double> %res
}

define <vscale x 8 x float> @min_nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b) nounwind {
; CHECK-LABEL: min_nxv8f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m4
; CHECK-NEXT:    vfmin.vv v16, v16, v20
; CHECK-NEXT:    ret
  %res = call <vscale x 8 x float> @llvm.minnum.nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b)
  ret <vscale x 8 x float> %res
}

define <vscale x 8 x double> @min_nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b) nounwind {
; CHECK-LABEL: min_nxv8f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    vfmin.vv v16, v16, v8
; CHECK-NEXT:    ret
  %res = call <vscale x 8 x double> @llvm.minnum.nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b)
  ret <vscale x 8 x double> %res
}

define <vscale x 16 x float> @min_nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b) nounwind {
; CHECK-LABEL: min_nxv16f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    vfmin.vv v16, v16, v8
; CHECK-NEXT:    ret
  %res = call <vscale x 16 x float> @llvm.minnum.nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b)
  ret <vscale x 16 x float> %res
}

define <vscale x 1 x double> @max_nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b) nounwind {
; CHECK-LABEL: max_nxv1f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m1
; CHECK-NEXT:    vfmax.vv v16, v16, v17
; CHECK-NEXT:    ret
  %res = call <vscale x 1 x double> @llvm.maxnum.nxv1f64(<vscale x 1 x double> %a, <vscale x 1 x double> %b)
  ret <vscale x 1 x double> %res
}

define <vscale x 2 x float> @max_nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b) nounwind {
; CHECK-LABEL: max_nxv2f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m1
; CHECK-NEXT:    vfmax.vv v16, v16, v17
; CHECK-NEXT:    ret
  %res = call <vscale x 2 x float> @llvm.maxnum.nxv2f32(<vscale x 2 x float> %a, <vscale x 2 x float> %b)
  ret <vscale x 2 x float> %res
}

define <vscale x 2 x double> @max_nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b) nounwind {
; CHECK-LABEL: max_nxv2f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m2
; CHECK-NEXT:    vfmax.vv v16, v16, v18
; CHECK-NEXT:    ret
  %res = call <vscale x 2 x double> @llvm.maxnum.nxv2f64(<vscale x 2 x double> %a, <vscale x 2 x double> %b)
  ret <vscale x 2 x double> %res
}

define <vscale x 4 x float> @max_nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b) nounwind {
; CHECK-LABEL: max_nxv4f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m2
; CHECK-NEXT:    vfmax.vv v16, v16, v18
; CHECK-NEXT:    ret
  %res = call <vscale x 4 x float> @llvm.maxnum.nxv4f32(<vscale x 4 x float> %a, <vscale x 4 x float> %b)
  ret <vscale x 4 x float> %res
}

define <vscale x 4 x double> @max_nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b) nounwind {
; CHECK-LABEL: max_nxv4f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m4
; CHECK-NEXT:    vfmax.vv v16, v16, v20
; CHECK-NEXT:    ret
  %res = call <vscale x 4 x double> @llvm.maxnum.nxv4f64(<vscale x 4 x double> %a, <vscale x 4 x double> %b)
  ret <vscale x 4 x double> %res
}

define <vscale x 8 x float> @max_nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b) nounwind {
; CHECK-LABEL: max_nxv8f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m4
; CHECK-NEXT:    vfmax.vv v16, v16, v20
; CHECK-NEXT:    ret
  %res = call <vscale x 8 x float> @llvm.maxnum.nxv8f32(<vscale x 8 x float> %a, <vscale x 8 x float> %b)
  ret <vscale x 8 x float> %res
}

define <vscale x 8 x double> @max_nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b) nounwind {
; CHECK-LABEL: max_nxv8f64:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e64,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    vfmax.vv v16, v16, v8
; CHECK-NEXT:    ret
  %res = call <vscale x 8 x double> @llvm.maxnum.nxv8f64(<vscale x 8 x double> %a, <vscale x 8 x double> %b)
  ret <vscale x 8 x double> %res
}

define <vscale x 16 x float> @max_nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b) nounwind {
; CHECK-LABEL: max_nxv16f32:
; CHECK:       # %bb.0:
; CHECK-NEXT:    vsetvli zero, zero, e32,m8
; CHECK-NEXT:    vle.v v8, (a0)
; CHECK-NEXT:    vfmax.vv v16, v16, v8
; CHECK-NEXT:    ret
  %res = call <vscale x 16 x float> @llvm.maxnum.nxv16f32(<vscale x 16 x float> %a, <vscale x 16 x float> %b)
  ret <vscale x 16 x float> %res
}

declare <vscale x 1 x double> @llvm.minnum.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>)
declare <vscale x 2 x float> @llvm.minnum.nxv2f32(<vscale x 2 x float>, <vscale x 2 x float>)
declare <vscale x 2 x double> @llvm.minnum.nxv2f64(<vscale x 2 x double>, <vscale x 2 x double>)
declare <vscale x 4 x float> @llvm.minnum.nxv4f32(<vscale x 4 x float>, <vscale x 4 x float>)
declare <vscale x 4 x double> @llvm.minnum.nxv4f64(<vscale x 4 x double>, <vscale x 4 x double>)
declare <vscale x 8 x float> @llvm.minnum.nxv8f32(<vscale x 8 x float>, <vscale x 8 x float>)
declare <vscale x 8 x double> @llvm.minnum.nxv8f64(<vscale x 8 x double>, <vscale x 8 x double>)
declare <vscale x 16 x float> @llvm.minnum.nxv16f32(<vscale x 16 x float>, <vscale x 16 x float>)

declare <vscale x 1 x double> @llvm.maxnum.nxv1f64(<vscale x 1 x double>, <vscale x 1 x double>)
declare <vscale x 2 x float> @llvm.maxnum.nxv2f32(<vscale x 2 x float>, <vscale x 2 x float>)
declare <vscale x 2 x double> @llvm.maxnum.nxv2f64(<vscale x 2 x double>, <vscale x 2 x double>)
declare <vscale x 4 x float> @llvm.maxnum.nxv4f32(<vscale x 4 x float>, <vscale x 4 x float>)
declare <vscale x 4 x double> @llvm.maxnum.nxv4f64(<vscale x 4 x double>, <vscale x 4 x double>)
declare <vscale x 8 x float> @llvm.maxnum.nxv8f32(<vscale x 8 x float>, <vscale x 8 x float>)
declare <vscale x 8 x double> @llvm.maxnum.nxv8f64(<vscale x 8 x double>, <vscale x 8 x double>)
declare <vscale x 16 x float> @llvm.maxnum.nxv16f32(<vscale x 16 x float>, <vscale x 16 x float>)
