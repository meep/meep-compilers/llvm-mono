; NOTE: Assertions have been autogenerated by utils/update_llc_test_checks.py
; RUN: llc -mtriple=riscv64 -mattr=+experimental-v -verify-machineinstrs < %s \
; RUN:    | FileCheck %s

define void @lmul1() nounwind optnone noinline {
; CHECK-LABEL: lmul1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -48
; CHECK-NEXT:    sd ra, 40(sp)
; CHECK-NEXT:    sd s0, 32(sp)
; CHECK-NEXT:    addi s0, sp, 48
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -48(s0)
; CHECK-NEXT:    addi sp, s0, -48
; CHECK-NEXT:    ld s0, 32(sp)
; CHECK-NEXT:    ld ra, 40(sp)
; CHECK-NEXT:    addi sp, sp, 48
; CHECK-NEXT:    ret
  %v = alloca <vscale x 1 x i64>
  ret void
}

define void @lmul2() nounwind optnone noinline {
; CHECK-LABEL: lmul2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a0, a0, 1
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v = alloca <vscale x 2 x i64>
  ret void
}

define void @lmul4() nounwind optnone noinline {
; CHECK-LABEL: lmul4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v = alloca <vscale x 4 x i64>
  ret void
}

define void @lmul8() nounwind optnone noinline {
; CHECK-LABEL: lmul8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a0, a0, 3
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v = alloca <vscale x 8 x i64>
  ret void
}

define void @lmul1_and_2() nounwind optnone noinline {
; CHECK-LABEL: lmul1_and_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a0, a0, 1
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 2 x i64>
  ret void
}

define void @lmul2_and_4() nounwind optnone noinline {
; CHECK-LABEL: lmul2_and_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 2 x i64>
  %v2 = alloca <vscale x 4 x i64>
  ret void
}

define void @lmul1_and_4() nounwind optnone noinline {
; CHECK-LABEL: lmul1_and_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 4 x i64>
  ret void
}

define void @lmul2_and_1() nounwind optnone noinline {
; CHECK-LABEL: lmul2_and_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    slli a0, a0, 1
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 2 x i64>
  %v2 = alloca <vscale x 1 x i64>
  ret void
}

define void @lmul4_and_1() nounwind optnone noinline {
; CHECK-LABEL: lmul4_and_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 4 x i64>
  %v2 = alloca <vscale x 1 x i64>
  ret void
}

define void @lmul4_and_2() nounwind optnone noinline {
; CHECK-LABEL: lmul4_and_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 4 x i64>
  %v2 = alloca <vscale x 2 x i64>
  ret void
}

define void @lmul4_and_2_x2_0() nounwind optnone noinline {
; CHECK-LABEL: lmul4_and_2_x2_0:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -80
; CHECK-NEXT:    sd ra, 72(sp)
; CHECK-NEXT:    sd s0, 64(sp)
; CHECK-NEXT:    addi s0, sp, 80
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -80(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    addi sp, s0, -80
; CHECK-NEXT:    ld s0, 64(sp)
; CHECK-NEXT:    ld ra, 72(sp)
; CHECK-NEXT:    addi sp, sp, 80
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 4 x i64>
  %v2 = alloca <vscale x 2 x i64>
  %v3 = alloca <vscale x 4 x i64>
  %v4 = alloca <vscale x 2 x i64>
  ret void
}

define void @lmul4_and_2_x2_1() nounwind optnone noinline {
; CHECK-LABEL: lmul4_and_2_x2_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -80
; CHECK-NEXT:    sd ra, 72(sp)
; CHECK-NEXT:    sd s0, 64(sp)
; CHECK-NEXT:    addi s0, sp, 80
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -80(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi sp, s0, -80
; CHECK-NEXT:    ld s0, 64(sp)
; CHECK-NEXT:    ld ra, 72(sp)
; CHECK-NEXT:    addi sp, sp, 80
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 4 x i64>
  %v3 = alloca <vscale x 4 x i64>
  %v2 = alloca <vscale x 2 x i64>
  %v4 = alloca <vscale x 2 x i64>
  ret void
}


define void @gpr_and_lmul1_and_2() nounwind optnone noinline {
; CHECK-LABEL: gpr_and_lmul1_and_2:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a0, a0, 1
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi a0, zero, 3
; CHECK-NEXT:    sd a0, -48(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %x1 = alloca i64
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 2 x i64>
  store volatile i64 3, i64* %x1
  ret void
}

define void @gpr_and_lmul1_and_4() nounwind optnone noinline {
; CHECK-LABEL: gpr_and_lmul1_and_4:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -64
; CHECK-NEXT:    sd ra, 56(sp)
; CHECK-NEXT:    sd s0, 48(sp)
; CHECK-NEXT:    addi s0, sp, 64
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a0, a0, 2
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi a0, zero, 3
; CHECK-NEXT:    sd a0, -48(s0)
; CHECK-NEXT:    addi sp, s0, -64
; CHECK-NEXT:    ld s0, 48(sp)
; CHECK-NEXT:    ld ra, 56(sp)
; CHECK-NEXT:    addi sp, sp, 64
; CHECK-NEXT:    ret
  %x1 = alloca i64
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 4 x i64>
  store volatile i64 3, i64* %x1
  ret void
}

define void @lmul_1_2_4_8() nounwind optnone noinline {
; CHECK-LABEL: lmul_1_2_4_8:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -80
; CHECK-NEXT:    sd ra, 72(sp)
; CHECK-NEXT:    sd s0, 64(sp)
; CHECK-NEXT:    addi s0, sp, 80
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    slli a1, a0, 2
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    slli a0, a0, 3
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -80(s0)
; CHECK-NEXT:    addi sp, s0, -80
; CHECK-NEXT:    ld s0, 64(sp)
; CHECK-NEXT:    ld ra, 72(sp)
; CHECK-NEXT:    addi sp, sp, 80
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 2 x i64>
  %v4 = alloca <vscale x 4 x i64>
  %v8 = alloca <vscale x 8 x i64>
  ret void
}

define void @lmul_1_2_4_8_x2_0() nounwind optnone noinline {
; CHECK-LABEL: lmul_1_2_4_8_x2_0:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -112
; CHECK-NEXT:    sd ra, 104(sp)
; CHECK-NEXT:    sd s0, 96(sp)
; CHECK-NEXT:    addi s0, sp, 112
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -80(s0)
; CHECK-NEXT:    slli a1, a0, 2
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -88(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -96(s0)
; CHECK-NEXT:    slli a0, a0, 3
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -104(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -112(s0)
; CHECK-NEXT:    addi sp, s0, -112
; CHECK-NEXT:    ld s0, 96(sp)
; CHECK-NEXT:    ld ra, 104(sp)
; CHECK-NEXT:    addi sp, sp, 112
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 1 x i64>
  %v2 = alloca <vscale x 1 x i64>
  %v3 = alloca <vscale x 2 x i64>
  %v4 = alloca <vscale x 2 x i64>
  %v5 = alloca <vscale x 4 x i64>
  %v6 = alloca <vscale x 4 x i64>
  %v7 = alloca <vscale x 8 x i64>
  %v8 = alloca <vscale x 8 x i64>
  ret void
}

define void @lmul_1_2_4_8_x2_1() nounwind optnone noinline {
; CHECK-LABEL: lmul_1_2_4_8_x2_1:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -112
; CHECK-NEXT:    sd ra, 104(sp)
; CHECK-NEXT:    sd s0, 96(sp)
; CHECK-NEXT:    addi s0, sp, 112
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -104(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -112(s0)
; CHECK-NEXT:    slli a1, a0, 1
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -88(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -96(s0)
; CHECK-NEXT:    slli a1, a0, 2
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    sub sp, sp, a1
; CHECK-NEXT:    sd sp, -80(s0)
; CHECK-NEXT:    slli a0, a0, 3
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    addi sp, s0, -112
; CHECK-NEXT:    ld s0, 96(sp)
; CHECK-NEXT:    ld ra, 104(sp)
; CHECK-NEXT:    addi sp, sp, 112
; CHECK-NEXT:    ret
  %v8 = alloca <vscale x 8 x i64>
  %v7 = alloca <vscale x 8 x i64>
  %v6 = alloca <vscale x 4 x i64>
  %v5 = alloca <vscale x 4 x i64>
  %v4 = alloca <vscale x 2 x i64>
  %v3 = alloca <vscale x 2 x i64>
  %v2 = alloca <vscale x 1 x i64>
  %v1 = alloca <vscale x 1 x i64>
  ret void
}

define void @masks() nounwind optnone noinline {
; CHECK-LABEL: masks:
; CHECK:       # %bb.0:
; CHECK-NEXT:    addi sp, sp, -80
; CHECK-NEXT:    sd ra, 72(sp)
; CHECK-NEXT:    sd s0, 64(sp)
; CHECK-NEXT:    addi s0, sp, 80
; CHECK-NEXT:    rdvtype ra
; CHECK-NEXT:    rdvl t0
; CHECK-NEXT:    vsetvli a0, zero, e8,m1
; CHECK-NEXT:    vsetvl zero, t0, ra
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -48(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -56(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -64(s0)
; CHECK-NEXT:    sub sp, sp, a0
; CHECK-NEXT:    sd sp, -72(s0)
; CHECK-NEXT:    addi sp, s0, -80
; CHECK-NEXT:    ld s0, 64(sp)
; CHECK-NEXT:    ld ra, 72(sp)
; CHECK-NEXT:    addi sp, sp, 80
; CHECK-NEXT:    ret
  %v1 = alloca <vscale x 1 x i1>
  %v2 = alloca <vscale x 2 x i1>
  %v4 = alloca <vscale x 4 x i1>
  %v8 = alloca <vscale x 8 x i1>
  ret void
}
