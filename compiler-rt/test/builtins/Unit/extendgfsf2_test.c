// RUN: %clang_builtins %s %librt -o %t && %run %t
// REQUIRES: librt_has_extendgfsf2

#include <stdio.h>

#include "fp_test.h"

float __extendgfsf2(uint8_t a);

int test__extendgfsf2(uint8_t a, float expected, const char *testName)
{
    float x = __extendgfsf2(a);
    int ret = compareResultC(x, expected);

    if (ret){
        printf("error in test %s: test__extendgfsf2(%#.2x) = %f, "
               "expected %f\n", testName, a, x, expected);
    }
    return ret;
}

char assumption_1[sizeof(_Float8_143) * CHAR_BIT == 8] = {0};

int main()
{
    // qNaN
    if (test__extendgfsf2(UINT8_C(0x7f),
                          makeQNaN32(),
                          "makeQNaN"))
        return 1;
    // NaN
    if (test__extendgfsf2(UINT8_C(0x7f),
                          makeNaN32(UINT32_C(0x8000)),
                          "makeNaN"))
        return 1;
    // inf
    // Skip the inf tests, since there's no inf value in f8_143,
    // so we'll never be able to produce a (-)makeInf32()
    // zero
    if (test__extendgfsf2(UINT8_C(0x0),
                          0.0f,
                          "zero"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x80),
                          -0.0f,
                          "minus zero"))
        return 1;

    if (test__extendgfsf2(UINT8_C(0x45),
                          3.25f,
                          "3.25f"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0xc5),
                          -3.25f,
                          "-3.25f"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x29),
                          0x1.2p-2f,
                          "random#1"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x89),
                          -0x1.2p-6f,
                          "random#2"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x38),
                          0x1.0p+0f,
                          "0x1.0p+0f"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x08),
                          0x1.0p-6f,
                          "min normal"))
        return 1;
    // denormal
    if (test__extendgfsf2(UINT8_C(0x03),
                          0.0029296875f,
                          "denormal#1"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x81),
                          -0.0009765625f,
                          "denormal#2"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x01),
                          0.0009765625f,
                          "minus denormal#1"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x01),
                          0.0009765625f,
                          "minus denormal#2"))
        return 1;
    // and back to zero
    if (test__extendgfsf2(UINT8_C(0x00),
                          0x1.0p-25f,
                          "so little that rounds to zero"))
        return 1;
    if (test__extendgfsf2(UINT8_C(0x80),
                          -0x1.0p-25f,
                          "so minus little(?) that rounds to minus zero(?!?!?)"))
        return 1;
    // max (precise)
    if (test__extendgfsf2(UINT8_C(0x7e),
                          448.0f,
                          "max (precise)"))
        return 1;
    return 0;
}
