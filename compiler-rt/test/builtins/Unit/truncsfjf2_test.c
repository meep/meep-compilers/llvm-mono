// RUN: %clang_builtins %s %librt -o %t && %run %t
// REQUIRES: librt_has_truncsfjf2

#include <stdio.h>

#include "fp_test.h"

uint8_t __truncsfjf2(float a);

int test__truncsfjf2(float a, uint8_t expected, const char *testName)
{
    uint8_t x = __truncsfjf2(a);
    int ret = compareResultC(x, expected);

    if (ret){
        printf("error in test %s: test__truncsfjf2(%f) = %#.2x, "
               "expected %#.2x\n", testName, a, x, fromRep8(expected));
    }
    return ret;
}

char assumption_1[sizeof(_Float8_152) * CHAR_BIT == 8] = {0};

int main()
{
    // qNaN
    if (test__truncsfjf2(makeQNaN32(),
                         UINT8_C(0x7e),
                         "makeQNaN"))
        return 1;
    // NaN
    if (test__truncsfjf2(makeNaN32(UINT32_C(0x8000)),
                         UINT8_C(0x7e),
                         "makeNaN"))
        return 1;
    // inf
    if (test__truncsfjf2(makeInf32(),
                         UINT8_C(0x7c),
                         "inf"))
        return 1;
    if (test__truncsfjf2(-makeInf32(),
                         UINT8_C(0xfc),
                         "minus inf"))
        return 1;
    // zero
    if (test__truncsfjf2(0.0f, UINT8_C(0x0),
                         "zero"))
        return 1;
    if (test__truncsfjf2(-0.0f, UINT8_C(0x80),
                         "minus zero"))
        return 1;
    if (test__truncsfjf2(3.5f,
                         UINT8_C(0x43),
                         "3.5f"))
        return 1;
    if (test__truncsfjf2(-3.5f,
                         UINT8_C(0xc3),
                         "-3.5f"))
        return 1;
    if (test__truncsfjf2(0x1.417836418743p+15f,
                         UINT8_C(0x79),
                         "rounding#1"))
        return 1;
    if (test__truncsfjf2(0x1.987124876876324p+12f,
                         UINT8_C(0x6e),
                         "rounding#2"))
        return 1;
    if (test__truncsfjf2(0x1.0p+0f,
                         UINT8_C(0x3c),
                         "0x1.0p+0f"))
        return 1;
    if (test__truncsfjf2(0x1.0p-14f,
                         UINT8_C(0x04),
                         "min normal"))
        return 1;
    // denormal
    if (test__truncsfjf2(0x1.0p-15f,
                         UINT8_C(0x02),
                         "denormal#1"))
        return 1;
    if (test__truncsfjf2(0x1.0p-16f,
                         UINT8_C(0x01),
                         "denormal#2"))
        return 1;
    if (test__truncsfjf2(-0x1.0p-15f,
                         UINT8_C(0x82),
                         "minus denormal#1"))
        return 1;
    if (test__truncsfjf2(-0x1.0p-16f,
                         UINT8_C(0x81),
                         "minus denormal#2"))
        return 1;
    // and back to zero
    if (test__truncsfjf2(0x1.0p-25f,
                         UINT8_C(0x00),
                         "so little that rounds to zero"))
        return 1;
    if (test__truncsfjf2(-0x1.0p-25f,
                         UINT8_C(0x80),
                         "so minus little(?) that rounds to minus zero(?!?!?)"))
        return 1;
    // max (precise)
    if (test__truncsfjf2(57344.0f,
                         UINT8_C(0x7b),
                         "max (precise)"))
        return 1;
    // max (rounded)
    if (test__truncsfjf2(57345.0f,
                         UINT8_C(0x7b),
                         "round to max"))
        return 1;
    // max (to +inf)
    if (test__truncsfjf2(573440.0f,
                         UINT8_C(0x7c),
                         "round to inf"))
        return 1;
    if (test__truncsfjf2(-573440.0f,
                         UINT8_C(0xfc),
                         "round to minus inf"))
        return 1;
    return 0;
}
