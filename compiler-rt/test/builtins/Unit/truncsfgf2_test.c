// RUN: %clang_builtins %s %librt -o %t && %run %t
// REQUIRES: librt_has_truncsfgf2

#include <stdio.h>

#include "fp_test.h"

uint8_t __truncsfgf2(float a);

int test__truncsfgf2(float a, uint8_t expected, const char *testName)
{
    uint8_t x = __truncsfgf2(a);
    int ret = compareResultC(x, expected);

    if (ret){
        printf("error in test %s: test__truncsfgf2(%f) = %#.2x, "
               "expected %#.2x\n", testName, a, x, fromRep8(expected));
    }
    return ret;
}

char assumption_1[sizeof(_Float8_143) * CHAR_BIT == 8] = {0};

int main()
{
    // qNaN
    if (test__truncsfgf2(makeQNaN32(),
                         UINT8_C(0x7f),
                         "makeQNaN"))
        return 1;
    // NaN
    if (test__truncsfgf2(makeNaN32(UINT32_C(0x8000)),
                         UINT8_C(0x7f),
                         "makeNaN"))
        return 1;
    // inf
    if (test__truncsfgf2(makeInf32(),
                         UINT8_C(0x7e),
                         "inf"))
        return 1;
    if (test__truncsfgf2(-makeInf32(),
                         UINT8_C(0xfe),
                         "minus inf"))
        return 1;
    // zero
    if (test__truncsfgf2(0.0f, UINT8_C(0x0),
                         "zero"))
        return 1;
    if (test__truncsfgf2(-0.0f, UINT8_C(0x80),
                         "minus zero"))
        return 1;

    if (test__truncsfgf2(3.25f,
                         UINT8_C(0x45),
                         "3.25f"))
        return 1;
    if (test__truncsfgf2(-3.25f,
                         UINT8_C(0xc5),
                         "-3.25f"))
        return 1;
    if (test__truncsfgf2(0x1.987124876876324p+8f,
                         UINT8_C(0x7d),
                         "rounding#1"))
        return 1;
    if (test__truncsfgf2(0x1.987124876876324p+5f,
                         UINT8_C(0x65),
                         "rounding#2"))
        return 1;
    if (test__truncsfgf2(0x1.0p+0f,
                         UINT8_C(0x38),
                         "0x1.0p+0f"))
        return 1;
    if (test__truncsfgf2(0x1.0p-6f,
                         UINT8_C(0x08),
                         "min normal"))
        return 1;
    // denormal
    if (test__truncsfgf2(0x0.6212619p-6f,
                         UINT8_C(0x03),
                         "round & denormal"))
        return 1;
    if (test__truncsfgf2(-0x0.81278361p-6f,
                         UINT8_C(0x84),
                         "round & denormal negative"))
        return 1;
    if (test__truncsfgf2(0x0.2p-6f,
                         UINT8_C(0x01),
                         "min denormal no zero"))
        return 1;
    if (test__truncsfgf2(-0x0.2p-6f,
                         UINT8_C(0x81),
                         "max negative denormal"))
        return 1;
    // and back to zero
     if (test__truncsfgf2(0x1.0p-25f,
                         UINT8_C(0x00),
                         "so little that it rounds to zero"))
        return 1;
    if (test__truncsfgf2(-0x1.0p-25f,
                         UINT8_C(0x80),
                         "so minus little(?) that rounds to minus zero(?!?!?)"))
        return 1;
    // max (precise)
    if (test__truncsfgf2(448.0f,
                         UINT8_C(0x7e),
                         "max (precise)"))
        return 1;
    // max (rounded)
    if (test__truncsfgf2(449.0f,
                         UINT8_C(0x7e),
                         "max (rounded)"))
       return 1;
    // max (to +inf)
    if (test__truncsfgf2(65520.0f,
                         UINT8_C(0x7e),
                         "round to inf (448.0, since f8_143 doesn't have infinities)"))
        return 1;
    if (test__truncsfgf2(-65520.0f,
                         UINT8_C(0xfe),
                         "round to -inf (-448.0, since f8_143 doesn't have infinities)"))
        return 1;
    return 0;
}
