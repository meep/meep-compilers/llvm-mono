// RUN: %clang_builtins %s %librt -o %t && %run %t
// REQUIRES: librt_has_extendjfsf2

#include <stdio.h>

#include "fp_test.h"

float __extendjfsf2(uint8_t a);

int test__extendjfsf2(uint8_t a, float expected, const char *testName)
{
    float x = __extendjfsf2(a);
    int ret = compareResultC(x, expected);

    if (ret){
        printf("error in test %s: test__extendjfsf2(%#.2x) = %f, "
               "expected %f\n", testName, a, x, expected);
    }
    return ret;
}

char assumption_1[sizeof(_Float8_152) * CHAR_BIT == 8] = {0};

int main()
{
    // qNaN
    if (test__extendjfsf2(UINT8_C(0x7e),
                          makeQNaN32(),
                          "makeQNaN"))
        return 1;
    // NaN
    if (test__extendjfsf2(UINT8_C(0x7e),
                          makeNaN32(UINT32_C(0x8000)),
                          "makeNaN"))
        return 1;
    // inf
    if (test__extendjfsf2(UINT8_C(0x7c),
                          makeInf32(),
                          "inf"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0xfc),
                          -makeInf32(),
                          "minus inf"))
        return 1;
    // zero
    if (test__extendjfsf2(UINT8_C(0x0),
                          0.0f,
                          "zero"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x80),
                          -0.0f,
                          "minus zero"))
        return 1;

    if (test__extendjfsf2(UINT8_C(0x43),
                          3.5f,
                          "3.5f"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0xc3),
                          -3.5f,
                          "-3.5f"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0xa5),
                          -0x1.4p-6f,
                          "random#1"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x62),
                          0x1.8p+9f,
                          "random#2"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x3c),
                          0x1.0p+0f,
                          "0x1.0p+0f"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x04),
                          0x1.0p-14f,
                          "min normal"))
        return 1;
    // denormal
    if (test__extendjfsf2(UINT8_C(0x02),
                          0x1.0p-15f,
                          "denormal#1"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x01),
                          0x1.0p-16f,
                          "denormal#2"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x82),
                          -0x1.0p-15f,
                          "minus denormal#1"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x81),
                          -0x1.0p-16f,
                          "minus denormal#2"))
        return 1;
    // and back to zero
    if (test__extendjfsf2(UINT8_C(0x00),
                          0x1.0p-25f,
                          "so little that rounds to zero"))
        return 1;
    if (test__extendjfsf2(UINT8_C(0x80),
                          -0x1.0p-25f,
                          "so minus little(?) that rounds to minus zero(?!?!?)"))
        return 1;
    // max (precise)
    if (test__extendjfsf2(UINT8_C(0x7b),
                          57344.0f,
                         "max (precise)"))
        return 1;
    return 0;
}
