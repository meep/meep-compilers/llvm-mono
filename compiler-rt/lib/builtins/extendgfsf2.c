//===-- lib/extendgfsf2.c - f8_143 -> single conversion -------------*- C -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define SRC_F8_143
#define DST_SINGLE
#include "fp_extend_impl.inc"

#include <math.h>

COMPILER_RT_ABI float __extendgfsf2(uint8_t a) {
  // f8_143 has a different interpretation of the max exponent bits
  // than the other floating point formats, so that should be managed
  // before normal processing of the number
  const uint8_t exp = a & 0x78;
  const uint8_t man = a & 0x07;
  if(exp == 0b01111000) {
    if(man == 0b00000111) return NAN;
    else return (1.0f+0.125f*man)*256;
  }
  return __extendXfYf2__(a);
}
