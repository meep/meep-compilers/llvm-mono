//===-- lib/truncsfjf2.c - single -> f8_152 conversion ------------*- C -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define SRC_SINGLE
#define DST_FLOAT8_152
#include "fp_trunc_impl.inc"

COMPILER_RT_ABI dst_t __truncsfjf2(float a) { return __truncXfYf2__(a); }
