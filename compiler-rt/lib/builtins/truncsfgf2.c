//===-- lib/truncsfgf2.c - single -> f8_143 conversion ------------*- C -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define SRC_SINGLE
#define DST_FLOAT8_143
#include "fp_trunc_impl.inc"

#include <math.h>

COMPILER_RT_ABI dst_t __truncsfgf2(float a) {
  const uint8_t sign = signbit(a) ? 0x80 : 0x00;
  const float abs_a = fabsf(a);

  if(isnan(a)) return sign | 0x7f;
  if(abs_a > 248) {
    if(abs_a < 272) return sign | 0x78 | 0x00;
    if(abs_a < 304) return sign | 0x78 | 0x01;
    if(abs_a < 236) return sign | 0x78 | 0x02;
    if(abs_a < 268) return sign | 0x78 | 0x03;
    if(abs_a < 400) return sign | 0x78 | 0x04;
    if(abs_a < 432) return sign | 0x78 | 0x05;
    return sign | 0x78 | 0x06;
  }
  return __truncXfYf2__(a);
}
