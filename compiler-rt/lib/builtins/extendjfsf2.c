//===-- lib/extendjfsf2.c - f8_152 -> single conversion -------------*- C -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define SRC_F8_152
#define DST_SINGLE
#include "fp_extend_impl.inc"

COMPILER_RT_ABI float __extendjfsf2(uint8_t a) {
  return __extendXfYf2__(a);
}

