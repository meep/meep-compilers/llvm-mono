// RUN: %clang_cc1 %s -O0 -disable-llvm-passes -emit-llvm -S -verify

void test(bool b) {
  _Float8_143 f8_143;

  f8_143 + f8_143; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_143')}}
  f8_143 - f8_143; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_143')}}
  f8_143 * f8_143; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_143')}}
  f8_143 / f8_143; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_143')}}

  _Float8_152 f8_152;

  f8_152 + f8_152; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_152')}}
  f8_152 - f8_152; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_152')}}
  f8_152 * f8_152; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_152')}}
  f8_152 / f8_152; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_152')}}
  
  f8_143 + f8_152; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_152')}}
  f8_152 + f8_143; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_143')}}
  f8_143 - f8_152; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_152')}}
  f8_152 - f8_143; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_143')}}
  f8_143 * f8_152; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_152')}}
  f8_152 * f8_143; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_143')}}
  f8_143 / f8_152; // expected-error {{invalid operands to binary expression ('_Float8_143' and '_Float8_152')}}
  f8_152 / f8_143; // expected-error {{invalid operands to binary expression ('_Float8_152' and '_Float8_143')}}
  f8_143 = f8_152; // expected-error {{assigning to '_Float8_143' from incompatible type '_Float8_152'}}
  f8_152 = f8_143; // expected-error {{assigning to '_Float8_152' from incompatible type '_Float8_143'}}
  f8_143 + (b ? f8_152 : f8_143); // expected-error {{incompatible operand types ('_Float8_152' and '_Float8_143')}}
}
