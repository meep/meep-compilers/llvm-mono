// RUN: %clang_cc1 -fsyntax-only -verify %s

_Float8_143 test_cast_from_float(float in) {
  return (_Float8_143)in; // expected-error {{cannot type-cast to _Float8_143}}
}

_Float8_143 test_cast_from_float_literal(void) {
  return (_Float8_143)1.0f; // expected-error {{cannot type-cast to _Float8_143}}
}

_Float8_143 test_cast_from_int(int in) {
  return (_Float8_143)in; // expected-error {{cannot type-cast to _Float8_143}}
}

_Float8_143 test_cast_from_int_literal(void) {
  return (_Float8_143)1; // expected-error {{cannot type-cast to _Float8_143}}
}

_Float8_143 test_cast_bfloat(_Float8_143 in) {
  return (_Float8_143)in; // this one should work
}

float test_cast_to_float(_Float8_143 in) {
  return (float)in; // expected-error {{cannot type-cast from _Float8_143}}
}

int test_cast_to_int(_Float8_143 in) {
  return (int)in; // expected-error {{cannot type-cast from _Float8_143}}
}

_Float8_143 test_implicit_from_float(float in) {
  return in; // expected-error {{returning 'float' from a function with incompatible result type '_Float8_143'}}
}

_Float8_143 test_implicit_from_float_literal(void) {
  return 1.0f; // expected-error {{returning 'float' from a function with incompatible result type '_Float8_143'}}
}

_Float8_143 test_implicit_from_int(int in) {
  return in; // expected-error {{returning 'int' from a function with incompatible result type '_Float8_143'}}
}

_Float8_143 test_implicit_from_int_literal(void) {
  return 1; // expected-error {{returning 'int' from a function with incompatible result type '_Float8_143'}}
}

_Float8_143 test_implicit_bfloat(_Float8_143 in) {
  return in; // this one should work
}

float test_implicit_to_float(_Float8_143 in) {
  return in; // expected-error {{returning '_Float8_143' from a function with incompatible result type 'float'}}
}

int test_implicit_to_int(_Float8_143 in) {
  return in; // expected-error {{returning '_Float8_143' from a function with incompatible result type 'int'}}
}

_Float8_143 test_cond(_Float8_143 a, _Float8_143 b, _Bool which) {
  // Conditional operator _should_ be supported, without nonsense
  // complaints like 'types _Float8_143 and _Float8_143 are not compatible'
  return which ? a : b;
}

_Float8_143 test_cond_float(_Float8_143 a, _Float8_143 b, _Bool which) {
  return which ? a : 1.0f; // expected-error {{incompatible operand types ('_Float8_143' and 'float')}}
}

_Float8_143 test_cond_int(_Float8_143 a, _Float8_143 b, _Bool which) {
  return which ? a : 1; // expected-error {{incompatible operand types ('_Float8_143' and 'int')}}
}
